﻿using Assets.Scripts.Tools;
using UnityEngine;
using UnityEngine.UI;
using MCoach;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Represents a UI instance of Team Member
/// </summary>
public class TeamMemberInstance : MonoBehaviour
{
	private class GridColorRange{
		public GridColorRange(float t, Color c){
			threshold = t;
			color = c;
		}

		public float threshold;
		public Color color;
	}

	private class ColorMap{

		public ColorMap(){
			ranges = new List<GridColorRange>();
		}

		public List<GridColorRange> ranges;
	}
	ColorMap Column2ColorMap;
	bool Column2ColorEnabled = true;
	ColorMap Column3ColorMap;
	bool Column3ColorEnabled = true;


    /// <summary>
    /// The UI text object responsible for displaying
    /// the team member's full name.
    /// </summary>
	public Image FillBackground;
    public Text FullName;
	public Text Title;
	public Text TQDPercent;
	public Text RankText;
	public Image RatingBarForeground;
	public Image RatingBarBackground;
	public Image CircleImage;
	public Text CoachingCount;
    public Texture2D DefaultImage;
	public Button teamMemberButton;
    /// <summary>
    /// The TeamMember data this instance represents.
    /// </summary>
	private TeamMember _teamMemberReference;
	private Dictionary<int, SellingSkillInfo> SkillRatings; 
	// sales data table from TAG
	private string[][] header;// = tagDict ["HEADER"];
	private string[][] grid;//   = tagDict ["GRID"];
	private static List<GameObject> _spawnedSalesDataItems = new List<GameObject>();


	private UIEvent _teamMemberSelected = new UIEvent(Constants.Events.TeamMemberSelectedEventId.ToString());
    private UIEvent _teamMemberName = new UIEvent(Constants.Events.TeamMemberNameEventId.ToString());
    private UIEvent _teamMemberTerritory = new UIEvent(Constants.Events.TeamMemberTerritoryEventId.ToString());
	private UIEvent _teamMemberLevelLabel = new UIEvent(Constants.Events.TeamMemberLevelLabelEventId.ToString());
    private UIEvent _teamMemberEmail = new UIEvent(Constants.Events.TeamMemberEmailEventId.ToString());
    private UIEvent _teamMemberDate = new UIEvent(Constants.Events.TeamMemberDateEventId.ToString());
    private UIEvent _teamMemberLocation = new UIEvent(Constants.Events.TeamMemberLocationEventId.ToString());
    private UIEvent _teamMemberPhone = new UIEvent(Constants.Events.TeamMemberPhoneEventId.ToString());
//    private UIEvent _menuButton6Text = new UIEvent(Constants.Events.MenuButton6TextEventId.ToString());
    private UIEvent _requestImage = new UIEvent(Constants.Events.RequestTeamLeaderProfileImageEventId.ToString());
    private UIEvent _teamMemberNormal = new UIEvent(Constants.Events.TeamMemberNormalEventId.ToString());
    private UIEvent _teamMemberCSIDs = new UIEvent(Constants.Events.TeamMemberCSIDsEventId.ToString());

    private void Awake()
	{
		GlobalMessageManager.AddEvent(_teamMemberSelected);
        GlobalMessageManager.AddEvent(_teamMemberName);
        GlobalMessageManager.AddEvent(_teamMemberTerritory);
		GlobalMessageManager.AddEvent(_teamMemberLevelLabel);
        GlobalMessageManager.AddEvent(_teamMemberEmail);
        GlobalMessageManager.AddEvent(_teamMemberDate);
        GlobalMessageManager.AddEvent(_teamMemberLocation);
        GlobalMessageManager.AddEvent(_teamMemberPhone);
//        GlobalMessageManager.AddEvent(_menuButton6Text);
        GlobalMessageManager.AddEvent(_requestImage);
        GlobalMessageManager.AddEvent(_teamMemberNormal);
        GlobalMessageManager.AddEvent(_teamMemberCSIDs);

        GlobalMessageManager.AddListener(Constants.Events.TeamMemberNormalEventId.ToString(), OnTeamMemberNormal);
		GlobalMessageManager.AddListener(Constants.Events.TeamMemberSelectedEventId.ToString(), OnTeamMemberSelected);

		Column2ColorMap = new ColorMap();
		// default colormaps
		Column2ColorMap.ranges.Add (new GridColorRange(95, new Color (1,0.8f,0.8f,1))); // red
		Column2ColorMap.ranges.Add (new GridColorRange(100, new Color (1,1,0.7f,1))); // yellow
		Column2ColorMap.ranges.Add (new GridColorRange(99999, new Color (0.8f,1,0.8f,1))); //green

		// note that these are 33, not 0.33
		Column3ColorMap = new ColorMap();
		Column3ColorMap.ranges.Add (new GridColorRange(33, new Color (0.8f,1,0.8f,1))); // green tint   
		Column3ColorMap.ranges.Add (new GridColorRange(66, new Color (1,1,0.7f,1))); // yellow tint
		Column3ColorMap.ranges.Add (new GridColorRange(99999, new Color (1,0.8f,0.8f,1))); // red tint
    }

    /// <summary>
    /// Populates instance data based off of received TeamMember data.
    /// </summary>
    /// <param name="teammember"></param>
    public void AddTeamMemberReference(TeamMember teamMember, Manifest manifest)
    {
		_teamMemberReference = teamMember;
		FullName.text = _teamMemberReference.Name;
		Title.text = _teamMemberReference.Title;
		SkillRatings = manifest.GetSkillRatings (teamMember.Id);

		SetupColorRanges (); // this could be done just once from somewhere else

		if (teamMember.Tag != null && teamMember.Tag != "") {
			var tagDict = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string,string[][]>> (teamMember.Tag);
			header = tagDict ["HEADER"];
			grid = tagDict ["GRID"];
		} else {
			header = new string[][]{new string[]{"",""},new string[]{"",""}};
			grid = new string[][]{new string[]{"Product","%Q/Units","Rank"},new string[0],new string[0],new string[0]};
			//grid = new string[][]{new string[]{"Product","%Q/Units","Rank"},new string[]{"","",""},new string[]{"","",""},new string[]{"","",""}};
		}
		if (grid.GetLength (0) < 4) // our old canned data would fail here
			grid = new string[][] {
				new string[]{"Product","%Q/Units","Rank"},
				new string[]{"Sales","",""},
				new string[] {
					"Data",
					"Format",
					"Mismatch"
				},
				new string[] {
					"Mismatch",
					"",
					""
				}
			};

		// override with custom labels if manifest.globalLabel has them
		if (MCoachModel.GetManifest().GlobalLabels.ContainsKey("PRODUCT COL D") && MCoachModel.GetManifest().GlobalLabels["PRODUCT COL D"]!= ""){
			grid[0][0] = MCoachModel.GetManifest().GlobalLabels["PRODUCT COL D"];
		}
		if (MCoachModel.GetManifest().GlobalLabels.ContainsKey("PRODUCT COL E") && MCoachModel.GetManifest().GlobalLabels["PRODUCT COL E"]!= ""){
			grid[0][1] = MCoachModel.GetManifest().GlobalLabels["PRODUCT COL E"];
		}
		if (MCoachModel.GetManifest().GlobalLabels.ContainsKey("PRODUCT COL F") && MCoachModel.GetManifest().GlobalLabels["PRODUCT COL F"]!= ""){
			grid[0][2] = MCoachModel.GetManifest().GlobalLabels["PRODUCT COL F"];
		}

		// BBraun, give this team member an unsubmittted FCR if they don't have one and the login is a team lead...
		if ( DrilldownController.GetInstance().Aliases.Count == 0 && manifest.UserInfo.LevelNumber == manifest.MaxLevels-1)
			manifest.AddFcr (teamMember.Id);

		int percent = 0;//(int)(Random.value * 100);
//		Dictionary<int, TeamMemberPerformanceOverview> performanceOverview = manifest.GetQtdPerformanceOverview (teamMember.Id);
		//TODO get the QTD from the API
//		int percent = (int)(100*performanceOverview [teamMember.Id].PerformanceQTD);
//		if (percent == 0) //TODO remove when data are available
//			percent = (int)(Random.value * 100);
		float rating = 0;
		int sessions = manifest.GetTeamMemberFcrCount(teamMember.Id);//(int)(Random.value * 15);// performanceOverview [teamMember.Id].NumCoachingSessions;
//		if (SkillRatings == null || SkillRatings.Count == 0)
//			SkillRatings = RandomSkillRatings (teamMember);

		foreach (int CSID in SkillRatings.Keys) {
			SellingSkillInfo info = SkillRatings [CSID];
			//sessions += info.CoachedPastSixMonths;
			CoachingSkillCategory CSCat = null;
			foreach (CoachingSkillCategory testcat in manifest.CoachingCategories) {
				if (testcat.ID == CSID) {
					CSCat = testcat;
					break;
				}
			}
			//normalize the rating for averaging
			// if all ratings operate with a final end value of 10, then this normalization isn't necessary
			float ratingRange = 10;
			if (CSCat != null && CSCat.Ratings.Length>0)
				ratingRange = CSCat.Ratings [CSCat.Ratings.Length - 1].EndValue; // Rating documentation says this should always be 10
			rating += info.AverageRating / ratingRange;
		}

		if (teamMember.LevelNumber < MCoachModel.GetManifest ().MaxLevels) {
			// don't show the QTD, RANK, Coaching Sessios for upper levels
			Title.gameObject.SetActive(true);
			TQDPercent.gameObject.SetActive(false);
			RankText.gameObject.SetActive(false);
			CoachingCount.gameObject.SetActive(false);
			RatingBarForeground.gameObject.SetActive(false);
			RatingBarBackground.gameObject.SetActive(false);
			CircleImage.gameObject.SetActive(false);
		} else {
			Title.gameObject.SetActive(false);
			rating /= SkillRatings.Count;
			rating *= 10; // use range of 10 for color and bar size calculations.
			TQDPercent.gameObject.SetActive(true);
			string label1 = "QTD";
			if (MCoachModel.GetManifest().GlobalLabels.ContainsKey("PRODUCT COL B") && MCoachModel.GetManifest().GlobalLabels["PRODUCT COL B"]!= "")
				label1 = MCoachModel.GetManifest().GlobalLabels["PRODUCT COL B"];
			TQDPercent.text = label1+" " + header [1] [0];
			label1 = "Rank";
			if (MCoachModel.GetManifest().GlobalLabels.ContainsKey("PRODUCT COL C") && MCoachModel.GetManifest().GlobalLabels["PRODUCT COL C"]!= "")
				label1 = MCoachModel.GetManifest().GlobalLabels["PRODUCT COL C"];
			RankText.gameObject.SetActive(true);
			RankText.text = label1+" " + header [1] [1];
			CoachingCount.gameObject.SetActive(true);
			CoachingCount.text = sessions.ToString ();
			RatingBarForeground.gameObject.SetActive(true);
			RatingBarForeground.fillAmount = 0.5f; //rating/10f;
			RatingBarBackground.gameObject.SetActive(true);
			if (DrilldownController.GetInstance().Aliases.Count == 0 && manifest.UserInfo.LevelNumber == manifest.MaxLevels)
				CircleImage.gameObject.SetActive(false);
			else
				CircleImage.gameObject.SetActive(true);

			/* ratings based color code
		if (rating < 4) {
			RatingBarForeground.color = new Color (0.898f, 0.047f, 0.102f, 1.0f); // red
			TQDPercent.color = Color.white;
		} else 
		if (rating < 7) {
			RatingBarForeground.color = new Color (0.862f, 0.815f, 0.0f, 1.0f); // yellow
			TQDPercent.color = Color.black;
		} else {
			RatingBarForeground.color = new Color (0.192f, 0.690f, 0.129f, 1.0f); // green
			TQDPercent.color = Color.white;
		}
		*/
		
			//TQD % based color code
			float qtdPercent = 0.0f; // incase there is no header value , or could use rating/10;
			if (float.TryParse (header [1] [0].Replace ("%", ""), out qtdPercent)) {
				if (qtdPercent < 95f) {
					RatingBarForeground.color = new Color (0.898f, 0.047f, 0.102f, 1.0f); // red
					TQDPercent.color = Color.white;
				} else 
			if (qtdPercent < 100) {
					RatingBarForeground.color = new Color (0.862f, 0.815f, 0.0f, 1.0f); // yellow
					TQDPercent.color = Color.black;
				} else {
					RatingBarForeground.color = new Color (0.192f, 0.690f, 0.129f, 1.0f); // green
					TQDPercent.color = Color.white;
				}
			} else {
				// not a valid float, set to black text on white background
				RatingBarForeground.color = Color.white;
				TQDPercent.color = Color.black;
			}
		}
		
		/* CJR
		if (rating < 5) {
			float percentage = (rating / 5f);
			RatingBarForeground.color = new Color (1, percentage, 0);
		} else {
			float percentage =  1f - ((10 - rating)/5f);
			RatingBarForeground.color = new Color (1f - percentage, 1, 0); 
		}
		*/
    }

	void SetupColorRanges(){

		// return, using defaults if null ColorRanges in manifest
		if (MCoachModel.GetManifest ().CompanyColorRanges == null)
			return;

		Column2ColorMap = new ColorMap();
		Column2ColorEnabled = false;
		Column3ColorMap = new ColorMap();
		Column3ColorEnabled = false;

		ColorRange productColorRange = null;
		ColorRange rankColorRange = null;

		foreach (ColorRange range in MCoachModel.GetManifest().CompanyColorRanges) {
			string typeAsString = range.Type.ToString();
			if (range.Type == ColorRangeType.Product && range.Enabled){ //TODO include enabled flag
				Column2ColorEnabled = true;
				Column2ColorMap.ranges.Add (new GridColorRange((float)range.StartValue, MainMenuView.hexToColor(range.StartColor)));
				Column2ColorMap.ranges.Add (new GridColorRange((float)range.EndValue, MainMenuView.hexToColor(range.MidColor)));
				Column2ColorMap.ranges.Add (new GridColorRange(float.MaxValue, MainMenuView.hexToColor(range.EndColor)));
			}
			if (range.Type == ColorRangeType.Rank && range.Enabled){ //TODO include enabled flag
				Column3ColorEnabled = true;
				Column3ColorMap.ranges.Add (new GridColorRange((float)range.StartValue, MainMenuView.hexToColor(range.StartColor)));
				Column3ColorMap.ranges.Add (new GridColorRange((float)range.EndValue, MainMenuView.hexToColor(range.MidColor)));
				Column3ColorMap.ranges.Add (new GridColorRange(float.MaxValue, MainMenuView.hexToColor(range.EndColor)));
			}
		}
	}
/*
	private Dictionary<int, SellingSkillInfo> RandomSkillRatings(TeamMember teammember){

		Dictionary<int, SellingSkillInfo> result = new Dictionary<int, SellingSkillInfo> ();
		for (int i = 0; i< teammember.CSID.Length; i++) {
			result.Add (teammember.CSID[i], RandomSellingSkillInfo());
		}
		return result;
	}

	//TODO remove this testing method and all references to it
	public static SellingSkillInfo RandomSellingSkillInfo(){ // This is just a development aid until we have data to display.
		SellingSkillInfo info = new SellingSkillInfo ();
		info.AverageRating = 0;
		info.CoachedPastSixMonths = 0;
		info.Trends = new SellingSkillRatingTrend[4];
		for (int j=0; j<4;j++){
			info.Trends[j] = new SellingSkillRatingTrend();
			info.Trends[j].Quarter = j+1;
			info.Trends[j].AverageRating = Random.value*10;
			info.AverageRating += info.Trends[j].AverageRating;
			if (j>1) info.CoachedPastSixMonths++;
		}
		info.AverageRating/=4;
		return info;
	}
*/	
	public void OnInstanceClicked()
    {
		Debug.Log ("On instance clicked!");
		if (_teamMemberReference == null) {
			Debug.Log ("No team member reference");
			return;
		}

		// reps cannot select teammate...
		if (MCoachModel.GetManifest ().UserInfo.LevelNumber == MCoachModel.GetManifest ().MaxLevels)
			return;

		// once we have the levels in the data
		if (_teamMemberReference.LevelNumber < MCoachModel.GetManifest().MaxLevels) { // if this is not a rep
			FillBackground.color = new Color(0.65f,0.65f,0.65f);
			teamMemberButton.interactable = false;
			teamMemberButton.gameObject.GetComponent<Image>().color = Color.black;	
			DrilldownController.GetInstance ().DrillDownAs (_teamMemberReference);
		} else {
			object teamMemberRef = _teamMemberReference;
			_teamMemberSelected.FireEvent (ref teamMemberRef);
		}
	}

	// break this out so other mechanisms can select the team member...
	public void OnTeamMemberSelected(string EventName, ref object Data){

		if (this == null) {
			// we've been deleted ! how ?
			GlobalMessageManager.RemoveListener( EventName, OnTeamMemberSelected);
			return;
		}

		TeamMember selectedTeamMember = (TeamMember)Data;

		if (selectedTeamMember != _teamMemberReference)
			return;
        
		string teamMemberLevelLabel = "Territory: ";
		if (MCoachModel.GetManifest ().LevelLabels != null && 
			MCoachModel.GetManifest ().LevelLabels.ContainsKey (_teamMemberReference.LevelNumber) &&
			MCoachModel.GetManifest ().LevelLabels [_teamMemberReference.LevelNumber] != "") {
			teamMemberLevelLabel = (MCoachModel.GetManifest ().LevelLabels [_teamMemberReference.LevelNumber] + ": ");
		}

        object fullNameRef = _teamMemberReference.Name;
        object territoryRef = _teamMemberReference.Territory;
		object teamMemberLevelLabelRef = teamMemberLevelLabel;
        object emailRef = _teamMemberReference.Email;
        object locationRef = _teamMemberReference.Location;
        object phoneRef = _teamMemberReference.Phone;
        object imageFile = _teamMemberReference.ThumbnailFile;
        object csIDs = _teamMemberReference.CSID;
        
        _teamMemberName.FireEvent(ref fullNameRef);
        _teamMemberTerritory.FireEvent(ref territoryRef);
		_teamMemberLevelLabel.FireEvent (ref teamMemberLevelLabelRef);
        _teamMemberEmail.FireEvent(ref emailRef);
        _teamMemberLocation.FireEvent(ref locationRef);
        _teamMemberPhone.FireEvent(ref phoneRef);



		// build out the sales data table from the team member TAG and the table template.

        //object menuBtn6Text = "Miller Heiman Resources";
        //_menuButton6Text.FireEvent(ref menuBtn6Text);
        Debug.Log("Request profile image: " + imageFile);
        _requestImage.FireEvent(ref imageFile);
        _teamMemberCSIDs.FireEvent(ref csIDs);
        
        _teamMemberNormal.FireEvent();
		GameObject mCoachBackgroundImage = GameObject.Find ("mCoachBackgroundImage");

		Color indicatorColor = Color.red;
		string dateText = "No Previous FCR";
		// the most recent FCR may be FCRList[1], or during drilldown, it may be FCRList[0];
		FCR mostRecentFCR = null;
		if (_teamMemberReference.FCRList.Count > 0 && _teamMemberReference.FCRList[0].ServerStatus != FCR.Status.UnSubmitted)
			mostRecentFCR = _teamMemberReference.FCRList[0];
		else if (_teamMemberReference.FCRList.Count > 1)
			mostRecentFCR = _teamMemberReference.FCRList[1];

		if (mostRecentFCR != null) {
			System.DateTime lastFCRDate = mostRecentFCR.CreationDate.Date; // was _teamMemberReference.FCRList.count-1, the last one
			dateText = lastFCRDate.ToString("MM/dd/yyyy");
			if (lastFCRDate > System.DateTime.Now.Date.AddDays (-30)) indicatorColor = Color.yellow;
			if (lastFCRDate > System.DateTime.Now.Date.AddDays (-15)) indicatorColor = Color.green;
		}
		mCoachBackgroundImage.transform.Find ("TeamMemberScreen/TeamMemberBackgroundImage/DateDataText").GetComponent<Text> ().text = dateText;
		mCoachBackgroundImage.transform.Find ("TeamMemberScreen/TeamMemberBackgroundImage/DateIndicatorImage").GetComponent<Image> ().color = indicatorColor;


		mCoachBackgroundImage.GetComponent<Animator>().Play("ShowTeamMemberScreen");

		// build out the sales data table from the team member TAG and the table template.
		GameObject salesDataPanel = mCoachBackgroundImage.transform.Find("TeamMemberScreen/TeamMemberBackgroundImage/SalesDataPanel").gameObject;
		GameObject salesDataItemTemplate = salesDataPanel.transform.Find ("SalesDataScrollView/SalesDataGridPanel/SalesDataValuesPanel").gameObject; // this has a layout element on it so you can just add them
		GameObject salesDataGridContainer = salesDataPanel.transform.Find ("SalesDataScrollView/SalesDataGridPanel").gameObject; // this has a vertical layout group

		salesDataPanel.transform.Find ("SalesDataHeaderPanel/HeaderLabel1Text").GetComponent<Text> ().text = header [0] [0];
		salesDataPanel.transform.Find ("SalesDataHeaderPanel/HeaderLabel2Text").GetComponent<Text> ().text = header [0] [1];
		salesDataPanel.transform.Find ("SalesDataHeaderPanel/HeaderValue1Text").GetComponent<Text> ().text = header [1] [0];
		salesDataPanel.transform.Find ("SalesDataHeaderPanel/HeaderValue2Text").GetComponent<Text> ().text = header [1] [1];

		salesDataPanel.transform.Find ("ColumnNamesPanel/GridLabel1Text").GetComponent<Text> ().text = grid [0] [0];
		salesDataPanel.transform.Find ("ColumnNamesPanel/GridLabel2Text").GetComponent<Text> ().text = grid [0] [1];
		salesDataPanel.transform.Find ("ColumnNamesPanel/GridLabel3Text").GetComponent<Text> ().text = grid [0] [2];

		// now spawn as many data rows as needed for the grid
		for (int spawnedIndex = 0; spawnedIndex < _spawnedSalesDataItems.Count; spawnedIndex++)
		{
			Destroy(_spawnedSalesDataItems[spawnedIndex]);
		}
		_spawnedSalesDataItems.Clear();
		salesDataItemTemplate.SetActive (false);

		for (int row = 0; row < grid[1].Length; row++) {
			GameObject newInstance = Instantiate(salesDataItemTemplate) as GameObject;
			newInstance.transform.SetParent(salesDataGridContainer.transform); // layout should do the rest.
			newInstance.transform.localScale = new Vector3(1,1,1); // compensate for the reparenting scale effect
			newInstance.SetActive (true);

			/*
			 * {"HEADER":[["QTD % Q","QTD Rank"],["101.55%","14"]],
			 * "GRID":[
			 * ["Product","%Q/Unit","Rank","Out of"],
			 * ["%Total Sales","%Introcan","%IVSets","Total Nutrition Gr","Total Onguard Gr","%Inf Unit","%Drug Delivery"],
			 * ["101.50%","101.30%","90.20%","(2,264)","115878","66.20%","109.70%"],
			 * ["25","25","56","44","6","11","7"],
			 * ["","","","64","64","",""]]}
			 * */

			Color gridColor = Color.white;

				if (Column2ColorEnabled && grid [1][row].Contains("%")) //TODO temporary comment out this color coding for pilot while awaiting design change
			{
				string sPct = grid[2][row].Replace("%","");
				float fPct = 0;
				float.TryParse(sPct,out fPct);
				/* refactored to use customer supplied colors and ranges
				gridColor = new Color(1,0.8f,0.8f,1); // red tint
				if (fPct > 95f) gridColor = new Color(1,1,0.7f,1); // yellow tint
				if (fPct >= 100f) gridColor = new Color(0.8f,1,0.8f,1); // green tint
				*/
				gridColor = GridColor( fPct,Column2ColorMap);
			}

			newInstance.transform.Find ("GridValue1Background/GridValue1Text").GetComponent<Text> ().text = grid [1][row]; //product name
			newInstance.transform.Find ("GridValue2Background").GetComponent<Image> ().color = gridColor;
			newInstance.transform.Find ("GridValue2Background/GridValue2Text").GetComponent<Text> ().text = grid [2][row]; //%q/unit
			newInstance.transform.Find ("GridValue2Background/GridValue2Text").GetComponent<Text> ().color = ForegroundColor(gridColor);

			gridColor = Color.white;
			float num = 0;
			float denom = 1;

			if (Column3ColorEnabled && grid.GetLength(0) > 4 && grid[4][row] != "" && float.TryParse(grid[4][row],out denom)){

				float.TryParse(grid[3][row],out num);
				float.TryParse(grid[4][row],out denom);
				float percent = num/denom;
				/* refactored to use customer supplied colors and ranges
				gridColor = new Color(0.8f,1,0.8f,1); // green tint   
				if (percent > 0.33f) gridColor = new Color(1,1,0.7f,1); // yellow tint
				if (percent > 0.66f) gridColor = new Color(1,0.8f,0.8f,1); // red tint
				*/
				gridColor = GridColor( 100*percent,Column3ColorMap);
			}
			newInstance.transform.Find ("GridValue3Background").GetComponent<Image> ().color = gridColor;
			newInstance.transform.Find ("GridValue3Background/GridValue3Text").GetComponent<Text> ().text = grid [3][row]; // rank
			newInstance.transform.Find ("GridValue3Background/GridValue3Text").GetComponent<Text> ().color = ForegroundColor(gridColor);

			_spawnedSalesDataItems.Add(newInstance);
		}


        //  Debug.Log("Set: " + this.name + " to selected state");
        this.GetComponent<Animator>().Play("TeamMemberSelected");
    }

	private Color GridColor(float value, ColorMap map){
		foreach (GridColorRange range in map.ranges) {
			if (value <= range.threshold)
				return range.color;
		}
		return Color.white; // if not found, make the cell white.
	}

	public static Color ForegroundColor(Color background){
		// could calculate lightness, which is (max+min)/2 of rgb
		// but that doesnt give us the result we are used to seeing for BBraun
		// so use avg(r,g,b) > 0.5f

		if (background.r + background.g + background.b > 1.5f)
			return Color.black;
		else
			return Color.white;
	
	}

    private void OnTeamMemberNormal(string EventName, ref object Data)
    {
        //  Debug.Log("Set: " + this.name + " to normal state");
        this.GetComponent<Animator>().Play("TeamMemberNormal");
    }
    
    private void OnDestroy()
    {
        GlobalMessageManager.RemoveEvent(_teamMemberName);
        GlobalMessageManager.RemoveEvent(_teamMemberTerritory);
		GlobalMessageManager.RemoveEvent(_teamMemberLevelLabel);
        GlobalMessageManager.RemoveEvent(_teamMemberLocation);
        GlobalMessageManager.RemoveEvent(_teamMemberPhone);
//        GlobalMessageManager.RemoveEvent(_menuButton6Text);
        GlobalMessageManager.RemoveEvent(_requestImage);

        GlobalMessageManager.RemoveListener(Constants.Events.TeamMemberNormalEventId.ToString(), OnTeamMemberNormal);
    }
}