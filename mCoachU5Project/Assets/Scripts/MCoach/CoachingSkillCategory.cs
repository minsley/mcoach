﻿namespace MCoach
{
    /// <summary>
    /// <para>Date: 07/17/2015</para>
    /// <para>Author: NuMedia</para>
    /// <para>Coaching Skill Data Object</para>
    /// </summary>
    public class CoachingSkillCategory
	{
		/// <summary>
		/// Stores the different ratings for the Coaching Skill
		/// This rating array is expected to define a range from 0-10
		/// Example: Ratings[0] Endvalue: 5, Ratings[1] Endvalue: 10
		/// </summary>
		public Rating[] Ratings { get; set; }
		/// <summary>
		/// Stores the Coaching Skill Title
		/// </summary>
		public string Title { get; set; }
		/// <summary>
		/// Stores the Coaching Skill Description
		/// </summary>
		public string Description { get; set; }
		/// <summary>
		/// Stores the file locations of the jpgs describing the coaching
		/// skill. The JPG are the generated from the pdfs on the server.
		/// Jpgs images need to be listed in the order in which they appear.
		/// </summary>
		public string[] InfoFiles { get; set; }
		/// <summary>
		/// Stores the unique ID for the CoachingSkillCategory.
		/// </summary>
	    public int ID { get; set; }
        /// <summary>
        /// If this skill is NOT active then it shouldnt be available to be added 
        /// to an FCR. 
        /// </summary>
        public bool IsActive { get; set; }
    }
}