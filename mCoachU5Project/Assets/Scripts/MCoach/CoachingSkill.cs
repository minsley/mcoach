﻿using System.Collections.Generic;

namespace MCoach
{
	/// <summary>
	/// <para>Date: 07/22/2015</para>
	/// <para>Author: NuMedia</para>
	/// <para></para>
	/// </summary>
	public class CoachingSkill
    {
        #region Constructors 

        /// <summary>
        /// Constructs a new coaching skill using the optional 
        /// category ID
        /// </summary>
        /// <param name="categoryId"></param>
	    public CoachingSkill(int? categoryId = null)
	    {
	        if (categoryId.HasValue)
	        {
	            CategoryID = categoryId.Value;
	        }

	        UserRating = "";
	        FieldRideObservations = "";
            CoachingPlanList = new List<CoachingPlan>();
	    }

        #endregion

        #region Properties

        /// <summary>
		/// ID of the corresponding CoachingSkillCategory
		/// </summary>
		public int CategoryID { get; set; }
		/// <summary>
		/// The rating for this coaching skill instance.
		/// </summary>
		public string UserRating { get; set; }

        /// <summary>
        /// The observations made during the ride
        /// </summary>
		public string FieldRideObservations { get; set; }

		/// <summary>
		/// CoachingPlans created for this CoachingSKill instance.
		/// </summary>
		public List<CoachingPlan> CoachingPlanList { get; set; }

        #endregion
    }
}