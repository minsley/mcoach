﻿using System;

namespace MCoach
{
	public class GoalForFieldRide
	{
        /// <summary>
        /// Stores the ID of the goal data item
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// The action item for the goal
        /// </summary>
		public string ActionItem { get; set; }

        /// <summary>
        /// The observations related to the goal
        /// </summary>
		public string Observations { get; set; }

        /// <summary>
        /// Rating of how well the rep achieved the goal.
        /// </summary>
		public float ExecutionRating { get; set; }

        /// <summary>
        /// The date/time the data object was created.
        /// </summary>
        public DateTime CreationDate { get; set; }
	}
}