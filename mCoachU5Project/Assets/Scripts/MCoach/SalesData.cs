﻿using System;

namespace MCoach
{
	public class SalesData
	{
        /// <summary>
        /// Stores the ID of the sales data item
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// The product the rep is selling
        /// </summary>
		public string ProductName { get; set; }

        /// <summary>
        /// The sales quota.
        /// </summary>
		public int Quota { get; set; }

        /// <summary>
        /// The Quarter To Date achievement 
        /// </summary>
		public int QTDAchieved { get; set; }

        /// <summary>
        /// The Year To Date achievement 
        /// </summary>
		public int YTDAchieved { get; set; }

        /// <summary>
        /// The date/time the sales data object was created.
        /// </summary>
        public DateTime CreationDate { get; set; }
	}
}