﻿namespace MCoach
{
    public class SellingSkillRatingTrend
    {
        /// <summary>
        /// The quarter to which the average rating applies to
        /// </summary>
        public int Quarter { get; set; }

        /// <summary>
        /// The average rating for the current quarter
        /// </summary>
        public float AverageRating { get; set; }
    }
}
