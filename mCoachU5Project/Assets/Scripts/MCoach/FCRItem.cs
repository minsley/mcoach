﻿using System;

namespace MCoach
{
	public enum FCRItemType{
        none,
		text,
		checkbox,
		dropdown,
	}

    [Serializable]
	public class FCRItem
	{
		public FCRItem(){
			DropdownValues = new string[0];
		}

		/// <summary>
		/// returns a deep copy of the Item
		/// </summary>
		public FCRItem DeepCopy(){
		    var copy = new FCRItem
		    {
		        Id = Id,
		        ItemType = ItemType,
		        ItemValue = ItemValue,
		        Required = Required,
		        ItemHeader = ItemHeader,
		        ItemBody = ItemBody,
		        DisplayBodyToRep = DisplayBodyToRep,
		        DropdownValues = new string[DropdownValues.Length]
		    };
		    Array.Copy (DropdownValues, copy.DropdownValues, DropdownValues.Length);
			copy.CreationDate = CreationDate;
			return copy;
		}

        /// <summary>
        /// Stores the ID of the item
        /// </summary>
        public int Id { get; set; }

		/// <summary>
		/// Stores the type of the item
		/// </summary>
		public FCRItemType ItemType { get; set; }

        /// <summary>
        /// The item value ("true" or "false" for checkbox)
        /// </summary>
		public string ItemValue { get; set; }

		/// <summary>
		/// Whether this item must have a value before an FCR can be submitted
		/// In the case of type checkbox, that value must be true
		/// </summary>
		public bool Required { get; set; }

		/// <summary>
		/// Very Brief description of the item to be used on buttons and title bar
		/// </summary>
		public string ItemHeader { get; set; }

		/// <summary>
		/// Complete description of the item to be used on UI panel
		/// </summary>
		public string ItemBody { get; set; }

		/// <summary>
		/// Whether this item body should be shown in the FCR max level view
		/// </summary>
		public bool DisplayBodyToRep { get; set; }

		/// <summary>
		/// A list of values which can be selected for type dropdown
		/// </summary>
		public string[] DropdownValues { get; set; }

        /// <summary>
        /// The date/time the data object was created.
        /// </summary>
        public DateTime CreationDate { get; set; }
	}
}