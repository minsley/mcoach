﻿using System;

namespace MCoach
{
    /// <summary>
    /// Represents a pre-calculated aggregate stat
    /// </summary>
    public class AggregateStat
    {
        public int CoachingSkillId { get; set; }
        public double AveSkillRating { get; set; }
        public int SixMonthFcrCount { get; set; }

        public double Q1Ave { get; set; }
        public double Q2Ave { get; set; }
        public double Q3Ave { get; set; }
        public double Q4Ave { get; set; }

        public DateTime? Q1Date { get; set; }
        public DateTime? Q2Date { get; set; }
        public DateTime? Q3Date { get; set; }
        public DateTime? Q4Date { get; set; }
    }
}