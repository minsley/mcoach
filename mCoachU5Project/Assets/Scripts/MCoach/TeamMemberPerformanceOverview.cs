﻿namespace MCoach
{
    public class TeamMemberPerformanceOverview
    {
        public int NumCoachingSessions { get; set; }
        public float PerformanceQTD { get; set; }
    }
}
