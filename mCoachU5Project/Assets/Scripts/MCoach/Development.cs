﻿using System;

namespace MCoach
{
	public class Development
	{
        /// <summary>
        /// Stores the ID of the development data item
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// The action item for the development of the rep
        /// </summary>
		public string ActionItem { get; set; }

        /// <summary>
        /// The action that was taken
        /// </summary>
		public string ActionTaken { get; set; }

        /// <summary>
        /// The results of the actions
        /// </summary>
		public string ResultsSeen { get; set; }

        /// <summary>
        /// The date/time the data object was created.
        /// </summary>
        public DateTime CreationDate { get; set; }
	}
}