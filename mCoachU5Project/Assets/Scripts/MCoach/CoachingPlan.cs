﻿using System;

namespace MCoach
{
	/// <summary>
	/// <para>Date: 07/22/2015</para>
	/// <para>Author: NuMedia</para>
	/// <para>About</para>
	/// </summary>
    public class CoachingPlan
	{
		/// <summary>
		/// Title of Coaching Plan
		/// </summary>
		public string Title { get; set; }

        /// <summary>
        /// The date the coaching plan was created
        /// </summary>
		public DateTime Date { get; set; }

        /// <summary>
        /// Send a reminder to the rep
        /// </summary>
		public bool Remind { get; set; }
	}
}