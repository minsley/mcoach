﻿namespace MCoach
{
	using System.Collections.Generic;
	
    public class TeamMember
    {
        #region Constructors 

        /// <summary>
        /// Creates a new team member object
        /// </summary>
        public TeamMember()
        {
            PreGeneratedStats = null;
            FCRList = null;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Unique Team Member Identifier
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Full name of Team Member
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The Team member's location
        /// </summary>
        public string Location { get; set; }

        /// <summary>
        /// The team member'sterritory
        /// </summary>
        public string Territory { get; set; }

        /// <summary>
        /// The team member's email
        /// </summary>
		public string Email { get; set; }

        /// <summary>
        /// The team member's phone number
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// The team member's thumbnail
        /// </summary>
	    public string ThumbnailFile { get; set; }

        /// <summary>
        /// The company assigned title for the team member
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// The username of the team member
        /// </summary>
        public string Username { get; set; }

        /// <summary>
        /// The password of the team member
        /// </summary>
        public string Password { get; set; }
	    
        /// <summary>
        /// IDs of CoachingSkillCategories to be displayed for the user.
        /// </summary>
	    public int[] CSID { get; set; }
	    
        /// <summary>
	    /// The user's FCRs
        /// </summary>
	    public List<FCR> FCRList { get; set; }

        /// <summary>
        /// Generic data to associate with the team member
        /// </summary>
        public string Tag { get; set; }

        /// <summary>
        /// The current "level" of the team member in the company hierarchy.
        /// </summary>
        public int LevelNumber { get; set; }

        /// <summary>
        /// Pre-generated aggregate stats
        /// </summary>
        public AggregateStat[] PreGeneratedStats { get; set; }

        #endregion
        
        #region Methods 

        /// <summary>
        /// Returns a copy of the received TeamMember
        /// </summary>
        /// <param name="teamMember"></param>
        public static TeamMember Copy(TeamMember teamMember)
        {
	        var member = new TeamMember
	        {
	            Id = teamMember.Id,
	            Name = teamMember.Name,
	            Location = teamMember.Location,
	            Territory = teamMember.Territory,
	            Email = teamMember.Email,
	            Phone = teamMember.Phone,
	            ThumbnailFile = teamMember.ThumbnailFile,
	            CSID = teamMember.CSID,
	            FCRList = teamMember.FCRList,
                Tag = teamMember.Tag
	        };


	        return member;
        }

        #endregion
    }
}