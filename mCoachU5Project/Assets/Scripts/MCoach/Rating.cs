﻿namespace MCoach
{
	/// <summary>
	/// <para>Date: 07/22/2015</para>
	/// <para>Author: NuMedia</para>
	/// <para>Rating Data Object. Represents a range for the rating screen.</para>
	/// </summary>
	public class Rating
	{
		/// <summary>
		/// The description of the current rating range
		/// </summary>
		public string Description { get; set; }

		/// <summary>
		/// Stores the endvalue of this rating range. Expected to be between 0 and 10
		/// If only one Rating is going to be stored in CoachingSkillCategory this should be 10
		/// Giving the total range 0-10.
		/// </summary>
		public float EndValue { get; set; }
	}
}