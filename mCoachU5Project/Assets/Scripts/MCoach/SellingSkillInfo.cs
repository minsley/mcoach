﻿namespace MCoach
{
    public class SellingSkillInfo
    {
        /// <summary>
        /// The average rating for all the FCRs 
        /// </summary>
        public float AverageRating { get; set; }

        /// <summary>
        /// The number of coached sections over the past six months
        /// </summary>
        public int CoachedPastSixMonths { get; set; }

        /// <summary>
        /// The skill rating trend for the 4th quarter of the previous year and the last 
        /// 3 quarters for the current year
        /// </summary>
        public SellingSkillRatingTrend[] Trends { get; set; }
    }
}
