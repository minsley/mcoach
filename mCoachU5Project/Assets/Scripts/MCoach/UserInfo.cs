﻿using System.Collections.Generic;

namespace MCoach
{
    public class UserInfo
    {
        #region Constructors 

        /// <summary>
        /// Creates a new user info object.
        /// </summary>
        public UserInfo()
        {
            MyTeam = null;
            MyContent = null;
            UserContent = null;
            PreGeneratedStats = null;
        }

        #endregion

        #region Properties

        /// <summary>
        /// The unique identifier.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// The user's full name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The user's login name.
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// The user's password
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// The user's email address
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// The user's district
        /// </summary>
        public string District { get; set; }

        /// <summary>
        /// The user's Territory
        /// </summary>
        public string Territory { get; set; }

        /// <summary>
        /// The user's avatar thumbnail
        /// </summary>
        public string ThumbnailFile { get; set; }

        /// <summary>
        /// The server's last known login date.
        /// </summary>
        public string LastLoginDate { get; set; }

        /// <summary>
        /// A list of the User's team members.
        /// </summary>
        public List<TeamMember> MyTeam { get; set; }

        /// <summary>
        /// A dictionary that associates a content type ("DISC", "Compentency Model" etc) to a list of content items.
        /// </summary>
        public Dictionary<string, Content[]> MyContent { get; set; } 

        /// <summary>
        /// Gets the user content files
        /// </summary>
        public string[] UserContent { get; set; }

        /// <summary>
        /// The company assigned title for the user
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// The current "level" of the user info in the company hierarchy.
        /// </summary>
        public int LevelNumber { get; set; }

        /// <summary>
        /// Pre-generated aggregate stats
        /// </summary>
        public AggregateStat[] PreGeneratedStats { get; set; }

        #endregion
    }
}