﻿using System;
using System.Text;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;

namespace MCoach
{
    public static class Utils
    {
        /// <summary>
        /// Contains yearly quarter information
        /// </summary>
        public class Quarter
        {
            public DateTime[] Dates { get; set; }
            public int Qtr { get; set; }
        }

        /// <summary>
        /// Returns all the quarter start and end dates for the supplied 
        /// year; If no year is supplied the current year is used
        /// </summary>
        /// <param name="year"></param>
        /// <returns></returns>
        public static Quarter[] GetYearQuarters(int? year = null)
        {
            var theYear = (year ?? DateTime.Now.Year);

            var quarterDates = new[]
            {
                new[] {"1/1/{0}", "3/31/{0} 11:59:59 PM"}, //Q1
                new[] {"4/1/{0}", "6/30/{0} 11:59:59 PM"}, //Q2
                new[] {"7/1/{0}", "9/30/{0} 11:59:59 PM"}, //Q3
                new[] {"10/1/{0}", "12/31/{0} 11:59:59 PM"} //Q4
            };

            var quarters = (from d in quarterDates
                let start = DateTime.Parse(string.Format(d[0], theYear))
                let end = DateTime.Parse(string.Format(d[1], theYear))
                select new[] {start, end}).ToArray();

            return quarters.Select((t, quarter) => new Quarter
            {
                Qtr = quarter + 1,
                Dates = t
            }).ToArray();
        }

        /// <summary>
        /// Returns all the quarter start and end dates for the supplied 
        /// year in the adjusted format; If no year is supplied the current year is used
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static Quarter[] GetAdjustedQuarters(DateTime? date = null)
        {
            var now = date ?? DateTime.Now;
            var year = now.Year;

            var quarterDates = new[]
            {
                new[] {"1/1/{0}", "3/31/{0} 11:59:59 PM"}, //Q1
                new[] {"4/1/{0}", "6/30/{0} 11:59:59 PM"}, //Q2
                new[] {"7/1/{0}", "9/30/{0} 11:59:59 PM"}, //Q3
                new[] {"10/1/{0}", "12/31/{0} 11:59:59 PM"} //Q4
            };

            var currQuarter = -1;

            //find which quarter we are currently in
            for (var q = 0; q < quarterDates.Length; q++)
            {
                var startDate = DateTime.Parse(string.Format(quarterDates[q][0], year));
                var endDate = DateTime.Parse(string.Format(quarterDates[q][1], year));
                if (now < startDate || now > endDate) continue;
                currQuarter = q;
                break;
            }

            var quarterBins = new List<Quarter>();

            //work backwards to include previous years
            // ReSharper disable once UnusedVariable
            foreach (var t in quarterDates)
            {
                var startDate = DateTime.Parse(string.Format(quarterDates[currQuarter][0], year));
                var endDate = DateTime.Parse(string.Format(quarterDates[currQuarter][1], year));

                var quarter = new Quarter
                {
                    Qtr = currQuarter + 1,
                    Dates = new[] {startDate, endDate}
                };

                quarterBins.Add(quarter);

                currQuarter -= 1;

                if (currQuarter < 0)
                {
                    year -= 1;
                    currQuarter = 3;
                }
            }

            return quarterBins.ToArray();
        }

        /// <summary>
        /// Returns an array of 4 quarters starting from the current quarter 
        /// and going into the past.
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static Quarter[] GetQuarterWindow(DateTime? date = null)
        {
            var now = date ?? DateTime.Now;
            var prevQuarters = GetYearQuarters(now.Year - 1);
            var currQuarters = GetYearQuarters(now.Year);
            var allQuarters = new List<Quarter>();
            allQuarters.AddRange(prevQuarters);
            allQuarters.AddRange(currQuarters);

            var quarters = allQuarters.ToArray();

            for (var index = 0; index < quarters.Length; index++)
            {
                if (now < quarters[index].Dates[0] || now > quarters[index].Dates[1]) continue;
                var newQuarters = new List<Quarter>();
                for (var i = 0; i < 4; i++)
                {
                    newQuarters.Insert(0, quarters[index - i]);
                }

                return newQuarters.ToArray();
            }

            return null;
        }

        /// <summary>
        /// Returns the start and end date for the supplied date; 
        /// If no date is supplied then the current date is used.
        /// </summary>
        /// <returns></returns>
        public static DateTime[] GetDateQuarter(DateTime? date = null)
        {
            var now = date ?? DateTime.Now;
            var quarters = GetYearQuarters(now.Year);
            return quarters.First(q => now >= q.Dates[0] && now <= q.Dates[1]).Dates;
        }

        /// <summary>
        /// Decodes a string from it's hexadecimal values back into a string..
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string HexDecode(string str)
        {
            var res = new StringBuilder();

            str = str.ToUpper();

            for (var i = 0; i < str.Length; i += 2)
            {
                var hex = str.Substring(i, 2);
                var dec = Convert.ToUInt32(hex, 16);

                res.Append((char)dec);
            }

            return res.ToString();
        }

        /// <summary>
        /// Encodes a string into it's hexadecimal equivalent
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        /// <remarks>Just delegates to the FastHexEncode method</remarks>
        public static string HexEncode(string str)
        {
            return FastHexEncode(str);
        }

		public static string FastHexEncode(string str){
			var strAsBytes = Encoding.UTF8.GetBytes(str);
			var builder = new StringBuilder (strAsBytes.Length * 2);
			for (var i = 0; i < strAsBytes.Length; i++) {
				builder.Append(BitConverter.ToString(strAsBytes,i,1)); // could set directly... builder[i] 
			}
			return builder.ToString ();
		}

        /// <summary>
        /// Performs a deep copy of the supplied object. Object must be marked as [Serializable]
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static T DeepCopy<T>(T obj)
        {
            using (var mStream = new MemoryStream())
            {
                var formatter = new BinaryFormatter();
                formatter.Serialize(mStream, obj);
                mStream.Position = 0;
                return (T) formatter.Deserialize(mStream);
            }
        }
    }
}
