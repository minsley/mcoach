﻿namespace MCoach
{
    public enum ColorRangeType
    {
        None,
        Product,
        Rank        
    }

    public class ColorRange
    {
        /// <summary>
        /// The type of the color range
        /// </summary>
        public ColorRangeType Type { get; set; }

        /// <summary>
        /// The starting color in #RRGGBB (hex) format
        /// </summary>
        public string StartColor { get; set; }

        /// <summary>
        /// The middle color in #RRGGBB (hex) format
        /// </summary>
        public string MidColor { get; set; }

        /// <summary>
        /// The end color in #RRGGBB (hex) format
        /// </summary>
        public string EndColor { get; set; }

        /// <summary>
        /// The starting floating point value
        /// </summary>
        public double StartValue { get; set; }

        /// <summary>
        /// The ending floating point value
        /// </summary>
        public double EndValue { get; set; }

        /// <summary>
        /// If set to true then apply the color coding
        /// </summary>
        public bool Enabled { get; set; }
    }
}
