﻿namespace MCoach
{
    /// <summary>
    /// Represents a single content item
    /// </summary>
    public class Content
    {
        /// <summary>
        /// The path to the content file
        /// </summary>
        public string ContentUrl { get; set; }

        /// <summary>
        /// The path to the content thumbnail
        /// </summary>
        public string ThumbnailUrl { get; set; }

        /// <summary>
        /// The title that will be displayed in the content list next to the thumbnail.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// The description that will be displayed in the content list next to the thumbnail.
        /// </summary>
        public string Description { get; set; }

    }
}