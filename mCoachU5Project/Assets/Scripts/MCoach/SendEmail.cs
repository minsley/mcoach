﻿namespace MCoach
{
    public class SendEmail
    {
        /// <summary>
        /// The From address
        /// </summary>
        public string From { get; set; }

        /// <summary>
        /// The To address
        /// </summary>
        public string To { get; set; }

        /// <summary>
        /// Comma separated list of CC addresses
        /// </summary>
        public string Cc { get; set; }

        /// <summary>
        /// The subject of the email
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        /// The message of the email
        /// </summary>
        public string Message { get; set; }
    }
}
