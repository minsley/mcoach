﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace MCoach
{
    public class Manifest
    {
        #region Classes/Types

        /// <summary>
        /// Sub rating class
        /// </summary>
        private class SubRating
        {
            public int SkillId { get; set; }
            public float Rating { get; set; }
            public DateTime CreationDate { get; set; }
        }

        /// <summary>
        /// The type of server that generated the manifest.
        /// </summary>
        public enum ServerTypes
        {
            // ReSharper disable once InconsistentNaming
            LIVE,
            // ReSharper disable once InconsistentNaming
            STAGING,
            // ReSharper disable once InconsistentNaming
            WIP,
            // ReSharper disable once InconsistentNaming
            DEV
        }

        #endregion

        #region Properties

        /// <summary>
        /// The maximum number of levels allowed in the hierarchy
        /// </summary>
        public int MaxLevels { get; set; }

        /// <summary>
        /// The login credentials for the user who is synching.
        /// </summary>
        public UserInfo UserInfo { get; set; }

        /// <summary>
        /// The payload request token to the separate payload .7z file; The archive file will contain only the changed
        /// or added files since the last sync. 
        /// </summary>
        public Guid? PayloadToken { get; set; }

        /// <summary>
        /// The result code for the sync operation ("ok", or and error message)
        /// </summary>
        public string Result { get; set; }

        /// <summary>
        /// These are the files that currenty reside on the CLIENT.
        /// </summary>
        public string[] CachedFiles { get; set; }

        /// <summary>
        /// This is a list of files that were in the cached files that were uploaded 
        /// to the server but are no longer needed; after syncing delete the files reference
        /// here.
        /// </summary>
        public string[] DeleteFiles { get; set; }

        /// <summary>
        /// This is a list of files that have either changed or were added since the last sync.
        /// These files will be stored in the .7z file referenced in the PayloadToken property.
        /// </summary>
        public string[] AddFiles { get; set; }

        /// <summary>
        /// Title for the Tip of the Day.
        /// </summary>
        public string TipOfTheDayTitle { get; set; }

        /// <summary>
        /// The description of the current tip of the day.
        /// </summary>
        public string TipOfTheDayDescription { get; set; }

        /// <summary>
        /// the date the tip of the day was published
        /// </summary>
        public DateTime? TipOfTheDayDate { get; set; }

        /// <summary>
        /// The company logo
        /// </summary>
        public string Logo { get; set; }

        /// <summary>
        /// Stores the scrolling message displayed at the bottom of
        /// the dashboard.
        /// </summary>
        public string SubscriptionUpdates { get; set; }

        /// <summary>
        /// Stores the Coaching Skill Title
        /// </summary>
        public string GlobalSkillTitle { get; set; }

        /// <summary>
        /// Stores Coaching Categories
        /// </summary>
        public CoachingSkillCategory[] CoachingCategories { get; set; }

        /// <summary>
        /// If set to true then the sync method will not generate a payload for the request
        /// </summary>
        public bool NoPayload { get; set; }

        /// <summary>
        /// IMPORTANT: this number must increment whenever there are changes to the structure of the manifest 
        /// </summary>
        public int ManifestVersion
        {
            get { return 1; }
        }

        /// <summary>
        /// Returns the type of server that generated the manifest
        /// </summary>
        public ServerTypes ServerType { get; set; }

        /// <summary>
        /// Company primary color
        /// </summary>
        public string PrimaryColor { get; set; }

        /// <summary>
        /// Company secondary color 
        /// </summary>
        public string SecondaryColor { get; set; }

        /// <summary>
        /// Lookup table for user assigned labels.
        /// </summary>
        public Dictionary<string, string> GlobalLabels { get; set; }

        /// <summary>
        /// The hierarchy level name lookup
        /// </summary>
        public Dictionary<int, string> LevelLabels { get; set; }

        /// <summary>
        /// This is the company specific FCR interface items
        /// </summary>
        public FCRItem[] CompanyFcrItems { get; set; }

        /// <summary>
        /// Definitions for the company product company ranges
        /// </summary>
        public ColorRange[] CompanyColorRanges { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        /// Creates a new manifest object
        /// </summary>
        public Manifest()
        {
            UserInfo = new UserInfo();
            PayloadToken = null;
            Result = "ok";
            LevelLabels = new Dictionary<int, string>();
            GlobalLabels = new Dictionary<string, string>();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Returns the unsubmitted FCR for the supplied team member
        /// </summary>
        /// <param name="teamMemberId"></param>
        /// <returns></returns>
        public FCR GetUnsubmittedFcr(int teamMemberId)
        {
            var teamMember = UserInfo.MyTeam.FirstOrDefault(f => f.Id == teamMemberId);
            if (teamMember == null) return null;
            return teamMember.FCRList == null
                ? null
                : teamMember.FCRList.FirstOrDefault(f => f.ServerStatus == FCR.Status.UnSubmitted);
        }

        /// <summary>
        /// "Submits" the FCR; which just sets the ID to Submitted.
        /// </summary>
        public void Submit(int teamMemberId)
        {
            var fcr = GetUnsubmittedFcr(teamMemberId);
            if (fcr == null) return;
            //fcr.Id = Submitted;
            fcr.ServerStatus = FCR.Status.Submitted;
        }

        /// <summary>
        /// Adds a new FCR for the given team member; creates a new unsubmitted FCR is one isnt found
        /// </summary>
        /// <param name="teamMemberId"></param>
        /// <returns></returns>
        public FCR AddFcr(int teamMemberId)
        {
            var teamMember = UserInfo.MyTeam.FirstOrDefault(f => f.Id == teamMemberId);
            if (teamMember == null) return null;
            if (teamMember.FCRList == null)
            {
                teamMember.FCRList = new List<FCR>();
            }

            var fcr = teamMember.FCRList.FirstOrDefault(f => f.ServerStatus == FCR.Status.UnSubmitted);
            // if the unsubmitted FCR exists, and has any custom items that have been edited, return that FCR
            if (fcr != null)
            {
                bool anyItemEdited = false;
                foreach (FCRItem item in fcr.ItemList)
                {
                    if (item.ItemValue != "")
                    {
                        anyItemEdited = true;
                        break;
                    }
                }
                if (anyItemEdited)
                    return fcr;
            }
            // if no dynamic items have been edited, then regenerate the dynamic list portion of the FCR so that it is current with the most recent definition

            List<FCRItem> fcrItems = new List<FCRItem>();

            if (CompanyFcrItems != null && CompanyFcrItems.Length > 0)
            {
#if UNITY_IPHONE
				foreach (FCRItem item in CompanyFcrItems)
					fcrItems.Add (item.DeepCopy());
#else
                fcrItems = new List<FCRItem>(Utils.DeepCopy(CompanyFcrItems));
#endif
            }
            if (fcr != null)
            {
                // this is the case where a new current items list has just been generated for an existing unsubmitted FCR that might have been out of date
                fcr.ItemList = fcrItems;
                return fcr;
            }

            fcr = new FCR
            {
                ServerStatus = FCR.Status.UnSubmitted,
                CoachingSkills = new List<CoachingSkill>(),
                DevelopmentList = new List<Development>(),
                GoalList = new List<GoalForFieldRide>(),
                SalesDataList = new List<SalesData>(),
                CreationDate = DateTime.Now,
                InCompliance = false,
                ItemList = fcrItems
            };

            teamMember.FCRList.Insert(0, fcr);

            return fcr;
        }

        /// <summary>
        /// Adds a new coaching skill to the FCR.
        /// </summary>
        /// <param name="teamMemberId"></param>
        /// <param name="skill"></param>
        public FCR AddFcrCoachingSkill(int teamMemberId, CoachingSkill skill)
        {
            var fcr = AddFcr(teamMemberId);
            var oldSkill = fcr.CoachingSkills.FirstOrDefault(f => f.CategoryID == skill.CategoryID);
            if (oldSkill != null)
            {
                fcr.CoachingSkills.Remove(oldSkill);
            }

            fcr.CoachingSkills.Add(skill);

            return fcr;
        }

        /// <summary>
        /// Returns an coaching skill for the given FCR
        /// </summary>
        /// <param name="teamMemberId"></param>
        /// <param name="categoryId"></param>
        public CoachingSkill GetFcrCoachingSkill(int teamMemberId, int categoryId)
        {
            var teamMember = UserInfo.MyTeam.FirstOrDefault(f => f.Id == teamMemberId);
            if (teamMember == null) return null;
            if (teamMember.FCRList == null) return null;
            var fcr = teamMember.FCRList.FirstOrDefault(f => f.ServerStatus == FCR.Status.UnSubmitted);
            return fcr == null ? null : fcr.CoachingSkills.FirstOrDefault(f => f.CategoryID == categoryId);
        }

        /// <summary>
        /// Returns the aggregate stats if available
        /// </summary>
        /// <returns></returns>
        private Dictionary<int, SellingSkillInfo> GetAggregateStats()
        {
            var result = new Dictionary<int, SellingSkillInfo>();
            var stats = UserInfo.PreGeneratedStats;

            foreach (var s in stats)
            {

                var trends = new[]
                {
                    new
                    {
                        Date = s.Q1Date,
                        Trend = new SellingSkillRatingTrend
                        {
                            AverageRating = (float) s.Q1Ave,
                            Quarter = 1
                        }
                    },
                    new
                    {
                        Date = s.Q2Date,
                        Trend = new SellingSkillRatingTrend
                        {
                            AverageRating = (float) s.Q2Ave,
                            Quarter = 2
                        }
                    },
                    new
                    {
                        Date = s.Q3Date,
                        Trend = new SellingSkillRatingTrend
                        {
                            AverageRating = (float) s.Q3Ave,
                            Quarter = 3
                        }
                    },
                    new
                    {
                        Date = s.Q4Date,
                        Trend = new SellingSkillRatingTrend
                        {
                            AverageRating = (float) s.Q4Ave,
                            Quarter = 4
                        }
                    }
                };

                var sellingInfo = new SellingSkillInfo
                {
                    CoachedPastSixMonths = s.SixMonthFcrCount,
                    AverageRating = (float) s.AveSkillRating,
                    Trends = trends.OrderBy(m => m.Date).Select(t => t.Trend).ToArray()
                };

                result[s.CoachingSkillId] = sellingInfo;
            }

            return result;
        }

        /// <summary>
        /// Returns the skill rating information for the supplied team member; if no parameter
        /// is supplied then the aggregated skill rating info is returned for ALL team members.
        /// </summary>
        /// <param name="teamMemberId"></param>
        /// <returns></returns>
        public Dictionary<int, SellingSkillInfo> GetSkillRatings(int teamMemberId = -1)
        {
            var result = new Dictionary<int, SellingSkillInfo>();

            //Check for any aggregate stats
            if (teamMemberId == -1)
            {
                if ((UserInfo.LevelNumber < MaxLevels - 1) && (UserInfo.PreGeneratedStats != null))
                {
                    return GetAggregateStats() ?? result;
                }
            }

            var ratings = (teamMemberId != -1) ? GetSkillRatingsByTeamMember(teamMemberId) : GetSkillRatingsForTeam();
            var quarters = Utils.GetQuarterWindow(DateTime.Now);
            var skillIds = ratings.Select(s => s.SkillId).Distinct().ToArray();

            foreach (var skillId in skillIds)
            {
                var sixMonthsPast = DateTime.Now.AddDays(-180);
                var pastSixMonths =
                    ratings.Where(d => d.CreationDate >= sixMonthsPast && d.SkillId == skillId).ToArray();
                var sellingSkills = ratings.Where(d => d.SkillId == skillId).ToArray();

                // ReSharper disable once CollectionNeverQueried.Local
                // ReSharper disable once UseObjectOrCollectionInitializer
                var sellingInfo = new SellingSkillInfo
                {
                    CoachedPastSixMonths = pastSixMonths.Length
                };
                sellingInfo.AverageRating = 0;

                foreach (var skill in sellingSkills)
                {
                    sellingInfo.AverageRating += skill.Rating;
                }

                if (sellingSkills.Length > 0)
                {
                    sellingInfo.AverageRating /= sellingSkills.Length;
                }

                var trends = new List<SellingSkillRatingTrend>();
                foreach (var t in quarters)
                {
                    var sId = skillId;
                    var subRatings =
                        ratings.Where(
                            d =>
                                d.CreationDate >= t.Dates[0] && d.CreationDate <= t.Dates[1] &&
                                d.SkillId == sId).ToArray();

                    var trend = new SellingSkillRatingTrend
                    {
                        Quarter = t.Qtr,
                        AverageRating = 0F
                    };

                    foreach (var s in subRatings)
                    {
                        trend.AverageRating += s.Rating;
                    }

                    if (subRatings.Length > 0)
                    {
                        trend.AverageRating /= subRatings.Length;
                    }

                    trends.Add(trend);
                }

                sellingInfo.Trends = trends.ToArray();

                result[skillId] = sellingInfo;
            }

            return result;
        }

        /// <summary>
        /// Returns the 0-8 most recent skill rating information for each skill for the supplied team member; 
        /// </summary>
        /// <param name="teamMemberId"></param>
        /// <returns></returns>
        public Dictionary<int, SellingSkillInfo> GetSkillRatingHistory(int teamMemberId)
        {
            var ratings = GetSkillRatingsByTeamMember(teamMemberId);
            var skillIds = ratings.Select(s => s.SkillId).Distinct().ToArray();
            var result = new Dictionary<int, SellingSkillInfo>();

            foreach (var skillId in skillIds)
            {
                var sixMonthsPast = DateTime.Now.AddDays(-180);
                var pastSixMonths =
                    ratings.Where(d => d.CreationDate >= sixMonthsPast && d.SkillId == skillId).ToArray();
                var sellingSkills = ratings.Where(d => d.SkillId == skillId).ToArray();

                // ReSharper disable once CollectionNeverQueried.Local
                // ReSharper disable once UseObjectOrCollectionInitializer
                var sellingInfo = new SellingSkillInfo
                {
                    CoachedPastSixMonths = pastSixMonths.Length
                };
                sellingInfo.AverageRating = 0;

                foreach (var skill in sellingSkills)
                {
                    sellingInfo.AverageRating += skill.Rating;
                }

                if (sellingSkills.Length > 0)
                {
                    sellingInfo.AverageRating /= sellingSkills.Length;
                }

                // for this method, return a skill rating trend element for each of the (up to 8) most recent sub ratings
                var trends = new List<SellingSkillRatingTrend>();
                var sId = skillId;
                var subRatings =
                    ratings.Where(
                        d =>
                                d.SkillId == sId).OrderByDescending(d => d.CreationDate).ToArray();

                // build the list, earliest rating first
                for (var r = 7; r >= 0; r--)
                {
                    if (r < subRatings.Length)
                    {
                        var trend = new SellingSkillRatingTrend
                        {
                            Quarter = r + 1,
                            AverageRating = 0F
                        };
                        trend.AverageRating = subRatings[r].Rating;

                        trends.Add(trend);
                    }
                }

                sellingInfo.Trends = trends.ToArray();

                result[skillId] = sellingInfo;
            }

            return result;
        }

        /// <summary>
        /// Creates a lookup for each selling skill that displays composite info for each 
        /// skill for all the team leader's team members.
        /// </summary>
        /// <returns></returns>
        private SubRating[] GetSkillRatingsForTeam()
        {
            var ratings = new List<SubRating>();

            foreach (var teamMember in UserInfo.MyTeam)
            {
                float rating;

                var subRatings = (from fcr in teamMember.FCRList
                    from skill in fcr.CoachingSkills
                    select new SubRating
                    {
                        SkillId = skill.CategoryID,
                        CreationDate = fcr.CreationDate,
                        Rating = float.TryParse(skill.UserRating, out rating) ? rating : 0
                    }).ToArray();

                ratings.AddRange(subRatings);
            }

            return ratings.ToArray();
        }

        /// <summary>
        /// Creates a lookup for each selling skill that displays composite info for each 
        /// skill for the supplied team member id
        /// </summary>
        /// <param name="teamMemberId"></param>
        /// <returns></returns>
        private SubRating[] GetSkillRatingsByTeamMember(int teamMemberId)
        {
            float rating;
            var teamMember = UserInfo.MyTeam.FirstOrDefault(u => u.Id == teamMemberId);
            if (teamMember == null) return new SubRating[0];

            //flatten out all the skill ratings into a single list.
            var ratings = (from fcr in teamMember.FCRList
                from skill in fcr.CoachingSkills
                select new SubRating
                {
                    SkillId = skill.CategoryID,
                    CreationDate = fcr.CreationDate,
                    Rating = float.TryParse(skill.UserRating, out rating) ? rating : 0
                }).ToList();

            return ratings.ToArray();
        }

        /// <summary>
        /// Returns a dictionary of performance overview values for the supplied team member; if 
        /// no team member is supplied then the dictionary contains values for the entire team.
        /// </summary>
        /// <param name="teamMemberId"></param>
        /// <returns></returns>
        public Dictionary<int, TeamMemberPerformanceOverview> GetQtdPerformanceOverview(int? teamMemberId = null)
        {
            var result = new Dictionary<int, TeamMemberPerformanceOverview>();

            try
            {
                var thisQuarter = Utils.GetDateQuarter();
                var maxRatingScales = new Dictionary<int, float>();

                foreach (var s in CoachingCategories)
                {
                    maxRatingScales[s.ID] = 0.0F;
                    var cat = s;
                    foreach (var r in s.Ratings.Where(r => r.EndValue >= maxRatingScales[cat.ID]))
                    {
                        maxRatingScales[s.ID] = r.EndValue;
                    }
                }

                foreach (var member in UserInfo.MyTeam.Where(m => m.Id == (teamMemberId ?? m.Id)))
                {
                    float rating;
                    var thisQuarterFcrs =
                        member.FCRList.Where(f => f.CreationDate >= thisQuarter[0] && f.CreationDate <= thisQuarter[1])
                            .ToArray();
                    var subRatings = (from f in thisQuarterFcrs
                        select new
                        {
                            f.CoachingSkills.Count,
                            PctSum = (from s in f.CoachingSkills
                                let skillId = member.CSID[s.CategoryID]
                                let skillRating = float.TryParse(s.UserRating, out rating) ? rating : 0
                                let pct =
                                (maxRatingScales[skillId] > 0) ? (skillRating/(maxRatingScales[skillId]*1.0F)) : 0F
                                select pct).Sum()
                        }).ToArray();

                    var totalRatingsCount = 0;
                    var totalRatingsPctSum = 0.0F;

                    foreach (var subRating in subRatings)
                    {
                        totalRatingsPctSum += subRating.PctSum;
                        totalRatingsCount += subRating.Count;
                    }

                    //avoid division by zero
                    var snapshot = new TeamMemberPerformanceOverview
                    {
                        NumCoachingSessions = thisQuarterFcrs.ToArray().Length,
                        PerformanceQTD = (totalRatingsCount > 0) ? (totalRatingsPctSum/(totalRatingsCount*1.0F)) : 0
                    };

                    result[member.Id] = snapshot;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
            return result;
        }

        /// <summary>
        /// Removes a sales data item from the FCR
        /// </summary>
        /// <param name="teamMemberId"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public void DeleteSalesDataFromFcr(int teamMemberId, SalesData data)
        {
            var fcr = AddFcr(teamMemberId);
            var oldData = fcr.SalesDataList.FirstOrDefault(d => d.Id == data.Id);
            if (oldData != null)
            {
                fcr.SalesDataList.Remove(oldData);
            }
        }

        /// <summary>
        /// Adds a sales data item to the FCR
        /// </summary>
        /// <param name="teamMemberId"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public SalesData AddSalesDataToFcr(int teamMemberId, SalesData data)
        {
            var fcr = AddFcr(teamMemberId);
            DeleteSalesDataFromFcr(teamMemberId, data);

            data.Id = (int) DateTime.Now.Ticks;
            data.CreationDate = DateTime.Now;
            fcr.SalesDataList.Insert(0, data);

            return data;
        }

        /// <summary>
        /// Adds a goal item to the FCR
        /// </summary>
        /// <param name="teamMemberId"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public void DeleteGoalFromFcr(int teamMemberId, GoalForFieldRide data)
        {
            var fcr = AddFcr(teamMemberId);
            var oldData = fcr.GoalList.FirstOrDefault(d => d.Id == data.Id);
            if (oldData != null)
            {
                fcr.GoalList.Remove(oldData);
            }
        }

        /// <summary>
        /// Adds a goal item to the FCR
        /// </summary>
        /// <param name="teamMemberId"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public GoalForFieldRide AddGoalToFcr(int teamMemberId, GoalForFieldRide data)
        {
            var fcr = AddFcr(teamMemberId);
            DeleteGoalFromFcr(teamMemberId, data);

            data.Id = (int) DateTime.Now.Ticks;
            data.CreationDate = DateTime.Now;
            fcr.GoalList.Insert(0, data);

            return data;
        }

        /// <summary>
        /// Adds a development item to the FCR
        /// </summary>
        /// <param name="teamMemberId"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public void DeleteDevelopmentFromFcr(int teamMemberId, Development data)
        {
            var fcr = AddFcr(teamMemberId);
            var oldData = fcr.DevelopmentList.FirstOrDefault(d => d.Id == data.Id);
            if (oldData != null)
            {
                fcr.DevelopmentList.Remove(oldData);
            }
        }

        /// <summary>
        /// Adds a development item to the FCR
        /// </summary>
        /// <param name="teamMemberId"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public Development AddDevelopmentToFcr(int teamMemberId, Development data)
        {
            var fcr = AddFcr(teamMemberId);
            DeleteDevelopmentFromFcr(teamMemberId, data);

            data.Id = (int) DateTime.Now.Ticks;
            data.CreationDate = DateTime.Now;
            fcr.DevelopmentList.Insert(0, data);

            return data;
        }

        /// <summary>
        /// Returns the number of FCRs for the specified team member.
        /// </summary>
        /// <param name="teamMemberId"></param>
        /// <returns></returns>
        public int GetTeamMemberFcrCount(int teamMemberId)
        {
            var member = UserInfo.MyTeam.FirstOrDefault(m => m.Id == teamMemberId);
            if (member == null) return 0;
            if (member.FCRList == null) return 0;

            return
                member.FCRList.Count(
                    m => m.ServerStatus != FCR.Status.UnSubmitted && m.ServerStatus != FCR.Status.Submitted);
        }

        /// <summary>
        /// Returns the most recent coaching plan for the team member and category id
        /// </summary>
        /// <param name="teamMemberId"></param>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        public CoachingPlan GetMostRecentCoachingPlan(int teamMemberId, int categoryId)
        {
            if (UserInfo.MyTeam == null) return null;

            var teamMember = UserInfo.MyTeam.FirstOrDefault(t => t.Id == teamMemberId);
            if (teamMember == null) return null;

            if (teamMember.FCRList == null) return null;
            var fcrs = teamMember.FCRList.OrderByDescending(f => f.CreationDate).ToArray();
            fcrs = fcrs.Where(f => f.ServerStatus != FCR.Status.UnSubmitted).ToArray();

            return (from fcr in fcrs
                where fcr.CoachingSkills != null
                select fcr.CoachingSkills.FirstOrDefault(s => s.CategoryID == categoryId)
                into skill
                where skill != null
                where skill.CoachingPlanList != null
                select skill.CoachingPlanList.LastOrDefault()).FirstOrDefault(plan => plan != null);

            //the correct FCR was not found
        }

        #endregion
    }
}
