﻿using System;
using System.Collections.Generic;

namespace MCoach
{
	public class FCR
    {
        #region Constructors 

        /// <summary>
        /// The FCR's default constructor
        /// </summary>
	    public FCR()
        {
            Id = -1;
            ServerStatus = Status.NoOp;
	    }

	    #endregion

        #region Members

        /// <summary>
        /// The status of the FCR determines how it is 
        /// handled by the server when it is sent
        /// </summary>
	    public enum Status
	    {
            UnSubmitted = -1,
            Submitted = -2,
            Modified = -3,
            NoOp = -4
	    }

	    #endregion

        #region Properties

        /// <summary>
        /// The database ID of the FCR. Set this to -1 if a new record is to be created
        /// </summary>
        public int Id { get; set; }

		/// <summary>
		/// Coaching skills in FCR
		/// </summary>
		public List<CoachingSkill> CoachingSkills { get; set; }

        /// <summary>
        /// Sales data records
        /// </summary>
		public List<SalesData> SalesDataList { get; set; }
		
        /// <summary>
		/// "Development since last visit" data object.
		/// </summary>
		public List<Development> DevelopmentList { get; set; }

        /// <summary>
        /// Goals for the ride along
        /// </summary>
		public List<GoalForFieldRide> GoalList { get; set; }

		/// <summary>
		/// Dynamically configurable FCR items
		/// </summary>
		public List<FCRItem> ItemList { get; set; }

        /// <summary>
        /// Set to the date the FCR was created
        /// </summary>
	    public DateTime CreationDate { get; set; }

        /// <summary>
        /// Set a filled out email info to the FCR before sending it to the server 
        /// to send an email with the data
        /// </summary>
        public SendEmail EmailInfo { get; set; }

        /// <summary>
        /// Set the field status to instruct the server on how to 
        /// deal with this FCR.
        /// </summary>
        public Status ServerStatus { get; set; }

        /// <summary>
        /// Returns true/false if the FCR is in compliance
        /// </summary>
        public bool InCompliance { get; set; }

        #endregion
    }
}