﻿using Assets.Scripts.Tools;
using UnityEngine;
using UnityEngine.UI;
using MCoach;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Represents a UI instance of Team Member
/// </summary>
public class BreadcrumbInstance : MonoBehaviour
{
    /// <summary>
    /// The UI text object responsible for displaying
    /// the team member's full name.
    /// </summary>

    public Text FullName;
	public Text Title;
	public Image boxOutline;
	private Manifest _myManifest;

    private void Awake()
	{
    }

    /// <summary>
    /// Populates instance data based off of received Manifest data.
    /// </summary>
    /// <param name="teammember"></param>
    public void AddBreadcrumbReference(Manifest manifest,bool isInteractive) // not interactive if the last breadcrumb
    {
		_myManifest = manifest;
		FullName.text = manifest.UserInfo.Name;
		Title.text = manifest.UserInfo.Title;
		if (isInteractive) {
			boxOutline.color = new Color(.85f,.85f,.85f);
		} else {
			boxOutline.color = new Color(.65f,.65f,.65f);
		}
		GetComponent<Button>().interactable = isInteractive;// the last button doesnt do anything.  might do myTeam later ?
    }

	public void OnInstanceClicked()
    {
		// if we're not the last breadcrumb...
		DrilldownController.GetInstance ().DrillOutAs (_myManifest);
	}

}