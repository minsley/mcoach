﻿using Assets.Scripts.Tools;
using UnityEngine;
using UnityEngine.UI;
using MCoach;
using System.Collections;
using System.Collections.Generic;

#if UNITY_IPHONE
using IndieYP;
#endif

/// <summary>
/// <para>Date: 07/17/2015</para>
/// <para>Author: NuMedia</para>
/// <para></para>
/// </summary>
public class CoachingSkillInstance : MonoBehaviour 
{
	public UIEvent CoachingCategorySelected = new UIEvent(Constants.Events.CoachingCategorySelectedEventId.ToString());
	public UIEvent CoachingCategoryTitle = new UIEvent(Constants.Events.CoachingCategoryTitleEventId.ToString());
	public UIEvent CoachingCategoryDescription = new UIEvent(Constants.Events.CoachingCategoryDescriptionEventId.ToString());
	public Text Title;
	public Text Description;
	public Text AverageRatingText;
	public Image AverageRatingImage;
	public Text CoachedPast6Months;
	public Text FirstQ, SecondQ, ThirdQ, FourthQ;
	public Image FirstQImage, SecondQImage, ThirdQImage, FourthQImage;
	public Image EditImage, InformationImage;
	public Sprite EditTexture, InfoTexture, PlusTexture, WarningTexture;
	private CoachingSkillCategory _csCategory;

	public CoachingSkillCategory Category { // need this to update the plus/edit
		get { return _csCategory;}
		set { _csCategory = value;}
	}
	private bool _allowAdd;
	
	private void Awake()
	{
		GlobalMessageManager.AddEvent(CoachingCategorySelected);
		GlobalMessageManager.AddEvent(CoachingCategoryTitle);
		GlobalMessageManager.AddEvent(CoachingCategoryDescription);
	}
	
	public void SetCoachingSkill(CoachingSkillCategory category, bool showPlus, CoachingSkill[] coachingSkills = null)
	{
        Title.text = category.Title;
        Description.text = category.Description;
		//SetAverageRatingText(Mathf.RoundToInt(csCategory.AverageRating));
		if (DrilldownController.GetTopManifest ().UserInfo.LevelNumber != MCoachModel.GetManifest ().MaxLevels-1) {
			EditImage.gameObject.SetActive(false);
		} else {

			if (showPlus) {
				bool showEdit = false;
				if (coachingSkills != null) {
					foreach (CoachingSkill cs in coachingSkills) {
						if (cs.CategoryID == category.ID &&
						    ((cs.CoachingPlanList != null && cs.CoachingPlanList.Count > 0) ||
						 ((cs.FieldRideObservations != null && cs.FieldRideObservations.Length>0)))) {
							showEdit = true;
							break;
						}
					}
				}
				if (showEdit)
					EditImage.sprite = EditTexture;
				else
					EditImage.sprite = PlusTexture;
				EditImage.gameObject.SetActive (true);
			
				if (_allowAdd) // ??  artifact ?
					_allowAdd = true;
			} else
				EditImage.gameObject.SetActive (false);
		}

		InformationImage.sprite = InfoTexture;
		if (DrilldownController.GetTopManifest ().UserInfo.LevelNumber == MCoachModel.GetManifest ().MaxLevels)
			InformationImage.gameObject.SetActive(false); // hide access to coaching tips for rep level
		else
			InformationImage.gameObject.SetActive(true); // allow viewing coaching tips for higher levels

		_csCategory = category;
		//csCategory.Ratings
	}

	public void DisplayRatings( SellingSkillInfo info, bool barGraph ){

		if (info == null) {
			transform.Find ("BarGraph").gameObject.SetActive (false);
			transform.Find ("LineGraph").gameObject.SetActive (false);
			return;
		}


		if (barGraph) {
			transform.Find ("BarGraph").gameObject.SetActive (true);
			transform.Find ("LineGraph").gameObject.SetActive (false);
			SetRatingDisplay (AverageRatingText, info.AverageRating.ToString ("F1"), AverageRatingImage, info.AverageRating, false);
			CoachedPast6Months.text = info.CoachedPastSixMonths.ToString ();
			if (info.Trends.Length > 3)
			SetRatingDisplay (FirstQ, "Q" + info.Trends [3].Quarter, FirstQImage, info.Trends [3].AverageRating, true);
			if (info.Trends.Length > 0)
			SetRatingDisplay (SecondQ, "Q" + info.Trends [0].Quarter, SecondQImage, info.Trends [0].AverageRating, true);
			if (info.Trends.Length > 1)
			SetRatingDisplay (ThirdQ, "Q" + info.Trends [1].Quarter, ThirdQImage, info.Trends [1].AverageRating, true);
			if (info.Trends.Length > 2)
			SetRatingDisplay (FourthQ, "Q" + info.Trends [2].Quarter, FourthQImage, info.Trends [2].AverageRating, true);
		} else {
			transform.Find("BarGraph").gameObject.SetActive(false);
			transform.Find("LineGraph").gameObject.SetActive(true);
			SetRatingDisplay (AverageRatingText, info.AverageRating.ToString ("F1"), AverageRatingImage, info.AverageRating, false);
			CoachedPast6Months.text = info.CoachedPastSixMonths.ToString ();
			WMG_Axis_Graph wag = transform.Find("LineGraph").GetComponent<WMG_Axis_Graph>();
			float ratingRange = 10;
			if (_csCategory	 != null)
				ratingRange = _csCategory.Ratings[_csCategory.Ratings.Length-1].EndValue; // Rating documentation says this should always be 10
			wag.yAxisMaxValue = ratingRange;

			WMG_Series series = transform.Find ("LineGraph/Series/Series1").GetComponent<WMG_Series>();

			// CJR - Set line & data color based on average rating			
			if (info.AverageRating < 0.4f*ratingRange) {
				series.pointColor = new Color (0.898f, 0.047f, 0.102f, 1.0f);
				series.lineColor  = new Color (0.898f, 0.047f, 0.102f, 1.0f);
			} else 
			if (info.AverageRating < 0.7f*ratingRange) {
				series.pointColor = new Color (0.862f, 0.815f, 0.0f, 1.0f);
				series.lineColor  = new Color (0.862f, 0.815f, 0.0f, 1.0f);
			} else {
				series.pointColor = new Color (0.192f, 0.690f, 0.129f, 1.0f);
				series.lineColor  = new Color (0.192f, 0.690f, 0.129f, 1.0f);
			}	
						
			List<Vector2> points = new List<Vector2>();
			for (int i = 0; i< info.Trends.Length; i++){
				points.Add ( new Vector2(i+1,info.Trends[i].AverageRating));
			}
			series.pointValues = points;
		}
	}
	
	public void SetRatingDisplay(Text textComponent, string text,  Image imageComponent, float rating, bool SetFillAmount)
	{
		// sets the text, color, and possibly fill amount, of a text and image pair (pass null if only one of them)
		if (imageComponent != null) {
			// sets color based on current 1-10 scale. We will need to ultimately generalize this function to include a "rating scale with min/max
			float ratingRange = 10;
			if (_csCategory	 != null)
				ratingRange = _csCategory.Ratings[_csCategory.Ratings.Length-1].EndValue; // Rating documentation says this should always be 10
			
			if (rating < 0.4f*ratingRange) {
				imageComponent.color = new Color (0.898f, 0.047f, 0.102f, 1.0f);
			} else 
			if (rating < 0.7f*ratingRange) {
				imageComponent.color = new Color (0.862f, 0.815f, 0.0f, 1.0f);
			} else {
		        imageComponent.color = new Color (0.192f, 0.690f, 0.129f, 1.0f);
			}	
			if (SetFillAmount)
				imageComponent.fillAmount = rating/ratingRange;
		}
		if (textComponent != null) {
			textComponent.text = text;
		}
	}

    public void OnInfoClicked()
    {
        if (_csCategory.InfoFiles == null) return;
        if (_csCategory.InfoFiles.Length <= 0) return;

        #if UNITY_IPHONE
		PDFReader.OpenHTMLLocal(_csCategory.InfoFiles[0], _csCategory.Title);
        #endif
    }

    public void OnAddClicked()
	{
		print("Coaching skill clicked!");
		object csRef = _csCategory;
		object csTitle = _csCategory.Title;
		object csDescription = _csCategory.Description;
		
		CoachingCategorySelected.FireEvent(ref csRef);
		CoachingCategoryTitle.FireEvent(ref csTitle);
		CoachingCategoryDescription.FireEvent(ref csDescription);
	}
}