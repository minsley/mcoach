﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using MCoach;
using Assets.Scripts.Tools;

// this class carries various elements so they can be populated on button objects for editing those elements,
// such as reminders, coaching plans, etc.  It's used on the reminders view and the FCR view buttons

public class FCRIndexInstance : MonoBehaviour 
{
	public FCR myFCR = null; // populate this as you build the index
	public TeamMember myTeamMember = null;	// used by the reminders popup to select team member for viewing this FCR from reminders
	public CoachingSkillCategory myCoachingSkillCategory = null;
	public CoachingPlan myCoachingPlan = null; // used by the reminders popup to clear reminder setting
	public Development myDevelopment = null; // used when editDevelopment is clicked
	public GoalForFieldRide myGoal = null; // used to edit goalfor field ride
	public TeamMemberFCRView fcrView ; // the component that will show my FCR when clicked. set this in the editor in the template for the button
	public GameObject editingPopup = null; // the appropriate popup for editing this particular object.
	private UIEvent _viewFCR = new UIEvent(Constants.Events.ViewFCREventID.ToString());
	private UIEvent _selectTeamMember = new UIEvent(Constants.Events.TeamMemberSelectedEventId.ToString());
	private UIEvent _selectCoachingCategory = new UIEvent(Constants.Events.CoachingCategorySelectedEventId.ToString());

	public void Awake()
	{
		GlobalMessageManager.AddEvent (_viewFCR);
		GlobalMessageManager.AddEvent (_selectTeamMember);
		GlobalMessageManager.AddEvent (_selectCoachingCategory);
	}

	public void OnClick(){
		if (myTeamMember != null) { // be sure the current team member is known...
			object teamMemberRef = (object)myTeamMember;
			_selectTeamMember.FireEvent(ref teamMemberRef);
		}
		if (myFCR != null && fcrView != null) { // maybe we dont need the fcrView component on this object any more
			object data = (object)myFCR;
			_viewFCR.FireEvent(ref data);
		}
	}

	// click handlers that pass the appropriate object for editing on to the fcrView, which will populate the desired popup for editing
	public void OnEditDevelopment(){
		// find the fields in the popup, initialize them, set the edit flags in the popup, and turn it on...
		if (editingPopup != null) {
			editingPopup.transform.Find ("ActionItemBorder/DevelopmentActionItemInputText").gameObject.GetComponent<InputField>().text = myDevelopment.ActionItem;
			editingPopup.transform.Find ("ActionItemBorder/DevelopmentActionItemInputText").gameObject.GetComponent<InputField>().MoveTextEnd(false);
			editingPopup.transform.Find ("ActionTakenBorder/DevelopmentActionTakenInputText").gameObject.GetComponent<InputField>().text = myDevelopment.ActionTaken;
			editingPopup.transform.Find ("ActionTakenBorder/DevelopmentActionTakenInputText").gameObject.GetComponent<InputField>().MoveTextEnd(false);
			editingPopup.transform.Find ("ResultsSeenBorder/DevelopmentResultsSeenInputText").gameObject.GetComponent<InputField>().text = myDevelopment.ResultsSeen;
			editingPopup.transform.Find ("ResultsSeenBorder/DevelopmentResultsSeenInputText").gameObject.GetComponent<InputField>().MoveTextEnd(false);
			TeamMemberFCRView.editingDevelopment = myDevelopment; // set this so Save method knows we are editing, not creating new.
			TeamMemberFCRView.editingFCR = myFCR;
			editingPopup.SetActive(true);
		}

	}

	public void OnEditGoal(){
		// find the fields in the popup, initialize them, set the edit flags in the popup, and turn it on...
		if (editingPopup != null) {
			editingPopup.transform.Find ("GoalBorder/GoalInputText").gameObject.GetComponent<NuInputField>().text = myGoal.ActionItem;
			editingPopup.transform.Find ("GoalBorder/GoalInputText").gameObject.GetComponent<NuInputField>().MoveTextEnd(false);
			editingPopup.SetActive(true);
			TeamMemberFCRView.editingGoal = myGoal; // set this so Save method knows we are editing, not creating new.
		}
	}

	public void OnEditCoachingSkill(){
		// for this one, we have to change to the skills page...
		// use events ?
		object refCoachingSkillCategory = (object)myCoachingSkillCategory;
		_selectCoachingCategory.FireEvent (ref refCoachingSkillCategory);
	}
}