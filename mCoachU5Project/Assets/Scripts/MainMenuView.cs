﻿using Assets.Scripts.Tools;
using UnityEngine;
using UnityEngine.UI;
using MCoach;

public class MainMenuView : MonoBehaviour
{
	// this class really acts as a controller of the set of dashboard buttons, making them
	// behave as radiobuttons, with the ability to diable certain buttons.  in it's current 
	// implementation, it has specific knowledge of what the buttons are, since it has handlers
	// for specific clicks rather than a general click that passes in the button object.
	// the buttons have a slot for a disabled event, but no enabled event, and none are used yet.
	// if a specific button were disabled, then clicking it should not set the menu to all normal
	// state, deselecting the current selected button...  Disabled event would need a bool parameter
	// 
	// I'm going to support diaabled by animating to the disabled state and turning off interactable
	// on the button object itself
	//
	// in future, this list of buttons may need to be build dynamically, so this should be refactored
	// to allow building the list from generic components and handling it that way.
	//
	// Unity's build in button component already supports all the animation capabilities in a known supported
	// package, so really should just make use of those capabilities.
	//

    public DashboardButton[] Buttons;
    private UIEvent _profileButtonClick, _dashboardButtonClick, _fcrButtonClick, _DISCButtonClick,
        _competencyModelButton, _coachingGuideButton, _pulseButton, _trainerResourcesButton,
        _logoutButton, _normalMenuState;

    private void Awake()
    {
        GlobalMessageManager.AddListener(Constants.Events.SelectDashboardEventId.ToString(), OnSelectDashboard);
        GlobalMessageManager.AddListener(Constants.Events.ColorSchemeEventId.ToString(), OnNewColorScheme);

        _profileButtonClick = new UIEvent(Constants.Events.ProfileButtonClickEventId.ToString());
        GlobalMessageManager.AddEvent(_profileButtonClick);

        _dashboardButtonClick = new UIEvent(Constants.Events.DashboardButtonClickEventId.ToString());
        GlobalMessageManager.AddEvent(_dashboardButtonClick);

        _fcrButtonClick = new UIEvent(Constants.Events.FCRButtonClickEventId.ToString());
        GlobalMessageManager.AddEvent(_fcrButtonClick);

        _DISCButtonClick = new UIEvent(Constants.Events.DISCButtonClickEventId.ToString());
        GlobalMessageManager.AddEvent(_DISCButtonClick);

        _competencyModelButton = new UIEvent(Constants.Events.CompetencyButtonClickEventId.ToString());
        GlobalMessageManager.AddEvent(_competencyModelButton);

        _coachingGuideButton = new UIEvent(Constants.Events.CoachingGuideButtonClickEventId.ToString());
        GlobalMessageManager.AddEvent(_coachingGuideButton);
        
        _pulseButton = new UIEvent(Constants.Events.PulseButtonClickEventId.ToString());
        GlobalMessageManager.AddEvent(_pulseButton);

        _trainerResourcesButton = new UIEvent(Constants.Events.TrainingResourcesButtonClickEventId.ToString());
        GlobalMessageManager.AddEvent(_trainerResourcesButton);
        
        _logoutButton = new UIEvent(Constants.Events.LogoutButtonClickEventId.ToString());
        GlobalMessageManager.AddEvent(_logoutButton);
        
        _normalMenuState = new UIEvent(Constants.Events.NormalMenuStateEventId.ToString());
        GlobalMessageManager.AddEvent(_normalMenuState);
    }

    public void OnProfileButton()
    {
        _normalMenuState.FireEvent();
        _profileButtonClick.FireEvent();
    }

    public void OnDashboardButton()
    {
        _normalMenuState.FireEvent();
        _dashboardButtonClick.FireEvent();
    }

    public void OnFCRButton()
    {
        _normalMenuState.FireEvent();
        _fcrButtonClick.FireEvent();
    }

    public void OnDISCButton()
    {
        _normalMenuState.FireEvent();
        _DISCButtonClick.FireEvent();
    }

    public void OnCompetencyModelButton()
    {
        _normalMenuState.FireEvent();
        _competencyModelButton.FireEvent();
    }

    public void OnCoachingGuideButton()
    {
        _normalMenuState.FireEvent();
        _coachingGuideButton.FireEvent();
    }

    public void OnPulseButton()
    {
        _normalMenuState.FireEvent();
        _pulseButton.FireEvent();
    }
    
    public void OnTrainerResourcesButton()
    {
        _normalMenuState.FireEvent();
        _trainerResourcesButton.FireEvent();
    }

    public void OnLogoutButton()
    {
        _normalMenuState.FireEvent();
        _logoutButton.FireEvent();
    }
    
    private void OnSelectDashboard(string EventName, ref object Data)
    {
        OnDashboardButton();
    }
    
    private void OnNewColorScheme(string EventName, ref object Data)
    {
		// apply the first value as button selected background, 
		// currently not using the second value
		string[] values = (string[])Data;

		if (values != null && values.Length > 0) {
			Color selectedColor = hexToColor (values [0]);

//        Color[] scheme = (Color[])Data;
        
			for (int buttonIndex = 0; buttonIndex < Buttons.Length; buttonIndex++) {
//            Buttons[buttonIndex].Normal = scheme[0];
//            Buttons[buttonIndex].Normal2 = scheme[1];
				Buttons [buttonIndex].Selected = selectedColor;
//            Buttons[buttonIndex].Selected2 = scheme[4];
//            Buttons[buttonIndex].Disabled = scheme[5];
//            Buttons[buttonIndex].Disabled2 = scheme[6];
			}
		}
    }

	public static Color hexToColor(string hex)
	{
		hex = hex.Replace ("0x", "");//in case the string is formatted 0xFFFFFF
		hex = hex.Replace ("#", "");//in case the string is formatted #FFFFFF
		byte a = 255;//assume fully visible unless specified in hex
		byte r = byte.Parse(hex.Substring(0,2), System.Globalization.NumberStyles.HexNumber);
		byte g = byte.Parse(hex.Substring(2,2), System.Globalization.NumberStyles.HexNumber);
		byte b = byte.Parse(hex.Substring(4,2), System.Globalization.NumberStyles.HexNumber);
		//Only use alpha if the string has enough characters
		if(hex.Length == 8){
			a = byte.Parse(hex.Substring(4,2), System.Globalization.NumberStyles.HexNumber);
		}
		return new Color32(r,g,b,a);
	}
}