﻿using Assets.Scripts.Tools;
using UnityEngine;
using UnityEngine.UI;
using MCoach;

public class SendPassword : MonoBehaviour 
{
     public Text EnterEmail;
	 public UIEvent SendPasswordEvent = new UIEvent(Constants.Events.LoginSendPasswordEventId.ToString());

	 public void Awake()
	 {
		 GlobalMessageManager.AddEvent(SendPasswordEvent);
	 }

	 public void FireEvent()
	 {
		 Debug.Log("Send Password Fire Event");
		 
		 if (!EnterEmail.text.Equals(string.Empty))
		 {
			 object textRef = EnterEmail.text;
			 SendPasswordEvent.FireEvent(ref textRef);
		 }
	 }
	 
	 void OnDestroy()
	 {
		 GlobalMessageManager.RemoveEvent(SendPasswordEvent);
	 }
}