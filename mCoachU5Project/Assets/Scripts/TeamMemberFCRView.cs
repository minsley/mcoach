﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Tools;
using MCoach;

public class TeamMemberFCRView : MonoBehaviour
{
	/* This class presents an FCR of the currently selected team member.
	 * 
	 */

	public bool FireTest;
	public bool Vertical;

	private RectTransform _scrollRectTransform;
	private RectTransform _containerRectTransform;
	private RectTransform _rowRectTransform;
	private List<GameObject> _spawnedItems = new List<GameObject>();
	private List<GameObject> _spawnedIndexItems = new List<GameObject>();
	private List<FCRItemInstance> _currentItemInstances = new List<FCRItemInstance>();
	public GameObject teamMemberListPanel; // deactivate this when we are active.
	public GameObject FCRPanel; // top level panel for FCR view and index
	public GameObject FCRSubmitButton; // show only when displaying an unsibmitted FCR
	public bool bEmailFCR = true;
	public GameObject FCREmailButton; // make this like a toggle
	public GameObject FCREmailCheckBox;
	public GameObject FCREmailCheckmark;
	public GameObject FCREmailTextNormal;
	public GameObject FCREmailTextPrevious;
	public GameObject FCREmailInProgress;
	public GameObject FCRItemDisplay; // activate when viewing an individual FCR
	public GameObject FCRIndexDisplay; // activate when viewing an index of FCRs
	public GameObject FCRIndexContainer;
	public GameObject FCRIndexTemplate;
	public GameObject FCRIndexItemCompetenciesContainer; // we need the one in the spawned clone so may need to search for this after spawning
	public GameObject FCRIndexItemCompetenciesTemplate;
	public static FCR editingFCR = null;
	public GameObject FCRItemTextTemplate = null;
	public GameObject FCRItemCheckboxTemplate = null;
	public GameObject FCRItemDropdownTemplate = null;
	public static FCRItemInstance editingFCRItem = null; // set by FCRItem edit button handler
	public GameObject salesDataHeader;
	public GameObject salesDataTemplate;
	public static SalesData editingSalesData = null;
	public GameObject developmentHeader;
	public GameObject developmentTemplate;
	public GameObject complianceHeader = null;
	public static Development editingDevelopment = null;
	public GameObject goalHeader;
	public GameObject goalTemplate;
	public static GoalForFieldRide editingGoal = null;
	public GameObject coachingHeader;
	public GameObject coachingTemplate;
//	public GameObject syncButton;
//	public GameObject syncInProgressIcon;
//	private bool performingSync = false;
	public float ySpacer = 15;
	private float currentY = 0;
	private bool _initialized = false;
	private float _width;
	private float _ratio;
	private float _height = 0;
	private float _startingHeight;
	/// <summary>
	/// The currently selected team member
	/// </summary>
	private TeamMember _csTeamMember = null;
	private FCR _csFCR = null;
	private Manifest _lastManifestReceived = null;
	private UIEvent _pushManifest = new UIEvent(Constants.Events.SyncManifestEventId.ToString());
//	private UIEvent _pingWebServices = new UIEvent(Constants.Events.PingWebServicesEventId.ToString());
	private UIEvent _nativeUICall = new UIEvent(Constants.Events.NativePopupEventId.ToString());
	private bool _processingSubmitFCR;


	public void Awake()
	{
		_processingSubmitFCR = false; 

		if (_initialized) return;
		_initialized = true;

        GlobalMessageManager.AddListener(Constants.Events.ViewFCREventID.ToString(), OnViewFCR);
		GlobalMessageManager.AddListener(Constants.Events.ViewFCRIndexEventID.ToString(), OnViewFCRIndex);
		GlobalMessageManager.AddListener(Constants.Events.ManifestUpdatedEventId.ToString(), OnManifestUpdated);
		GlobalMessageManager.AddListener(Constants.Events.TeamMemberSelectedEventId .ToString(), OnTeamMemberSelected);
//		GlobalMessageManager.AddListener(Constants.Events.OnlineStatusChangedEventId.ToString(), OnOnlineStatusChanged);
//		GlobalMessageManager.AddListener (Constants.Events.PingWebServicesResultEventId.ToString (), OnPingWebServicesResult);


		GlobalMessageManager.AddEvent(_pushManifest);
//		GlobalMessageManager.AddEvent(_pingWebServices);
		GlobalMessageManager.AddEvent(_nativeUICall);

		_scrollRectTransform = transform.parent.gameObject.GetComponent<RectTransform>();
//		_rowRectTransform = ItemPrefab.GetComponent<RectTransform>();
		_containerRectTransform = gameObject.GetComponent<RectTransform>();
		// SET THE INITIAL SYNC BUTTON STATE
//		syncButton.SetActive (true);// (WebCallController.isOnline);
	}
	
	void Start()
	{
		// Start is not called on game objects that are inactive
        //calculate the width and height of each child item.
//		_width = _containerRectTransform.rect.width ;
//		_ratio = _width / _rowRectTransform.rect.width;
//		_height = _rowRectTransform.rect.height * _ratio;

	}

	private void OnManifestUpdated(string EventName, ref object Data){
		// this one might work since the manifest needs to be in place before a team member can be selected
		_lastManifestReceived = (Manifest)Data;
	}

	private void OnTeamMemberSelected(string EventName, ref object Data){
		_csTeamMember = (TeamMember)Data;
	}

	public void OnFCREmailButtonClicked(){
		if (bEmailFCR) {
			bEmailFCR=false;
			FCREmailCheckmark.SetActive(false); 
			//could change the highlight color of the button here too
		} else {
			bEmailFCR=true;
			FCREmailCheckmark.SetActive(true);
		}
		ColorBlock cb = FCREmailButton.GetComponent<Button>().colors;
		if (bEmailFCR) {
			cb.normalColor = Color.white;
			cb.highlightedColor = Color.white;
		} else {
			cb.normalColor = new Color(.9f,.9f,.9f);;
			cb.highlightedColor = cb.normalColor;
		}
		FCREmailButton.GetComponent<Button> ().colors = cb;

		if (_csFCR.ServerStatus != FCR.Status.UnSubmitted && bEmailFCR){
			// they have asked to email an existing FCR.
			EmailSubmittedFCR();
			// hide the emailFCR button till next time
			FCREmailInProgress.SetActive(true); // covers the button
		}
	}

	public void EmailSubmittedFCR(){
		if (_lastManifestReceived != null &&
		    _csTeamMember != null &&
		    _csFCR != null) {
			
			FCR toSubmit = _csFCR;
			toSubmit.ServerStatus = FCR.Status.Modified; // flag that this FCR has been modified, and needs to be emailed
			if (bEmailFCR){
				MCoach.SendEmail emailInfo = new MCoach.SendEmail();
				emailInfo.From = _lastManifestReceived.UserInfo.Email;
				emailInfo.To = _csTeamMember.Email;
				emailInfo.Cc = _lastManifestReceived.UserInfo.Email;
				emailInfo.Subject = "[mCoach] FCR for "+_csTeamMember.Name+" submitted on "+toSubmit.CreationDate.ToString ("MM/dd/yyyy");
				emailInfo.Message = "Please view the attached FCR";
				toSubmit.EmailInfo = emailInfo;
			}else{
				toSubmit.EmailInfo = null;
			}
			// now, if we are online, start an upload/download round trip, and when completed, return to FCR Display of this FCR with submitted date, 
			// or for now, just let the existing codepath show the team dashboard...
			
			object manifestObj = (object) _lastManifestReceived;			
			// for now, use the exiting turnouround logic path that occurs on login, which leaves us at the dashboard screen, with possibly a team member selected.
			_pushManifest.FireEvent(ref manifestObj);
		}
	}

	// This is the button handler 
	public void SubmitFCR(){
		//processingSubmitFCR = true;
		if (_processingSubmitFCR) {
			return;
		}
		_processingSubmitFCR = true;

		if (_lastManifestReceived != null &&
		    _csTeamMember != null &&
		    _lastManifestReceived.GetUnsubmittedFcr (_csTeamMember.Id) != null) {
			FCRSubmitButton.SetActive (false);

			FCR toSubmit = _lastManifestReceived.GetUnsubmittedFcr (_csTeamMember.Id);
			toSubmit.CreationDate = System.DateTime.Now;
			if (bEmailFCR) {
				MCoach.SendEmail emailInfo = new MCoach.SendEmail ();
				emailInfo.From = _lastManifestReceived.UserInfo.Email;
				emailInfo.To = _csTeamMember.Email;
				emailInfo.Cc = _lastManifestReceived.UserInfo.Email;
				emailInfo.Subject = "[mCoach] FCR for " + _csTeamMember.Name + " submitted on " + System.DateTime.Now.Date.ToString ("MM/dd/yyyy");
				emailInfo.Message = "Please view the attached FCR";
				toSubmit.EmailInfo = emailInfo;
			} else {
				toSubmit.EmailInfo = null;
			}

			//now go to what...
			object data = (object)_lastManifestReceived.GetUnsubmittedFcr (_csTeamMember.Id);
			
			_lastManifestReceived.Submit (_csTeamMember.Id); // this justmarks the flag as submitted locally
			_lastManifestReceived.AddFcr (_csTeamMember.Id); // add a NEW unsubmitted FCR for this team member.
						
			OnViewFCR ("", ref data); // show the FCR as submitted locally.

			// now, if we are online, start an upload/download round trip, and when completed, return to FCR Display of this FCR with submitted date, 
			// or for now, just let the existing codepath show the team dashboard...

			object manifestObj = (object)_lastManifestReceived;

			// This is a hack to avoid dealing with the deep event tree that follows.
			//TODO there is the possiblilty of error here, as the submit/sync sequence determines online status in a slightly different way
			// so there could be a case where the sync starts when we didnt expect it to.
			// The sync button handler notices that the application has lost connectivity when wifi is lost, whereas the WebCallController
			// and the sync sequence will need to wait for a call to the host to time out to notice that we are offline
			if (SyncButtonHandler.webServiceReachable) {
			
				// We want to disable the user from interacting with the sync button during an FCR submit, therefore creating duplicate FCRs	
				SyncButtonHandler.performingSync = true;

				// Telling sync button we're starting a sync		
				FindObjectOfType<SyncButtonHandler> ().OnSyncButtonUp (); 

				// moving this into the block dependent on SyncButtonHandler.webServiceReachable PAA 10/28/16 defect 852
				// for now, use the exiting turnouround logic path that occurs on login, which leaves us at the dashboard screen, with possibly a team member selected.
				_pushManifest.FireEvent (ref manifestObj);
			}
		} else {
			_processingSubmitFCR = false; 
		}
	}

	public void EditDevelopment(Development development){
		// initialize the popup fields from this development, and

	}

	// refactored to SyncButtonHandler
	/*
	// Putting these handlers here because the object is in the scene so easy to hook up.
	// having the controllers in another scene means they can't be connected to the GUI objects, so
	// you either have to search for them, or use those nasty events.  this is so much simpler.


	public void OnOnlineStatusChanged(string EventName, ref object Data){
		// when do we want this button on ?
		// whenever we are online ?  or only if there are submitted but unsync'd FCR's present ?
		syncButton.SetActive ((bool)Data);

	}


	public void OnSyncButton(){
		// if we are online, and there are any submitted but not synced FCR's enable the sync button...

		// now, if we are online, start an upload/download round trip, and when completed, return to FCR Display of this FCR with submitted date, 
		// or for now, just let the existing codepath show the team dashboard...

		if (true){  // if (WebCallController.isOnline) {

			object manifestObj = (object)_lastManifestReceived;
			
			// for now, use the exiting turnouround logic path that occurs on login, which leaves us at the dashboard screen, with possibly a team member selected.
			_pushManifest.FireEvent (ref manifestObj);

			performingSync = true;
			syncButton.GetComponent<Image> ().color = Color.yellow;

			// send off a WebService Ping request, and warn the user that the sync may not have succeeded if the ping times out.
			object arg = (object) performingSync;
			_pingWebServices.FireEvent(ref arg);
		} else
			syncButton.SetActive (false); // have to wait till heartbeat puts us online again to enable
	}


	// moved this handler to SyncButtonHandler
	private void OnPingWebServicesResult(string EventName, ref object Data){
		// put up a warning, and turn the syncButton back to white
		object uiInfo = new string[] { "Offline", "Cannot perform Sync.", "OK" };
		_nativeUICall.FireEvent(ref uiInfo);
		performingSync = false;
		syncButton.GetComponent<Image> ().color = Color.white;
	}

	private void OnDownloadProgress(string EventName, ref object Data){
		// put up a warning, and turn the syncButton back to white
		float progress = (float)Data;

		if (progress == 0) {
			performingSync = true;
			syncButton.SetActive(false);
			syncInProgressIcon.SetActive(true);
			syncInProgressIcon.GetComponent<Animation>().Play();

		} else if (progress == 1) {
			performingSync = false;
			syncButton.SetActive(true);
			syncInProgressIcon.SetActive(false);
		} else {
			// intermediate progress at this point
		}
	}
	*/

	private void OnViewFCRIndex(string EventName, ref object Data){

		FCR[] theFCRs = (FCR[]) Data;

		//populate and display a list of clickable thumbnails of this current team member's FCR's
		// destroy anything from previous incarnation
		for (int spawnedIndex = 0; spawnedIndex < _spawnedIndexItems.Count; spawnedIndex++)
		{
			Destroy(_spawnedIndexItems[spawnedIndex]);
		}
		
		_spawnedIndexItems.Clear();
		
		FCRPanel.SetActive (true); // toggle on the FCR sub panel
		FCRItemDisplay.SetActive(false); // activate when viewing an individual FCR
		FCRIndexDisplay.SetActive (true);
		teamMemberListPanel.SetActive (false); // and toggle off the List Panel

		FCRIndexTemplate.SetActive(false);
		foreach (FCR fcr in theFCRs){

			// skip unsubmitted FCR if it contains no dat yet
//			if (fcr.ServerStatus == FCR.Status.UnSubmitted &&
//			    ( fcr.CoachingSkills == null || fcr.CoachingSkills.Count == 0) &&
//			    (fcr.DevelopmentList == null || fcr.DevelopmentList.Count == 0) &&
//			    (fcr.GoalList == null || fcr.GoalList.Count == 0) &&
//			    (fcr.SalesDataList == null || fcr.SalesDataList.Count == 0)){
//				continue;
//			}

			GameObject newInstance = Instantiate(FCRIndexTemplate) as GameObject;
			newInstance.transform.SetParent(FCRIndexContainer.transform); // layout should do the rest.
			newInstance.transform.localScale = new Vector3(1,1,1); // compensate for the reparenting scale effect
			newInstance.SetActive (true);
			// set the textvalues.
			string submittedStatusText = "Unsubmitted FCR";
			if (fcr.ServerStatus == FCR.Status.Submitted) submittedStatusText = "Submitted locally on ";
			if (fcr.Id > 0) submittedStatusText = "Submitted on ";
			newInstance.transform.Find("FCRInstanceButtonPanel/FCRSubmittedOnText").GetComponent<Text>().text = submittedStatusText;
			newInstance.transform.Find("FCRInstanceButtonPanel/FCRSubmittedOnText/FCRCreationDateText").GetComponent<Text>().text = fcr.CreationDate.Date.ToString("MM/dd/yyyy");
//			newInstance.transform.Find("FCROtherText").GetComponent<Text>().text = ( cp.Remind ? "Y" : "N" );
			newInstance.GetComponent<FCRIndexInstance>().myFCR = fcr;
			newInstance.GetComponent<FCRIndexInstance>().myTeamMember = _csTeamMember;

			_spawnedIndexItems.Add(newInstance);

			// now build the list of competencies 
			// find the container in the new instance
			Transform CompetencyContainer = newInstance.transform.Find("FCRInstanceButtonPanel/FCRSubmittedOnText/FCRCreationDateText/FCRIndexItemCompetenciesPanel");
			GameObject CompetencyItemTemplate = CompetencyContainer.Find ("CompetencyItemPanelTemplate").gameObject;
			foreach (CoachingSkill skill in fcr.CoachingSkills) {
				GameObject newCompetencyItem = Instantiate(CompetencyItemTemplate) as GameObject;
				newCompetencyItem.transform.SetParent(CompetencyContainer); // layout should do the rest.
				newCompetencyItem.transform.localScale = new Vector3(1,1,1); // compensate for the reparenting scale effect
				newCompetencyItem.SetActive (true);

				// find the name.  does this seem like a friendly way to look it up ?
				CoachingSkillCategory csCategory = null;
				string categoryTitle = "";
				for (int csIndex = 0; csIndex < _csTeamMember.CSID.Length; csIndex++)
				{
					for (int manifestCSIndex = 0; manifestCSIndex < _lastManifestReceived.CoachingCategories.Length; manifestCSIndex++)
					{
						if (skill.CategoryID.Equals(_lastManifestReceived.CoachingCategories[manifestCSIndex].ID))
						{
							csCategory = _lastManifestReceived.CoachingCategories[manifestCSIndex];
							break;
						}
					}
				}
				if (csCategory != null)
					categoryTitle = csCategory.Title;

				newCompetencyItem.transform.Find ("CompetencyNameText").GetComponent<Text> ().text = categoryTitle;
				string dateString = "";
				if (skill.CoachingPlanList.Count > 0) {
					dateString = "Due Date: ";
					int dateCount = 0;
					foreach (CoachingPlan plan in skill.CoachingPlanList) {
						dateString += plan.Date.ToShortDateString ();
						if (++dateCount < 3 && dateCount < skill.CoachingPlanList.Count) {
							dateString += ", ";
						} else {
							break;
						}
					}
				}
				if (skill.CoachingPlanList.Count > 3)
					dateString += " ...";
				newCompetencyItem.transform.Find ("CompetencyDueDateText").GetComponent<Text> ().text = dateString;

			}
			CompetencyItemTemplate.SetActive (false);
		}
	}
	
	private void OnViewFCR(string EventName, ref object Data)
	{
		FCREmailInProgress.SetActive(false); // uncover the button
		_csFCR = (FCR)Data;
		if (_csFCR == null)
			_csFCR = _lastManifestReceived.GetUnsubmittedFcr (_csTeamMember.Id);

		if (_csFCR == null)
			return; // error condition

		if (_csFCR.ServerStatus == FCR.Status.UnSubmitted && _lastManifestReceived.UserInfo.LevelNumber == _lastManifestReceived.MaxLevels-1) {
			UpdateSubmitButtonVisibility();
			FCREmailButton.SetActive (true);
			FCREmailCheckBox.SetActive(true);
			FCREmailTextNormal.SetActive(true);
			FCREmailTextPrevious.SetActive(false);
			ColorBlock cb = FCREmailButton.GetComponent<Button>().colors;
			if (bEmailFCR) {
				cb.normalColor = Color.white;
				cb.highlightedColor = Color.white;
			} else {
				cb.normalColor = new Color(.9f,.9f,.9f);;
				cb.highlightedColor = cb.normalColor;
			}
			FCREmailButton.GetComponent<Button> ().colors = cb;
			FCREmailCheckmark.SetActive(bEmailFCR);
		} else {
			bEmailFCR = (_csFCR.EmailInfo != null); // set initial state to match the FCR, was it emailed already ?
			FCRSubmitButton.SetActive (false);
			if (_lastManifestReceived.UserInfo.LevelNumber == _lastManifestReceived.MaxLevels-1){
				FCREmailButton.SetActive (true); // allow team leads to email previously submitted FCR's
				FCREmailCheckmark.SetActive (false); // don't show the confusing checkmark in this case
				FCREmailCheckBox.SetActive (false); // don't show the confusing checkmark in this case
				FCREmailTextNormal.SetActive(false);
				FCREmailTextPrevious.SetActive(true);
			}
			else
				FCREmailButton.SetActive (false);
		}

		// destroy anything from previous incarnation
		for (int spawnedIndex = 0; spawnedIndex < _spawnedItems.Count; spawnedIndex++)
		{
			Destroy(_spawnedItems[spawnedIndex]);
		}
		
		_spawnedItems.Clear();

		FCRPanel.SetActive (true); // toggle on the FCR sub panel
		FCRItemDisplay.SetActive(true); // activate when viewing an individual FCR
		FCRIndexDisplay.SetActive (false);
		teamMemberListPanel.SetActive (false); // and toggle off the List Panel

		GameObject submittedText = transform.Find ("FCRSubmittedOnText").gameObject;
		string submittedDate = "Unsubmitted FCR";
		if (_csFCR.ServerStatus != FCR.Status.UnSubmitted && _csFCR.CreationDate != null)
			submittedDate = "Submitted on "+_csFCR.CreationDate.ToString ("MM/dd/yyyy");
		if (_csFCR.ServerStatus == FCR.Status.Submitted)
			submittedDate += " (not yet synchronized)";
		submittedText.GetComponent<Text> ().text = submittedDate;//"FCR Submitted on WhateverFCRSays";
		RectTransform strt = submittedText.GetComponent<RectTransform> ();
		currentY = strt.anchoredPosition.y - strt.rect.height/2;
//		PopulateSalesData (); // not used for BBraun
		PopulateDevelopment ();
//		PopulateCompliance ();

		// for testing, put up some fake dynamic elements on the unsubmitted FCR
//		if (_csFCR.ServerStatus == FCR.Status.UnSubmitted && (_csFCR.ItemList == null || _csFCR.ItemList.Count == 0))
//			BuildTestDynamicItems ();

		PopulateDynamicItems ();
		PopulateGoal ();
		PopulateFieldCoaching ();

		UpdateSubmitButtonVisibility();

		// after adding all the items, set the container height to match the contents
		if (currentY < -transform.parent.GetComponent<RectTransform>().rect.height)
			GetComponent<RectTransform> ().SetSizeWithCurrentAnchors (RectTransform.Axis.Vertical, -currentY + 50); // a little extra
		else
			GetComponent<RectTransform>().SetSizeWithCurrentAnchors (RectTransform.Axis.Vertical, transform.parent.GetComponent<RectTransform>().rect.height);

		transform.parent.GetComponent<ScrollRect>().verticalNormalizedPosition = 1;
	}

	public void ResizeViewContainer(float deltaSize){

		float desiredY = transform.GetComponent<RectTransform> ().rect.height + deltaSize;

		if (desiredY > transform.parent.GetComponent<RectTransform>().rect.height)
			GetComponent<RectTransform> ().SetSizeWithCurrentAnchors (RectTransform.Axis.Vertical, desiredY); // was -
		else
			GetComponent<RectTransform>().SetSizeWithCurrentAnchors (RectTransform.Axis.Vertical, transform.parent.GetComponent<RectTransform>().rect.height);

	
	
	}

	// position objects vertically by setting their RectTransform.anchoredPosition.y to the desired center
	// may also have to set offsetMax to 0,topY   offsetMin to 0,botY (y are negative for this scrollable space...

	private void PopulateSalesData(){
		// for each sales data item in the FCR, spawn a table row, set it's content, and add to the hierarchy.
		currentY -= ySpacer; // now at the top of where we want to next block
		RectTransform sdrt = salesDataHeader.GetComponent<RectTransform> ();
		sdrt.anchoredPosition = new Vector2 (0, currentY - sdrt.rect.height / 2);
		currentY -= sdrt.rect.height;

		if (_csFCR == null)
			return;

		GameObject addButton = salesDataHeader.transform.Find("FCRSalesDataTableHeaderBackgroundImage/FCRAddSalesDataButton").gameObject;
		if (_csFCR.ServerStatus == FCR.Status.UnSubmitted &&
		    DrilldownController.GetInstance().Aliases.Count == 0 &&
		    _lastManifestReceived.UserInfo.LevelNumber == _lastManifestReceived.MaxLevels-1){
			addButton.SetActive(true);
		}else{
			addButton.SetActive(false);
		}

		// to test layout, make some copies of the template and add them...
		salesDataTemplate.SetActive (false);
		foreach(SalesData salesData in _csFCR.SalesDataList){
//		for (int i=0; i<5; i++) {
			GameObject newInstance = Instantiate(salesDataTemplate) as GameObject;
			RectTransform nirt = newInstance.GetComponent<RectTransform> ();
			newInstance.transform.SetParent(transform); // this totally messes plays with transform values...
			newInstance.transform.localScale = new Vector3(1,1,1); // compensate for the reparenting scale effect
			nirt.anchoredPosition = new Vector2(0,currentY - nirt.rect.height / 2);
			nirt.offsetMax = new Vector2(0,currentY);
			currentY -= nirt.rect.height;
			nirt.offsetMin = new Vector2(0,currentY);
			newInstance.SetActive(true);
			_spawnedItems.Add(newInstance);

			// here we would find each text field in the new instance and set values from FCR
			newInstance.transform.Find("FCRSalesProductTextField").GetComponent<Text>().text = salesData.ProductName;
			newInstance.transform.Find("FCRSalesQuotaTextField").GetComponent<Text>().text = salesData.Quota.ToString();
			newInstance.transform.Find("FCRSalesQTDTextField").GetComponent<Text>().text = salesData.QTDAchieved.ToString();
			newInstance.transform.Find("FCRSalesYTDTextField").GetComponent<Text>().text = salesData.YTDAchieved.ToString();
		}
	}

	private void PopulateDevelopment(){
		currentY -= ySpacer; // now at the top of where we want to next block
		RectTransform sdrt = developmentHeader.GetComponent<RectTransform> ();
		sdrt.anchoredPosition = new Vector2 (0, currentY - sdrt.rect.height / 2);
		currentY -= sdrt.rect.height;

		GameObject addButton = developmentHeader.transform.Find("FCRDevelopmentTableHeaderBackgroundImage/FCRAddDevelopmentButton").gameObject;
		if (_csFCR.ServerStatus == FCR.Status.UnSubmitted &&  
		    DrilldownController.GetInstance().Aliases.Count == 0 && 
		    _lastManifestReceived.UserInfo.LevelNumber == _lastManifestReceived.MaxLevels-1){
			addButton.SetActive(true);
		}else{
			addButton.SetActive(false);
		}
		
		// to test layout, make some copies of the template and add them...
		developmentTemplate.SetActive (false);
		foreach(Development development in _csFCR.DevelopmentList){
//		for (int i=0; i<5; i++) {
			GameObject newInstance = Instantiate(developmentTemplate) as GameObject;
			RectTransform nirt = newInstance.GetComponent<RectTransform> ();
			newInstance.transform.SetParent(transform); // this totally messes plays with transform values...
			newInstance.transform.localScale = new Vector3(1,1,1); // compensate for the reparenting scale effect
			nirt.anchoredPosition = new Vector2(0,currentY - nirt.rect.height / 2);
			nirt.offsetMax = new Vector2(0,currentY);
			currentY -= nirt.rect.height;
			nirt.offsetMin = new Vector2(0,currentY);
			newInstance.SetActive(true);
			_spawnedItems.Add(newInstance);
			
			// here we would find each text field in the new instance and set values from FCR
			newInstance.transform.Find("FCRDevelopmentActionScrollRect/FCRDevelopmentActionTextField").GetComponent<Text>().text = development.ActionItem;
			newInstance.transform.Find("FCRDevelopmentActionTakenScrollRect/FCRDevelopmentActionTakenTextField").GetComponent<Text>().text = development.ActionTaken;
			newInstance.transform.Find("FCRDevelopmentResultsScrollRect/FCRDevelopmentResultsTextField").GetComponent<Text>().text = development.ResultsSeen;
			FCRIndexInstance ii = newInstance.transform.Find("EditDevelopmentButton").GetComponent<FCRIndexInstance>();
			if (ii != null){
				if 	(_csFCR.ServerStatus == FCR.Status.UnSubmitted){
					ii.gameObject.SetActive(true);
					ii.myDevelopment = development;
					ii.myFCR = _csFCR;
				}else{
					ii.gameObject.SetActive(false);
				}
			}
		}
	}

	private void PopulateCompliance(){
		currentY -= ySpacer; // now at the top of where we want to next block
		RectTransform sdrt = complianceHeader.GetComponent<RectTransform> ();
		sdrt.anchoredPosition = new Vector2 (0, currentY - sdrt.rect.height / 2);
		currentY -= sdrt.rect.height;
		
		Toggle complianceToggle = complianceHeader.transform.Find("FCRComplianceToggle").gameObject.GetComponent<Toggle>();
		complianceToggle.isOn = _csFCR.InCompliance;

		//TODO determine 
		UpdateSubmitButtonVisibility ();

		GameObject complianceDefinitionText = complianceHeader.transform.Find("FCRComplianceDefinitionBackgroundImage").gameObject;
		complianceDefinitionText.SetActive(!(_lastManifestReceived.UserInfo.LevelNumber == _lastManifestReceived.MaxLevels));

	}

	public void UpdateSubmitButtonVisibility(){
		//TODO Clear boolean when true
		bool isVisible = (_csFCR != null &&
			_csFCR.ServerStatus == FCR.Status.UnSubmitted && 
			DrilldownController.GetInstance ().Aliases.Count == 0 && 
			_lastManifestReceived.UserInfo.LevelNumber == _lastManifestReceived.MaxLevels - 1);

		// there may be a compliance toggle, or it may be baked into the dynamic items list...
		/*
		Transform complianceToggleTransform = complianceHeader.transform.Find ("FCRComplianceToggle");
		if (complianceToggleTransform != null) {
			Toggle complianceToggle = complianceToggleTransform.gameObject.GetComponent<Toggle>();
			isVisible &= complianceToggle.isOn;
		}
		*/

		if (isVisible) {
			// and in all the required dynamic elements
			foreach (FCRItemInstance itemInstance in _currentItemInstances){
				if (!itemInstance.CanSubmitFCR){
					isVisible = false;
					break;
				}
			}
		}	
		_processingSubmitFCR = false; 
		FCRSubmitButton.SetActive (isVisible);
	}

	public void OnComplianceToggleChanged(){

		UpdateSubmitButtonVisibility ();
	}

	public void OnFCRItemSave(){
		// called by the save button on the edit FCRItemPopup
		// TeamMemberFCRView.editingFRCItem should contain the instance being edited
		if (editingFCRItem != null)
			editingFCRItem.OnSaveText ();
		editingFCRItem = null; // should not be able to save again
		UpdateSubmitButtonVisibility ();
	}

	private void PopulateGoal(){
		currentY -= ySpacer; // now at the top of where we want to next block
		RectTransform sdrt = goalHeader.GetComponent<RectTransform> ();
		sdrt.anchoredPosition = new Vector2 (0, currentY - sdrt.rect.height / 2);
		currentY -= sdrt.rect.height;
		
		GameObject addButton = goalHeader.transform.Find("FCRGoalTableHeaderBackgroundImage/FCRAddGoalButton").gameObject;
		if (_csFCR.ServerStatus == FCR.Status.UnSubmitted  &&
		    DrilldownController.GetInstance().Aliases.Count == 0 &&
		    _lastManifestReceived.UserInfo.LevelNumber == _lastManifestReceived.MaxLevels-1){
			addButton.SetActive(true);
		}else{
			addButton.SetActive(false);
		}
		
		// to test layout, make some copies of the template and add them...
		goalTemplate.SetActive (false);
		foreach(GoalForFieldRide goal in _csFCR.GoalList){
//		for (int i=0; i<5; i++) {
			GameObject newInstance = Instantiate(goalTemplate) as GameObject;
			RectTransform nirt = newInstance.GetComponent<RectTransform> ();
			newInstance.transform.SetParent(transform); // this totally messes plays with transform values...
			newInstance.transform.localScale = new Vector3(1,1,1); // compensate for the reparenting scale effect
			nirt.anchoredPosition = new Vector2(0,currentY - nirt.rect.height / 2);
			nirt.offsetMax = new Vector2(0,currentY);
			currentY -= nirt.rect.height;
			nirt.offsetMin = new Vector2(0,currentY);
			newInstance.SetActive(true);
			_spawnedItems.Add(newInstance);
			
			// here we would find each text field in the new instance and set values from FCR
			if (goal.ActionItem == null) goal.ActionItem = ""; // this null can cause a null ref when setting up edit goal popup
			newInstance.transform.Find("FCRGoalScrollRect/FCRGoalTextField").GetComponent<Text>().text = goal.ActionItem;
			FCRIndexInstance ii = newInstance.transform.Find("EditGoalButton").GetComponent<FCRIndexInstance>();
			if (ii != null){
				if 	(_csFCR.ServerStatus == FCR.Status.UnSubmitted){
					ii.gameObject.SetActive(true);
					ii.myGoal = goal;
					ii.myFCR = _csFCR;
				}else{
					ii.gameObject.SetActive(false);
				}
			}
		}
	}

	private void BuildTestDynamicItems(){
		// just for development, build 3 dynamic items, one of each type...
//		if (_csFCR.ItemList == null)
			_csFCR.ItemList = new List<FCRItem> ();


		FCRItem textItem = new FCRItem();
		textItem.ItemType = FCRItemType.text;
		textItem.ItemHeader = "FCR Text Item";
		textItem.ItemBody = "This is a sample text item body, a description of what the field is all about";
		textItem.ItemValue = "";
		textItem.Required = true;

		_csFCR.ItemList.Add(textItem);

		FCRItem checkboxItem = new FCRItem();
		checkboxItem.ItemType = FCRItemType.checkbox;
		checkboxItem.ItemHeader = "FCR Checkbox Item";
		checkboxItem.ItemBody = "This is a sample checkbox item body, a description of what the field is all about";
		checkboxItem.Required = true;
		_csFCR.ItemList.Add(checkboxItem);


		FCRItem dropdownItem = new FCRItem();
		dropdownItem.ItemType = FCRItemType.dropdown;
		dropdownItem.ItemHeader = "FCR Dropdown Item";
		dropdownItem.ItemBody = "This is a sample dropdown item body, a description of what the field is all about";
		dropdownItem.DropdownValues = new string[]{
			"I pledge allegiance to the flag of the United States of America, and to the republic for which it stands, one nation under God, indivisible, with lib",
			"I pledge allegiance to the flag of the United States of America, and to the republic for which it stands, one nation under God, indivisible, with liberty and justice for all. 1 I pledge allegiance to the flag of the United States of America, an250",
			"Value 3",
			"Value 4",
			"Value 5",
			"Value 6",
			"Value 7",
			"Value 8",
			"Value 9",
			"Value 10",
			"Value 12",
			"Value 13"};
		dropdownItem.Required = true;

		_csFCR.ItemList.Add(dropdownItem);
	
	}

	private void PopulateDynamicItems(){

		_currentItemInstances.Clear ();

		if (_csFCR.ItemList == null)
			return;

		bool allowEdit = (_csFCR.ServerStatus == FCR.Status.UnSubmitted  &&
		                  DrilldownController.GetInstance().Aliases.Count == 0 &&
		                  _lastManifestReceived.UserInfo.LevelNumber == _lastManifestReceived.MaxLevels-1);

		foreach (FCRItem theItem in _csFCR.ItemList) {

			if (theItem.ItemType == FCRItemType.none) continue;

			currentY -= ySpacer; // now at the top of where we want to next block
			// spawn the appropriate template, let its FCRItemInstance populate the UI fields, and parent it to the FCR Scrollview Content

			if (theItem.ItemHeader == null) theItem.ItemHeader = "";
			if (theItem.ItemBody == null) theItem.ItemBody = "";
			if (theItem.ItemValue == null) theItem.ItemValue = "";

			GameObject newItemPanel = null;
			FCRItemCheckboxTemplate.SetActive (false);
			FCRItemDropdownTemplate.SetActive (false);
			FCRItemTextTemplate.SetActive (false);

			switch(theItem.ItemType){
			case FCRItemType.checkbox:
				newItemPanel = Instantiate (FCRItemCheckboxTemplate) as GameObject;
				break;
			case FCRItemType.dropdown:
				newItemPanel = Instantiate (FCRItemDropdownTemplate) as GameObject;
				break;
			case FCRItemType.text:
				newItemPanel = Instantiate (FCRItemTextTemplate) as GameObject;
				break;
			}

			RectTransform nirt = newItemPanel.GetComponent<RectTransform> ();
			newItemPanel.transform.SetParent (transform); // this totally messes plays with transform values...
			newItemPanel.transform.localScale = new Vector3 (1, 1, 1); // compensate for the reparenting scale effect
			nirt.anchoredPosition = new Vector2 (0, currentY - nirt.rect.height / 2);
			nirt.offsetMax = new Vector2 (0, currentY);
			currentY -= nirt.rect.height;
			nirt.offsetMin = new Vector2 (0, currentY);
			newItemPanel.SetActive (true);
			_spawnedItems.Add (newItemPanel);

			FCRItemInstance newItemInstance = newItemPanel.GetComponent<FCRItemInstance>();
			_currentItemInstances.Add(newItemInstance);
			newItemInstance.PopulateFields(theItem,allowEdit); 

			// update currentY to fit the new item.  this would be better to use Unity content size fitter ?

			/*
			currentY -= ySpacer; // now at the top of where we want to next block
			RectTransform sdrt = goalHeader.GetComponent<RectTransform> ();
			sdrt.anchoredPosition = new Vector2 (0, currentY - sdrt.rect.height / 2);
			currentY -= sdrt.rect.height;
		
			GameObject addButton = goalHeader.transform.Find ("FCRGoalTableHeaderBackgroundImage/FCRAddGoalButton").gameObject;
			if (_csFCR.ServerStatus == FCR.Status.UnSubmitted &&
				DrilldownController.GetInstance ().Aliases.Count == 0 &&
				_lastManifestReceived.UserInfo.LevelNumber == _lastManifestReceived.MaxLevels - 1) {
				addButton.SetActive (true);
			} else {
				addButton.SetActive (false);
			}
		
			goalTemplate.SetActive (false);
			foreach (GoalForFieldRide goal in _csFCR.GoalList) {
				GameObject newInstance = Instantiate (goalTemplate) as GameObject;
				RectTransform nirt = newInstance.GetComponent<RectTransform> ();
				newInstance.transform.SetParent (transform); // this totally messes plays with transform values...
				newInstance.transform.localScale = new Vector3 (1, 1, 1); // compensate for the reparenting scale effect
				nirt.anchoredPosition = new Vector2 (0, currentY - nirt.rect.height / 2);
				nirt.offsetMax = new Vector2 (0, currentY);
				currentY -= nirt.rect.height;
				nirt.offsetMin = new Vector2 (0, currentY);
				newInstance.SetActive (true);
				_spawnedItems.Add (newInstance);
			
				// here we would find each text field in the new instance and set values from FCR
				if (goal.ActionItem == null)
					goal.ActionItem = ""; // this null can cause a null ref when setting up edit goal popup
				newInstance.transform.Find ("FCRGoalTextField").GetComponent<Text> ().text = goal.ActionItem;
				FCRIndexInstance ii = newInstance.transform.Find ("EditGoalButton").GetComponent<FCRIndexInstance> ();
				if (ii != null) {
					if (_csFCR.ServerStatus == FCR.Status.UnSubmitted) {
						ii.gameObject.SetActive (true);
						ii.myGoal = goal;
						ii.myFCR = _csFCR;
					} else {
						ii.gameObject.SetActive (false);
					}
				}
			}
			*/
		}
	}

	private void PopulateFieldCoaching(){
		currentY -= ySpacer; // now at the top of where we want to next block
		RectTransform sdrt = coachingHeader.GetComponent<RectTransform> ();
		sdrt.anchoredPosition = new Vector2 (0, currentY - sdrt.rect.height / 2);
		currentY -= sdrt.rect.height;

//TODO  Note that the skill rating trend is being generated from CURRENT data, not from the time the FCR was submitted, this might not be the desired behavior
		Dictionary<int, SellingSkillInfo> skillRatings = null;
		if (_csTeamMember != null) skillRatings = _lastManifestReceived.GetSkillRatings (_csTeamMember.Id);
		
		// for each skill rated in this fcr, make a copy of the template and add them...
		coachingTemplate.SetActive (false);
		foreach (CoachingSkill coachingSkill in _csFCR.CoachingSkills){
//		for (int i=0; i<5; i++) {
			GameObject newInstance = Instantiate(coachingTemplate) as GameObject;
			RectTransform nirt = newInstance.GetComponent<RectTransform> ();
			newInstance.transform.SetParent(transform); // this totally messes plays with transform values...
			newInstance.transform.localScale = new Vector3(1,1,1); // compensate for the reparenting scale effect
			nirt.anchoredPosition = new Vector2(0,currentY - nirt.rect.height / 2);
			nirt.offsetMax = new Vector2(0,currentY);
			currentY -= nirt.rect.height;
			nirt.offsetMin = new Vector2(0,currentY);
			newInstance.SetActive(true);
			_spawnedItems.Add(newInstance);
			
			// here we would find each text field in the new instance and set values from FCR

			// find the name.  does this seem like a friendly way to look it up ?
			CoachingSkillCategory csCategory = null;
			for (int csIndex = 0; csIndex < _csTeamMember.CSID.Length; csIndex++)
			{
				for (int manifestCSIndex = 0; manifestCSIndex < _lastManifestReceived.CoachingCategories.Length; manifestCSIndex++)
				{
					if (coachingSkill.CategoryID.Equals(_lastManifestReceived.CoachingCategories[manifestCSIndex].ID))
					{
						csCategory = _lastManifestReceived.CoachingCategories[manifestCSIndex];
						break;
					}
				}
			}
			if (csCategory != null)
				newInstance.transform.Find("FCRCoachingSkillTextField").GetComponent<Text>().text = csCategory.Title;

			GameObject editButton = newInstance.transform.Find("FCRCoachingSkillEditButton").gameObject;
			if (_csFCR.ServerStatus == FCR.Status.UnSubmitted  &&
			    DrilldownController.GetInstance().Aliases.Count == 0 &&
			    _lastManifestReceived.UserInfo.LevelNumber == _lastManifestReceived.MaxLevels-1){
				editButton.SetActive(true);
			}else{
				editButton.SetActive(false);
			}

			editButton.GetComponent<FCRIndexInstance>().myCoachingSkillCategory = csCategory;

			if (skillRatings != null && skillRatings.ContainsKey (coachingSkill.CategoryID)){
				SellingSkillInfo skillInfo = skillRatings[coachingSkill.CategoryID];
				// Average Rating Circle
				SetRatingDisplay( newInstance.transform.Find("AverageRatingCircleBackgroundImage/AverageRatingText").GetComponent<Text>(),
				                 skillInfo.AverageRating.ToString ("F1"),
				                 newInstance.transform.Find("AverageRatingCircleBackgroundImage/AverageRatingCircleForegroundImage").GetComponent<Image>(),
				                 skillInfo.AverageRating,
				                 false);
				// Each Quarter Graph, ignoring the text for now   
				SetRatingDisplay( null,
				                 "Q"+skillInfo.Trends[0].Quarter,
				                 newInstance.transform.Find("GraphBackgroundImage/Q1Image").GetComponent<Image>(),
				                 skillInfo.Trends[0].AverageRating,
				                 true);
				SetRatingDisplay( null,
				                 "Q"+skillInfo.Trends[1].Quarter,
				                 newInstance.transform.Find("GraphBackgroundImage/Q2Image").GetComponent<Image>(),
				                 skillInfo.Trends[1].AverageRating,
				                 true);
				SetRatingDisplay( null,
				                 "Q"+skillInfo.Trends[2].Quarter,
				                 newInstance.transform.Find("GraphBackgroundImage/Q3Image").GetComponent<Image>(),
				                 skillInfo.Trends[2].AverageRating,
				                 true);
				SetRatingDisplay( null,
				                 "Q"+skillInfo.Trends[3].Quarter,
				                 newInstance.transform.Find("GraphBackgroundImage/Q4Image").GetComponent<Image>(),
				                 skillInfo.Trends[3].AverageRating,
				                 true);


			}
			// This ride rating
			float rating = 0;
			float.TryParse(coachingSkill.UserRating, out rating);
			SetRatingDisplay( newInstance.transform.Find("CurrentRatingCircleBackgroundImage/CurrentRatingText").GetComponent<Text>(),
			                 coachingSkill.UserRating,
			                 newInstance.transform.Find("CurrentRatingCircleBackgroundImage/CurrentRatingCircleForegroundImage").GetComponent<Image>(),
			                 rating,
			                 false);

			newInstance.transform.Find("FCRCoachingObservationsScrollRect/FCRCoachingObservationsTextField").GetComponent<Text>().text = coachingSkill.FieldRideObservations;
			// here, we really need to populate a list of these
			if (coachingSkill.CoachingPlanList.Count>0){
				float combinedHeight = 0;
				// find the template in the instance just created, instantiate a copy for each list item and populate
				GameObject planScrollRect = newInstance.transform.Find ("FCRCoachingDevelopmentScrollRect").gameObject;
				GameObject planContainer = newInstance.transform.Find ("FCRCoachingDevelopmentScrollRect/FCRCoachingDevelopmentContainer").gameObject;
				GameObject planTemplate = planContainer.transform.Find ("FCRCoachingDevelopmentTemplate").gameObject;
				foreach (CoachingPlan cp in coachingSkill.CoachingPlanList){
					GameObject newCoachingPlanRow = Instantiate(planTemplate) as GameObject; 
					newCoachingPlanRow.transform.SetParent(planContainer.transform); // this totally messes plays with transform values...
					newCoachingPlanRow.transform.localScale = new Vector3(1,1,1); // compensate for the reparenting scale effect
					newCoachingPlanRow.SetActive(true);
					//_spawnedItems.Add(newCoachingPlanRow);//TODO are we asking for trouble mixing spawned items at this level? when our parent is deleted, arent we ?
					newCoachingPlanRow.transform.Find("FCRCoachingDevelopmentTextField").GetComponent<Text>().text = cp.Title;
					newCoachingPlanRow.transform.Find("FCRCoachingDueDateTextField").GetComponent<Text>().text = cp.Date.Date.ToString("MM/dd/yyyy");
					// try to set the preferred height height of the row to the height of the plan text box, which has a size fitter on it
					// the preferred height is not available until the end of the frame, this returns a -1 until then...

					// This doesnt actually work..  Placing a content size fitter on the text element will set it's preferred height to fit the text,
					// and that will drive the layout, but content size fitters seem unable to propagate that size up so an intermediate CoachingPlanView
					// performs that task during it's update method.
					// This actually can work well if the text element is the 'content' of the scrollRect.
					float elementHeight = newCoachingPlanRow.transform.Find("FCRCoachingDevelopmentTextField").GetComponent<LayoutElement>().preferredHeight;
					elementHeight = 24 + newCoachingPlanRow.GetComponent<LayoutElement>().preferredHeight * cp.Title.Length/350.0f;

					newCoachingPlanRow.GetComponent<LayoutElement>().preferredHeight = elementHeight;
					combinedHeight += elementHeight;

				}
				planTemplate.SetActive(false);

				// the SizeFitter component doesn't do well at this level, so we will set the container size ourselves.
				//		TeamMemberContainer.GetComponent<RectTransform> ().SetSizeWithCurrentAnchors (RectTransform.Axis.Horizontal, (_teamMemberInstances.Count+0.5f) * TeamMemberTemplate.GetComponent<RectTransform> ().rect.width);
				
				planContainer.GetComponent<RectTransform> ().anchorMax = new Vector2(1, 0);
				planContainer.GetComponent<RectTransform> ().anchorMin = new Vector2(.0f, 0f);
				float height = (coachingSkill.CoachingPlanList.Count + 0.25f) * planTemplate.GetComponent<RectTransform> ().rect.height;
				height = combinedHeight + 24;
				if (height < planContainer.transform.parent.GetComponent<RectTransform> ().rect.height)
					height = planContainer.transform.parent.GetComponent<RectTransform> ().rect.height;
				planContainer.GetComponent<RectTransform> ().sizeDelta = new Vector2(0,height);
				planContainer.transform.parent.GetComponent<ScrollRect> ().verticalNormalizedPosition = 1;



			}
		}
	}

	public void SetRatingDisplay(Text textComponent, string text,  Image imageComponent, float rating, bool SetFillAmount)
	{
		// duplicate of the method in CoachingSkillInstance
		// sets the text, color, and possibly fill amount, of a text and image pair (pass null if only one of them)
		if (imageComponent != null) {
			imageComponent.color = GetRatingColor(rating);
			if (SetFillAmount)
				imageComponent.fillAmount = rating/10f;
		}
		if (textComponent != null) {
			textComponent.text = text;
		}
	}

	private Color GetRatingColor(float rating){
		Color result      = Color.red;
		float ratingRange = 5.0f; // CJR - Needs to be dynamic from admin portal
		
		// CJR - Set line & data color based on average rating			
		if (rating < 0.4f*ratingRange) {
			result = new Color (0.898f, 0.047f, 0.102f, 1.0f);
		} else 
		if (rating < 0.7f*ratingRange) {
			result = new Color (0.862f, 0.815f, 0.0f, 1.0f);
		} else {
			result = new Color (0.192f, 0.690f, 0.129f, 1.0f);
		}	
				
		/* CJR - Stubbed for BBraun
		if (rating < 5)
		{
			var colorPct = (rating / 5);
			result = new Color(1, colorPct, 0);
		}
		else
		{
			var percentage = 1f - ((10 - rating) / 5);
			result = new Color(1f - percentage, 1, 0);
		}
		*/
				
		return result;
	}
	
	/*
	private void AddInstance(CoachingSkillCategory coachingSkillCategory)
	{
		//  Debug.Log("Coaching Skill Title: " + coachingSkillCategory.Title);
		
		var startingCount = _spawnedItems.Count;
		int rowCount;

        //adjust the height of the container so that it will just barely fit all its children
		float scrollHeight = _height * rowCount;
		Debug.Log(_scrollRectTransform.rect.height + " : " + scrollHeight);
		if (_scrollRectTransform.rect.height < scrollHeight)
		{
			float offset = scrollHeight - _scrollRectTransform.rect.height;
			_containerRectTransform.offsetMin = new Vector2(_containerRectTransform.offsetMin.x, -offset);
             //_containerRectTransform.offsetMax = new Vector2(_containerRectTransform.offsetMin.x, offset);  
		}
		
		int j = 0;//_itemCount - NewInstances;
		for (int i = 0; i < _itemCount; i++)
		{   
            //this is used instead of a double for loop because itemCount may not fit perfectly into the rows/columns
			if (i % _columnCount == 0)
				j++;
			
			GameObject newItem; // = (i < startingCount) ? _spawnedItems[i] : Instantiate(ItemPrefab) as GameObject;
			if (i < startingCount)
			{
				newItem = _spawnedItems[i];
			}
			else
			{
				newItem = Instantiate(ItemPrefab) as GameObject;
				
				var coachingInstance = newItem.GetComponent<CoachingSkillInstance>();
				coachingInstance.SetCoachingSkill(coachingSkillCategory, true);
				SellingSkillInfo info = null;
				if (_csTeamMember != null){
					Dictionary<int, SellingSkillInfo> dict = _lastManifestReceived.GetSkillRatings(_csTeamMember.Id);
					if (dict.ContainsKey(coachingSkillCategory.ID))
						info = dict[coachingSkillCategory.ID];
					else
					    info = TeamMemberInstance.RandomSellingSkillInfo(); //TODO remove this once we have data

					coachingInstance.DisplayRatings(info);
				}
			}
			
            //create a new item, name it, and set the parent
			if (i >= startingCount)
				_spawnedItems.Add(newItem);
			
			newItem.name = gameObject.name + " item at (" + i + "," + j + ")";
			newItem.transform.SetParent(gameObject.transform);
			
            //move and size the new item
			RectTransform rectTransform = newItem.GetComponent<RectTransform>();
			
			float x = -_containerRectTransform.rect.width / 2 + _width * (i % _columnCount);
			float y = _containerRectTransform.rect.height / 2 - _height * j;
			rectTransform.offsetMin = new Vector2(x, y);
			
			x = rectTransform.offsetMin.x + _width;
			y = rectTransform.offsetMin.y + _height;
			rectTransform.offsetMax = new Vector2(x, y);
		}
	}
	*/
}
