﻿using UnityEngine;
using UnityEngine.UI;

public class MakePhoneCall : MonoBehaviour
{

	public Text PhoneText;

    public void OnSendEmail()
    {
        if (PhoneText.text.Equals(string.Empty)) return;
        Application.OpenURL("tel:" + PhoneText.text);
	}
}
