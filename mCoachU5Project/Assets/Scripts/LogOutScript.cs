﻿using UnityEngine;
using System.Collections;

public class LogOutScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void StartLogoutSequence(){
		StartCoroutine ("LogoutCoroutine");
	}

	IEnumerator LogoutCoroutine()
	{
		yield return new WaitForSeconds(.25f); // 
		SpawnMVC.Reset (); // allow a new MVC instance to be spawned
		Application.LoadLevel("2Login");
	}
}
