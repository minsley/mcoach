﻿using System.Text;
using System.IO;
using System.Net;
using Assets.Scripts.Tools;

/// <summary>
/// Class manages a new call to the server and stores the server's response.
/// This was made to be called in a separate thread.
/// </summary>
public class WebCall
{
    /// <summary>
    /// Stores data received from the server.
    /// </summary>
    public volatile string ResponseFromServer = "";
    /// <summary>
    /// Store any error message.
    /// </summary>
    public volatile string ErrorMessage = "";
    /// <summary>
    /// Stores true if the StartCall method has not completed.
    /// </summary>
	public volatile bool ThreadStarted = false;
    public volatile bool ThreadRunning = false;  // handshaking with only this can fail
	public volatile bool ThreadCompleted = false;
    /// <summary>
    /// Address of the server.
    /// </summary>
    private string _site = WebServiceTools.SyncUrl;

    /// <summary>
    /// Token required for validation.
    /// </summary>
    private string _token = WebServiceTools.CallToken;
    /// <summary>
    /// Stores the payload token of zipped file on server.
    /// </summary>
    private string _payloadToken = "";
    /// <summary>
    /// Stores the JSON data which will be pushed to the server.
    /// </summary>
    private string _json = "";
    /// <summary>
    /// Stores true if logging in
    /// </summary>
    private bool _login = false;
    
    public string Site
    {
        get { return _site; }
        set { _site = value; }
    }

    /// <summary>
    /// Initializes class, requires json data or payload token to be pushed to the server.
    /// </summary>
    /// <param name="data">Data to be pushed to the server</param>
    /// <param name="isLogin"></param>
    public WebCall(string data, bool isLogin)
    {
        if (isLogin)
            _json = data;
        else
        {
            _payloadToken = data;
            _site = WebServiceTools.GetPayloadUrl;
        }
        _login = isLogin;
    }

    /// <summary>
    /// Method call to be triggered in thread. Specific for logging in.
    /// </summary>
    public void StartCall()
    {
        StartWebCall(_login);
    }

    /// <summary>
    /// Starts up the web call.
    /// </summary>
    /// <param name="isLogin"></param>
    private void StartWebCall(bool isLogin)
    {
		ThreadStarted = true;
        ThreadRunning = true;
		ThreadCompleted = false;

		#if (!UNITY_IPHONE || UNITY_EDITOR)
		{
			ServicePointManager
			.ServerCertificateValidationCallback += 
				(sender, cert, chain, sslPolicyErrors) => true; //TODO Remove this, added to ignore certificate issues on the server for testing
		}
		#endif

        try
        {
            // Create a request using a URL that can receive a post. 
            WebRequest request = WebRequest.Create(_site);
            // Set the Method property of the request to POST.
            request.Method = "POST";
            // Create POST data and convert it to a byte array.
            string postData = "";

            if (isLogin)
            {
				// this call to the hex encoder turns out to be very expensive, upwards of 35 sec to run with a reasonable manifest size...
				// the encryption may be unnecessary since the json we are sending was generated and saved as a file.
				// sadly, this is not the case, and the encode seems to be necessary here.
                postData = "token=" + _token + "&data=" + "[[ENC_" + MCoach.Utils.FastHexEncode(_json);
				//postData = "token=" + _token + "&data=" + _json;
            }
            else
            {
                postData = "token=" + _token + "&payloadToken=" + _payloadToken; 
            }

            
            byte[] byteArray = Encoding.UTF8.GetBytes(postData);
            // Set the ContentType property of the WebRequest.
            request.ContentType = "application/x-www-form-urlencoded";
            // Set the ContentLength property of the WebRequest.
            request.ContentLength = byteArray.Length;
            // Get the request stream.
            Stream dataStream = request.GetRequestStream();
            // Write the data to the request stream.
            dataStream.Write(byteArray, 0, byteArray.Length);
            // Close the Stream object.
            dataStream.Close();
            // Get the response.
            WebResponse response = request.GetResponse();
            // Get the stream containing content returned by the server.
            dataStream = response.GetResponseStream();
            // Open the stream using a StreamReader for easy access.
            StreamReader reader = new StreamReader(dataStream);
            // Read the content.
            ResponseFromServer = reader.ReadToEnd();
            // Clean up the streams.
            reader.Close();
            dataStream.Close();
            response.Close();
        }
        catch (WebException webex)
        {
            WebResponse errResp = webex.Response;
			if (errResp != null){
            using (Stream respStream = errResp.GetResponseStream())
            {
                StreamReader reader = new StreamReader(respStream);
                string text = reader.ReadToEnd();
                ErrorMessage = text;
            }
			}
			else{
				ErrorMessage = "null response from Server on web call";
			}
        }
        
        ThreadRunning = false;
		ThreadCompleted = true;
    }
}