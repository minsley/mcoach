﻿using UnityEngine;
using UnityEngine.UI;
using Assets.Scripts.Tools;

public class SendEmail : MonoBehaviour
{
    public Text EmailText;
	public bool supressMailTo = false; // if you just want to open the url as written
	public bool prependWebServicesURL = false;

    public void OnSendEmail()
    {
        if (EmailText.text.Equals(string.Empty)) return;
		string url = EmailText.text;
		if (prependWebServicesURL)
			url = "https://"+WebServiceTools.WebServiceDomainName + "/" + url; //SSL change 9/1/16
		if (supressMailTo)
        	Application.OpenURL(url);
		else
			Application.OpenURL("mailto:" + url);
	}
}
