﻿using Assets.Scripts.Tools;
using UnityEngine;
using UnityEngine.UI;
using MCoach;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Represents a UI instance of Team Member
/// </summary>
public class CoachingPlanView : MonoBehaviour
{
    /// <summary>
    /// The UI text object responsible for displaying
    /// the team member's full name.
    /// </summary>

	public CoachingPlan plan = null;
	private UIEvent _editPlan = new UIEvent(Constants.Events.EditCoachingPlanPopUpShowEventId.ToString());
	public bool fitHeightToContent = false;
	public RectTransform childToFit = null;

    void Awake()
	{
    }

	void Start(){
		GlobalMessageManager.AddEvent(_editPlan); //
	}

	public void OnEditClicked()
    {
		object refCoachingPlan = (object)plan;
		_editPlan.FireEvent (ref refCoachingPlan);
	}
	public void OnDeleteClicked(){
		// probably need a confirm dialog, or make put the delete button in the popup ?
	}

	public void Update(){ // the size fitter doesnt perform well in this hierarchy, try overriding
		if (fitHeightToContent && childToFit != null && GetComponent<RectTransform>().rect.height < childToFit.rect.height) {
//			GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical,childToFit.rect.height);
			GetComponent<LayoutElement>().minHeight = childToFit.rect.height;
//			LayoutRebuilder.MarkLayoutForRebuild (transform as RectTransform);
		}
	}

}