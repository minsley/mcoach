﻿using UnityEngine;
using UnityEngine.UI;
using System;

/// <summary>
/// <para>Date: 07/09/2015</para>
/// <para>Author: NuMedia</para>
/// <para>Updates RawImage component when event is received</para>
/// </summary>

public class UpdateRawImage : MonoBehaviour
{
	/// <summary>
	/// Event name which will push a new texture.
	/// </summary>
	public string TextureEventName;
	
	/// <summary>
	/// References the RawImage component of the gameobject.
	/// </summary>
	private RawImage _rawImage;
	
	/// <summary>
	/// Manages the aspect ratio of the image.
	/// </summary>
	private AspectRatioFitter _aspectRatio;
	
	private bool Initialized = false;
	
	/// <summary>
	/// Initializes the class. Attaches TextureEvent listener.
	/// </summary>
    public void Awake()
	{
		if (Initialized) return;
		Initialized = true;
		
		_rawImage = this.GetComponent<RawImage>();
		_aspectRatio = this.GetComponent<AspectRatioFitter>();
		GlobalMessageManager.AddListener(TextureEventName, OnTextureEvent);
	}
	
	/// <summary>
	/// Handles the texture event. Updates the RawImage.
	/// </summary>
	/// <param name="EventName"></param>
	/// <param name="Data"></param>
	private void OnTextureEvent(string EventName, ref object Data)
	{
		try
		{
			_rawImage.texture = (Texture2D)Data;
			/*
			_aspectRatio.enabled = false;
			
			RectTransform rect = GetComponent<RectTransform>();
			rect.offsetMax = new Vector2(0, 0);
			rect.offsetMin = new Vector2(0, 0);
	
			_aspectRatio.aspectMode = (_rawImage.texture.width > _rawImage.texture.height) ? 
				AspectRatioFitter.AspectMode.WidthControlsHeight : AspectRatioFitter.AspectMode.HeightControlsWidth;
			
			_aspectRatio.aspectRatio = (float)_rawImage.texture.width/(float)_rawImage.texture.height;
			_aspectRatio.enabled = true;
			*/
		}
		catch (Exception e)
		{
			print("Exception thrown in: " + this.name);
			Debug.LogError(e);
		}
	}
}