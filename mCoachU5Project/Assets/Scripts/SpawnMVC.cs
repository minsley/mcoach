﻿using UnityEngine;
using System.Collections;

public class SpawnMVC : MonoBehaviour 
{
	private float _startTime = 0f;
	private static bool _triggered = false;

	
	// Use this for initialization
	void Awake()
	{
		_startTime = Time.time;
	}
	
	void Update()
	{
		if (_triggered) return;
		if (Time.timeSinceLevelLoad > .5f) // ???
		{
			if ( FindObjectOfType<LoginController>() == null)
				Instantiate(Resources.Load("MVC"));
			_triggered = true;
			// could destroy this object here, but it will be gone when we load the level
		}
	}

	public static void Reset(){
		_triggered = false;
	}
}