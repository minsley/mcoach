﻿public class Tools
{
    /// <summary>
    /// Check a string array for a single string. Returns true if the array contains
    /// passed string.
    /// </summary>
    /// <param name="stringArray"></param>
    /// <param name="singleString">List of files</param>
    public static bool StringArrayContains(string[] stringArray, string singleString)
    {
        for (int stringIndex = 0; stringIndex < stringArray.Length; stringIndex++)
        {
            if (stringArray[stringIndex].Equals(singleString))
            {
                return true;
            }
        }
        
        return false;
    }
}