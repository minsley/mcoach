﻿#define _USE_APP_SETTINGS_DOMAIN

using System;
using System.Linq;
using System.Xml;

namespace Assets.Scripts.Tools

{
    public static class WebServiceTools
    {
        #region Members 

        /// <summary>
        /// Staging server domain name
        /// </summary>
        private const string LiveDomainName = "mcoachportal.com";

        /// <summary>
        /// Staging server domain name
        /// </summary>
        private const string StagingDomainName = "mcoachadmin.numediainnovations.com";

        /// <summary>
        /// Local server domain name
        /// </summary>
        private const string LocalDomainName = "localhost/pll_150428_mcoach_admin_portal";

		/// <summary>
		/// Staging server domain name
		/// </summary>
		private static string AppSettingsDomainName = "mcoachportal.com";

        #endregion

        #region Properties

        /// <summary>
        /// Set to true to use the localhost domain name
        /// </summary>
        public static bool IsLocalHost { get; set; }

        /// <summary>
        /// returns the secret web calling token
        /// </summary>
        public static string CallToken
        {
            get { return "D752C178-2A6F-4E31-A8F2-096435D2C421"; }
        }

		/// <summary>
        /// Gets the base web service domain name
        /// </summary>
        public static string WebServiceDomainName
        {
            get
            {
#if _USE_LIVE
                return LiveDomainName;
#else 
    #if _USE_LOCAL
                    return LocalDomainName;
    #else
				#if _USE_STAGING
				return StagingDomainName;
				#else
				return AppSettingsDomainName;
#endif
#endif
#endif
            }
			set
			{
#if _USE_APP_SETTINGS_DOMAIN
				AppSettingsDomainName = value;
#endif
			}
        }

		public static int maxRetries = 30;

        /// <summary>
        /// Returns the web service URL
        /// </summary>
        public static string WebServicesUrl
        {
            get
            {
                return "https://" + WebServiceDomainName + "/Services.asmx"; //SSL change 9/1/16
            }
        }

        /// <summary>
        /// returns the method to the ping URL
        /// </summary>
        public static string PingUrl
        {
            get { return WebServicesUrl + "/Ping"; }
        }

        /// <summary>
        /// Returns the sync URL
        /// </summary>
        public static string SyncUrl
        {
            get { return WebServicesUrl + "/Sync"; }
        }

        /// <summary>
        /// Returns the get payload URL
        /// </summary>
        public static string GetPayloadUrl
        {
            get { return WebServicesUrl + "/GetPayloadUrl"; }
        }

        /// <summary>
        /// Returns the resolved IP address of the web services
        /// </summary>
        public static string WebServiceIpAddress
        {
            get
            {
                try
                {
                    if (WebServiceDomainName.ToLower().IndexOf("localhost") >= 0)
                    {
                        return "127.0.0.1";
                    }

                    var addrs = System.Net.Dns.GetHostAddresses(WebServiceDomainName);
                    var domainAddr = addrs.FirstOrDefault();
                    if (domainAddr != null) return domainAddr.ToString();

                    UnityEngine.Debug.Log("no addresses attached to that domain name!");
                }
                catch (Exception ex)
                {
                    UnityEngine.Debug.Log("GetReturnContent->Exception Caught: " + ex.Message);
                }

                return "";
            }
        }

        #endregion
        
        #region Methods
         
        /// <summary>
        /// Parses the result string from a web service call and returns the contents
        /// </summary>
        /// <param name="resultXmlString"></param>
        /// <returns></returns>
        public static string GetReturnContent(string resultXmlString)
        {
            try
            {
                //when logging in locally (if the connection is slow or doesnt exits) 
                //a result string is passed in that doesnt have enclosed <string> elements
                if (resultXmlString.ToLower().IndexOf("</string>") <= 0)
                {
                    return resultXmlString;
                }

                var xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(resultXmlString);
                resultXmlString = xmlDoc.LastChild.InnerText;

                return resultXmlString;
            }
            catch (Exception ex)
            {
                UnityEngine.Debug.Log("GetReturnContent->Exception Caught: " + ex.Message);
            }

            return null;
        }

        #endregion
    }
}
