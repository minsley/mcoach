﻿namespace Assets.Scripts.Tools
{
    public static class Constants
    {
		/* These event type enums allow following who calls what where a little easier than with just strings.
		 * 
		 * In addition to the hardcoded events, there are several classes which are placed in the levels and set to listen for
		 * particular events, however, those events may not be in this list, they are just strings... :/
		 * 
		 * for example, a GUI element called Rating2Text on the dashboard ratings panel is waiting for an event called 
		 * Rating2Description, but that doesnt occur here.  It may be coded in somewhere else, in the scene perhaps ?
		 * 
		 * UpdateText.cs and UpdateRawImage.cs appear to be the only components that register listeners in this way.
		 * 
		 * any UIElement component can add an event to be fired (only 4 of these in use)
		 * KeyboardScript and TriggerEventAfterTime also potentially create events from scene data
		 * 
		 * DashboardButton class objects listen for events to change states. but MainMenuView has a list of all of these Buttons (9)
		 * 
		 */
        public enum Events
        {
			AcceptAgreementEventId,				// [1Agreement:AcceptButton] -> AgreementController.OnAcceptAgreement() Loads Level 2Login
			CoachingCategoryDescriptionEventId,	// CoachingSkillInstance.CoachingCategorySelected -> no listeners in code
			CoachingCategorySelectedEventId,	// CoachingSkillInstance.CoachingCategorySelected -> DashboardController.OnCoachingCategorySelected(CoachingSkillCategory)
			CoachingCategoryTitleEventId,		// CoachingSkillInstance.CoachingCategoryTitle ->, no listeners in code
			CoachingSkillsEventId,				// DashboardController._coachingSkills-> CoachingCategoryScrollableList.OnCoachingSkills(CoachingSkillCategory[])
			ColorSchemeEventId,					// X-> MainMenuView.OnNewColorScheme(Color[])

			MyContentUpdatedEventId,			// DashboardController._myContentUpdate -> ContentListController.OnMyContentUpdated{ Dictionary<string, Content[]> )
			MyContentSectionUpdateEventId,		// ContentListController._myContentSectionNotify -> ContentListView.OnMyContentSectionUpdate( Content[] )
			MyContentSectionRequestEventId,		// ContentListView._requestSection -> ContentListController.OnRequestSection()

			ContentListItemSelectedEventId,		// ContentListItemView._contentItemSelected -> no listeners in code
			ContentListItemDescriptionEventId,	// X -> no listeners in code
			ContentListItemThumbnailEventId,	// FileSystemController._contentListItemImage -> ContentListView.OnContentImageRecieved( Texture2D )
			RequestContentListItemImageEventId,	// ContentListView._requestThumbnail -> FileSystemController.OnRequestContentImage( string path )            

            // dashboard buttons
			DISCButtonClickEventId,				// MainMenuView._DISCButtonClick -> ContentListController.OnDiscButtonClick() => ContentButtonClickHandler("DISC")
			FCRButtonClickEventId,				// MainMenuView._fcrButtonClick -> no listeners in code
			FCRButtonDisableEventId,			// DashboardController -> FCR DashboardButton
			CompetencyButtonClickEventId,		// MainMenuView._competencyModelButton -> ContentListController.OnCompetencyButtonClicked => ContentButtonClickHandler("COMP_MODEL");
			CoachingGuideButtonClickEventId,	// MainMenuView._coachingGuideButton -> ContentListController.OnCoachingGuideButtonClicked => ContentButtonClickHandler("COACH_GUIDE");
			TrainingResourcesButtonClickEventId,// MainMenuView._trainerResourcesButton -> ContentListController.OnTrainingResourceButtonClicked => ContentButtonClickHandler("TRAIN_RES");
			PulseButtonClickEventId,			// MainMenuView._pulseButton -> ContentListController.OnPulseButtonClicked => ContentButtonClickHandler("PULSE");

            CreateNewCoachingPlanPopUpShowEventId,
            CreateNewCoachingPlanPopUpHideEventId,
            CreateNewCoachingPlanPopUpSaveEventId,
			EditCoachingPlanPopUpShowEventId,
			CreateNewSalesDataPopUpSaveEventId,
			CreateNewDevelopmentPopUpSaveEventId,
			CreateNewGoalForFieldRidePopUpSaveEventId,
			CreateNewSalesDataPopUpHideEventId,
			CreateNewDevelopmentPopUpHideEventId,
			CreateNewGoalForFieldRidePopUpHideEventId,
			
			AddToFCRButtonClickEventId,			// [3.AddToFCRBtnImage -> RatingsController.OnAddToFCR grab the data from the observations field
			UnsubmittedFCRUpdatedEventId,		//  RatingsController._unsumbittedFCRUpdated -> DashoardController.OnUnsubmittedFCRUpdated put the dashboard back into teammember view? FCR view?
			CurrentDateEventId,					// DashboardController._currentDate -> no listerners in code
												// [3.MyTeamImage] -> DashboardButtonClick
			DashboardButtonClickEventId,		// MainMenuView._dashboardButtonClick -> DashboardController.OnDashboard() => _teamMemberNormal.FireEvent();
			DashboardLoadedEventId,				// X -> DashboardController.OnDashboardLoaded()
			DecompressPayloadEventId,			// MCoachModel._decompressPayload -> FileSystemController.OnDecompressTempFile( string[] ) => StartCoroutine(DecompressFile(files[0], files[1]));
			DeleteLocalFilesEventId,			// MCoachModel._deleteLocalFiles -> FileSystemController.OnDeleteLocalFiles( Manifest )
			DownloadPayloadEventId,				// WebCallController._downloadPayload -> no listeners in code
			DownloadProgressEventId,			// WebCAllController -> TeamMemberFCRView updates syncInProgress button.
			DownloadRetryEventId,				// WebCAllController -> updates syncInProgress button
			FolderForPayloadEventId,			// WebCallController._getLocalFolderForPayload -> MCoachModel.OnFolderForPayload( string )
			GetPayloadEventId,					// DashboardController._getPayload -> WebCallController.OnGetPayload( string )
			GlobalSkillTitleEventId,			// DashboardController._globalSkillTitle -> no listeners in code
			HideNativeUIEventId,				// LoginController._hideNativeUI -> NativeUIController.OnHideNativeUI && BlockInputManager.OnDisableBlocker
			LocalLoginEventId,					// LoginController._localLogin -> FileSystemController.OnLocalLogin( Manifest )
			LocalLoginResponseEventId,			// FileSystemController._pushLocalManifest -> no listeners in code
			LocalUserFilesEventId,				// FileSystemController._localUserFiles -> MCoachModel.OnLocalUserFiles( List<string> )
			LoginCompleteEventId,				// FileSystemController._loginComplete -> LoginController.OnOnlineWebcallComplete( string )
												// WebCallController._loginComplete -> same, this is a duplicate UIEvent created
			LoginEmailAddressEventId,			// [3.EnterEmailAddressBackgroundImage] -> LoginController.OnLoginEmailAddress ( string )
			LoginEventId,						// LoginController._login -> FileSystemController.OnLogin ( Manifest )
			LoginPasswordEventId,				// [3.PasswordBackgroundImage] -> LoginController -> OnLoginPassword ( string )
			LoginSendPasswordEventId,			// SendPassword.SendPasswordEvent -> WebCallController.OnGetPassword( string ) => StartCoroutine(ThreadedResetPassword( string )
			LoginSignInEventId,					// [2Login.SignInButton] -> LoginController.OnLoginSignIn()
			LoginSuccessEventId,				// LoginController._loginSuccess -> MCoachModel.OnLogin( Manifest )
			LoginUsernameEventId,				// [3.UsernameBackgroundImage] -> LoginController.OnLoginUsername( string )
			LoginWebCallEventId,				// LoginController._loginWebCall -> WebCallController.OnLogin( string ) (json manifest)
												// RunTest._loginWebCall -> DUPLICATE
			LogoImageEventId,					// FileSystemController._logoImage -> no listeners in code
			LogoutButtonClickEventId,			// MainMenuView,_logoutButton -> DashboardController.OnLogoutButtonClick() => LoadLevel("2Login")
			ManifestUpdatedEventId,				// MCoachModel._manifestUpdated -> FileSystemController.OnManifestUpdated( Manifest )
												// && -> DashboardController.OnManifestUpdated( Manifest ) and 3 calls to fire this event from MCoachModel
			//misc dynamic text strings
			CompetencyHeaderTextEventId,		// dashboardController.OnManifestUpdated -> UpdateText listeners in scene
			PlanNameTextEventId,
			PreviousPlanNameTextEventId,
			ProductHeaderTextEventId,

			// updates to dynamic menu buttons from file system controller to ui elements:
			Menu1DisableEventId,				// used if no content found for a section
			Menu2DisableEventId,				// 
			Menu3DisableEventId,				// 
			Menu4DisableEventId,				// 
			Menu5DisableEventId,				//
			Menu1ImageEventId,				// used to update the image on the button
			Menu2ImageEventId,				//
			Menu3ImageEventId,				// 
			Menu4ImageEventId,				// 
			Menu5ImageEventId,				// 
			Menu1TextEventId,				// used to update the text on a button
			Menu2TextEventId,				// 
			Menu3TextEventId,				// 
			Menu4TextEventId,				// 
			Menu5TextEventId,				// 

			MyTeamUpdateEventId,				// DashboardController._myTeamUpdate ->
												// MyProfileScrollableList.OnTeamMembers( List<TeamMember> ) [dynamic]
												// MyTeamHeaderView.OnMyTeamUpdate( List<TeamMember> ) [dynamic]
			NativePopupEventId,					// FileSystemController._nativeUICall -> NativeUIController.OnNativePopup( string[] )
												// LoginController._nativeUICall ->  DUPLICATE UIEvent
			NormalMenuStateEventId,				// MainMenuView._normalMenuState -> no listeners in code
			OnlineStatusDataEventId,			// WebCallController._onlineStatus -> OnOnlineStatusData( object[] ) bool, manifest serialized
			OnlineStatusEventId,				// FileSystemController._onlineStatus -> WebCallController.OnOnlineStatus( object for login ? )
												// RunTest._onlineStatus -> DUPLICATE UIEvent
			OnlineStatusChangedEventId,			// webCallController.HeartbeatPing -> TeamMemberFCRView. updates sync button
			PayloadDecompressBeginEventId,		// FileSystemController -> SyncButtonHandler
			PayloadDecompressEndEventId,		// FileSystemController -> SyncButtonHandler
			PingWebServicesEventId,				// TeamMemberFCRView -> WebCallController when performing a sync.
			PingWebServicesResultEventId,		// WebCAllController -> TeamMemberFCRView if not online
			ProfileButtonClickEventId,			// MainMenuView._profileButtonClick -> DashboardController.OnProfile()
			ProfileImageEventId,				// FileSystemController._profileImage -> no listeners in code
			PurgeUserFilesEventId,				// SyncButtonController - > FileSystemController
			
			RatingsEventId,						// DashboardController._ratings -> RatingController.OnSetRating( Rating[] )
			RepresentativeSummaryClickEventId,	// [3.RepresentativeSumaryButton] -> DashboardController.OnRepresentativeSummaryClicked()
			RequestManifestEventId,				// DashboardController._requestManifest -> MCoachModel.OnRequestManifest()
			RequestLogoImageEventId,			// DashboardController._requestProfileImage -> FileSystemController.OnRequestProfileImage( string filepath) => StartCoroutine(LoadProfileImageCoRoutine()
			RequestMenu1ImageEventId,			// DashboardController._requestProfileImage -> FileSystemController.OnRequestProfileImage( string filepath) => StartCoroutine(LoadProfileImageCoRoutine()
			RequestMenu2ImageEventId,			// DashboardController._requestProfileImage -> FileSystemController.OnRequestProfileImage( string filepath) => StartCoroutine(LoadProfileImageCoRoutine()
			RequestMenu3ImageEventId,			// DashboardController._requestProfileImage -> FileSystemController.OnRequestProfileImage( string filepath) => StartCoroutine(LoadProfileImageCoRoutine()
			RequestMenu4ImageEventId,			// DashboardController._requestProfileImage -> FileSystemController.OnRequestProfileImage( string filepath) => StartCoroutine(LoadProfileImageCoRoutine()
			RequestMenu5ImageEventId,			// DashboardController._requestProfileImage -> FileSystemController.OnRequestProfileImage( string filepath) => StartCoroutine(LoadProfileImageCoRoutine()
			RequestProfileImageEventId,			// DashboardController._requestProfileImage -> FileSystemController.OnRequestProfileImage( string filepath) => StartCoroutine(LoadProfileImageCoRoutine()
			RequestTeamLeaderProfileImageEventId, // TeamMemberInstance._requestImage -> FileSystemController.OnRequestTeamLeaderImage( string path ) => LoadTeamLeaderImageCoRoutine()
			SelectDashboardEventId,				// DashboardController._selectDashboard -> MainMenuView.OnSelectDashboard() => OnDashboardButton()
			ShowLoadingEventId,					// LoginController._displayLoader -> NativeUIController.OnShowLoading( string )
												// ->BlockInputManager.OnActivateBlocker() [hasRemove]
			TeamCoachingSkillsEventId,			// DashboardController._teamCoachingSkills -> TeamMemberScrollableList.OnTeamCoachingSkills( CoachingSkillCategory[] )			
			
												//TeamMemberInstance.OnInstanceClicked sends the following group:
			TeamMemberCSIDsEventId,				// TeamMemberInstance._teamMemberCSIDs -> DashboardController.OnTeamMemberCSIDs( int[] )
			TeamMemberDateEventId,				// TeamMemberInstance._teamMemberDate -> not fired in code -> TeamMemberView.OnTeamMemberDate( string )
			TeamMemberEmailEventId,				// TeamMemberInstance._teamMemberEmail -> TeamMemberView.OnTeamMemberEmail( string )
			TeamMemberLocationEventId,			// TeamMemberInstance._teamMemberLocation -> TeamMemberView.OnTeamMemberLocation( string )
			TeamMemberNameEventId,				// TeamMemberInstance._teamMemberName -> TeamMemberView.OnTeamMemberName( string )
			TeamMemberLevelLabelEventId,				// the label for their territory, district, etc.  dynamic from level labels
			TeamMemberNormalEventId,			// TeamMemberInstance._teamMemberNormal -> TeamMemberInstance.OnTeamMemberNormal() [hasRemove]			
												// DashboardController._teamMemberNormal -> DUPLICATE UIEvent
			TeamMemberPhoneEventId,				// TeamMemberInstance._teamMemberPhone -> TeamMemberView.OnTeamMemberPhone( string )
			
			TeamMemberProfileImageEventId,		// FileSystemController._teamMemberProfileImage -> no listeners in code
			TeamMemberSelectedEventId,			// TeamMemberInstance._teamMemberSelected -> DashboardController.OnTeamMemberSelected( TeamMember )
			TeamMemberTerritoryEventId,			// TeamMemberInstance._teamMemberTerritory -> TeamMemberView.OnMemberTerritory( string )
			TestEventEventId,					// X -> RunTest.OnTestEvent()
			TipOfTheDayDescriptionEventId,		// DashboardController._tipOfTheDayDescription -> TipOfTheDayView.OnTipOfTheDayDescription( string )
			TipOfTheDayTitleEventId,			// DashboardController._tipOfTheDayTitle -> TipOfTheDayView.OnTipOfTheDayTitle( string )
			
												//DashboardController.OnManifestUpdated fires these:
			UserDistrictEventId,				// DashboardController._userDistrict -> no listeners in code
			UserEmailEventId,					// DashboardController._userEmail -> no listeners in code
			UserFirstNameEventId,				// DashboardController._userFirstName -> no listeners in code
			UserFullNameEventId,				// DashboardController._userFullName -> no listeners in code
			UserLastLoginDateEventId,			// DashboardController._userLastLoginDate -> no listeners in code
			UserLastNameEventId,				// DashboardController._userLastName -> no listeners in code
			UserNameEventId,					// DashboardController._userName -> no listeners in code
			UserLevelLabelEventId,				// the label for their territory, district, etc.  dynamic from level labels
			UserTerritoryEventId,				// DashboardController._userTerritory -> no listeners in code

			ViewFCREventID,						// DashboardController,_viewFCR -> TeamMemberFCRView.OnViewFCR (FCR)
			ViewFCRIndexEventID,				// DashboardController,_viewFCRIndex -> TeamMemberFCRView.OnViewFCRIndex (FCR)
            
			SyncManifestEventId,						//called to start a round trip that syncs the manifest
			SaveLocalManifest					//called when the manifest needs to be saved directly to disk
        }
    }
}
