﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CrashReporter : MonoBehaviour {

	public bool AutoSendReports = false;
	public GameObject crashReportingPopup = null;
	public GameObject crashInstanceTemplate = null;
	public Text crashInstanceTemplateTime = null; // fill this out before instantiating
	public Transform crashListContainer = null;
	public GameObject reportViewPanel = null;
	public Text reportViewTimeTitle = null;
	public Text reportViewContent = null;
	public Text sendToEmail = null;

	int reportLimit = 10; // how many to show

	// Use this for initialization
	void Start () {
		if (CrashReport.reports.Length > 0) {

			if (AutoSendReports){
				AutoSend();
			}else{

				crashReportingPopup.SetActive(true);
				PopulateReports();
			}
		}
		else
		{
			// temp testing force a crash
			crashReportingPopup.SetActive(true);
//			crashReportingPopup = null;
//			crashReportingPopup.SetActive(false);
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void AutoSend(){
		foreach (CrashReport report in CrashReport.reports) {
			EmailReport(report);
			// remove after viewing
			report.Remove();
		}
	}

	void PopulateReports(){
		int index = 0;
		foreach (CrashReport report in CrashReport.reports) {
			crashInstanceTemplateTime.text = report.time.ToString();
			GameObject newInstance = Instantiate(crashInstanceTemplate) as GameObject;
			newInstance.name = index++.ToString();
			newInstance.transform.SetParent(crashListContainer);
			newInstance.transform.localScale = new Vector3(1,1,1);
			newInstance.SetActive(true);
			if (index > reportLimit)
				break;
		}
		crashInstanceTemplate.SetActive (false);
	}

	public void OnViewReport(GameObject reportInstance){
		CrashReport report = ReportFromName (reportInstance);
		if (report != null) {
			reportViewPanel.SetActive(true);
			reportViewTimeTitle.text = report.time.ToString();
			reportViewContent.text = report.text;
		}
	}

	public void OnRemoveReport(GameObject reportInstance){
		CrashReport report = ReportFromName (reportInstance);
		if (report != null)
			report.Remove ();
	}

	public void OnSendReport(GameObject reportInstance){

//		return;  // disabled for now

		CrashReport report = ReportFromName (reportInstance);
		if (report != null) {
			EmailReport(report);
			// remove after viewing
//			report.Remove();
		}

	}

	void EmailReport(CrashReport report){
		// use IOS native mailer
		Application.OpenURL (WWW.EscapeURL("mailto:"+sendToEmail.text+"subject="+report.time.ToString()+"&body="+report.text).Replace("+","%20"));
	}

	CrashReport ReportFromName(GameObject reportInstance){
		if (reportInstance != null) {
			int index = -1;
			if (int.TryParse(reportInstance.name,out index)){
				if (CrashReport.reports.Length > index)
					return CrashReport.reports[index];
			}
		}
		return null;
	}
}
