﻿using UnityEngine;
using MCoach;

/// <summary>
/// <para>Date: 07/15/2015</para>
/// <para>Author: NuMedia</para>
/// <para>Awake function does not always fire at the start of the scene.
/// (Awake is not called on game objects that are not set active in the scene,
/// but it can be forced this way.)
/// 
/// 
/// The architecural reason for needing to initialize these inactive objects primarily comes from the need for them
/// to receive events before they are activated, primarily because events are they way they learn about global state,
/// such as the received manifest or currently selected player. This is a lot clumsier than just accessing that state
/// thru a singleton, which would have simplifed a lot of this code greatly.
/// 
/// Since Awake can't be called on a generic MonoBehavior, this script has to have public properties of every type to be
/// initialized, requireing the addition of code every time a new class needs to be initialized while inactive...
/// 
/// 
/// This garuntees child script's awake functions are called at the start.
/// Called scripts must have a flag in their Awake call to make sure Awake
/// Is not called twice! Awake must be public.</para>
/// </summary>
public class InitializeScripts : MonoBehaviour 
{
	public RatingController ratingController;
	public TeamMemberScrollableList TMScrollableList;
	public TeamMemberFCRView TMFCRView;
	public RemindersPopupController remindersPopupController;

	/// <summary>
	/// Initializes scripts.
	/// </summary>
	void Awake()
	{
		UpdateText[] textScripts = GetComponentsInChildren<UpdateText>(true);
		UpdateRawImage[] imageScripts = GetComponentsInChildren<UpdateRawImage>(true);
		UIElement[] uiElementScripts = GetComponentsInChildren<UIElement>(true);
		MyProfileScrollableList[] profileList = GetComponentsInChildren<MyProfileScrollableList>(true);
		
		for(int textIndex = 0; textIndex < textScripts.Length; textIndex++)
			textScripts[textIndex].Awake();

		for(int imageIndex = 0; imageIndex < imageScripts.Length; imageIndex++)
			imageScripts[imageIndex].Awake();

		for(int elementIndex = 0; elementIndex < uiElementScripts.Length; elementIndex++)
			uiElementScripts[elementIndex].Initialize(); // awake is not visible due to some strange relection thingy
		
		for(int profileListIndex = 0; profileListIndex < profileList.Length; profileListIndex++)
			profileList[profileListIndex].Awake();

		
		ratingController.Awake();
		TMScrollableList.Awake();
		TMFCRView.Awake ();
		remindersPopupController.Awake();
	}
}