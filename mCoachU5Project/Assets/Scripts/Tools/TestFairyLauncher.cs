﻿using UnityEngine;
using System.Collections;
using TestFairyUnity;

public class TestFairyLauncher : MonoBehaviour {

	bool useTestFairy = true;
	// Use this for initialization
	void Start () {

		if (PlayerPrefs.HasKey("useTestFairy")){
			useTestFairy = (PlayerPrefs.GetString("useTestFairy") == true.ToString());
		}
		PlayerPrefs.SetString("useTestFairy",useTestFairy.ToString());

		if (useTestFairy)
			TestFairy.begin ("3f4196277728e858cd382fbe4a270945a07ff26c");
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
