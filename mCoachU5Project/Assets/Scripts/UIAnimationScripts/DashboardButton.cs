﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// <para>Date: 06/30/2015</para>
/// <para>Author: NuMedia</para>
/// <para>Manages the colors of a Dasbhoard menu button.</para>
/// </summary>
public class DashboardButton : MonoBehaviour
{
    /// <summary>
    /// References the primary color states
    /// </summary>
    public Color Normal, Selected, Disabled;
    /// <summary>
    /// References the secondary color states
    /// </summary>
    public Color Normal2, Selected2, Disabled2;
    /// <summary>
    /// Images apart of the button
    /// </summary>
    public Image Background, Icon, Selection;
    /// <summary>
    /// References the Text elements displaying the button name
    /// </summary>
    public Text MainText;
    /// <summary>
    /// Events to listen for.
    /// </summary>
    public string NormalEventName, SelectedEventName, DisabledEventName;
    /// <summary>
    /// The start and end state of the primary color animation
    /// </summary>
    private Color _startingColor, _endingColor;
    /// <summary>
    /// The start and end state of the secondary color animation
    /// </summary>
    private Color _startingColor2, _endingColor2;
    /// <summary>
    /// The start and end value of the selection's opacity
    /// </summary>
    private float _selectionStartOpacity, _selectionEndOpacity;
    /// <summary>
    /// The gametime the animation started (elapsed time program has been active at this point)
    /// </summary>
    private float _startTime = 0;
    /// <summary>
    /// Animation length in seconds
    /// </summary>
    private float _animTime = .5f;
    /// <summary>
    /// Set to true if an animation is in progress
    /// </summary>
    private bool _animating = false;

	public bool isDisabled = false; // ew.

    /// <summary>
    /// Method fired at the start the application or initial spawn of the script.
    /// Initializes event listeners.
    /// </summary>
    private void Awake()
    {
        if (!NormalEventName.Equals(string.Empty))
        {
            GlobalMessageManager.AddListener(NormalEventName, AnimToNormalState);
        }
        if (!SelectedEventName.Equals(string.Empty))
        {
            GlobalMessageManager.AddListener(SelectedEventName, AnimToSelectedState);
        }
        if (!DisabledEventName.Equals(string.Empty))
        {
			GlobalMessageManager.AddListener(DisabledEventName, OnDisabledEvent);
        }
    }

    /// <summary>
    /// Handles animation. Function is fired every frame.
    /// </summary>
    public void Update()
    {   
        if (!_animating) return;

        float percentage = (Time.time - _startTime) / (_animTime);
        
        if (percentage < 1f)
        {
    	    Background.color = 
                new Color(_startingColor.r - ((_startingColor.r - _endingColor.r) * percentage),
                _startingColor.g - ((_startingColor.g - _endingColor.g) * percentage),
                _startingColor.b - ((_startingColor.b - _endingColor.b) * percentage));

            MainText.color = Icon.color =
                new Color(_startingColor2.r - ((_startingColor2.r - _endingColor2.r) * percentage),
                _startingColor2.g - ((_startingColor2.g - _endingColor2.g) * percentage),
                _startingColor2.b - ((_startingColor2.b - _endingColor2.b) * percentage));

            Selection.color =
                new Color(Selection.color.r, Selection.color.g, Selection.color.b,
                _selectionStartOpacity - ((_selectionStartOpacity - _selectionEndOpacity)* percentage));
        }
        else
        {
            _animating = false;
            Background.color = _endingColor;
            MainText.color = Icon.color = _endingColor2;
            Selection.color =
                new Color(Selection.color.r, Selection.color.g, Selection.color.b, _selectionEndOpacity);
        }
    }

    /// <summary>
    /// Starts animating button to it's normal state
    /// </summary>
    /// <param name="eventName">Name of event triggering method</param>
    /// <param name="Data">Data assocaited with the event</param>
    public void AnimToNormalState(string eventName, ref object Data)
    {
        SetStartAndEndValues(Normal, Normal2, 1f);
    }
    
    /// <summary>
    /// Starts animating button to it's selected state
    /// </summary>
    /// <param name="eventName">Name of event triggering method</param>
    /// <param name="Data">Data assocaited with the event</param>
    public void AnimToSelectedState(string eventName, ref object Data)
    {
        SetStartAndEndValues(Selected, Selected2, 1f);
    }


	public void OnDisabledEvent(string eventName, ref object Data){
		bool wasDisabled = isDisabled;
	    isDisabled = (bool?) Data ?? true;

//		if (wasDisabled == isDisabled)
//			return;  // animations seem to be hanging

		if (isDisabled) {
			GetComponent<Button>().interactable = false;
			SetStartAndEndValues(Disabled, Disabled2, 0);
		}
		else { // is enabled
			GetComponent<Button>().interactable = true;
			SetStartAndEndValues(Normal, Normal2, 1f);
		}
	}
    
    /// <summary>
    /// Starts animating button to it's disabled state
    /// </summary>
    /// <param name="eventName">Name of event triggering method</param>
    /// <param name="Data">Data assocaited with the event</param>
    public void AnimToDisabledState(string eventName, ref object Data)
    {
        SetStartAndEndValues(Disabled, Disabled2, 0);
    }

    /// <summary>
    /// Starts a button animation and sets the final state of the button
    /// </summary>
    /// <param name="c1">Final state of Primary color</param>
    /// <param name="c2">Final state of Secondary color</param>
    /// <param name="opacity">Final opacity level of selection image</param>
    private void SetStartAndEndValues(Color c1, Color c2, float opacity)
    {
        _startingColor = Background.color;
        _startingColor2 = MainText.color;
        _endingColor = c1;
        _endingColor2 = c2;
        _selectionStartOpacity = Selection.color.a;
        _selectionEndOpacity = opacity;

        _startTime = Time.time;
        _animating = true;
    }
}