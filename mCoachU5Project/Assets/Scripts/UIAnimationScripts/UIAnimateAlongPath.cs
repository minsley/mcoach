﻿using UnityEngine;

public class UIAnimateAlongPath : MonoBehaviour 
{
	public iTweenPath Path;
	public float Percentage;
	private Vector3[] _path;

	void Start()
	{
		_path = Path.nodes.ToArray();
	}

	// Update is called once per frame
	void Update () {
		iTween.PutOnPath(this.transform, _path, Percentage);
	}
}