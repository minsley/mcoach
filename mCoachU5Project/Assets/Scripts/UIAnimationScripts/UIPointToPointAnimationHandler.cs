﻿using UnityEngine;

public class UIPointToPointAnimationHandler : MonoBehaviour 
{
	public float AnimPercentage;
	public GameObject FinalLocation, StartLocation;
	public bool Animating;
	public bool DifferenceCalculated = false;

	private float xDifference, yDifference;

	void Update () 
	{
		if (!Animating) return;
		if (AnimPercentage > 1) AnimPercentage = 1;
		if (AnimPercentage < 0) AnimPercentage = 0;

		if (!DifferenceCalculated)
		{
			DifferenceCalculated = true;
			xDifference = FinalLocation.transform.position.x - StartLocation.transform.position.x;
			yDifference = FinalLocation.transform.position.y - StartLocation.transform.position.y;
		}

		this.transform.position = new Vector2(StartLocation.transform.position.x + (xDifference * AnimPercentage),
		                                      StartLocation.transform.position.y + (yDifference * AnimPercentage));
	}
}