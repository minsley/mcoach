﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Assets.Scripts.Tools;
using MCoach;

public class SyncButtonHandler : MonoBehaviour {

	// handles state of sync button, including a recurring network reachability test and handlers for the sync in progress icon
	// and click handler for the button.


	public Sprite syncTexture;
	public Sprite offlineTexture;
	public Sprite[] syncInProgress;
	private int syncProgressFrame = 0;
	private float syncProgressAmount = 0; // this may not actually track during download
	private int progressCalls = 0; // fake file countdown...
	private int previousRetries = 0;
	public static NetworkReachability currentReachability = NetworkReachability.NotReachable;
	public static bool webServiceReachable = false; // only set to true if positive ping result from WebCallController
	public static bool performingSync = false;
	public static bool downloadingPayload = false;
	public static bool performingDecompression = false;
	bool animationCallPending = false;
	public GameObject syncInProgressIcon;
	public Image syncProgressPie = null;
	public Text syncProgressText = null;
	private float buttonDownTime = 0;
	public float buttonHoldThreshold = 3; // how long to hold for full sync request
	bool longPressQueued = false;
	bool longPressRunning = false;

	private UIEvent _pingWebServices = new UIEvent(Constants.Events.PingWebServicesEventId.ToString());
	private UIEvent _nativeUICall = new UIEvent(Constants.Events.NativePopupEventId.ToString());
	private UIEvent _pushManifest = new UIEvent(Constants.Events.SyncManifestEventId.ToString());
	private UIEvent _purgeUserFiles = new UIEvent(Constants.Events.PurgeUserFilesEventId.ToString());

	void Awake(){
		GlobalMessageManager.AddListener (Constants.Events.PingWebServicesResultEventId.ToString (), OnPingWebServicesResult);
		GlobalMessageManager.AddListener (Constants.Events.DownloadProgressEventId.ToString (), OnDownloadProgress);
		GlobalMessageManager.AddListener (Constants.Events.DownloadRetryEventId.ToString (), OnDownloadRetry);
		GlobalMessageManager.AddListener(Constants.Events.ManifestUpdatedEventId.ToString(), OnManifestUpdated);
		GlobalMessageManager.AddListener(Constants.Events.GetPayloadEventId.ToString(), OnGetPayload);
		GlobalMessageManager.AddListener(Constants.Events.PayloadDecompressBeginEventId.ToString(), OnPayloadDecompressBegin);
		GlobalMessageManager.AddListener(Constants.Events.PayloadDecompressEndEventId.ToString(), OnPayloadDecompressEnd);

		GlobalMessageManager.AddEvent(_pingWebServices);
		GlobalMessageManager.AddEvent(_nativeUICall);
		GlobalMessageManager.AddEvent(_pushManifest);
		GlobalMessageManager.AddEvent(_purgeUserFiles);
	}

	// Use this for initialization
	void Start () {
		InvokeRepeating("CheckReachability",0.1f,10); // when reachability becomes true, we will ping web services
		gameObject.GetComponent<Image>().sprite = offlineTexture; // assume offline to start
		gameObject.GetComponent<Image>().color = Color.gray;
		performingSync = true; // we are initially performing a sync

	}

	void OnDestroy()
	{
		GlobalMessageManager.RemoveListener (Constants.Events.PingWebServicesResultEventId.ToString (), OnPingWebServicesResult);
		GlobalMessageManager.RemoveListener (Constants.Events.DownloadProgressEventId.ToString (), OnDownloadProgress);
		GlobalMessageManager.RemoveListener (Constants.Events.DownloadRetryEventId.ToString (), OnDownloadRetry);
		GlobalMessageManager.RemoveListener(Constants.Events.ManifestUpdatedEventId.ToString(), OnManifestUpdated);
		GlobalMessageManager.RemoveListener(Constants.Events.GetPayloadEventId.ToString(), OnGetPayload);
		GlobalMessageManager.RemoveListener(Constants.Events.PayloadDecompressBeginEventId.ToString(), OnPayloadDecompressBegin);
		GlobalMessageManager.RemoveListener(Constants.Events.PayloadDecompressEndEventId.ToString(), OnPayloadDecompressEnd);
	}
	
	// Update is called once per frame
	void Update () {

	}

	public void OnSyncButtonDown(){
		// in 0rder
		if (!longPressQueued && !performingSync && !performingDecompression) {
			// Start a call for long button hold
			Invoke ("OnLongButtonPress", buttonHoldThreshold);
			longPressQueued = true;
		}
	}

	public void OnSyncButtonUp(){
		if (performingSync || performingDecompression) {
			AnimateSyncInProgress();
			return;
		}

		// if we are offline, popup a message
		if (!webServiceReachable) {
			CancelInvoke("OnLongButtonPress");
			longPressQueued = false;
			performingSync = false;
			AnimateSyncInProgress(); // show the offline status
		}

		if (longPressQueued){
			// handle short button press
			CancelInvoke("OnLongButtonPress");
			longPressQueued = false;
			if (!webServiceReachable) {
				ShowOfflineWarning();
				return;
			}
			// enable sync in progress icon
//			syncInProgressIcon.SetActive(true);
			performingSync = true;
			downloadingPayload = false;
			AnimateSyncInProgress();
//			syncInProgressIcon.GetComponent<Animator>().Play("Sync_Progress");
			gameObject.GetComponent<Image> ().color = Color.yellow; // as a backup, set us to yellow.
			// perform a manifest round trip
			object manifestObj = (object)MCoachModel.GetManifest();
			// for now, use the exiting turnouround logic path that occurs on login, which leaves us at the dashboard screen, with possibly a team member selected.
			_pushManifest.FireEvent (ref manifestObj);
		}else{
			// long button press, handled by invoke, so nothing to do here
		}


	}

	void OnLongButtonPress(){
		longPressQueued = false;
		// handle long button press
		if (!webServiceReachable) {
			ShowOfflineWarning();
			return;
		}

		object uiInfo = new string[] { "Full Resync", "All local files are being updated.", "OK" };
		_nativeUICall.FireEvent(ref uiInfo);
		// enable sync in progress icon
		performingSync = true;
		downloadingPayload = false;
		AnimateSyncInProgress();
//		syncInProgressIcon.SetActive(true);
		// purge local documents for this user and start a fresh manifest round trip

		// file systemController.purgeUserFiles(userinfo.id);
		object manifestObj = (object)MCoachModel.GetManifest();
		_purgeUserFiles.FireEvent (ref manifestObj);

		// perform a manifest round trip
		manifestObj = (object)MCoachModel.GetManifest();
		// for now, use the exiting turnouround logic path that occurs on login, which leaves us at the dashboard screen, with possibly a team member selected.
		_pushManifest.FireEvent (ref manifestObj);

	}

	void ShowOfflineWarning(){
		object uiInfo = new string[] { "Offline", "Working with Cached Data only.", "OK" };
		_nativeUICall.FireEvent(ref uiInfo);
	}

	void CheckReachability(){
		if (Application.internetReachability != currentReachability) {
			currentReachability = Application.internetReachability;
			if (currentReachability != NetworkReachability.NotReachable) {
				// start a web service ping, the result will trigger the handler to complete the transition.
//				Debug.Log("reachability changed, sending ping");
				_pingWebServices.FireEvent ();
				// if web service ping fails, continue to ping at long intervals while reachable
			} else {
				// we have lost reachability
//				Debug.Log("lost application.internetReachability");
				performingSync = false;
				webServiceReachable = false;
				gameObject.GetComponent<Image> ().sprite = offlineTexture;
				gameObject.GetComponent<Image> ().color = Color.gray;
			}
		} else { // we start out unreachable and stay that way.  
			if (currentReachability == NetworkReachability.NotReachable) {
				performingSync = false;
				webServiceReachable = false;
				gameObject.GetComponent<Image> ().sprite = offlineTexture;
				gameObject.GetComponent<Image> ().color = Color.gray;
			}
		}
	}

	private void OnPingWebServicesResult(string EventName, ref object Data){
		// put up a warning, and turn the syncButton back to white
		bool pingResult = (bool)Data;
//		Debug.Log("SBH Ping Result: "+ pingResult.ToString());
		if (pingResult) {
			// Yay, we have confirmed we can reach our services.

			webServiceReachable = true;
			gameObject.GetComponent<Image>().sprite = syncTexture;
			gameObject.GetComponent<Image>().color = Color.white;
			if (performingSync) // initially we are.
				AnimateSyncInProgress();
		} else {
			performingSync = false;
			webServiceReachable = false;
			gameObject.GetComponent<Image>().sprite = offlineTexture;
			gameObject.GetComponent<Image>().color = Color.gray;
			// if we're still reachable, and the web service didnt respond, send another ping in a while...
			if (Application.internetReachability != NetworkReachability.NotReachable)
				Invoke ("SendPing",30);
		}

		gameObject.GetComponent<Image> ().color = Color.white;
	}

	void SendPing(){
		if (Application.internetReachability != NetworkReachability.NotReachable){
			// start a web service ping, the result will trigger the handler to complete the transition.
			_pingWebServices.FireEvent();
			// if web service ping fails, continue to ping at long intervals while reachable
		}
	}

	private void OnDownloadProgress(string EventName, ref object Data){
		// put up a warning, and turn the syncButton back to white
		float progress = (float)Data;
		syncProgressAmount = progress;
		if (progressCalls < MCoachModel.GetManifest ().AddFiles.Length - 1)
			progressCalls++;
		
		if (progress == 0) {
			performingSync = true;
//			gameObject.SetActive(false); // maybe just the renderer ?
//			syncInProgressIcon.SetActive(true);
			AnimateSyncInProgress();
//			syncInProgressIcon.GetComponent<Animator>().Play("Sync_Progress");
			
		} else if (progress == 1) {
			if (syncProgressText != null)
				syncProgressText.gameObject.SetActive(false);
			performingSync = false; // wait for user files to be decompressed ?
//			gameObject.SetActive(true); // maybe just the renderer ?
//			syncInProgressIcon.SetActive(false);
		} else {
			// intermediate progress at this point
		}
	}

	private void OnDownloadRetry(string EventName, ref object Data){
		// put up a warning, and turn the syncButton back to white
		int retries = (int)Data;

		if (syncProgressText != null && retries != previousRetries) {
			previousRetries = retries;
			if (retries > 0) {
				syncProgressText.text = retries.ToString();
				syncProgressText.gameObject.SetActive(true);
				// reset the pie amount
				progressCalls = 1; // maybe 0?

			} else {
				syncProgressText.text = "";
				syncProgressText.gameObject.SetActive(false);
			}
		}
	}

	private void OnManifestUpdated(string EventName, ref object Data){
		// update the sync button, sync is done
		if (DrilldownController.GetInstance().Aliases.Count == 0 &&
		    !MCoachModel.GetManifest().NoPayload &&
		    !(MCoachModel.GetManifest().AddFiles.Length == 0)) // full login, not alias drilldown. Keep animating until decompression completes
			return; // when do we need to stop ? not until docompression completes ?


		performingSync = false; // i really want to wait for the progress or file decompressed event but this is okay for starters.

		gameObject.GetComponent<Image>().color = Color.white;
	}

	private void OnGetPayload(string EventName, ref object Data){
		//TODO  Yet another less than ideal solution:  The dashboard controller is going to send out the GetPayload event even if we are not online when we log in
		// or any time the manifest is updated and has a 'get files' list.
		// If we are offline, we would start the animation but never see the payload decompress events so we just keep spinning.
		// there are better solutions, but for now, just don't start the sync process if we are offline when the payload is requested.
		if (!webServiceReachable) return;

		// update the sync button, sync is done
		progressCalls = 0;
		performingSync = true;
		downloadingPayload = true;
		AnimateSyncInProgress();
	}

	private void OnPayloadDecompressBegin(string EventName, ref object Data){
		// update the sync button, sync is done
		performingSync = false;
		performingDecompression = true;
		AnimateSyncInProgress();
	}
	private void OnPayloadDecompressEnd(string EventName, ref object Data){
		// update the sync button, sync is done
		performingSync = false;
		performingDecompression = false;
		AnimateSyncInProgress();
	}

	void UpdateSyncAnimation(){
		if (!animationCallPending)
			return; // this is an error condition and should never happen...
		animationCallPending = false;
		AnimateSyncInProgress ();
	}

	void AnimateSyncInProgress(){
		if (animationCallPending)
			return; // we are already looping the animation, dont start another 'thread'
		if (performingSync || performingDecompression) {
			syncProgressPie.gameObject.SetActive(true);
			if (performingSync){
				if (downloadingPayload){
					float filesRemainingEstimate = 0.75f*(((float)MCoachModel.GetManifest().AddFiles.Length - progressCalls)/(float)MCoachModel.GetManifest().AddFiles.Length);
					if (filesRemainingEstimate == 0) 
						filesRemainingEstimate = 0.75f;
					syncProgressPie.fillAmount = filesRemainingEstimate;
				}
				else
				{
					syncProgressPie.fillAmount = 0.9f;;
				}
				gameObject.GetComponent<Image> ().color = Color.yellow; 
			}
			else{
				gameObject.GetComponent<Image> ().color = Color.green;  // enable us to see the difference
			}

			gameObject.GetComponent<Image> ().sprite = syncInProgress [syncProgressFrame++];
			if (syncProgressFrame >= syncInProgress.Length)
				syncProgressFrame = 0;
			animationCallPending = true;
			Invoke ("UpdateSyncAnimation", 0.25f);
		}
		else {
			syncProgressPie.gameObject.SetActive(false);
			if (webServiceReachable){
				gameObject.GetComponent<Image> ().sprite = syncInProgress [0];
				gameObject.GetComponent<Image> ().color = Color.white;
			}
			else{
				gameObject.GetComponent<Image> ().sprite = offlineTexture;
				gameObject.GetComponent<Image> ().color = Color.gray;
			}
			if (syncProgressText != null)
				syncProgressText.gameObject.SetActive(false);
			syncProgressFrame = 0;
		}
	}


}
