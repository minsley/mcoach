﻿using System;
using System.Collections.Generic;
using System.IO;

/// <summary>
/// <para>Date: 07/08/2015</para>
/// <para>Author: NuMedia</para>
/// <para>This class is created to be called inside a separate thread.
/// Decompresses a 7z file to a specific location.</para>
/// </summary>
public class DecompressPackageInThread
{
	/// <summary>
    /// Stores true if the StartCall method has not completed.  This two step is runing check can fail 
    /// </summary>
	public volatile bool ThreadStarted = false;
    public volatile bool ThreadRunning = false;
	public volatile bool ThreadCompleted = false;
    /// <summary>
    /// Report from the lzma decompress. Defaults at -5 meaning no report received.
    /// </summary>
	public volatile int Report = -5;
    /// <summary>
    /// Stores the report of a caught exception. By default stores an empty string.
    /// </summary>
	public volatile string ErrorReport = "";
    /// <summary>
    /// Array of decompressed file names. Default is null.
    /// </summary>
	public volatile List<string> FileNames;
    /// <summary>
    /// Stores the full path of the compressed files.
    /// </summary>
	private string _compressedPath;
    /// <summary>
    /// Stores the full path to store decompressed files.
    /// </summary>
	private string _decompressedPath;
	
    /// <summary>
    /// Decompresses a 7z file to a given location. Requires the full path to the compressed
    /// file and the full path of where the decompressed files should be stored.
    /// </summary>
    /// <param name="compressedFileLocation"></param>
    /// <param name="decompressedLocation"></param>
	public DecompressPackageInThread(string compressedFileLocation, string decompressedLocation)
	{
		_compressedPath = compressedFileLocation;
		_decompressedPath = decompressedLocation;
		ThreadCompleted = false;
	}
	
    /// <summary>
    /// This method is what should be called for Thread.Start().
    /// </summary>
	public void StartThread()
	{
		ThreadRunning = true;
		ThreadStarted = true;
		ThreadCompleted = false;

        try
        {
            Report = lzma.doDecompress7zip(_compressedPath, _decompressedPath, true, false);

            List<string> files = new List<string>(Directory.GetFiles(_decompressedPath));

            for (int fileIndex = 0; fileIndex < files.Count; fileIndex++)
            {
                if (files[fileIndex].Equals(_decompressedPath + "/json.text"))
                {
                    files.RemoveAt(fileIndex);
                }
            }

            FileNames = files;

            if (Report == 1)
            {
                if (File.Exists(_compressedPath))
                {
                    File.Delete(_compressedPath);
                }
            }
        }
        catch (Exception e)
        {
            ErrorReport = e.ToString();
        }
		
		ThreadRunning = false;
		ThreadCompleted = true;
	}
}