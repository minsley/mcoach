﻿using System.IO;
using System.Net;
using System;
using System.Diagnostics;

/// <summary>
/// Class manages a new call to the server and stores the server's response.
/// This was made to be called in a separate thread.
/// </summary>
public class DownloadFileInThread
{
    /// <summary>
    /// Stores true if the StartCall method has not completed.
    /// </summary>
    public volatile bool ThreadRunning = false;
	public volatile bool ThreadStarted = false;
	public volatile bool ThreadCompleted = false;

    /// <summary>
    /// Stores true if the StartCall method has not completed.
    /// </summary>
    public volatile bool LocalFileExists = false;

    /// <summary>
    /// When set to true the thread will exit before attempting to 
    /// retry downloading the file.
    /// </summary>
    public volatile bool SkipRetries = false;

	/// <summary>
	/// Set to true if the thread tried to download the file too many times (exceeded
	/// MaxRetries)
	/// </summary>
	public volatile bool TooManyAttempts = false;
	public volatile int retryCount = 0;

	public volatile string error = "";

    /// <summary>
    /// URL location of payload file.
    /// </summary>
    private string _url;

    /// <summary>
    /// Location of downloaded file on HD.
    /// </summary>
    private string _localFileLocation;

    /// <summary>
    /// Initializes class, requires json data or payload token to be pushed to the server.
    /// </summary>
    /// <param name="data">Data to be pushed to the server</param>
    /// <param name="isLogin"></param>
    public DownloadFileInThread(string URL, string LocalFileLocation)
    {
        _url = URL;
        _localFileLocation = LocalFileLocation;
    }

    /// <summary>
    /// Starts up the web call.
    /// </summary>
    /// <param name="isLogin"></param>
    public void DownloadURL()
    {
        ThreadRunning = true;     
		ThreadStarted = true;
		ThreadCompleted = false;

		TooManyAttempts = false;
        LocalFileExists = false;
        SkipRetries = false;

        var msg = "";
		var retries = FileSystemController.maxRetries;

        while (!LocalFileExists && retries > 0 && !SkipRetries)
        {

            try
            {
                var client = new WebClient();
                client.DownloadFile(_url, _localFileLocation);
                LocalFileExists = File.Exists(_localFileLocation);
            }
            catch (Exception e)
            {
                msg = e.Message;
				error += msg+"\n"; // concatenate so we can see how many failures there were
                Debug.WriteLine("DownloadURL()->" + msg);
				msg = "";
            }

			//this is terrible code...
			var waitStart = DateTime.Now;
			while((DateTime.Now - waitStart).TotalMilliseconds <= 2000){
				if(SkipRetries){
					break;
				}
			}

            retries--;
			retryCount++;
        }        
		TooManyAttempts = (retries <= 0);
        ThreadRunning = false;
		ThreadCompleted = true;
    }
}