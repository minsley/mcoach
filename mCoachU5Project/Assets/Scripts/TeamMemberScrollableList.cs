﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Tools;
using MCoach;

public class TeamMemberScrollableList : MonoBehaviour
{
/// <summary>
    /// Prefab of the UI object to be duplicated
    /// </summary>
	public GameObject ItemPrefab;
	public bool FireTest;
	public bool Vertical;
    /// <summary>
    /// Number of items to be dynamically created for testing purposes
    /// </summary>
	private int _itemCount = 0;
    /// <summary>
    /// Number of columns
    /// </summary>
	private int _columnCount = 1;
	
	private RectTransform _scrollRectTransform;
	private RectTransform _containerRectTransform;
	private RectTransform _rowRectTransform;
	public GameObject ListPanel;
	public GameObject FCRView; //Set this to inactive when we are displaying
	private List<GameObject> _spawnedItems = new List<GameObject>();
	private bool _instantiated = false;
	private float _width;
	private float _ratio;
	private float _height = 0;
	private float _startingHeight;
	/// <summary>
	/// The currently selected team member
	/// </summary>
	private TeamMember _csTeamMember = null;
	private Manifest _lastManifestReceived = null;

		
	
	public void Awake()
	{
		if (_instantiated) return;
		_instantiated = true;

        GlobalMessageManager.AddListener(Constants.Events.TeamCoachingSkillsEventId.ToString(), OnTeamCoachingSkills);
		GlobalMessageManager.AddListener(Constants.Events.ManifestUpdatedEventId.ToString(), OnManifestUpdated);
		GlobalMessageManager.AddListener(Constants.Events.TeamMemberSelectedEventId .ToString(), OnTeamMemberSelected);

		_scrollRectTransform = transform.parent.gameObject.GetComponent<RectTransform>();
		_rowRectTransform = ItemPrefab.GetComponent<RectTransform>();
		_containerRectTransform = gameObject.GetComponent<RectTransform>();
	}
	
	void Start()
	{
		// Start is not called on game objects that are inactive
        //calculate the width and height of each child item.
		_width = _containerRectTransform.rect.width / _columnCount;
		_ratio = _width / _rowRectTransform.rect.width;
		_height = _rowRectTransform.rect.height * _ratio;
	}

	private void OnManifestUpdated(string EventName, ref object Data){
		// this one might work since the manifest needs to be in place before a team member can be selected
		_lastManifestReceived = (Manifest)Data;
	}

	private void OnTeamMemberSelected(string EventName, ref object Data){
		_csTeamMember = (TeamMember)Data;
	}

	private void OnTeamCoachingSkills(string EventName, ref object Data)
	{
		object[] DataObjects = Data as object[];
		_csTeamMember = (TeamMember)DataObjects [0];

		// start is not called before the first time this is needed..
		_width = _containerRectTransform.rect.width / _columnCount;
		_ratio = _width / _rowRectTransform.rect.width;
		_height = _rowRectTransform.rect.height * _ratio;
		if (ListPanel != null)
			ListPanel.SetActive (true);
		if (FCRView != null)
			FCRView.SetActive (false); // Hide the FCR Panel, but we share the player info area

		for (int spawnedIndex = 0; spawnedIndex < _spawnedItems.Count; spawnedIndex++)
		{
			Destroy(_spawnedItems[spawnedIndex]);
		}
		
		_spawnedItems.Clear();
		_itemCount = 0;
		// reset the scrollview

		
		CoachingSkillCategory[] CS = (CoachingSkillCategory[]) DataObjects[1];
		
		for (int coachingIndex = 0; coachingIndex < CS.Length; coachingIndex++)
		{
			if (CS[coachingIndex].IsActive)
				AddInstance(CS[coachingIndex]);
		}
		_scrollRectTransform.gameObject.GetComponent<ScrollRect> ().verticalNormalizedPosition = 1;
	}
	
	private void AddInstance(CoachingSkillCategory coachingSkillCategory)
	{
		//  Debug.Log("Coaching Skill Title: " + coachingSkillCategory.Title);

		var startingCount = _spawnedItems.Count;
		_itemCount += 1;
		int rowCount;
		if (Vertical)
		{
			rowCount = 1;
			_columnCount = _itemCount;
		}
		else
		{
			rowCount = _itemCount / _columnCount;
		}
		if (_itemCount % rowCount > 0)
			rowCount++;
		
        //adjust the height of the container so that it will just barely fit all its children
		float scrollHeight = _height * rowCount;
//		Debug.Log(_scrollRectTransform.rect.height + " : " + scrollHeight);
		if (_scrollRectTransform.rect.height < scrollHeight)
		{
			float offset = scrollHeight - _scrollRectTransform.rect.height;
			_containerRectTransform.offsetMin = new Vector2(_containerRectTransform.offsetMin.x, -offset);
             //_containerRectTransform.offsetMax = new Vector2(_containerRectTransform.offsetMin.x, offset);  
		}
		
		int j = 0;//_itemCount - NewInstances;
		for (int i = 0; i < _itemCount; i++)
		{   
            //this is used instead of a double for loop because itemCount may not fit perfectly into the rows/columns
			if (i % _columnCount == 0)
				j++;
			
			GameObject newItem; // = (i < startingCount) ? _spawnedItems[i] : Instantiate(ItemPrefab) as GameObject;
			if (i < startingCount)
			{
				newItem = _spawnedItems[i];
			}
			else
			{
				newItem = Instantiate(ItemPrefab) as GameObject;
				
				var coachingInstance = newItem.GetComponent<CoachingSkillInstance>();
				// lets see if there is an existing rating for this ride, and if so show the edit instead of add icon
				CoachingSkill[] coachingSkills = new CoachingSkill[0];
				if (_csTeamMember != null &&
				    _lastManifestReceived != null && 
				    _lastManifestReceived.GetUnsubmittedFcr(_csTeamMember.Id) != null){
					coachingSkills = _lastManifestReceived.GetUnsubmittedFcr(_csTeamMember.Id).CoachingSkills.ToArray();
				}
				coachingInstance.SetCoachingSkill(coachingSkillCategory, true, coachingSkills);
				SellingSkillInfo info = null;
				if (_csTeamMember != null){

					Dictionary<int, SellingSkillInfo> dict = MCoachModel.GetManifest().GetSkillRatingHistory(_csTeamMember.Id);
					if (dict.ContainsKey(coachingSkillCategory.ID))
						info = dict[coachingSkillCategory.ID];

					bool useBarGraph = false;
//					if (DrilldownController.GetTopManifest().UserInfo.LevelNumber == DrilldownController.GetTopManifest().MaxLevels)
//						useBarGraph = false; // only show line graph to reps

					coachingInstance.DisplayRatings(info, useBarGraph);
				}
			}
			
            //create a new item, name it, and set the parent
			if (i >= startingCount)
				_spawnedItems.Add(newItem);
			
			newItem.name = gameObject.name + " item at (" + i + "," + j + ")";
			newItem.transform.SetParent(gameObject.transform);
			newItem.transform.localScale = new Vector3(1,1,1); // compensate for the reparenting scale effect
/*			
            //move and size the new item
			RectTransform rectTransform = newItem.GetComponent<RectTransform>();
			
			float x = -_containerRectTransform.rect.width / 2 + _width * (i % _columnCount);
			float y = _containerRectTransform.rect.height / 2 - _height * j;
			rectTransform.offsetMin = new Vector2(x, y);
			
			x = rectTransform.offsetMin.x + _width;
			y = rectTransform.offsetMin.y + _height;
			rectTransform.offsetMax = new Vector2(x, y);
*/
		}
	}
}
