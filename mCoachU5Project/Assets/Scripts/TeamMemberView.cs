﻿using Assets.Scripts.Tools;
using UnityEngine;
using UnityEngine.UI;

public class TeamMemberView : MonoBehaviour
{
	//public RawImage ProfileImage1, ProfileImage2;
	public Text Name, CSName, RatingName;
    public Text Territory;
    public Text Email;
    public Text Date;
    public Text Location;
    public Text Phone;

    private void Awake()
	{
		//GlobalMessageManager.AddListener("TeamMemberProfileImage", OnTeamMemberProfileImage);
        GlobalMessageManager.AddListener(Constants.Events.TeamMemberNameEventId.ToString(), OnTeamMemberName);
        GlobalMessageManager.AddListener(Constants.Events.TeamMemberTerritoryEventId.ToString(), OnTeamMemberTerritory);
        GlobalMessageManager.AddListener(Constants.Events.TeamMemberEmailEventId.ToString(), OnTeamMemberEmail);
        GlobalMessageManager.AddListener(Constants.Events.TeamMemberDateEventId.ToString(), OnTeamMemberDate);
        GlobalMessageManager.AddListener(Constants.Events.TeamMemberLocationEventId.ToString(), OnTeamMemberLocation);
        GlobalMessageManager.AddListener(Constants.Events.TeamMemberPhoneEventId.ToString(), OnTeamMemberPhone);
    }
	
	//public void OnTeamMemberProfileImage(string EventName, ref object Data)
	//{
	//	ProfileImage1.texture = (Texture2D)Data;
	//	ProfileImage2.texture = (Texture2D)Data;
	//}

    public void OnTeamMemberName(string EventName, ref object Data)
    {
        Name.text = (string)Data;
    }
	
	public void OnTeamMemberTerritory(string EventName, ref object Data)
    {
        Territory.text = (string)Data;
    }
	
	public void OnTeamMemberEmail(string EventName, ref object Data)
    {
        Email.text = (string)Data;
    }
	
	public void OnTeamMemberDate(string EventName, ref object Data)
    {
        Date.text = (string)Data;
    }
	
	public void OnTeamMemberLocation(string EventName, ref object Data)
    {
        Location.text = (string)Data;
    }
	
	public void OnTeamMemberPhone(string EventName, ref object Data)
    {
        Phone.text = (string)Data;
    }
}