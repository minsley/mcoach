﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using MCoach;
using Assets.Scripts.Tools;

// assume an instance of this class is placed on the item template for each item.  The template will be appropriate for the
// class of item this
// when an editing popup is required (for text type), point the TeamMemberFCRView.editingFCRItem to this instance, so the popup "SAVE" button
// can be routed back to this instance from the View.

public class FCRItemInstance : MonoBehaviour 
{

	public bool CanSubmitFCR{ // shortcut to check if FCRSubmit button can be shown
		get{
			//TODO special case for checkbox == true
			bool whatIsTruth = true;
			if (myItem.ItemType == FCRItemType.checkbox && myItem.Required)
				return (myItem.ItemValue == whatIsTruth.ToString());
			return (!myItem.Required || myItem.ItemValue != "");
		}
	}

	// link these fields in the editor, in the template for each item type
	public Text headerText = null; // label
	public Text bodyText = null; // description
	public Text valueText = null; // this will be used for both dropdown and text types
	public Text requiredText = null;
	public GameObject editButton = null;
	Color valueTextOriginalColor;
	public Toggle checkbox = null;
	public RectTransform dropdownContainer = null; // the content child of a panel with a size fitter
	public GameObject dropdownItemTemplate = null; // instantiate and reparent these to build dropdown list.  Has a child named "text"
	public GameObject closeDropdownButton = null;
	public GameObject editingPopup = null; // Popup calls save method on TeamMemberFCRView
	public Text editingPopupTitleText = null;
	public Text editingPopupPromptText = null;
	public Text editingPopupPlaceholderText = null;
	public InputField editingPopupInputFieldText = null;
	public Text editingPopupSaveButtonLabel = null;
	//public Text asterisk;


	public FCRItem myItem = null;
	float buttonHeight = 0;

	// carried over from FCRIndexInstance, maybe we need these, maybe not...
	public FCR myFCR = null; // populate this as you build the index
	public TeamMember myTeamMember = null;	// used by the reminders popup to select team member for viewing this FCR from reminders
	public TeamMemberFCRView fcrView ; // the component that will show my FCR when clicked. set this in the editor in the template for the button
	private UIEvent _viewFCR = new UIEvent(Constants.Events.ViewFCREventID.ToString());
	private UIEvent _selectTeamMember = new UIEvent(Constants.Events.TeamMemberSelectedEventId.ToString());

	public void Awake()
	{
		GlobalMessageManager.AddEvent (_viewFCR);
		GlobalMessageManager.AddEvent (_selectTeamMember);
		if (valueText != null)
			valueTextOriginalColor = Color.black; //valueText.color;
		//asterisk.text = "";
		//asterisk.text = "*";
		//asterisk.color = Color.red;
	}
	public void Start() {

//		asterisk.text = "";
		//asterisk.text = "";
		//Debug.Log (asterisk);
		//asterisk.color = Color.red;
	}

	public void OnClick(){
		if (myTeamMember != null) { // be sure the current team member is known...
			object teamMemberRef = (object)myTeamMember;
			_selectTeamMember.FireEvent(ref teamMemberRef);
		}
		if (myFCR != null && fcrView != null) { // maybe we dont need the fcrView component on this object any more
			object data = (object)myFCR;
			_viewFCR.FireEvent(ref data);
		}
	}

	// click handlers that pass the appropriate object for editing on to the fcrView

	public void OnEditItem(){
		// Set the member on the TeamMemberFCRView so the callback knows who the popup is editing
		TeamMemberFCRView.editingFCRItem = this;
		// Initialize the editing popup fields
		// find the fields in the popup, initialize them, set the edit flags in the popup, and turn it on...
		if (editingPopup != null) {
			editingPopupTitleText.text = "Add "+myItem.ItemHeader;
			editingPopupPromptText.text = myItem.ItemHeader +" Text:";
			editingPopupPlaceholderText.text = "Enter "+myItem.ItemHeader+" Text Here";
			editingPopupInputFieldText.text = myItem.ItemValue;
			editingPopupSaveButtonLabel.text = "Save "+myItem.ItemHeader;
			editingPopup.SetActive(true);
		}
	}

	public void OnSaveText(){
		// called from TeamMemberFCRView when the popup calls save
		// get the text from the popup
		myItem.ItemValue = editingPopupInputFieldText.text;
		valueText.text = myItem.ItemValue;
		//headerText.text = myItem.ItemHeader; //

		if (myItem.ItemValue == "" && myItem.Required) {
			requiredText.gameObject.SetActive (true);
			//headerText.text = myItem.ItemHeader + "  (required) ";
			//valueText.text = "*   "; //
			//valueText.color = Color.red;
			//valueText.fontSize = 20;
		} else {
			requiredText.gameObject.SetActive (false);
			//valueText.color = valueTextOriginalColor;
			//headerText.text = myItem.ItemHeader;
		}
		fcrView.UpdateSubmitButtonVisibility ();

	}

	public void PopulateDropdown(){
		
		// here, we have to build a container full of buttons populated to look like the dropdown items in the FCRItem
		foreach (string optionValue in myItem.DropdownValues) {
			GameObject newItemButton = Instantiate (dropdownItemTemplate) as GameObject;
			newItemButton.transform.SetParent (dropdownContainer.transform); // this totally messes plays with transform values...
			newItemButton.transform.localScale = new Vector3 (1, 1, 1); // compensate for the reparenting scale effect
			newItemButton.SetActive(true);
			Text itemText = newItemButton.transform.FindChild("Text").GetComponent<Text>();
			itemText.text = optionValue;
			if (optionValue == myItem.ItemValue){
				// highlight current value
				Button newButton = newItemButton.GetComponent<Button>();
				ColorBlock buttonColors = newButton.colors;
				Color backgroundColor = MainMenuView.hexToColor(MCoachModel.GetManifest().PrimaryColor);
				// pastel this so black lettering shows better
				//backgroundColor = new Color((backgroundColor.r+1/2f),(backgroundColor.g+1/2f),(backgroundColor.b+1/2f),1.0f);
				// or choose a contrasting foreground color
				itemText.color = TeamMemberInstance.ForegroundColor(backgroundColor);
				buttonColors.normalColor = backgroundColor;
				newButton.colors = buttonColors;
			}
		}

		if (!myItem.Required) {
			// include an empty option
			GameObject emptyItemButton = Instantiate (dropdownItemTemplate) as GameObject;
			emptyItemButton.transform.SetParent (dropdownContainer.transform); // this totally messes plays with transform values...
			emptyItemButton.transform.localScale = new Vector3 (1, 1, 1); // compensate for the reparenting scale effect
			emptyItemButton.SetActive (true);
			Text emptyItemText = emptyItemButton.transform.FindChild ("Text").GetComponent<Text> ();
			emptyItemText.text = "< No Selection >";

			if ("" == myItem.ItemValue){
				// highlight current value
				Button newButton = emptyItemButton.GetComponent<Button>();
				ColorBlock buttonColors = newButton.colors;
				Color backgroundColor = MainMenuView.hexToColor(MCoachModel.GetManifest().PrimaryColor);
				// pastel this so black lettering shows better
				//backgroundColor = new Color((backgroundColor.r+1/2f),(backgroundColor.g+1/2f),(backgroundColor.b+1/2f),1.0f);
				// or choose a contrasting foreground color
				emptyItemText.color = TeamMemberInstance.ForegroundColor(backgroundColor);
				buttonColors.normalColor = backgroundColor;
				newButton.colors = buttonColors;
			}
		}

		// Make the dropdown containter the last sibling of it's parent so it is drawn on top.  This only works because
		// We are not using Unity's vertical layout group for positioning.  If that were to change, some other scheme will be needed here
		dropdownContainer.transform.parent.SetAsLastSibling();

		// add some height to the FCR View container just in case it isnt high enough to allow scrolling the full dropdown
		buttonHeight = dropdownItemTemplate.transform.GetComponent<RectTransform> ().rect.height * myItem.DropdownValues.Length;
		buttonHeight -= transform.GetComponent<RectTransform> ().rect.height;
		fcrView.ResizeViewContainer (buttonHeight);
	}

	public void OnDropdownSelect(GameObject textGameObject){  


		// Get the text from the item whose button was pushed...
		if (textGameObject != null) {
			Text textComponent = textGameObject.GetComponent<Text> ();
			if (textComponent != null)
				myItem.ItemValue = textComponent.text;
		}
		headerText.text = myItem.ItemHeader;
		if (myItem.ItemValue == "" && myItem.Required) {
			requiredText.gameObject.SetActive(true);
			valueText.text = "< No Selection >"; //
			//valueText.color = Color.red;
			//valueText.fontSize = 20;
		} else {
			valueText.text = myItem.ItemValue;
			requiredText.gameObject.SetActive(false);
			//valueText.color = valueTextOriginalColor;

		}
		fcrView.UpdateSubmitButtonVisibility ();

		// hide the dropdown, by deleting all it's buttons
		List<GameObject> toDelete = new List<GameObject>();
		foreach (Transform t in dropdownContainer.transform) {
			if (t.gameObject.activeSelf) // don't delete the template
				toDelete.Add(t.gameObject);
		}
		foreach (GameObject g in toDelete)
			DestroyImmediate (g as Object);

		if (closeDropdownButton != null)
			closeDropdownButton.SetActive (false);

		fcrView.ResizeViewContainer (-buttonHeight);
		buttonHeight = 0;
	}

	public void OnCheckboxChanged(){
		myItem.ItemValue = checkbox.isOn.ToString();
		bool whatIsTruth = true;
		//headerText.text = myItem.ItemHeader;
		if (myItem.Required && myItem.ItemValue != whatIsTruth.ToString()) {
			requiredText.gameObject.SetActive(true);
			//headerText.text = myItem.ItemHeader + "  (required) " ;
			//valueText.text = "*   "; //
			//valueText.color = Color.red;
			//valueText.fontSize = 20;
		} else {
			requiredText.gameObject.SetActive(false);
			//valueText.text = "";
			//headerText.text = myItem.ItemHeader;
		}
		fcrView.UpdateSubmitButtonVisibility ();
	}

	public void PopulateFields(FCRItem theItem, bool allowEdit){ //


		if (theItem == null)
			return;

		myItem = theItem;
		// assume we are attached to a template of the appripriate type and our members are linked to template fields
		if (headerText != null)
			headerText.text = myItem.ItemHeader;

		// supress display of body text to rep level logins if so configured.
		if (bodyText != null){
			if (myItem.DisplayBodyToRep || MCoachModel.GetManifest().UserInfo.LevelNumber < MCoachModel.GetManifest().MaxLevels) {
				bodyText.text = myItem.ItemBody;
			} else {
				bodyText.text = "";
			}
		}
		if (valueText != null) {
			if (myItem.Required){
				if (myItem.ItemType == FCRItemType.checkbox){
					bool whatIsTruth = true;

					if (checkbox != null){
						checkbox.isOn = myItem.ItemValue == whatIsTruth.ToString();
					}

					if (myItem.ItemValue != whatIsTruth.ToString()) {
						
						requiredText.gameObject.SetActive(true);;
		
						//valueText.text = "*   "; //
						//valueText.color = Color.red;
						//valueText.fontSize = 20;
					} else {
						requiredText.gameObject.SetActive(false);
						//valueText.text = "";
						//valueText.color = valueTextOriginalColor;
					}
				}else{
					valueText.text = myItem.ItemValue;
					if (myItem.ItemValue == "") {
						requiredText.gameObject.SetActive(true);
						//headerText.text = myItem.ItemHeader + "  (required) " ;
						 //
						//valueText.color = Color.red;
						//valueText.fontSize = 20;
					} else {
						requiredText.gameObject.SetActive(false);
						//valueText.text = myItem.ItemValue;
						//valueText.color = valueTextOriginalColor;
					}
				}
			}
			else{ // not a required field
				requiredText.gameObject.SetActive(false);
				if (myItem.ItemType == FCRItemType.checkbox){
					bool whatIsTruth = true;
					if (checkbox != null){
						checkbox.isOn = myItem.ItemValue == whatIsTruth.ToString();
					}
					valueText.text = "";

				}else{
					valueText.text = myItem.ItemValue;
				}
			}
		}

		if (editButton != null)
			editButton.SetActive (allowEdit);

		switch(myItem.ItemType){
		case FCRItemType.checkbox:
			if (checkbox != null){
				checkbox.interactable = allowEdit;
			}
			break;
		case FCRItemType.dropdown:

			break;
		case FCRItemType.text:

			break;
		}

	}
}