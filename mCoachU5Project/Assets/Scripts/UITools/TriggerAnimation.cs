﻿using UnityEngine;

public class TriggerAnimation : MonoBehaviour
{
	public Animator MainAnimator;
    public void FireAnimation(string AnimationName)
    {
        MainAnimator.enabled = true;
        MainAnimator.Play(AnimationName);
    }
}