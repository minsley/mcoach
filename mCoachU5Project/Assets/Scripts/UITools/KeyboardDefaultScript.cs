﻿using UnityEngine;
using UnityEngine.UI;
using MCoach;

/// <summary>
/// Handles the touchscreen keyboard.
/// </summary>
public class KeyboardDefaultScript : MonoBehaviour
{
    /// <summary>
    /// True if keyboard is already activated for a different
    /// onscreen ui input.
    /// </summary>
    public static bool Active = false;
    /// <summary>
    /// References the touchscreen keyboard instance.
    /// </summary>
    private TouchScreenKeyboard _keyboard;
    /// <summary>
    /// References text item where keyboard input should be
    /// displayed.
    /// </summary>
    public Text InputText;
    /// <summary>
    /// References the animator responsible to change
    /// state if the keyboard is triggered. NOT REQUIRED.
    /// </summary>
    public Animator AnimationManager;
    /// <summary>
    /// The event to be fired when keyboard input
    /// is received.
    /// </summary>
    private UIEvent _keyboardInputEvent;
    /// <summary>
    /// The name of the state to be triggered when the keyboard
    /// is displayed.
    /// </summary>
    public string AnimationName;
    /// <summary>
    /// The default text of the InputText element.
    /// </summary>
    public string StartingText;
    /// <summary>
    /// Name of the event to be triggered when text input is received.
    /// </summary>
    public string TextChangeEventName;
    public string TestText;
    public Color StartColor, TextColor;
    public bool FireEvent;
    private string _defaultText;
    /// <summary>
    /// Stores the last received text from the keyboard.
    /// </summary>
    private string _lastKnownText = "";
    /// <summary>
    /// Flags if this instance of KeyboardScript's keyboard
    /// is active.
    /// </summary>
    private bool _keyboardActive;
    /// <summary>
    /// True if script has been properly initialized.
    /// </summary>
    private bool _initialized;

    /// <summary>
    /// Triggered once when the script is activated.
    /// </summary>
    private void Awake()
    {
        if (_initialized) return;
        _initialized = true;
        _keyboardInputEvent = new UIEvent(TextChangeEventName);
        GlobalMessageManager.AddEvent(_keyboardInputEvent);
        _defaultText = InputText.text;
    }

    /// <summary>
    /// Triggered when user has selected an input element.
    /// </summary>
    public void OnEvent()
    {
        if (Active) return;
        TouchScreenKeyboard.hideInput = true;
        
        //No text has been entered
        if (InputText.text.Equals(StartingText) || InputText.text.Equals(string.Empty))
        {
            _keyboard = TouchScreenKeyboard.Open("", TouchScreenKeyboardType.Default, false, false, false, false, StartingText);
            _lastKnownText = _keyboard.text;
           InputText.color = TextColor;
        }
        else //User selects a partially filled text element.
        {
            _keyboard = TouchScreenKeyboard.Open(InputText.text, TouchScreenKeyboardType.Default, false, false, false, false);
            _keyboard.text = InputText.text;
            _lastKnownText = _keyboard.text;
        }

        if (AnimationManager != null)
        {
            AnimationManager.enabled = true;
            AnimationManager.Play(AnimationName);
        }

        _keyboardActive = true;
        Active = true;
    }

    /// <summary>
    /// Fires every frame. Validates Text element to
    /// make sure it is displaying keyboard input.
    /// </summary>
    public void Update()
    {
        if (FireEvent)
        {
            FireEvent = false;
            object refText = TestText;
            _keyboardInputEvent.FireEvent(ref refText);
        }
        if (!_keyboardActive) return;
        
        if (_lastKnownText != _keyboard.text)
        {
            object newText = _keyboard.text;
            _keyboardInputEvent.FireEvent(ref newText);
            _lastKnownText = _keyboard.text;
        }
        
        InputText.text = _keyboard.text;

        if (!_keyboard.active)
        {
            if (_keyboard.text.Equals(string.Empty) || _keyboard.text.Equals(StartingText))
            {
                InputText.text = StartingText;
            }
	        //if (AnimationManager != null)
	        //{
	            //AnimationManager.Play(AnimationName + "Back");
        //}
            _keyboardActive = false;
            Active = false;
        }
    }
    
    void OnDestroy()
    {
        GlobalMessageManager.RemoveEvent(_keyboardInputEvent);
    }
}