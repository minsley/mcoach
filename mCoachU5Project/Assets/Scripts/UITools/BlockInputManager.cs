﻿using Assets.Scripts.Tools;
using UnityEngine;

public class BlockInputManager : MonoBehaviour
{
    public GameObject InputBlock;

    void Awake()
    {
        GlobalMessageManager.AddListener(Constants.Events.ShowLoadingEventId.ToString(), OnActivateBlocker);
        GlobalMessageManager.AddListener(Constants.Events.HideNativeUIEventId.ToString(), OnDisableBlocker);
		GlobalMessageManager.AddListener (Constants.Events.DownloadProgressEventId.ToString (), OnDownloadProgress);
	}
    
    void OnDestroy()
    {
        GlobalMessageManager.RemoveListener(Constants.Events.ShowLoadingEventId.ToString(), OnActivateBlocker);
        GlobalMessageManager.RemoveListener(Constants.Events.HideNativeUIEventId.ToString(), OnDisableBlocker);
		GlobalMessageManager.RemoveListener(Constants.Events.DownloadProgressEventId.ToString(), OnDownloadProgress);
    }

	private void OnDownloadProgress(string EventName, ref object Data){
		// while progress is incomplete, block input.
		float progress = (float)Data;
		
		if (progress < 1) {
			InputBlock.SetActive(true);
			
		} else if (progress == 1) {
			InputBlock.SetActive(false);
		}
	}

    private void OnActivateBlocker(string EventName, ref object Data)
    {
        InputBlock.SetActive(true);
    }
	
	private void OnDisableBlocker(string EventName, ref object Data)
    {
		InputBlock.SetActive(false);
	}
}