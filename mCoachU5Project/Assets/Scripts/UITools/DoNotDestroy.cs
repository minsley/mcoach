﻿using UnityEngine;

/// <summary>
/// <para>Date: 05/18/2015</para>
/// <para>Author: NuMedia</para>
/// <para>Script stops a GameObject from being destroyed during a screen transition.</para>
/// </summary>
public class DoNotDestroy : MonoBehaviour
{
    void Start()
    {
        DontDestroyOnLoad(this.transform);
    }
}