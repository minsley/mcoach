﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// <para>Date: 05/14/2015</para>
/// <para>Author: NuMedia</para>
/// <para>Scales a Text Object's font property.
/// Keeps the text size proportional with the object's size.</para>
public class CustomFontScaler : MonoBehaviour
{
    /// <summary>
    /// Other text elements to be set to the same fontsize as the
    /// text element this script is attached to.
    /// </summary>
    public Text[] ChildTextObjects;
    /// <summary>
    /// Default height of text object.
    /// </summary>
    public float DefaultHeight = 314.6455f;
    /// <summary>
    /// Default font size.
    /// </summary>
    public float DefaultFontSize = 12;
    /// <summary>
    /// Text element of the GameObject this script is attached to.
    /// </summary>
    private Text _text;
    /// <summary>
    /// The gameobject's recttransform.
    /// </summary>
    private RectTransform _rectTransform;
    /// <summary>
    /// Last known width.
    /// </summary>
    private float _lastWidth = 0;

    /// <summary>
    /// Gets the text and recttransform elements of gameobject script is attached to.
    /// </summary>
    void Awake()
    {
        _text = this.GetComponent<Text>();
        _rectTransform = this.GetComponent<RectTransform>();
    }

    /// <summary>
    /// When recttransform size changes, updates the latest script
    /// </summary>
    void Update()
    {
        if (_rectTransform.rect.height != _lastWidth)
        {
            float ratio = _rectTransform.rect.height / DefaultHeight;
            _text.fontSize = (int)(DefaultFontSize * ratio);
            _lastWidth = _rectTransform.rect.height;
            
            for (int textIndex = 0; textIndex < ChildTextObjects.Length; textIndex++)
            {
                ChildTextObjects[textIndex].fontSize = _text.fontSize;
            }
        }
    }
}