﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using MCoach;

public class DateWidget : MonoBehaviour {

	public string UIEventName = ""; // sends the new date string as payload of this event
	private UIEvent _dateChangedEvent; // 
	public bool initializeToTodayPlus = true;
	public int plusDays = 30;
	public string Date = "01/01/2015";
	public Text monthDisplayElement;
	public Text dayDisplayElement;
	public Text yearDisplayElement;
	public GameObject[] ScrollPanels;
	public System.DateTime DateTime = new System.DateTime(2015,1,1);
	public GameObject day29;
	public GameObject day30;
	public GameObject day31;

	private string _month = "01";
	public string Month
	{ 
		set{
			_month = value;
			EnableValidDays ();
			BuildDate ();
		}

		get{
			return _month;
		}
	}
	private string _day = "01";
	public string Day
	{ 
		set{
			_day = value;
			BuildDate ();
		}
		
		get{
			return _day;
		}
	}
	private string _year = "2015";
	public string Year
	{ 
		set{
			_year = value;
			BuildDate ();
		}
		
		get{
			return _year;
		}
	}

	public void SetMonth(string mm){
		Month = mm;
	}
	public void SetDay(string dd){
		Day = dd;
	}
	public void SetYear(string yyyy){
		Year = yyyy;
	}

	private void BuildDate(){
		// could handle formatting, etc here
		Date = Month + "/" + Day + "/" + Year;
		DateTime = new System.DateTime(int.Parse(Year),int.Parse (Month),int.Parse (Day));
		monthDisplayElement.text = Month;
		dayDisplayElement.text = Day;
		yearDisplayElement.text = Year;
		// hide the scroll panels
		foreach (GameObject g in ScrollPanels)
			g.SetActive (false);

		// notify someone who cares that the date has been set
		if (UIEventName != "") {
			object DateObject = (object)Date;
			_dateChangedEvent.FireEvent(ref DateObject);
		}
			//notifyObject.SendMessage (notifyMethod, (object)Date);
	}
	private void EnableValidDays(){
		// set active on final days of the month buttons...
		int DaysInMonth = System.DateTime.DaysInMonth (int.Parse (Year), int.Parse (Month));
		if (DaysInMonth > 28)
			day29.SetActive (true);
		else
			day29.SetActive (false);
		if (DaysInMonth > 29)
			day30.SetActive (true);
		else
			day30.SetActive (false);
		if (DaysInMonth > 30)
			day31.SetActive (true);
		else
			day31.SetActive (false);
		// dont allow invalid current date when setting month
		if (int.Parse (Day) > DaysInMonth)
			Day = DaysInMonth.ToString("D2");
	}

	// Use this for initialization
	void Start () {
		// initialize to today
		if (initializeToTodayPlus) {
			System.DateTime InitValue = System.DateTime.Today.AddDays(plusDays);
			Day = InitValue.Day.ToString ("D2");
			Month = InitValue.Month.ToString ("D2");
			Year = InitValue.Year.ToString ("D4");
		}
		if (UIEventName != "") {
			_dateChangedEvent = new UIEvent (UIEventName);
			GlobalMessageManager.AddEvent (_dateChangedEvent);
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
		
}
