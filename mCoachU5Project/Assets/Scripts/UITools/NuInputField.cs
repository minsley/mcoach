﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


	public class NuInputField : InputField {

	// this class was an attempt to extend the normal input field to allow touch to place edit point and
	// other enhancements.  since ios ignores assignment of caret position in unity 4.6, this class was not useful.
	// it may be helpful in unity 5, or maybe unity 5 will have a better behaved input field on its own

	public override void OnPointerDown (UnityEngine.EventSystems.PointerEventData eventData)
	{
		if (isFocused)
		{
			Vector2 mPos;
			RectTransformUtility.ScreenPointToLocalPointInRectangle(textComponent.rectTransform, eventData.position, eventData.pressEventCamera, out mPos);
			Vector2 cPos = GetLocalCaretPosition ();
			Debug.Log ("C:" + cPos + "  M:" + mPos);
		}

		int characterIndex = GetCharacterIndexFromPosition (eventData.position);
		caretPosition = textComponent.text.Length/2; //can we set this successfully? //characterIndex;

		base.OnPointerDown (eventData);
	}
	public Vector2 GetLocalCaretPosition ()
	{
		if (isFocused) 
		{
			TextGenerator gen = m_TextComponent.cachedTextGenerator;
			UICharInfo charInfo = gen.characters[caretPosition];
			float x = (charInfo.cursorPos.x + charInfo.charWidth) / m_TextComponent.pixelsPerUnit;
			float y = (charInfo.cursorPos.y) / m_TextComponent.pixelsPerUnit;
			return new Vector2(x, y);
		}
		else
			return new Vector2 (0f,0f);
	}
}
