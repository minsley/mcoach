﻿using UnityEngine;
using UnityEngine.UI;

public class TeamMemberColorLerp : MonoBehaviour
{
    public Color[] Colors;
    public float Percentage = 0;
    private Image _filledImage;

    private void Start()
    {
        _filledImage = GetComponent<Image>();
    }
	
	public void OnValueChanged(float percentage)
    {
        _filledImage.fillAmount = Percentage;
        
		// sets color based on current 1-10 scale. We will need to ultimately generalize this function to include a "rating scale with min/max
		if (percentage < 0.4) {
			_filledImage.color = new Color (0.898f, 0.047f, 0.102f, 1.0f);
		} else 
		if (percentage < 0.7) {
			_filledImage.color = new Color (0.862f, 0.815f, 0.0f, 1.0f);
		} else {
			_filledImage.color = new Color (0.192f, 0.690f, 0.129f, 1.0f);
		}	

		/* CJR - Disabled for BBraun
        if (percentage < .5f)
        {
            percentage = percentage * 2f;
            _filledImage.color = new Color(Colors[0].r - ((Colors[0].r - Colors[1].r) * percentage),
			    Colors[0].g - ((Colors[0].g - Colors[1].g) * percentage),
				Colors[0].b - ((Colors[0].b - Colors[1].b) * percentage));
        }
        else
        {
            percentage = (percentage - .5f) * 2f;
            _filledImage.color = new Color(Colors[1].r - ((Colors[1].r - Colors[2].r) * percentage),
			    Colors[1].g - ((Colors[1].g - Colors[2].g) * percentage),
				Colors[1].b - ((Colors[1].b - Colors[2].b) * percentage));
		}
		*/
	}
}