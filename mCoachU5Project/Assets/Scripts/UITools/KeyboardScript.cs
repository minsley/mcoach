﻿using UnityEngine;
using UnityEngine.UI;
using MCoach;

/// <summary>
/// Handles the touchscreen keyboard.
/// </summary>
public class KeyboardScript : MonoBehaviour
{
    /// <summary>
    /// True if keyboard is already activated for a different
    /// onscreen ui input.
    /// </summary>
    public static bool Active = false;
    /// <summary>
    /// References the touchscreen keyboard instance.
    /// </summary>
    private TouchScreenKeyboard _keyboard;
    /// <summary>
    /// References text item where keyboard input should be
    /// displayed.
    /// </summary>
    public Text InputText;
	public float caretOffset = 0.5f;
    /// <summary>
    /// References the animator responsible to change
    /// state if the keyboard is triggered. NOT REQUIRED.
    /// </summary>
    public Animator AnimationManager;
    /// <summary>
    /// The event to be fired when keyboard input
    /// is received.
    /// </summary>
    private UIEvent _keyboardInputEvent;
    public Color StartColor, EndColor;
    /// <summary>
    /// The name of the state to be triggered when the keyboard
    /// is displayed.
    /// </summary>
    public string AnimationName;
    /// <summary>
    /// The default text of the InputText element.
    /// </summary>
    public string StartingText;
    /// <summary>
    /// Name of the event to be triggered when text input is received.
    /// </summary>
    public string TextChangeEventName;
    public string TestText;
    public bool FireEvent;
    /// <summary>
    /// Stores the last received text from the keyboard.
    /// </summary>
    private string _lastKnownText = "";
    /// <summary>
    /// Flags if this instance of KeyboardScript's keyboard
    /// is active.
    /// </summary>
    private bool _keyboardActive;
    /// <summary>
    /// True if script has been properly initialized.
    /// </summary>
    private bool _initialized;

    /// <summary>
    /// Triggered once when the script is activated.
    /// </summary>
    private void Awake()
    {
        if (_initialized) return;
        _initialized = true;
        _keyboardInputEvent = new UIEvent(TextChangeEventName);
        GlobalMessageManager.AddEvent(_keyboardInputEvent);
    }

	private GameObject caret = null;

	void Start(){

	}
	
	/// <summary>
	/// Triggered when user has selected an input element.
    /// </summary>
    public void OnEvent()
    {
		// look for an input caret created for the text object, and fix it's vertical alignment since Unity has a ccurent bug about this
		// caused by font import size being used to calculate the caret vertical alignment
		string name = InputText.gameObject.name + " Input Caret";
		caret = InputText.transform.parent.Find(name).gameObject;
		if (caret != null) {
			RectTransform rt = caret.GetComponent<RectTransform> ();
			Vector2 pos = rt.pivot;
			pos.y = caretOffset; // this really needs to be calculated more carefullt
			rt.pivot = pos;
		}

		InputField myInputField = transform.GetComponentInChildren<InputField> ();
		if (myInputField != null) {
//			myInputField.caretWidth = 10; // Unity 5 ?
		}
		
		
        Debug.Log("SHOW KEYBOARD!");
        
        if (Active) return;
        TouchScreenKeyboard.hideInput = true;

        if (!StartingText.Equals(string.Empty))
        {
            InputText.color = EndColor;
        }

        //No text has been entered

        if (InputText.text.Equals(StartingText) || InputText.text.Equals(string.Empty))
        {
            _keyboard = TouchScreenKeyboard.Open("", TouchScreenKeyboardType.Default, false, false, false, false, StartingText);
            //if the keyboard isnt present we arent on a touch enabled device
            if (_keyboard == null) return;

            _lastKnownText = _keyboard.text;
        }
        else //User selects a partially filled text element.
        {          
            _keyboard = TouchScreenKeyboard.Open(InputText.text, TouchScreenKeyboardType.Default, false, false, false, false);
            if (_keyboard == null) return;

            _keyboard.text = InputText.text;
            _lastKnownText = _keyboard.text;
        }

        if (AnimationManager != null)
        {
            AnimationManager.enabled = true;
            AnimationManager.Play(AnimationName);
        }

        //_keyboardActive = true;
		Invoke ("SetKeyboardActive",0.51f); // delay until the animation completes
		Active = true;

    }

	void SetKeyboardActive(){
		_keyboardActive = true;
	}



    /// <summary>
    /// Fires every frame. Validates Text element to
    /// make sure it is displaying keyboard input.
    /// </summary>
    public void Update()
    {
        if (FireEvent)
        {
            FireEvent = false;
            object refText = TestText;
            _keyboardInputEvent.FireEvent(ref refText);
        }

		if (caret != null) {
			RectTransform rt = caret.GetComponent<RectTransform> ();
			Vector2 pos = rt.pivot;
			pos.y = caretOffset; // this really needs to be calculated more carefully, but doesnt work on the device.
			rt.pivot = pos;
		}

		if (!_keyboardActive) return;
/*        
        if (_lastKnownText != _keyboard.text)
        {
            object newText = _keyboard.text;
            _keyboardInputEvent.FireEvent(ref newText);
            _lastKnownText = _keyboard.text;
        }
        
        InputText.text = _keyboard.text;
*/
        if (!_keyboard.active && !Application.isEditor)
        {
//            if (_keyboard.text.Equals(string.Empty) || _keyboard.text.Equals(StartingText))
//           {
//                InputText.text = StartingText;
//                InputText.color = StartColor;
//            }
            /*if (AnimationManager != null)
            {
                AnimationManager.Play(AnimationName + "Back"); // in the editor, _keyboard.active is not going to be true... :/
            }*/
            _keyboardActive = false;
            Active = false;
        }
    }

	/*void OnDisable(){ // we may be disabled by a panel change from a button.
		if (Active && AnimationName != "" && AnimationManager != null) {
			AnimationManager.Play (AnimationName + "Back"); // in the editor, _keyboard.active is not going to be true... :/
			_keyboardActive = false;
			Active = false;
		}
	}*/

void OnDestroy()
{
	GlobalMessageManager.RemoveEvent(_keyboardInputEvent);
    }
}