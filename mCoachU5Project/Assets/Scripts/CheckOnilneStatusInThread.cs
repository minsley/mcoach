﻿using UnityEngine;
using System.Collections;

public class CheckOnilneStatusInThread
{
	/// <summary>
    /// Stores true if the StartCall method has not completed.
    /// </summary>
	public volatile bool ThreadRunning = false;
	public volatile bool ThreadStarted = false;
	public volatile bool ThreadCompleted = false;
	/// <summary>
    /// Stores true if application is currently online.
    /// </summary>
	public volatile bool IsOnline = false;
	
	/// <summary>
    /// This method is what should be called for Thread.Start().
    /// </summary>
	public void CheckStatus()
	{
		ThreadRunning = true;
		ThreadStarted = true;
		ThreadCompleted = false;
		
		System.Net.WebClient client = null;
		System.IO.Stream stream = null;
		
		try
		{
			client = new System.Net.WebClient();
			stream = client.OpenRead("https://google.com/");
			//object status = new object[]{true, Data};
			Debug.Log("IS ONLINE");
			//_onlineStatus.FireEvent(ref status);
		}
		catch (System.Exception ex)
		{
		//object status = new object[]{false, Data};
			Debug.Log("IS OFFLINE");
			//_onlineStatus.FireEvent(ref status);
		}
		finally
		{
			if (client != null) { client.Dispose(); }
			if (stream != null) { stream.Dispose(); }
		}
			
		ThreadRunning = false;
		ThreadCompleted = true;
	}
}