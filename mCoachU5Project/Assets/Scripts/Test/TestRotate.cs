﻿using UnityEngine;

public class TestRotate : MonoBehaviour
{

    // Update is called once per frame
    void Update()
    {
        float percentage = (Time.time % 5) / 5;
        this.transform.eulerAngles = new Vector2(percentage * 360f,0);
    }
}
