﻿using UnityEngine;
using System.Net;
using System.Text;
using System.IO;
using System.Threading;
using System.Collections;
using Assets.Scripts.Tools;

public class ThreadingTest : MonoBehaviour
{
    public bool stop = false;
    // Use this for initialization
    void Start()
    {
        StartCoroutine(ThreadedWebCall());
    }

    void Update()
    {
        if (Time.time > 20f && !stop)
        {
            Debug.Log("Stop thread!");
            stop = true;
        }
    }

    IEnumerator ThreadedWebCall()
    {
        Debug.Log("Start thread!");
        Test test = new Test();
        Thread thread = new Thread(test.StartThread);
        thread.Start();
        while (!stop || !test.HasComplete)
            yield return null;
        test.RunThread = false;
        thread.Abort();
        thread.Join();
        Debug.Log("Thread closed");
        Debug.Log("Response: " + test._responseFromServer);
    }
}

public class Test
{
    public volatile bool RunThread = true;
    public volatile bool HasComplete = false;
    public volatile string _responseFromServer = "";
    public void StartThread()
    {
        while (RunThread)
        {
            // Create a request using a URL that can receive a post. 
            WebRequest request = WebRequest.Create(WebServiceTools.SyncUrl);
            // Set the Method property of the request to POST.
            request.Method = "POST";
            // Create POST data and convert it to a byte array.
            string postData = "token=" + WebServiceTools.CallToken + "&data={}";
            byte[] byteArray = Encoding.UTF8.GetBytes(postData);
            // Set the ContentType property of the WebRequest.
            request.ContentType = "application/x-www-form-urlencoded";
            // Set the ContentLength property of the WebRequest.
            request.ContentLength = byteArray.Length;
            // Get the request stream.
            Stream dataStream = request.GetRequestStream();
            // Write the data to the request stream.
            dataStream.Write(byteArray, 0, byteArray.Length);
            // Close the Stream object.
            dataStream.Close();
            // Get the response.
            WebResponse response = request.GetResponse();
            // Get the stream containing content returned by the server.
            dataStream = response.GetResponseStream();
            // Open the stream using a StreamReader for easy access.
            StreamReader reader = new StreamReader(dataStream);
            // Read the content.
            _responseFromServer = reader.ReadToEnd();
            // Clean up the streams.
            reader.Close();
            dataStream.Close();
            response.Close();
            HasComplete = true;
        }
    }
}