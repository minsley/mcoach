﻿using Assets.Scripts.Tools;
using UnityEngine;
using MCoach;
using Newtonsoft.Json;

public class RunTest : MonoBehaviour
{
    public string Site = WebServiceTools.SyncUrl;
    public string Token = WebServiceTools.CallToken;
    public string ContentType = "application/x-www-form-urlencoded";
    public bool FireTest;
    private UIEvent _onlineStatus = new UIEvent(Constants.Events.OnlineStatusEventId.ToString());
    private UIEvent _loginWebCall = new UIEvent(Constants.Events.LoginWebCallEventId.ToString());

    void Start()
    {
        GlobalMessageManager.AddListener(Constants.Events.TestEventEventId.ToString(), OnTestEvent);
        //object test = new Manifest();
        //Debug.Log("Object type: " + test.GetType());
        //         Manifest manitest = new Manifest();
        //         Debug.Log("ID:? " + manitest.UserInfo.Id);
        //         GlobalMessageManager.AddEvent(_onlineStatus);
        //         GlobalMessageManager.AddEvent(_loginWebCall);
        //     	GlobalMessageManager.AddListener("OnlineStatusData", OnOnlineStatusData);
        //     	GlobalMessageManager.AddListener("LoginWebcallComplete", OnOnlineWebcallComplete);
        //ReceivedDataTest();
        //TestWebConnection();
        //OtherWebConnectionTest();
        //StartCoroutine(ThreadedWebCall(GetTestData()));
    }
    
    private void OnTestEvent(string eventname, ref object Data)
    {
        Debug.Log("Test event fired");
    }

    void Update()
    {
//         if (!FireTest && Time.time > 5f)
//         {
//             FireTest = true;
//             Debug.Log("Fire test!");
//             //_onlineStatus.FireEvent();
// //             object jsonData = "{}";
// //             _loginWebCall.FireEvent(ref jsonData);
//         }
    }
    
    private void OnOnlineStatusData(string EventName, ref object Data)
    {
        bool isOnline = (bool)Data;
        Debug.Log("Online status: " + isOnline);
        if (isOnline)
        {
            UserInfo userInfo = new UserInfo()
            {
                UserName = "teamleader",
                Password = "teamleader"
            };
            Manifest manifest = new Manifest()
            {
                UserInfo = userInfo
            };

            Debug.Log("Display loader!");
            object loaderText = "Logging In";
            //_displayLoader.FireEvent(ref loaderText);
            object jsonData = JsonConvert.SerializeObject(manifest);
            Debug.Log("Firing login web call");
            _loginWebCall.FireEvent(ref jsonData);
        }
        else
        {
            Debug.Log("Not online");
        }
    }
    
    private void OnOnlineWebcallComplete(string EventName, ref object Data)
    {
        Debug.Log("Online webcall complete!");
        string webReturnedData = (string)Data;
//         _hideNativeUI.FireEvent();
        webReturnedData = ParseOutXMl(webReturnedData);

        Manifest exampleManifest = new Manifest();
        Manifest receivedManifest = JsonConvert.DeserializeAnonymousType(webReturnedData, exampleManifest);

        if (receivedManifest != null)
        {
            Debug.Log("Result: " + receivedManifest.Result);

//             if (receivedManifest.UserInfo.UserName.Equals("teamleader")
//                     && receivedManifest.UserInfo.Password.Equals("teamleader"))
//             {
//                 Debug.Log("login to dashboard");
//                 object pushManifest = receivedManifest;
//                 //_login.FireEvent(ref pushManifest);
// //                 Application.LoadLevel("3Dashboard");
//             }
//             else if (receivedManifest.Result.Equals("invalid credentials:user_is_not_active"))
//             {
//                 Debug.Log("Invalid username");
//                 object uiInfo = new string[] { "Notice", "The entered username is not active", "OK" };
//                 //_nativeUICall.FireEvent(ref uiInfo);
//             }
//             else if (receivedManifest.Result.Equals("invalid credentials:user_is_not_active"))
//             {
//                 Debug.Log("Invalid password");
//                 object uiInfo = new string[] { "Notice", "Incorrect password", "OK" };
//                 //_nativeUICall.FireEvent(ref uiInfo);
//             }
        }
        else
        {
            object uiInfo = new string[] { "Notice", "Please login again", "OK" };
            //_nativeUICall.FireEvent(ref uiInfo);
        }
    }
    
    private string ParseOutXMl(string xmlJson)
    {
        string remove1 = "<string xmlns=\"http://enhancedecho.com/\">";
        string remove2 = "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
        string remove3 = "</string>";
        //string remove4 = "\n";
        xmlJson = xmlJson.Replace(remove1, "");
        xmlJson = xmlJson.Replace(remove2, "");
        xmlJson = xmlJson.Replace(remove3, "");
        //xmlJson = xmlJson.Replace(remove4, "");
        return xmlJson;
    }

//     IEnumerator ThreadedWebCall(string jsonData)
//     {
//         Debug.Log("Started threaded webcall!");
//         WebcallInThread webCall = new WebcallInThread(jsonData);
//         Thread webThread = new Thread(new ThreadStart(webCall.StartCall));
//         webThread.Start();
//         while (!webCall._threadRunning)
//         {
//             Debug.Log("Waiting to start");
//             yield return null;
//         }
//         Debug.Log("Web thread running!");
//         while (webCall._threadRunning)
//         {
//             Debug.Log("Webcall running");
//             yield return null;
//         }
//         Debug.Log("Returned string: " + webCall._responseFromServer);
//         Debug.Log("Error message: " + webCall._errorMessage);
//         webThread.Abort();
//         webThread.Join();
//     }

//     private void ReceivedDataTest()
//     {
//         string json = "{\"UserInfo\":{\"Id\":6,\"Name\":\"Team Leader\",\"UserName\":\"teamleader\",\"Password\":\"teamleader\",\"Email\":\"teamleader@aol.com\",\"District\":\"com1_dis1\",\"LastLoginDate\":\"5/18/2015 9:21 PM\",\"MyTeam\":[]},\"PayloadUrl\":\"\",\"Result\":\"ok\"}";
//         Manifest manifest = new Manifest();
//         Manifest received = JsonConvert.DeserializeAnonymousType(json, manifest);
//         Debug.Log("Result: " + received.Result);
//     }

//     private string GetTestData()
//     {
//         var user = new UserInfo();
//         user.Name = "teamleader";
//         user.UserName = "teamleader";
//         user.Password = "teamleader";
//         user.Email = "";
//         user.LastLoginDate = "";
// 
//         Manifest manifest = new Manifest();
//         manifest.UserInfo = user;
// 
//         string serializedJson = JsonConvert.SerializeObject(manifest);
//         return serializedJson;
//     }

//     private void TestWebConnection()
//     {
//         try
//         {
//             // Create a request using a URL that can receive a post. 
//             WebRequest request = WebRequest.Create(Site);
//             // Set the Method property of the request to POST.
//             request.Method = "POST";
//             // Create POST data and convert it to a byte array.
//             string postData = "token=" + Token + "&data=" + GetTestData();
//             byte[] byteArray = Encoding.UTF8.GetBytes(postData);
//             // Set the ContentType property of the WebRequest.
//             request.ContentType = "application/x-www-form-urlencoded";
//             // Set the ContentLength property of the WebRequest.
//             request.ContentLength = byteArray.Length;
//             // Get the request stream.
//             Stream dataStream = request.GetRequestStream();
//             // Write the data to the request stream.
//             dataStream.Write(byteArray, 0, byteArray.Length);
//             // Close the Stream object.
//             dataStream.Close();
//             // Get the response.
//             WebResponse response = request.GetResponse();
//             // Get the stream containing content returned by the server.
//             dataStream = response.GetResponseStream();
//             // Open the stream using a StreamReader for easy access.
//             StreamReader reader = new StreamReader(dataStream);
//             // Read the content.
//             string responseFromServer = reader.ReadToEnd();
//             // Display the content.
//             Debug.Log(responseFromServer);
//             // Clean up the streams.
//             reader.Close();
//             dataStream.Close();
//             response.Close();
//         }
//         catch (WebException webex)
//         {
//             WebResponse errResp = webex.Response;
//             using (Stream respStream = errResp.GetResponseStream())
//             {
//                 StreamReader reader = new StreamReader(respStream);
//                 string text = reader.ReadToEnd();
//                 Debug.Log(text);
//             }
//         }
//     }
}

// public class WebcallInThread
// {
//     private string _site = WebServiceTools.SyncUrl;
//     private string _token = WebServiceTools.CallToken; 
//     private string _contentType = "application/x-www-form-urlencoded";
//     private string _json = "";
//     public volatile string _responseFromServer = "";
//     public volatile string _errorMessage = "";
//     public volatile bool _threadRunning = false;
// 
//     public WebcallInThread(string json)
//     {
//         _json = json;
//     }
// 
//     public void StartCall()
//     {
//         _threadRunning = true;
//     	
//         try
//         {
//             // Create a request using a URL that can receive a post. 
//             WebRequest request = WebRequest.Create(_site);
//             // Set the Method property of the request to POST.
//             request.Method = "POST";
//             // Create POST data and convert it to a byte array.
//             string postData = "token=" + _token + "&data=" + _json;
//             byte[] byteArray = Encoding.UTF8.GetBytes(postData);
//             // Set the ContentType property of the WebRequest.
//             request.ContentType = "application/x-www-form-urlencoded";
//             // Set the ContentLength property of the WebRequest.
//             request.ContentLength = byteArray.Length;
//             // Get the request stream.
//             Stream dataStream = request.GetRequestStream();
//             // Write the data to the request stream.
//             dataStream.Write(byteArray, 0, byteArray.Length);
//             // Close the Stream object.
//             dataStream.Close();
//             // Get the response.
//             WebResponse response = request.GetResponse();
//             // Get the stream containing content returned by the server.
//             dataStream = response.GetResponseStream();
//             // Open the stream using a StreamReader for easy access.
//             StreamReader reader = new StreamReader(dataStream);
//             // Read the content.
//             _responseFromServer = reader.ReadToEnd();
//             // Clean up the streams.
//             reader.Close();
//             dataStream.Close();
//             response.Close();
//         }
//         catch (WebException webex)
//         {
//             WebResponse errResp = webex.Response;
//             using (Stream respStream = errResp.GetResponseStream())
//             {
//                 StreamReader reader = new StreamReader(respStream);
//                 string text = reader.ReadToEnd();
//                 _errorMessage = text;
//             }
//         }
//         
//         _threadRunning = false;
//     }
// }