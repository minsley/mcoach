using System.Text;
using System.IO;
using System.Net;
using Assets.Scripts.Tools;

public class PingWebCall
{
	/// <summary>
    /// Stores data received from the server.
    /// </summary>
    public volatile string ResponseFromServer = "";
    /// <summary>
    /// Store any error message.
    /// </summary>
    public volatile string ErrorMessage = "";
    /// <summary>
    /// Stores true if the StartCall method has not completed.
    /// </summary>
    public volatile bool ThreadStarted = false;
	public volatile bool ThreadRunning = false;
	public volatile bool ThreadCompleted = false;


	public bool RequestServerType = true;

    /// <summary>
    /// Address of the server.
    /// </summary>
    private string _site = WebServiceTools.PingUrl;

    /// <summary>
    /// Token required for validation.
    /// </summary>
    private string _token = WebServiceTools.CallToken;
    /// <summary>
    /// Stores the JSON data which will be pushed to the server.
    /// </summary>
    private string _email = "";
    /// <summary>
    /// Stores true if logging in
    /// </summary>
    private bool _login = false;
    
    public string Site
    {
        get { return _site; }
        set { _site = value; }
    }

    /// <summary>
    /// Method call to be triggered in thread. Specific for logging in.
    /// </summary>
    public void StartCall()
    {
		ThreadStarted = false;
		ThreadRunning = false;
		ThreadCompleted = false;
        StartWebCall(_login);
    }

    /// <summary>
    /// Starts up the web call.
    /// </summary>
    /// <param name="isLogin"></param>
    private void StartWebCall(bool isLogin)
    {
		ThreadStarted = true;
        ThreadRunning = true;
		ThreadCompleted = false;

		#if (!UNITY_IPHONE || UNITY_EDITOR)
		{
			ServicePointManager
				.ServerCertificateValidationCallback += 
					(sender, cert, chain, sslPolicyErrors) => true; //TODO Remove this, added to ignore certificate issues on the server for testing
		}
		#endif
    	
        try
        {
            // Create a request using a URL that can receive a post. 
            WebRequest request = WebRequest.Create(_site);
            // Set the Method property of the request to POST.
            request.Method = "POST";
            // Create POST data and convert it to a byte array.
			string postData =  "token=" + _token + "&data=" + _email; // legacy from whatever this class was cloned from
			if (RequestServerType) postData = "token=" + _token + "&data=server_type";
            
            byte[] byteArray = Encoding.UTF8.GetBytes(postData);
            // Set the ContentType property of the WebRequest.
            request.ContentType = "application/x-www-form-urlencoded";
            // Set the ContentLength property of the WebRequest.
            request.ContentLength = byteArray.Length;
            // Get the request stream.
            Stream dataStream = request.GetRequestStream();
            // Write the data to the request stream.
            dataStream.Write(byteArray, 0, byteArray.Length);
            // Close the Stream object.
            dataStream.Close();

			//request.Timeout = 5000; // ms  default timeout is 100 seconds
            // Get the response.
            WebResponse response = request.GetResponse();
            // Get the stream containing content returned by the server.
            dataStream = response.GetResponseStream();
            // Open the stream using a StreamReader for easy access.
            StreamReader reader = new StreamReader(dataStream);
            // Read the content.
            ResponseFromServer = reader.ReadToEnd();
            // Clean up the streams.
            reader.Close();
            dataStream.Close();
            response.Close();
        }
        catch (WebException webex)
        {
            WebResponse errResp = webex.Response;
			if (errResp != null){
            using (Stream respStream = errResp.GetResponseStream())
	            {
	                StreamReader reader = new StreamReader(respStream);
	                string text = reader.ReadToEnd();
	                ErrorMessage = text;
	            }
			}
			else{
				ErrorMessage = "null response from Server";
			}
        }
        
        ThreadRunning = false;
		ThreadCompleted = true;
    }
}