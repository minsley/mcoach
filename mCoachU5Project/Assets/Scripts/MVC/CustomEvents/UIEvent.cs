namespace MCoach
{
    /// <summary>
    /// Manages UI events.
    /// </summary>
    /// <param name="EventName"></param>
    /// <param name="Element"></param>  
    public delegate void MessageEvent(string EventName, ref object Data);

    public class UIEvent
    {
        /// <summary>
        /// References the listeners.
        /// </summary>
        public MessageEvent CustomEvent;
        /// <summary>
        /// The event name.
        /// </summary>
        private string _eventName = "";
        /// <summary>
        /// Data relating to the triggered event.
        /// </summary>
        private object _eventData = null;

        /// <summary>
        /// The name of the event.
        /// </summary>
        public string EventName
        {
            get { return _eventName; }
        }

        /// <summary>
        /// Instantiates the UIEvent.
        /// </summary>
        /// <param name="eventName"></param>
        public UIEvent(string eventName)
        {
            this._eventName = GetEventName(eventName);
        }

        /// <summary>
        /// Used for firing empty data. Do not use if passing actual data.
        /// </summary>
        public void FireEvent()
        {
            if (CustomEvent != null)
                CustomEvent.Invoke(_eventName, ref _eventData);
        }

        /// <summary>
        /// Only use to pass valid data through event.
        /// </summary>
        /// <param name="Data"></param>
        public void FireEvent(ref object Data)
        {
            if (CustomEvent != null)
                CustomEvent.Invoke(_eventName, ref Data);
        }

        /// <summary>
        /// Returns the formatted event name
        /// </summary>
        /// <param name="eventName"></param>
        /// <returns></returns>
        public static string GetEventName(string eventName)
        {
            eventName = eventName.Replace(" ", "");
            if (!eventName.EndsWith("EventId"))
            {
                eventName += "EventId";
            }

            return eventName;
        }
    }
}