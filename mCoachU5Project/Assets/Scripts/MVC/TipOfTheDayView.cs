﻿using Assets.Scripts.Tools;
using UnityEngine;
using UnityEngine.UI;
using MCoach;

public class TipOfTheDayView : MonoBehaviour 
{
    public Text DashboardTitle;
    public Text PopupTitle;
    public Text Description;
	public Text DashboardTODDate;
	public Text DashboardDescription;

    void Awake()
    {
//        Debug.Log("tip of the day initialized");

        GlobalMessageManager.AddListener(Constants.Events.TipOfTheDayTitleEventId.ToString(), OnTipOfTheDayTitle);
        GlobalMessageManager.AddListener(Constants.Events.TipOfTheDayDescriptionEventId.ToString(), OnTipOfTheDayDescription);
    }

    private void OnTipOfTheDayTitle(string EventName, ref object Data)
    {
//        Debug.Log("Tip of the day title received: " + Data);
        string tipOfTheDay = (string)Data;
        DashboardTitle.text = tipOfTheDay;
        PopupTitle.text = tipOfTheDay;
		Manifest _csManifest = MCoachModel.GetManifest ();
//		if (_csManifest != null && _csManifest.TipOfTheDayDate != null)
//			DashboardTODDate.text = ((System.DateTime)_csManifest.TipOfTheDayDate).DayOfWeek + ", " + ((System.DateTime)_csManifest.TipOfTheDayDate).Date.ToString ("MM/dd/yyyy");
//		else
			DashboardTODDate.text = System.DateTime.Now.DayOfWeek + ", " + System.DateTime.Now.Date.ToString ("MM/dd/yyyy");
    }
	
	private void OnTipOfTheDayDescription(string EventName, ref object Data)
    {
        Description.text = (string)Data;
		DashboardDescription.text = (string)Data;
    }
}