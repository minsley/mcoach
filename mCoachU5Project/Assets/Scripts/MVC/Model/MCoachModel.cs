﻿using System.Collections.Generic;
using Assets.Scripts.Tools;
using UnityEngine;
using MCoach;

/// <summary>
/// <para>Date: 07/02/2015</para>
/// <para>Author: NuMedia</para>
/// <para>Manages mCoach data</para>
/// </summary>
public class MCoachModel : MonoBehaviour
{
	private static MCoachModel _instance = null;
    /// <summary>
    /// Main manifest storing user info, team info, and other important information.
    /// </summary>
    private Manifest _manifest;
	private object refManifest; //try keeping this here as instance data to avoid possible garbage collection crash

	public static Manifest GetManifest(){
		if (_instance == null)
			return null;
		else
			return _instance._manifest;
	}

	public static string LastLoginDate = "Last Login:"; // this comes from user pref then gets updated...
    
    /// <summary>
    /// Stores the Manifest Updated event. This event should be triggered
    /// when a chnage has happened to the manifest.
    /// </summary>
    private UIEvent _manifestUpdated = new UIEvent(Constants.Events.ManifestUpdatedEventId.ToString());
    /// <summary>
    /// This event should be triggered when a saved payload needs to be decompressed.
    /// Push user's local folder.
    /// </summary>
    private UIEvent _decompressPayload = new UIEvent(Constants.Events.DecompressPayloadEventId.ToString());
    private UIEvent _deleteLocalFiles = new UIEvent(Constants.Events.DeleteLocalFilesEventId.ToString());

    /// <summary>
    /// Awake is called first and only once during the script's lifetime.
    /// </summary>
    private void Awake()
    {
		if (_instance == null) {
			_instance = this;
			Initialize ();
		} else {
			Debug.LogError("Multiple instances of MCoachModel Awake.  Destroying later instance");
			DestroyImmediate(this);
		}
    }

    /// <summary>
    /// Initializes script events. This method should only be called once.
    /// </summary>
    private void Initialize()
    {
        GlobalMessageManager.AddEvent(_manifestUpdated);
        GlobalMessageManager.AddEvent(_decompressPayload);
        GlobalMessageManager.AddEvent(_deleteLocalFiles);
        GlobalMessageManager.AddListener(Constants.Events.LoginSuccessEventId.ToString(), OnLogin);
        GlobalMessageManager.AddListener(Constants.Events.RequestManifestEventId.ToString(), OnRequestManifest);
        GlobalMessageManager.AddListener(Constants.Events.FolderForPayloadEventId.ToString(), OnFolderForPayload);
        GlobalMessageManager.AddListener(Constants.Events.LocalUserFilesEventId.ToString(), OnLocalUserFiles);
    }

    /// <summary>
    /// Handles the Login success event.
    /// </summary>
    private void OnLogin(string EventName, ref object Data)
    {
        _manifest = (Manifest)Data;
//        Debug.Log("Is received manifest null? " + (_manifest == null));
//        Debug.Log("Is username null? " + (_manifest.UserInfo.Name == null));
        bool test1 = _manifest != null;
        bool test2 = !_manifest.UserInfo.Name.Equals(string.Empty);
        bool test3 = !_manifest.UserInfo.Password.Equals(string.Empty);

        if (_manifest != null && !_manifest.UserInfo.Name.Equals(string.Empty)
            && !_manifest.UserInfo.Password.Equals(string.Empty))
        {
            //GC CrashFix //object refNewManifest = _manifest;
			refManifest = _manifest;
            
            //  Debug.Log("Delete Files: " + _manifest.DeleteFiles.Length);
            //  Debug.Log("Cached Files: " + _manifest.CachedFiles.Length);
            //  Debug.Log("Add Files: " + _manifest.AddFiles.Length);
            if (_manifest.DeleteFiles.Length > 0)
            {
                //GC Crash Fix //object refManifest = _manifest;
                _deleteLocalFiles.FireEvent(ref refManifest);
            }
            
            _manifestUpdated.FireEvent(ref refManifest); //GC Crash Fix// was refNewManifest
        }
        else
        {
            Debug.LogError("MCoachModel manifest insufficient data!");
        }
    }

    /// <summary>
    /// Method handles the Request Manifest event. This method
    /// pushes out a reference to its manifest.
    /// </summary>
    private void OnRequestManifest(string EventName, ref object Data)
    {
//        Debug.Log("OnRequestManifest");
        if (_manifest != null)
        {
            // object refManifest = _manifest;
            _manifestUpdated.FireEvent(ref refManifest);
        }
    }

    /// <summary>
    /// Triggered when the user's local data file is needed.
    /// </summary>
    /// <param name="EventName"></param>
    /// <param name="Data">CompressedPayload location</param>
    private void OnFolderForPayload(string EventName, ref object Data)
    {
        string[] localPaths = new string[2]
        {
            (string)Data,
            Application.persistentDataPath + "/" + _manifest.UserInfo.Id
        };
        
        object pushData = (object)localPaths;
        _decompressPayload.FireEvent(ref pushData);
    }

    /// <summary>
    /// Triggered local user files are updated.
    /// </summary>
    /// <param name="EventName"></param>
    /// <param name="Data">List of files</param>
    private void OnLocalUserFiles(string EventName, ref object Data)
    {
        List<string> files = (List<string>)Data;
        List<string> cachedFiles = new List<string>(_manifest.CachedFiles);
        char[] delimiters = {'/'};

        for (int fileIndex = 0; fileIndex < files.Count; fileIndex++)
        {
            string[] splitResult = files[fileIndex].Split(delimiters);
            if (splitResult.Length > 0)
                files[fileIndex] = splitResult[splitResult.Length - 1];
        }

        for (int fileIndex = 0; fileIndex < files.Count; fileIndex++)
        {
            if (!Tools.StringArrayContains(_manifest.CachedFiles, files[fileIndex]))
            {
//                Debug.Log("New Locally Cached File: " + files[fileIndex]);
                cachedFiles.Add(files[fileIndex]);
            }
        }

        _manifest.CachedFiles = cachedFiles.ToArray();
        //GC Fix //object manifestRef = _manifest;
        _manifestUpdated.FireEvent(ref refManifest); //GC Fix // was manifestRef
    }
}