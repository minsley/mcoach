﻿using System;
using System.Linq;
using Assets.Scripts.Tools;
using UnityEngine;
using UnityEngine.UI;
using MCoach;

public class CreateNewCoachingPlanPopUpController : MonoBehaviour
{
    #region Properties

    /// <summary>
    /// The selected category.
    /// </summary>
    private CoachingSkillCategory _csCategory;

    /// <summary>
    /// The manifest in memory.
    /// </summary>
    private Manifest _csManifest;

    /// <summary>
    /// The currently selected team member
    /// </summary>
    private TeamMember _csTeamMember;

	private CoachingPlan editingPlan = null; // the existing plan we are editing, or null in new plan

    /// <summary>
    /// Stores the Manifest Updated event. This event should be triggered
    /// when a chnage has happened to the manifest.
    /// </summary>
    private readonly UIEvent _saveManifest = new UIEvent(Constants.Events.SaveLocalManifest.ToString());

	private string sPlanNameText = "Coaching Plan";

    #endregion

    #region Events

    /// <summary>
    /// Displays the coaching plan pop up
    /// </summary>
    /// <param name="eventName"></param>
    /// <param name="data"></param>
    private void OnShowCreateNewCoachingPlanPopUp(string eventName, ref object data)
    {
		GameObject popupGameObject = GameObject.Find("mCoachBackgroundImage").transform.Find ("CreateNewCoachingPlanPopUp").gameObject;

		var prevRemarksText = popupGameObject.transform.Find("PreviousRemarksPanel/Container/Text");
		var prevRemarksTitle = popupGameObject.transform.Find("PreviousEntryTitleText");

		// we need to find out the team member and skill category ID of this plan...
		CoachingPlan prevPlan = null;
		RatingController rc = FindObjectOfType<RatingController>();
		if (rc != null)
			prevPlan = MCoachModel.GetManifest().GetMostRecentCoachingPlan(rc.GetTeamMember().Id, rc.GetCoachingSkillCategory().ID);
		if (prevPlan != null) {
			prevRemarksText.GetComponent<Text> ().text = prevPlan.Title;
			prevRemarksTitle.gameObject.SetActive (true);
		} else {
			prevRemarksText.GetComponent<Text> ().text = "";
			prevRemarksTitle.gameObject.SetActive (false);
		}


		var child = popupGameObject.transform.Find("CreateCoachingPlanBtn/CreateCoachingPlanText");
		if (child != null)
		{
			child.GetComponent<Text>().text = "Create "+sPlanNameText;
		}
		child = popupGameObject.transform.Find("DatePickerCalendar");
		if (child != null)
		{
			child.GetComponent<CalendarController>().SetDateTime(System.DateTime.Now); // initialize to today...
			child.GetComponent<CalendarController>().SetDate(null); // update the popup fields with the date.
		}
        GameObject.Find("mCoachBackgroundImage").GetComponent<Animator>().Play("CreateNewCoachingPlanPopupShow");
    }

	private void OnEditCoachingPlanPopUp(string eventName, ref object data)
	{
		editingPlan = (CoachingPlan)data;
		if (editingPlan != null) {

			GameObject popupGameObject = GameObject.Find("mCoachBackgroundImage").transform.Find ("CreateNewCoachingPlanPopUp").gameObject;

			var prevRemarksText = popupGameObject.transform.Find("PreviousRemarksPanel/Container/Text");
			var prevRemarksTitle = popupGameObject.transform.Find("PreviousEntryTitleText");

			// we need to find out the team member and skill category ID of this plan...
			CoachingPlan prevPlan = null;
			RatingController rc = FindObjectOfType<RatingController>();
			if (rc != null)
				prevPlan = MCoachModel.GetManifest().GetMostRecentCoachingPlan(rc.GetTeamMember().Id, rc.GetCoachingSkillCategory().ID);
			if (prevPlan != null) {
				prevRemarksText.GetComponent<Text> ().text = prevPlan.Title;
				prevRemarksTitle.gameObject.SetActive (true);
			} else {
				prevRemarksText.GetComponent<Text> ().text = "";
				prevRemarksTitle.gameObject.SetActive (false);
			}


			var child = popupGameObject.transform.Find("CreateCoachingPlanBtn/CreateCoachingPlanText");
			if (child != null)
			{
				child.GetComponent<Text>().text = "Save "+sPlanNameText;
			}

			// initialize all fields from existing plan data
			child = popupGameObject.transform.Find("DatePickerCalendar");
			if (child != null)
			{
				child.GetComponent<CalendarController>().SetDateTime(editingPlan.Date);
				child.GetComponent<CalendarController>().SetDate(null); // update the popup fields with the date.
			}
			
			child = popupGameObject.transform.Find("Coaching Plan Border/CoachingPlanInputText");
			if (child != null)
			{
				child.GetComponent<InputField>().text = editingPlan.Title;
			}  
			
			child = popupGameObject.transform.Find("AddReminderToggle");
			if (child != null)
			{
				child.GetComponent<Toggle>().isOn = editingPlan.Remind;
			}
		}
		// now show the initialized plan
		GameObject.Find("mCoachBackgroundImage").GetComponent<Animator>().Play("CreateNewCoachingPlanPopupShow");
	}

    /// <summary>
    /// Hides the coaching plan pop up
    /// </summary>
    /// <param name="eventName"></param>
    /// <param name="data"></param>
    private void OnHideCreateNewCoachingPlanPopUp(string eventName, ref object data)
    {
		editingPlan = null; // don't keep this around after cancelling. it is persistent!

		var child = GameObject.Find("CoachingPlanInputText");
		if (child != null)
		{
			var txt = child.GetComponent<Text>();
			txt.GetComponent<InputField>().text = ""; // clear this field for the next add
		}
		 GameObject.Find("mCoachBackgroundImage").GetComponent<Animator>().Play("CreateNewCoachingPlanPopupHide");
    }

    /// <summary>
    /// Saved the new coaching plan to the current, unsubmitted FCR and hides the coaching plan pop up
    /// </summary>
    /// <param name="eventName"></param>
    /// <param name="data"></param>
    private void OnSaveCreateNewCoachingPlanPopUp(string eventName, ref object data)
    {
        var userSkill = _csManifest.GetFcrCoachingSkill(_csTeamMember.Id, _csCategory.ID);
		if (userSkill == null){
			// we need to have a skill to add this plan to, so add it to the unsubmitted FCR at this point.
			{
				userSkill = new CoachingSkill(_csCategory.ID);
				_csManifest.AddFcrCoachingSkill(_csTeamMember.Id, userSkill);
			}
		}
        if (userSkill != null)
        {
			CoachingPlan coachingPlan;

			if (editingPlan == null){
				coachingPlan = new CoachingPlan
	            {
	                Title = "CoachingPlan"
	            };
			}
			else{
				coachingPlan = editingPlan;
				editingPlan = null; // don't keep this around after using it. it is persistent!
			}
/*
            var child = GameObject.Find("CoachingPlanDateInputText");
            if (child != null)
            {
                var txt = child.GetComponent<Text>();
                DateTime date;

                coachingPlan.Date = !DateTime.TryParse(txt.text, out date) ? DateTime.Now : date;
            }
            			var child = GameObject.Find("CoachingPlanDateWidget");
			if (child != null)
			{
				var txt = child.GetComponent<DateWidget>().Date;
				DateTime date;
				
				coachingPlan.Date = !DateTime.TryParse(txt, out date) ? DateTime.Now : date;
			}
*/
			var child = GameObject.Find("DatePickerCalendar");
			if (child != null)
			{
				var txt = child.GetComponent<CalendarController>().selectedDate;
				DateTime date;
				
				coachingPlan.Date = !DateTime.TryParse(txt, out date) ? DateTime.Now : date;
			}

            child = GameObject.Find("CoachingPlanInputText");
            if (child != null)
            {
                var txt = child.GetComponent<Text>();
				coachingPlan.Title = txt.GetComponent<InputField>().text; // get the value from the inputfield not the txt cmpnt
				txt.GetComponent<InputField>().text = ""; // clear this field for the next add
            }  

			child = GameObject.Find("AddReminderToggle");
			if (child != null)
			{

				var toggle = child.GetComponent<Toggle>();
				coachingPlan.Remind = toggle.isOn;
			}

			if (!userSkill.CoachingPlanList.Contains(coachingPlan))
            	userSkill.CoachingPlanList.Add(coachingPlan);
            userSkill.CoachingPlanList = userSkill.CoachingPlanList.OrderByDescending(f => f.Date).ToList();
            object manifestRef = _csManifest;

            //need to make sure the manifest is serialized to the drive.
            _saveManifest.FireEvent(ref manifestRef);
			// also updated the reminders icon...

        }

        GameObject.Find("mCoachBackgroundImage").GetComponent<Animator>().Play("CreateNewCoachingPlanPopupHide");
    }

    /// <summary>
    /// Fires when the coaching skill is selected
    /// </summary>
    /// <param name="eventName"></param>
    /// <param name="data"></param>
    private void OnCoachingCategorySelected(string eventName, ref object data)
    {
        _csCategory = (CoachingSkillCategory)data;
    }

    /// <summary>
    /// Handles the ManifestUpdated event. 
    /// </summary>
    /// <param name="eventName"></param>
    /// <param name="data"></param>
    private void OnManifestUpdated(string eventName, ref object data)
    {
        _csManifest = (Manifest)data;
    }

	private void OnPlanNameTextChanged(string eventName, ref object data)
	{
		sPlanNameText = (string)data;
	}

    /// <summary>
    /// Stores the current team member when they are selected
    /// </summary>
    /// <param name="eventName"></param>
    /// <param name="data"></param>
    private void OnTeamMemberSelected(string eventName, ref object data)
    {
        _csTeamMember = (TeamMember)data;
    }

    #endregion

    #region Methods

    /// <summary>
    /// Called once during the duration of the app.
    /// Initalizes events and listeners. Method should only
    /// be triggered one time!
    /// </summary>
    private void Awake()
    {
        //Debug.LogError("Content List Controller awoken");
        InitializeEvents();
        InitializeListeners();
    }

    /// <summary>
    /// Initializes the dashboard controller events. ONLY CALL ONCE!
    /// </summary>
    private void InitializeEvents()
    {
       GlobalMessageManager.AddEvent(_saveManifest);
    }

    /// <summary>
    /// Initializes listeners for dashboard controller. ONLY CALL ONCE!
    /// </summary>
    private void InitializeListeners()
    {
		GlobalMessageManager.AddListener(Constants.Events.PlanNameTextEventId.ToString(), OnPlanNameTextChanged);
        GlobalMessageManager.AddListener(Constants.Events.ManifestUpdatedEventId.ToString(), OnManifestUpdated);
        GlobalMessageManager.AddListener(Constants.Events.TeamMemberSelectedEventId.ToString(), OnTeamMemberSelected);
        GlobalMessageManager.AddListener(Constants.Events.CoachingCategorySelectedEventId.ToString(), OnCoachingCategorySelected);
        GlobalMessageManager.AddListener(Constants.Events.CreateNewCoachingPlanPopUpShowEventId.ToString(), OnShowCreateNewCoachingPlanPopUp);
		GlobalMessageManager.AddListener(Constants.Events.EditCoachingPlanPopUpShowEventId.ToString(), OnEditCoachingPlanPopUp);
        GlobalMessageManager.AddListener(Constants.Events.CreateNewCoachingPlanPopUpHideEventId.ToString(), OnHideCreateNewCoachingPlanPopUp);
        GlobalMessageManager.AddListener(Constants.Events.CreateNewCoachingPlanPopUpSaveEventId.ToString(), OnSaveCreateNewCoachingPlanPopUp);
    }

    #endregion
}