﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using MCoach;
using Assets.Scripts.Tools;
using System.Collections;
using System.Collections.Generic;

public class RemindersPopupController : MonoBehaviour {

	public GameObject remindersIcon;

	public GameObject remindersContainer; // scrollable list for reminder templates
	public GameObject reminderTemplate; // spawn these when populating

	private class Reminder{
		public TeamMember teamMember;
		public FCR fcr;
		public CoachingSkill coachingSkill;
		public CoachingPlan coachingPlan;
	}

	private bool initialized = false;
	private List<Reminder> reminders;
	private List<GameObject> spawnedItems = new List<GameObject>();
	/// <summary>
	/// Stores the Manifest Updated event. This event should be triggered
	/// when a chnage has happened to the manifest.
	/// </summary>
	private readonly UIEvent _saveManifest = new UIEvent(Constants.Events.SaveLocalManifest.ToString());

	public void Awake(){
		if (initialized)
			return;
		initialized = true;
		GlobalMessageManager.AddEvent(_saveManifest); // CreateNewCoachingPlanPopUpSaveEventId
		GlobalMessageManager.AddListener (Constants.Events.CreateNewCoachingPlanPopUpSaveEventId.ToString (), OnNewCoachingPlan);
		PopulateReminders ();
	}
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnEnable(){
		// populate the scrollable list of reminders
		PopulateReminders ();
	}

	private void OnNewCoachingPlan(string eventName, ref object data){
		PopulateReminders ();
	}
	
	public void PopulateReminders(){
		Manifest csManifest = MCoachModel.GetManifest ();

		reminders = new List<Reminder>();
		bool showBell = false;
		// As long as you have a valid manifest and the user is a team leader or rep
        if (csManifest != null && csManifest.UserInfo.LevelNumber >= 4)
        {
			foreach (TeamMember tm in csManifest.UserInfo.MyTeam)
			{
			    if ((csManifest.UserInfo.LevelNumber == 5) && (tm.Id != csManifest.UserInfo.Id)) continue;

				foreach (FCR fcr in tm.FCRList){
					if (fcr.ServerStatus != FCR.Status.UnSubmitted)// || fcr.ServerStatus == FCR.Status.UnSubmitted)
                    { // TODO  should we go ahead and show that there is a reminder created ?
						System.DateTime reminderTime = System.DateTime.Now.AddDays(7);
						foreach (CoachingSkill cs in fcr.CoachingSkills){
							foreach (CoachingPlan cp in cs.CoachingPlanList){
								if (cp.Remind && cp.Date < reminderTime)
									showBell = true;
								if (cp.Remind){ //cp.Remind && 
									Reminder newReminder = new Reminder();
									newReminder.teamMember = tm;
									newReminder.fcr = fcr;
									newReminder.coachingSkill = cs;
									newReminder.coachingPlan = cp;

									reminders.Add(newReminder);
								}
							}
						}
					}
				}
			}
		}
		Color bellColor = remindersIcon.GetComponent<Image> ().color;
		if (showBell) {
			bellColor.a = 1.0f;
		} else {
			bellColor.a = 0;
		}
		remindersIcon.GetComponent<Image> ().color = bellColor;
		remindersIcon.SetActive (true);//reminders.Count > 0);

		for (int spawnedIndex = 0; spawnedIndex < spawnedItems.Count; spawnedIndex++)
		{
			Destroy(spawnedItems[spawnedIndex]);
		}
		spawnedItems.Clear();

		reminderTemplate.SetActive(false);
		foreach (Reminder theReminder in reminders){
			
			GameObject newInstance = Instantiate(reminderTemplate) as GameObject;
			newInstance.transform.SetParent(remindersContainer.transform); // layout should do the rest.
			newInstance.transform.localScale = new Vector3(1,1,1);
			newInstance.SetActive (true);
			// set the textvalues.

			newInstance.transform.Find("ReminderTeamMember").GetComponent<Text>().text = theReminder.teamMember.Name;
			newInstance.transform.Find("PlanScrollRect/Container/ReminderPlan").GetComponent<Text>().text = theReminder.coachingPlan.Title;
			newInstance.transform.Find("ReminderDate").GetComponent<Text>().text = theReminder.coachingPlan.Date.Date.ToString("MM/dd/yyyy");
			//			newInstance.transform.Find("FCROtherText").GetComponent<Text>().text = ( cp.Remind ? "Y" : "N" );
			newInstance.GetComponent<FCRIndexInstance>().myFCR = theReminder.fcr;
			newInstance.GetComponent<FCRIndexInstance>().myTeamMember = theReminder.teamMember;
			newInstance.GetComponent<FCRIndexInstance>().myCoachingPlan = theReminder.coachingPlan;
			
			spawnedItems.Add(newInstance);
		}
	}

	// go thru the list of spawned items and turn off reminder for any items checked...
	public void RemoveCheckedReminders(){
		bool changed = false;
		foreach (GameObject GO in spawnedItems) {
			if (GO.transform.Find ("ReminderCheckbox").GetComponent<Toggle>().isOn){
				GO.GetComponent<FCRIndexInstance>().myCoachingPlan.Remind = false; // do we need to flag the FCR changed so this propagates up ?
				changed = true;
			}
			
		}
		if (changed) {
			PopulateReminders ();
			//save change to manifest locally
			object manifestRef = (object)MCoachModel.GetManifest ();
			_saveManifest.FireEvent(ref manifestRef);
		}
	}
}
