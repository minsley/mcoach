﻿using System;
using System.Linq;
using Assets.Scripts.Tools;
using UnityEngine;
using UnityEngine.UI;
using MCoach;

class AddToFCRPopUpController : MonoBehaviour
{
    #region Properties

    /// <summary>
    /// The selected category.
    /// </summary>
    private CoachingSkillCategory _csCategory;

    /// <summary>
    /// The manifest in memory.
    /// </summary>
    private Manifest _csManifest;

    /// <summary>
    /// The currently selected team member
    /// </summary>
    private TeamMember _csTeamMember;



    #endregion

    #region Events

	/// <summary>
	/// Stores the Manifest Updated event. This event should be triggered
	/// when a chnage has happened to the manifest.
	/// </summary>
	private readonly UIEvent _saveManifest = new UIEvent(Constants.Events.SaveLocalManifest.ToString());
	private UIEvent _viewFCREvent = new UIEvent(Constants.Events.ViewFCREventID.ToString());
	
	#endregion
	
	#region Methods
    /// <summary>
    /// Saves the new sales data to the current, unsubmitted FCR
    /// </summary>
    /// <param name="eventName"></param>
    /// <param name="data"></param>
	private void OnCreateNewSalesDataPopUpSave(string eventName, ref object data)
    {
		FCR theFCR = _csManifest.GetUnsubmittedFcr (_csTeamMember.Id);
		if (theFCR != null)
		{
			SalesData newSalesData = new SalesData();
            var productInputObject = GameObject.Find("SalesDataProductInputText");
			if (productInputObject != null)
            {
				var productTxt = productInputObject.GetComponent<Text>();
				newSalesData.ProductName = productTxt.GetComponent<InputField>().text;
				productTxt.GetComponent<InputField>().text = "";
			}
			var quotaInputObject = GameObject.Find("SalesDataQuotaInputText");
			if (quotaInputObject != null)
			{
				var quotaTxt = quotaInputObject.GetComponent<Text>();
				newSalesData.Quota = int.Parse (quotaTxt.GetComponent<InputField>().text);
				quotaTxt.GetComponent<InputField>().text = "";
			}

			var QTDInputObject = GameObject.Find("SalesDataQTDInputText");
			if (QTDInputObject != null)
			{
				var qtdTxt = QTDInputObject.GetComponent<Text>();
				newSalesData.QTDAchieved = int.Parse (qtdTxt.GetComponent<InputField>().text);
				qtdTxt.GetComponent<InputField>().text = "";
			}
			var YTDInputObject = GameObject.Find("SalesDataYTDInputText");
			if (YTDInputObject != null)
			{
				var ytdTxt = YTDInputObject.GetComponent<Text>();
				newSalesData.YTDAchieved = int.Parse (ytdTxt.GetComponent<InputField>().text);
				ytdTxt.GetComponent<InputField>().text = "";
			}
			newSalesData.CreationDate = System.DateTime.Now;

			// TODO there is a method on the manifest to add this, should we be using that ?
			theFCR.SalesDataList.Add (newSalesData);

			// refresh the FCR display
			_viewFCREvent.FireEvent();

//            userSkill.CoachingPlanList.Add(coachingPlan);
//            userSkill.CoachingPlanList = userSkill.CoachingPlanList.OrderByDescending(f => f.Date).ToList();
            object manifestRef = _csManifest;

            //need to make sure the manifest is serialized to the drive.
            _saveManifest.FireEvent(ref manifestRef);
        }
    }

	/// <summary>
	/// Saves the new sales data to the current, unsubmitted FCR
	/// </summary>
	/// <param name="eventName"></param>
	/// <param name="data"></param>
	private void OnCreateNewDevelopmentPopUpSave(string eventName, ref object data)
	{
		bool editing = false;
		FCR theFCR; 
		if (TeamMemberFCRView.editingFCR != null){
			theFCR = TeamMemberFCRView.editingFCR;
			TeamMemberFCRView.editingFCR = null;
		}
		else
			theFCR = _csManifest.GetUnsubmittedFcr (_csTeamMember.Id); 

		if (theFCR != null)
		{	
			Development newDevelopment;
			if (TeamMemberFCRView.editingDevelopment != null){
				newDevelopment = TeamMemberFCRView.editingDevelopment;
				TeamMemberFCRView.editingDevelopment = null;
				editing = true;
			}
			else
				newDevelopment = new Development();

			var actionItemInputObject = GameObject.Find("DevelopmentActionItemInputText");
			if (actionItemInputObject != null)
			{
				var actionItemTxt = actionItemInputObject.GetComponent<Text>();
				newDevelopment.ActionItem = actionItemTxt.GetComponent<InputField>().text;
				actionItemTxt.GetComponent<InputField>().text = ""; // clear the input for next time
			}
			var actionTakenInputObject = GameObject.Find("DevelopmentActionTakenInputText");
			if (actionTakenInputObject != null)
			{
				var actionTakenTxt = actionTakenInputObject.GetComponent<Text>();
				newDevelopment.ActionTaken = actionTakenTxt.GetComponent<InputField>().text;
				actionTakenTxt.GetComponent<InputField>().text = "";
			}
			
			var resultsInputObject = GameObject.Find("DevelopmentResultsSeenInputText");
			if (resultsInputObject != null)
			{
				var resultsTxt = resultsInputObject.GetComponent<Text>();
				newDevelopment.ResultsSeen = resultsTxt.GetComponent<InputField>().text;
				resultsTxt.GetComponent<InputField>().text = "";
			}
			if (!editing){
				newDevelopment.CreationDate = System.DateTime.Now;
				// TODO there is a method on the manifest to add this, should we be using that ?
				theFCR.DevelopmentList.Add (newDevelopment);
			}

			// refresh the FCR display
			_viewFCREvent.FireEvent();

			object manifestRef = _csManifest;
			//need to make sure the manifest is serialized to the drive.
			_saveManifest.FireEvent(ref manifestRef);
		}
	}

	private void OnCreateNewDevelopmentPopUpHide(string eventName, ref object data)
	{
		var actionItemInputObject = GameObject.Find("DevelopmentActionItemInputText");
		if (actionItemInputObject != null)
		{
			actionItemInputObject.GetComponent<InputField>().text = ""; // clear the input for next time
		}
		var actionTakenInputObject = GameObject.Find("DevelopmentActionTakenInputText");
		if (actionTakenInputObject != null)
		{
			actionTakenInputObject.GetComponent<InputField>().text = "";
		}
		
		var resultsInputObject = GameObject.Find("DevelopmentResultsSeenInputText");
		if (resultsInputObject != null)
		{
		resultsInputObject.GetComponent<InputField>().text = "";
		}
	}

	/// <summary>
	/// Saves the new sales data to the current, unsubmitted FCR
	/// </summary>
	/// <param name="eventName"></param>
	/// <param name="data"></param>
	private void OnCreateNewGoalForFieldRidePopUpSave(string eventName, ref object data)
	{
		bool editing = false;
		FCR theFCR; 
		if (TeamMemberFCRView.editingFCR != null) {
			theFCR = TeamMemberFCRView.editingFCR;
			TeamMemberFCRView.editingFCR = null;
		}
		else
			theFCR = _csManifest.GetUnsubmittedFcr (_csTeamMember.Id); 
		if (theFCR != null)
		{
			GoalForFieldRide newGoal;
			if (TeamMemberFCRView.editingGoal != null){
				newGoal = TeamMemberFCRView.editingGoal;
				TeamMemberFCRView.editingGoal = null; 
				editing = true;
			}
			else
				newGoal = new GoalForFieldRide();

			var goalInputObject = GameObject.Find("GoalInputText");
			if (goalInputObject != null)
			{
				var goalTxt = goalInputObject.GetComponent<Text>();
				newGoal.ActionItem = goalTxt.GetComponent<InputField>().text;
				goalTxt.GetComponent<InputField>().text = "";
			}
			// judging from the GoalForFiledRide struct, there is more to this than i have so far.
			if (!editing){
				newGoal.CreationDate = System.DateTime.Now;
				// TODO there is a method on the manifest to add this, should we be using that ?
				theFCR.GoalList.Add (newGoal);
			}

			// refresh the FCR display
			_viewFCREvent.FireEvent();
			
			//            userSkill.CoachingPlanList.Add(coachingPlan);
			//            userSkill.CoachingPlanList = userSkill.CoachingPlanList.OrderByDescending(f => f.Date).ToList();
			object manifestRef = _csManifest;
			
			//need to make sure the manifest is serialized to the drive.
			_saveManifest.FireEvent(ref manifestRef);
		}
	}
	private void OnCreateNewGoalForFieldRidePopUpHide(string eventName, ref object data)
	{
		var goalInputObject = GameObject.Find("GoalInputText");
		if (goalInputObject != null)
		{
			goalInputObject.GetComponent<InputField>().text = "";
		}
	}

    /// <summary>
    /// Handles the ManifestUpdated event. 
    /// </summary>
    /// <param name="eventName"></param>
    /// <param name="data"></param>
    private void OnManifestUpdated(string eventName, ref object data)
    {
        _csManifest = (Manifest)data;
    }

    /// <summary>
    /// Stores the current team member when they are selected
    /// </summary>
    /// <param name="eventName"></param>
    /// <param name="data"></param>
    private void OnTeamMemberSelected(string eventName, ref object data)
    {
        _csTeamMember = (TeamMember)data;
    }


    /// <summary>
    /// Called once during the duration of the app.
    /// Initalizes events and listeners. Method should only
    /// be triggered one time!
    /// </summary>
    private void Awake()
    {
        //Debug.LogError("Content List Controller awoken");
        InitializeEvents();
        InitializeListeners();
    }

    /// <summary>
    /// Initializes the dashboard controller events. ONLY CALL ONCE!
    /// </summary>
    private void InitializeEvents()
    {
       GlobalMessageManager.AddEvent(_saveManifest);
		GlobalMessageManager.AddEvent (_viewFCREvent);
    }

    /// <summary>
    /// Initializes listeners for dashboard controller. ONLY CALL ONCE!
    /// </summary>
    private void InitializeListeners()
    {
        GlobalMessageManager.AddListener(Constants.Events.ManifestUpdatedEventId.ToString(), OnManifestUpdated);
        GlobalMessageManager.AddListener(Constants.Events.TeamMemberSelectedEventId.ToString(), OnTeamMemberSelected);
		GlobalMessageManager.AddListener(Constants.Events.CreateNewSalesDataPopUpSaveEventId.ToString(), OnCreateNewSalesDataPopUpSave);
		GlobalMessageManager.AddListener(Constants.Events.CreateNewDevelopmentPopUpSaveEventId.ToString(), OnCreateNewDevelopmentPopUpSave);
		GlobalMessageManager.AddListener(Constants.Events.CreateNewGoalForFieldRidePopUpSaveEventId.ToString(), OnCreateNewGoalForFieldRidePopUpSave);
//		GlobalMessageManager.AddListener(Constants.Events.CreateNewSalesDataPopUpHideEventId.ToString(), OnCreateNewSalesDataPopUpHide);
		GlobalMessageManager.AddListener(Constants.Events.CreateNewDevelopmentPopUpHideEventId.ToString(), OnCreateNewDevelopmentPopUpHide);
		GlobalMessageManager.AddListener(Constants.Events.CreateNewGoalForFieldRidePopUpHideEventId.ToString(), OnCreateNewGoalForFieldRidePopUpHide);
    }

    #endregion
}