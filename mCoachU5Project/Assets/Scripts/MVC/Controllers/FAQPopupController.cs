﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using MCoach;
using Assets.Scripts.Tools;
using System.Collections;
using System.Collections.Generic;

public class FAQPopupController : MonoBehaviour {

	public GameObject faqContainer; // scrollable list for reminder templates
	public GameObject faqHeaderTemplate; // spawn these when populating
	public GameObject faqAnswerTemplate;
	public GameObject faqSpacerTemplate;

	public class FAQItem{
		public string header;
		public string answer;
	}

	private bool initialized = false;
	private List<FAQItem> faqs;
	private List<GameObject> spawnedItems = new List<GameObject>();

	public void Awake(){
		if (initialized)
			return;
		initialized = true;
		PopulateFAQ ();
	}
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnEnable(){
		// populate the scrollable list of reminders
		PopulateFAQ ();
	}


	public void PopulateFAQ(){
		Manifest csManifest = MCoachModel.GetManifest ();

		faqs = new List<FAQItem>();

		// As long as you have a valid manifest
		if (csManifest != null)
        { 
			//TODO  populate the list of FAQItems from the manifest
			// look for a content block of type FAQ in the manifest, split the description field string on "|",
			// this will yield Q A Q A Q A in a string array.
			if (csManifest.UserInfo.MyContent.ContainsKey("FAQ")){
				MCoach.Content[] faqContent = csManifest.UserInfo.MyContent["FAQ"];
				string[] parts = faqContent[0].Description.Split('|');
				for (int i=0;i<parts.Length;i+=2){
					FAQItem testItem = new FAQItem();
					testItem.header = parts[i];
					if (i+1 < parts.Length)
						testItem.answer = parts[i+1];
					else
						testItem.answer = "WARNING. Inconsistent data in FAQ source, missing an answer or a separator:\n"+faqContent[0].Description;
					faqs.Add(testItem);
				}
			}

			GameObject newInstance = null;

			for (int spawnedIndex = 0; spawnedIndex < spawnedItems.Count; spawnedIndex++)
			{
				Destroy(spawnedItems[spawnedIndex]);
			}
			spawnedItems.Clear();

			faqHeaderTemplate.SetActive(false);
			faqAnswerTemplate.SetActive(false);
			faqSpacerTemplate.SetActive(false);

			foreach (FAQItem theItem in faqs){
				
				newInstance = Instantiate(faqHeaderTemplate) as GameObject;
				newInstance.transform.SetParent(faqContainer.transform); // layout should do the rest.
				newInstance.transform.localScale = new Vector3(1,1,1);
				newInstance.GetComponent<Text>().text = theItem.header;
				string[] data = theItem.header.Split('\n');
				int wraps = 0;
				foreach (string s in data)
					if (data.Length > 60) wraps++;
				int height = 30+ data.Length*60 + wraps*60;
				newInstance.GetComponent<LayoutElement>().minHeight = height;
				newInstance.SetActive (true);
				spawnedItems.Add(newInstance);

				newInstance = Instantiate(faqAnswerTemplate) as GameObject;
				newInstance.transform.SetParent(faqContainer.transform); // layout should do the rest.
				newInstance.transform.localScale = new Vector3(1,1,1);
				newInstance.GetComponent<Text>().text = theItem.answer;
				data = theItem.answer.Split('\n');
				wraps = 0;
				foreach (string s in data)
					if (data.Length > 80) wraps++;
				height = data.Length*50 + wraps*50;
				newInstance.GetComponent<LayoutElement>().minHeight = height;
				newInstance.SetActive (true);
				spawnedItems.Add(newInstance);

				newInstance = Instantiate(faqSpacerTemplate) as GameObject;
				newInstance.transform.SetParent(faqContainer.transform); // layout should do the rest.
				newInstance.transform.localScale = new Vector3(1,1,1);
				newInstance.SetActive (true);
				spawnedItems.Add(newInstance);
			}
		}
	}
}
