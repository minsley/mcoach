﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using MCoach;
using Newtonsoft.Json;
using System.Xml;
using Assets.Scripts.Tools;

public class LoginController : MonoBehaviour
{
    private UIEvent _login = new UIEvent(Constants.Events.LoginEventId.ToString());
    //  /// <summary>
    //  /// References the OnlineStatus Event. Event triggers when the
    //  /// connection status is requested.
    //  /// </summary>
    //  private UIEvent _onlineStatus = new UIEvent(Constants.Events.OnlineStatusEventId.ToString());
    /// <summary>
    /// Triggered when the user is attempting to login online.
    /// </summary>
    private UIEvent _loginWebCall = new UIEvent(Constants.Events.LoginWebCallEventId.ToString());
    /// <summary>
    /// Triggered when the user has successfully logged in. Pushes
    /// the latest received manifest from the web.
    /// </summary>
    private UIEvent _loginSuccess = new UIEvent(Constants.Events.LoginSuccessEventId.ToString());
    /// <summary>
    /// Triggered when the user is offline and logging in.
    /// </summary>
    private UIEvent _localLogin = new UIEvent(Constants.Events.LocalLoginEventId.ToString());
    /// <summary>
    /// Event is fired when a request for the device's native popup
    /// is made.
    /// </summary>
    private UIEvent _nativeUICall = new UIEvent(Constants.Events.NativePopupEventId.ToString());
    /// <summary>
    /// Event is fired when a request for the device's native loader
    /// is made.
    /// </summary>
    private UIEvent _displayLoader = new UIEvent(Constants.Events.ShowLoadingEventId.ToString());
    /// <summary>
    /// Event is fired when all system level popups or loads should
    /// be hidden.
    /// </summary>
    private UIEvent _hideNativeUI = new UIEvent(Constants.Events.HideNativeUIEventId.ToString());

	private UIEvent _pingWebServices = new UIEvent(Constants.Events.PingWebServicesEventId.ToString());

	private bool loginPingPending = false; // used to wait for a ping result when signing in.
	public string version = "not_set";

	/// <summary>
    /// References the user entered username. This is updated via a listener.
    /// </summary>
    private string _username = "";
	public string Username{ // as we begin to migrate away from all these events...
		get{ return _username;}
		set{ _username = value;}
	}
    /// <summary>
    /// References the user entered password. This is updated via a listener.
    /// </summary>
    private string _password = "";
	public string Password{
		get{ return _password;}
		set{ _password = value;}
	}

	//private string _versionText = "Version ???";
	//public string VersionText{
	//	get{ return _versionText;}
	//}
    /// <summary>
    /// References the user entered email. This is updated via a listener.
    /// </summary>
    private string _resetPassEmail = "";
    /// <summary>
    /// References if loader event should be triggered.
    /// </summary>
    private bool _fireLoaderEvent = false;
	private bool _isOnline = false;

    /// <summary>
    /// Triggered during the first frame of the application.
    /// I'm treating it as a constructor of sorts.
    /// </summary>
    private void Awake()
    {
        InitializeEvents();
        InitializeListeners();
		GameObject versionObject = GameObject.Find ("VersionText");
		if (versionObject != null && versionObject.GetComponent<Text> () != null)
			//_versionText = versionObject.GetComponent<Text> ().text;
			versionObject.GetComponent<Text> ().text  = "Version "+Application.version;
		version = PlayerPrefs.GetString ("version", "none");
		if (version != Application.version)
			OnNewVersion ();
    }

	void OnNewVersion(){
		// when a new version had been installed, get the WebServices URL, retry count, and version and set them in the player prefs
		// this also sets them in the settings app
		//( unfortunately, the settings bundle values do not show up as player pref values until they are edited in the settings app, so this is a workaround)
		Debug.Log("New version detected, setting to use "+WebServiceTools.WebServiceDomainName);
		PlayerPrefs.SetString("webServicesDomainName",WebServiceTools.WebServiceDomainName); // set this to the default for the build...
		PlayerPrefs.SetString ("maxRetries", FileSystemController.maxRetries.ToString());				// 
		PlayerPrefs.SetString ("version", Application.version);
	}

	public void Start(){
		_pingWebServices.FireEvent(); // check to see if we are connected.
	}
    
    /// <summary>
    /// Triggers every frame.
    /// </summary>
    private void Update()
    {
        if (_fireLoaderEvent)
        {
            _fireLoaderEvent = false;
            object loaderText = "Logging In";
            _displayLoader.FireEvent(ref loaderText);
        }
    }

    #region EventHandlers

    /// <summary>
    /// Fires when the user changes the username input.
    /// </summary>
    /// <param name="EventName"></param>
    /// <param name="Data">Modified username</param>
    private void OnLoginUsername(string EventName, ref object Data)
    {
        if (Data != null)
            _username = (string)Data;
    }

    /// <summary>
    /// Fires when the user changes the password input.
    /// </summary>
    /// <param name="EventName"></param>
    /// <param name="Data">New Password</param>
    private void OnLoginPassword(string EventName, ref object Data)
    {
//		Debug.Log("OnLoginPassword called");

        if (Data != null)
            _password = (string)Data;
    }

    /// <summary>
    /// Handles the application login.
    /// </summary>
    /// <param name="EventName"></param>
    /// <param name="Data"></param>
    private void OnLoginSignIn(string EventName, ref object Data)
    {
		StartCoroutine("DoLoginSignin");
	}

	public IEnumerator DoLoginSignin(){

		// we need the state of our connection to be current
		loginPingPending = true;
		_pingWebServices.FireEvent ();
		while (loginPingPending)
			yield return null;

        if (_username != string.Empty && _password != string.Empty)
        {
            UserInfo userInfo = new UserInfo()
            {
                UserName = _username,
                Password = _password
            };
            Manifest manifest = new Manifest()
            {
                UserInfo = userInfo
            };

			//TODO if we are drilling down, set the 'no payload' flag in the manifest
			if (DrilldownController.GetInstance().Aliases.Count > 0)
				manifest.NoPayload = true;
			else{
				// grab the previous last login date for this user if playerprefs has it
				if (PlayerPrefs.HasKey(_username.ToLower()+"LastLogin")){ // this is updated on a successful online login
					MCoachModel.LastLoginDate = PlayerPrefs.GetString(_username.ToLower()+"LastLogin");
				}
				else
					MCoachModel.LastLoginDate = " not found.";//+System.DateTime.Now.ToShortDateString();
			}
            
			if (_isOnline)
            	_fireLoaderEvent = true;
            object manifestRef = manifest;
            _login.FireEvent(ref manifestRef);
            //_onlineStatus.FireEvent();
        }
        else
        {
            if (_username == string.Empty)
            {
                object uiInfo = new string[] { "Notice", "No username entered!", "OK" };
                _nativeUICall.FireEvent(ref uiInfo);
            }
            else if (_password == string.Empty)
            {
                object uiInfo = new string[] { "Notice", "No password entered!", "OK" };
                _nativeUICall.FireEvent(ref uiInfo);
            }
        }
		yield break;
    }

    /// <summary>
    /// Handles the received online status.
    /// </summary>
    /// <param name="EventName"></param>
    /// <param name="Data"></param>
    private void OnOnlineStatusData(string EventName, ref object Data)
    {
        object[] receivedData = (object[])Data;
        bool isOnline = (bool)receivedData[0];
        string manifestDat = (string)receivedData[1];
        
        UserInfo userInfo = new UserInfo()
        {
            UserName = _username,
            Password = _password
        };

        Manifest manifest = new Manifest()
        {
            UserInfo = userInfo
        };

		_isOnline = isOnline;
        if (isOnline)
        {
            //_fireLoaderEvent = true;
            
            //  object jsonData = JsonConvert.SerializeObject(manifest);
            StartCoroutine(LoginCoroutine(receivedData[1]));//Spun off call to give time for LoaderEvent to display
        }
        else//VALIDATE PASSWORD!!
        {
            object refManifest = manifest;
            _localLogin.FireEvent(ref refManifest);
			// the login hasnt even been shown yet...
			//_hideNativeUI.FireEvent(); // dismiss the logging in notice.
			_fireLoaderEvent = false;
			object uiInfo = new string[] { "Offline", "Working with Cached Data only.", "OK" };
			_nativeUICall.FireEvent(ref uiInfo);
        }
    }

    /// <summary>
    /// Handles the login complete request online.
    /// </summary>
    /// <param name="EventName"></param>
    /// <param name="Data"></param>
    private void OnOnlineWebcallComplete(string EventName, ref object Data)
    {
        string webReturnedData = (string)Data;
        _hideNativeUI.FireEvent();
        webReturnedData = WebServiceTools.GetReturnContent(webReturnedData);

        Manifest exampleManifest = new Manifest();
        Manifest receivedManifest = JsonConvert.DeserializeAnonymousType(webReturnedData, exampleManifest);

        if (receivedManifest != null)
        {
            Debug.Log("Manifest Result: " + receivedManifest.Result);

			bool isDrilldown = (DrilldownController.GetInstance().Aliases.Count > 0);//TODO, close this hole...
            
			// check if the global labesl dictionary didnt get created
			if (receivedManifest.GlobalLabels == null){
				receivedManifest.GlobalLabels = new System.Collections.Generic.Dictionary<string, string>();
			}


            if (receivedManifest.UserInfo.UserName != null && receivedManifest.UserInfo.UserName.ToLower().Equals(_username.ToLower())
                    && ( receivedManifest.UserInfo.Password.Equals(_password) || isDrilldown)) 
            {
                Debug.Log("login to dashboard");
                object pushManifest = receivedManifest;
                _loginSuccess.FireEvent(ref pushManifest);
                object empty = null;
				if (!isDrilldown) // then set the last login date for display on the next login
					PlayerPrefs.SetString (_username.ToLower()+"LastLogin",System.DateTime.Now.ToShortDateString());
                OnLogin(null, ref empty);
            }
            else if (receivedManifest.Result.Contains("invalid credentials:user_is_not_active"))
            {
                Debug.Log("Invalid username");
                object uiInfo = new string[] { "Notice", "Incorrect Username", "OK" };
                _nativeUICall.FireEvent(ref uiInfo);
            }
            else if (receivedManifest.Result.Contains("invalid credentials:invalid_password")) //|| !receivedManifest.UserInfo.Password.Equals(_password))
            {
                Debug.Log("Invalid password" + (receivedManifest.UserInfo.Password == null));
                object uiInfo = new string[] { "Notice", "Incorrect password", "OK" };
                _nativeUICall.FireEvent(ref uiInfo);
            }
			else if (receivedManifest.Result.Contains("password_expired")) //|| !receivedManifest.UserInfo.Password.Equals(_password))
			{
				Debug.Log("Expired password" + (receivedManifest.UserInfo.Password == null));
				// Find the popup and activate it.
				GameObject theCanvas = GameObject.Find("Canvas");
				if (theCanvas != null) {
					GameObject passwordExpiredPopup = theCanvas.transform.Find ("PasswordExpiredPopup").gameObject;
					if (passwordExpiredPopup != null) {
						passwordExpiredPopup.SetActive (true);
					} else {
						object uiInfo = new string[] { "Notice", "Expired password", "OK" };
						_nativeUICall.FireEvent(ref uiInfo);
					}
				}
			}

			else{ // we fall through here, with status OK but password didn't match and a million other reasons
				Debug.Log("Login Error" + (receivedManifest.Result));
				object uiInfo = new string[] { "Login Error", receivedManifest.Result, "OK" };
				_nativeUICall.FireEvent(ref uiInfo);
			}
		}
		else
		{
			object uiInfo = new string[] { "Notice", "Please login again", "OK" };
            _nativeUICall.FireEvent(ref uiInfo);
            Debug.Log("Received null manifest!");
        }
    }

    /// <summary>
    /// Handles the Login event.
    /// </summary>
    /// <param name="EventName"></param>
    /// <param name="Data"></param>
    private void OnLogin(string EventName, ref object Data)
    {
		if (Application.loadedLevelName != "3Dashboard")
        	Application.LoadLevel("3Dashboard");
    }

    /// <summary>
    /// Handles user input email.
    /// </summary>
    /// <param name="EventName"></param>
    /// <param name="Data"></param>
    private void OnLoginEmailAddress(string EventName, ref object Data)
    {
        if (Data != null)
            _resetPassEmail = (string)Data;
    }
    #endregion

    /// <summary>
    /// Initializes Login Controller Events.
    /// </summary>
    private void InitializeEvents()
    {
        GlobalMessageManager.AddEvent(_login);
        //GlobalMessageManager.AddEvent(_onlineStatus);
        GlobalMessageManager.AddEvent(_loginWebCall);
        GlobalMessageManager.AddEvent(_loginSuccess);
        GlobalMessageManager.AddEvent(_localLogin);
        GlobalMessageManager.AddEvent(_nativeUICall);
        GlobalMessageManager.AddEvent(_displayLoader);
        GlobalMessageManager.AddEvent(_hideNativeUI);
		GlobalMessageManager.AddEvent(_pingWebServices);
	}

    /// <summary>
    /// Initializes Login Controller Listeners.
    /// </summary>
    private void InitializeListeners()
    {
        GlobalMessageManager.AddListener(Constants.Events.OnlineStatusDataEventId.ToString(), OnOnlineStatusData);
        GlobalMessageManager.AddListener(Constants.Events.LoginCompleteEventId.ToString(), OnOnlineWebcallComplete);
        GlobalMessageManager.AddListener(Constants.Events.LoginUsernameEventId.ToString(), OnLoginUsername);
        GlobalMessageManager.AddListener(Constants.Events.LoginPasswordEventId.ToString(), OnLoginPassword);
        GlobalMessageManager.AddListener(Constants.Events.LoginSignInEventId.ToString(), OnLoginSignIn);
        GlobalMessageManager.AddListener(Constants.Events.LoginEmailAddressEventId.ToString(), OnLoginEmailAddress);
		GlobalMessageManager.AddListener (Constants.Events.PingWebServicesResultEventId.ToString (), OnPingWebServicesResult);
        //  GlobalMessageManager.AddListener("LoginSendPassword", OnLoginSendPassword);
    }

    /// <summary>
    /// Triggers LoginWebCall event after .25 of a second.
    /// </summary>
    /// <param name="jsonData">json data as a String</param>
    /// <returns></returns>
    IEnumerator LoginCoroutine(object jsonData)
    {
        yield return new WaitForSeconds(.25f);
        _loginWebCall.FireEvent(ref jsonData);
    }

	private void OnPingWebServicesResult(string EventName, ref object Data){
		loginPingPending = false;
		// put up a warning, and turn the syncButton back to white
		_isOnline = (bool)Data;
	}




		
}
