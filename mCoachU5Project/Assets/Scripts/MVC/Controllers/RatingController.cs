﻿using MCoach;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Tools;

#if UNITY_IPHONE
using IndieYP;
#endif

public class RatingController : MonoBehaviour 
{
	public float currValue = 10;
	public Image KnobImage, BackImage;
	public Text[] RatingText;
	public Text[] NumberText;
	public Text TodaysRating;
	public Text NameText;

	public InputField observationsText;
	public GameObject coachingPlanContainer;
	public GameObject coachingPlanTemplate; // will be added to container for each coaching plan
	
	private readonly List<Text> _numberText = new List<Text>();
	private readonly List<Text> _descriptionText = new List<Text>();	
	private bool _initialized;
    private Rating _maxRating;
    private Rating[] _ratings;
	private List<GameObject> _spawnedCoachingPlanRows;

	private CoachingSkillCategory _csCategory;
	private TeamMember _csTeamMember = null;
	private Manifest _lastManifestReceived = null;
	private UIEvent _unsubmittedFCRUpdated;


    #region Events 

    public void Awake()
	{
		if (_initialized) return;
		_initialized = true;
		
		//Debug.LogError("RATING SLIDER AWAKE"); AddToFCRButtonClickEventID



		_unsubmittedFCRUpdated = new UIEvent (Constants.Events.UnsubmittedFCRUpdatedEventId.ToString ());
		GlobalMessageManager.AddEvent (_unsubmittedFCRUpdated);

        GlobalMessageManager.AddListener(Constants.Events.RatingsEventId.ToString(), OnSetRating);
		GlobalMessageManager.AddListener(Constants.Events.CoachingCategorySelectedEventId.ToString(), OnCoachingCategorySelected);
		GlobalMessageManager.AddListener(Constants.Events.ManifestUpdatedEventId.ToString(), OnManifestUpdated);
		GlobalMessageManager.AddListener(Constants.Events.TeamMemberSelectedEventId .ToString(), OnTeamMemberSelected);
		//this next one is sketchy.  It depends on the popup controller being called first, which should normally happen
		// since it registered first, but adding YET ANOTHER EVENT fired from there would be safer.
		GlobalMessageManager.AddListener (Constants.Events.CreateNewCoachingPlanPopUpSaveEventId.ToString (), OnCreateNewCoachingPlanPopUpSave);
		// In yet another failure of this event driven architecture, because the button UIElement that owns this event will not have
		// registered it before this script is awakened, this AddListener will fail.  So we have to add our own UIEvent in order
		// for this registration to succeed. :/
		// even this doesnt work, because the UIElement, registering second, doesnt know it's event didnt get added to the list,
		// and the callback added here is to the successful registrant.  To fix this would require that every AddEvent call
		// return the successfully registered event, new or previous.
		// for a HACK fix, i added al UIElements to the Initialize Scripts, to be sure they run before THIS awake.  very bad.
		GlobalMessageManager.AddListener(Constants.Events.AddToFCRButtonClickEventId.ToString(), OnAddToFCR);

		_spawnedCoachingPlanRows = new List<GameObject> ();
	}

	private void OnManifestUpdated(string EventName, ref object Data){
		// this one might work since the manifest needs to be in place before a team member can be selected
		_lastManifestReceived = (Manifest)Data;
	}
	
	private void OnTeamMemberSelected(string EventName, ref object Data){
		_csTeamMember = (TeamMember)Data;
		_csCategory = null; // no longer valid  // this needs to happen system wide, too
	}

	private void OnCoachingCategorySelected(string EventName, ref object Data){
		_csCategory = (CoachingSkillCategory)Data;
		UpdateFromCurrentData ();
	}

	private void UpdateFromCurrentData(){
		// grab existing values from FCR if present and initialize, or clear field observations and rating slider to 5
		Slider theSlider = transform.Find ("RatingSlider").GetComponent<Slider> ();	

		CoachingSkill coachingSkill = GetCoachingSkill ();

		if (coachingSkill == null)
			coachingSkill = GetMostRecentCoachingSkill ();

		if (coachingSkill != null) {
			transform.Find ("FieldObservationsEntryBoxImage/FieldObservationsEntryText").GetComponent<InputField> ().text = coachingSkill.FieldRideObservations;
			float rating = _maxRating.EndValue;
			if (!float.TryParse(coachingSkill.UserRating, out rating)) rating = _maxRating.EndValue;
			OnRatingChanged(rating/_maxRating.EndValue);
			theSlider.value = rating/_maxRating.EndValue;
		} else {
			transform.Find ("FieldObservationsEntryBoxImage/FieldObservationsEntryText").GetComponent<InputField> ().text = "";
			// set the slider to the right value...
			OnRatingChanged(1);
			theSlider.value = 1;
		}
		PopulateObservations ();
		PopulateCoachingPlans ();
	}

	private void OnAddToFCR(string EventName, ref object Data){ // no payload here yet
		// the coaching skill was once already part of the FCR, it was added when the page was opened by dashboard controller, but now we do that here
		// but the text and slider values have not been set in it yet.  do that now

		if (_lastManifestReceived == null) return; // we cant succeed.

		//check if there is an unsubmitted coaching skill for this FCR already.
		var fcrSkill = _lastManifestReceived.GetFcrCoachingSkill(_csTeamMember.Id, _csCategory.ID);
		if (fcrSkill == null)
		{
			fcrSkill = new CoachingSkill(_csCategory.ID);
			_lastManifestReceived.AddFcrCoachingSkill(_csTeamMember.Id, fcrSkill);
		}

		CoachingSkill coachingSkill = GetCoachingSkill ();
		if (coachingSkill != null) {
			coachingSkill.FieldRideObservations = transform.Find("FieldObservationsEntryBoxImage/FieldObservationsEntryText").GetComponent<InputField>().text;
			coachingSkill.UserRating = currValue.ToString("F1");
		}
		_unsubmittedFCRUpdated.FireEvent (); // dashboard controller returns to team member view
	}

	public TeamMember GetTeamMember(){
		return _csTeamMember;
	}
	public CoachingSkillCategory GetCoachingSkillCategory(){
		return _csCategory;
	}

	private void OnCreateNewCoachingPlanPopUpSave(string EventName, ref object Data){
		PopulateCoachingPlans ();
		// also clear out the popup entry field
	}

    /// <summary>
    /// Fires when the rating control is setup
    /// </summary>
    /// <param name="eventName"></param>
    /// <param name="data"></param>
    private void OnSetRating(string eventName, ref object data)
    {
        try
        {
            //clear all the fields by default
            foreach (var numTxt in NumberText)
            {
                numTxt.text = "";
            }

            foreach (var ratingTxt in RatingText)
            {
                ratingTxt.text = "";
            }

            _ratings = (Rating[]) data;
            if (_ratings == null) return;

            _maxRating = _ratings.OrderByDescending(a => a.EndValue).First();

            _numberText.Clear();
            _descriptionText.Clear();

            // set the team members name field
            if (_csTeamMember != null)
                NameText.text = _csTeamMember.Name;

            // if there's already a rating set for this skill, move the slider to that value.
            PopulateObservations();
            PopulateCoachingPlans();

            switch (_ratings.Length)
            {
                case 1:
                    NumberText[0].text = "";
                    NumberText[1].text = "";
                    NumberText[2].text = "0 - " + _ratings[0].EndValue;
                    NumberText[3].text = "";
                    NumberText[4].text = "";
                    RatingText[2].text = _ratings[0].Description;
                    _numberText.Add(NumberText[2]);
                    _descriptionText.Add(RatingText[2]);
                    break;
                case 2:
                    NumberText[0].text = "0 - " + _ratings[0].EndValue;
                    NumberText[1].text = "";
                    NumberText[2].text = "";
                    NumberText[3].text = "";
                    NumberText[4].text = _ratings[0].EndValue + " - " + _ratings[1].EndValue;
                    RatingText[0].text = _ratings[0].Description;
                    RatingText[4].text = _ratings[1].Description;
                    _numberText.Add(NumberText[0]);
                    _numberText.Add(NumberText[4]);
                    _descriptionText.Add(RatingText[0]);
                    _descriptionText.Add(RatingText[4]);
                    break;
                case 3:
                    NumberText[0].text = "0 - " + _ratings[0].EndValue;
                    NumberText[1].text = "";
                    NumberText[2].text = _ratings[0].EndValue + " - " + _ratings[1].EndValue;
                    NumberText[3].text = "";
                    NumberText[4].text = _ratings[1].EndValue + " - " + _ratings[2].EndValue;
                    RatingText[0].text = _ratings[0].Description;
                    RatingText[2].text = _ratings[1].Description;
                    RatingText[4].text = _ratings[2].Description;
                    _numberText.Add(NumberText[0]);
                    _numberText.Add(NumberText[1]);
                    _numberText.Add(NumberText[2]);
                    _descriptionText.Add(RatingText[0]);
                    _descriptionText.Add(RatingText[2]);
                    _descriptionText.Add(RatingText[4]);
                    break;
                case 4:
                    NumberText[0].text = "0 - " + _ratings[0].EndValue;
                    NumberText[1].text = _ratings[0].EndValue + " - " + _ratings[1].EndValue;
                    NumberText[2].text = _ratings[1].EndValue + " - " + _ratings[2].EndValue;
                    NumberText[3].text = "";
                    NumberText[4].text = _ratings[2].EndValue + " - " + _ratings[3].EndValue;
                    RatingText[0].text = _ratings[0].Description;
                    RatingText[1].text = _ratings[1].Description;
                    RatingText[2].text = _ratings[2].Description;
                    RatingText[4].text = _ratings[4].Description;
                    _numberText.Add(NumberText[0]);
                    _numberText.Add(NumberText[1]);
                    _numberText.Add(NumberText[2]);
                    _numberText.Add(NumberText[3]);
                    _descriptionText.Add(RatingText[0]);
                    _descriptionText.Add(RatingText[1]);
                    _descriptionText.Add(RatingText[2]);
                    _descriptionText.Add(RatingText[4]);
                    break;
                default:
                    if (_ratings.Length >= 5)
                    {
                        NumberText[0].text = "0 - " + _ratings[0].EndValue;
                        NumberText[1].text = _ratings[0].EndValue + " - " + _ratings[1].EndValue;
                        NumberText[2].text = _ratings[1].EndValue + " - " + _ratings[2].EndValue;
                        NumberText[3].text = _ratings[2].EndValue + " - " + _ratings[3].EndValue;
                        NumberText[4].text = _ratings[3].EndValue + " - " + _ratings[4].EndValue;

                        for (var ratingIndex = 0; ratingIndex < _ratings.Length; ratingIndex++)
                        {
                            RatingText[ratingIndex].text = _ratings[ratingIndex].Description;
                            _descriptionText.Add(RatingText[ratingIndex]);
                        }

                        _numberText.Add(NumberText[0]);
                        _numberText.Add(NumberText[1]);
                        _numberText.Add(NumberText[2]);
                        _numberText.Add(NumberText[3]);
                        _numberText.Add(NumberText[4]);
                    }
                    break;
            }
        }
        catch (Exception ex)
        {
            Debug.LogException(ex);
        }
    }

    public void OnUpdateCoachingPlans(){
		PopulateCoachingPlans ();
	}

	public void OnInfoButton(){
		if (_csCategory.InfoFiles == null) return;
		if (_csCategory.InfoFiles.Length <= 0) return;
		
		#if UNITY_IPHONE
		PDFReader.OpenHTMLLocal(_csCategory.InfoFiles[0], _csCategory.Title);
		#endif
	}

	private void PopulateObservations(){

		observationsText.text = "";
		CoachingSkill coachingSkill = GetCoachingSkill();
		if (coachingSkill != null){
			observationsText.text = coachingSkill.FieldRideObservations;
		}
	}

	private void PopulateCoachingPlans(){

		foreach (GameObject go in _spawnedCoachingPlanRows)
		{
			Destroy(go);
		}
		_spawnedCoachingPlanRows.Clear();
		coachingPlanTemplate.SetActive (false);

		CoachingSkill coachingSkill = GetCoachingSkill();
		if (coachingSkill != null){
			// it would be more efficient to check if these match then only add the new one,
			// but this is the lazy way... 

			if (coachingSkill.CoachingPlanList.Count>0){
				float combinedHeight = 0;
				foreach (CoachingPlan cp in coachingSkill.CoachingPlanList){
					GameObject newInstance = Instantiate(coachingPlanTemplate) as GameObject;
					newInstance.GetComponent<CoachingPlanView>().plan = cp;
					newInstance.transform.SetParent(coachingPlanContainer.transform); // layout should do the rest.
					newInstance.transform.localScale = new Vector3(1,1,1);
					newInstance.SetActive (true);
					// set the textvalues.
					newInstance.transform.Find("CoachingPlanText").GetComponent<Text>().text = cp.Title;
					newInstance.transform.Find("DueDateText").GetComponent<Text>().text = cp.Date.Date.ToString("MM/dd/yyyy");
					newInstance.transform.Find("AddReminderText").GetComponent<Text>().text = ( cp.Remind ? "Y" : "N" );
					_spawnedCoachingPlanRows.Add(newInstance);

					float elementHeight = 48 + newInstance.GetComponent<LayoutElement>().preferredHeight * cp.Title.Length/350.0f;
					newInstance.GetComponent<LayoutElement>().preferredHeight = elementHeight;
					combinedHeight += elementHeight;
				}
				// the SizeFitter component doesn't do well at this level, so we will set the container size ourselves.
				//		TeamMemberContainer.GetComponent<RectTransform> ().SetSizeWithCurrentAnchors (RectTransform.Axis.Horizontal, (_teamMemberInstances.Count+0.5f) * TeamMemberTemplate.GetComponent<RectTransform> ().rect.width);
				
				coachingPlanContainer.GetComponent<RectTransform> ().anchorMax = new Vector2(1, 0);
				coachingPlanContainer.GetComponent<RectTransform> ().anchorMin = new Vector2(.0f, 0f);
				float height = (coachingSkill.CoachingPlanList.Count + 0.25f) * coachingPlanContainer.GetComponent<RectTransform> ().rect.height;
				height = combinedHeight + 24;
				if (height < coachingPlanContainer.transform.parent.GetComponent<RectTransform> ().rect.height)
					height = coachingPlanContainer.transform.parent.GetComponent<RectTransform> ().rect.height;
				coachingPlanContainer.GetComponent<RectTransform> ().sizeDelta = new Vector2(0,height);
				coachingPlanContainer.transform.parent.GetComponent<ScrollRect> ().verticalNormalizedPosition = 1;
			}
		}
	}

	private CoachingSkill GetCoachingSkill(){
		CoachingSkill result = null;

		if (_lastManifestReceived != null &&
			_csTeamMember != null &&
			_csCategory != null &&
		    _lastManifestReceived.GetUnsubmittedFcr (_csTeamMember.Id) != null ) {
			FCR unsubmittedFCR = _lastManifestReceived.GetUnsubmittedFcr (_csTeamMember.Id);

			foreach (CoachingSkill cs in unsubmittedFCR.CoachingSkills) {
				if (cs.CategoryID == _csCategory.ID) {
					result = cs;
					break;
				}
			}
		}
		return result;
	}

	private CoachingSkill GetMostRecentCoachingSkill(){
		// go through ALL this team member's FCR's searching for the most recent skill rating for this skill.
		// first, find the list of FCR's for this team member
		// find the team member in the FCR UserInfo
		CoachingSkill mostRecentCoachingSkill = null;
		System.DateTime mostRecentSkillRatingDate = System.DateTime.MinValue;
		foreach (FCR testFCR in _csTeamMember.FCRList) {
			if (testFCR.CreationDate > mostRecentSkillRatingDate){
				foreach (CoachingSkill testCS in testFCR.CoachingSkills){
					if (testCS.CategoryID == _csCategory.ID){
						mostRecentCoachingSkill = testCS;
						mostRecentSkillRatingDate = testFCR.CreationDate;
						break;
					}
				}

			}
		}
		return mostRecentCoachingSkill;
	}

    /// <summary>
    /// Fires when the rating slider changes; changes the color of the slider and 
    /// the slider button text box
    /// </summary>
    /// <param name="pct"></param>
	public void OnRatingChanged(float pct)
	{
        currValue = (_maxRating.EndValue * pct);	  
	    var bin = _ratings.FirstOrDefault(p => currValue < p.EndValue);

	    if (currValue >= _maxRating.EndValue || bin == null)
	    {
	        bin = _maxRating;
	    }

	    var index = Array.IndexOf(_ratings, bin);
	    var midRating = (_maxRating.EndValue*0.5F);
	    TodaysRating.text = "Today's Rating" + '\n' + decimal.Parse((currValue).ToString("0.0"));

        HighlightText(index);

        if (currValue < midRating)
	    {
			var colorPct = 0.9f*currValue/midRating; //(index / (_ratings.Length * 1F));
            KnobImage.color = new Color(0.9f, colorPct, 0);
            BackImage.color = new Color(0.9f, colorPct, 0);
	    }
	    else
	    {
            var percentage = 0.9f - ((_maxRating.EndValue - currValue) / midRating)*0.9f;
            KnobImage.color = new Color(0.9f - percentage, 0.9f, 0);
            BackImage.color = new Color(0.9f - percentage, 0.9f, 0);
	    }
	}
	
    /// <summary>
    /// Highlights the rating text
    /// </summary>
    /// <param name="index"></param>
	private void HighlightText(int index)
	{
        for (var textIndex = 0; textIndex < _numberText.Count; textIndex++)
		{
			if (index == textIndex)
			{
				_descriptionText[textIndex].color = Color.black;
				_descriptionText[textIndex].gameObject.SetActive(true);
				_numberText[textIndex].color = Color.black;
			}
			else
			{
			    if (textIndex >= _numberText.Count) continue;
			    _numberText[textIndex].color = new Color(.717647f, .717647f, .717647f);
				_descriptionText[textIndex].gameObject.SetActive(false);
			    _descriptionText[textIndex].color = new Color(.717647f, .717647f, .717647f);
			}
		}
    }

    #endregion
}