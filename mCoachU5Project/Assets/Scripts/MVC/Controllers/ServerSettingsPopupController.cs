﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Assets.Scripts.Tools;
using MCoach;

public class ServerSettingsPopupController : MonoBehaviour {

	public Text ServerTypeText;
	public Text ServerStatusText;
	public InputField ServerURLInput;
	public GameObject TestURLButton;
	public InputField RetryCountInput;
	public GameObject SetRetryButton;
	public GameObject WarningPanel;
	public GameObject OfflineWarningPanel;
	public Text WarningURL;
	public Text NoConnectionURL;
	public GameObject ResetPasswordButton;
	public Text ResetPasswordURLText;
	float lastFieldUpdate = 0;
	float lastPing = 15;

	private UIEvent _pingWebServices = new UIEvent(Constants.Events.PingWebServicesEventId.ToString());
	
	// Use this for initialization
	void Start () {
		GlobalMessageManager.AddEvent(_pingWebServices);
		Invoke ("InitFields", 1.5f); // need to allow webCallControler to get spawned and pull values from prefs.
	}

	void OnDestroy()
	{
		GlobalMessageManager.RemoveEvent(_pingWebServices);
	}

	private void InitFields(){
		WarningURL.text = WebServiceTools.WebServiceDomainName;
		NoConnectionURL.text = WebServiceTools.WebServiceDomainName;
		// moved to OnShowDialog
//		RetryCountInput.text = FileSystemController.maxRetries.ToString ();  // Setting an input field's text can affect other input fields, so don't do it here.
	}
	
	// Update is called once per frame
	void Update () {
		lastFieldUpdate -= Time.deltaTime;
		lastPing -= Time.deltaTime;
		if (lastFieldUpdate < 0) {
			UpdateFields();
			lastFieldUpdate = 1; // check 1 sec intervals
		}
		if (lastPing < 0){
			_pingWebServices.FireEvent();
			// these online status checks will only happen when the app is launched and on the login screen.
			if (WebCallController.isOnline)
				lastPing = 60; // if online, only check for loss of server every 1 minutes
			else
				lastPing = 15; // if offline, look for server every 15 seconds because ping timeout is 10 sec
		}

	}

	void UpdateFields(){

		WarningURL.text = WebServiceTools.WebServiceDomainName;
		NoConnectionURL.text = WebServiceTools.WebServiceDomainName;


		if (WebCallController.isOnline) {
			ServerStatusText.text = "CONNECTED";
			OfflineWarningPanel.SetActive (false);
			ResetPasswordButton.SetActive( true); 
			if (WebCallController.ServerType != Manifest.ServerTypes.LIVE) {
				WarningPanel.SetActive(true);
				ResetPasswordURLText.text = "https://"+WebServiceTools.WebServiceDomainName+"/ChangePassword.aspx"; //SSL change 9/1/16
			}
			else
			{
				WarningPanel.SetActive(false);
				ResetPasswordURLText.text = "https://"+WebServiceTools.WebServiceDomainName+"/ChangePassword.aspx";
			}
			ServerTypeText.text = WebCallController.ServerType.ToString ();
		} else {
			ResetPasswordButton.SetActive( false); 
			OfflineWarningPanel.SetActive (true);
			ServerStatusText.text = "OFFLINE";
			WarningPanel.SetActive(false);
		}
		if (ServerURLInput.text == WebServiceTools.WebServiceDomainName)
			TestURLButton.SetActive (false);
		else
			TestURLButton.SetActive (true);

		if (RetryCountInput.text == PlayerPrefs.GetString ("maxRetries","30"))
			SetRetryButton.SetActive (false);
		else
			SetRetryButton.SetActive (true);
		
	}

	public void OnShowDialog(){
		ServerURLInput.textComponent.text = WebServiceTools.WebServiceDomainName;
		ServerURLInput.text = WebServiceTools.WebServiceDomainName;
		RetryCountInput.text = FileSystemController.maxRetries.ToString ();
	}

	public void OnTestURL(){

		// If we are changing servers, purge the file cache
		if (PlayerPrefs.HasKey("webServicesDomainName")){
			string currentServer = PlayerPrefs.GetString("webServicesDomainName");
			if (currentServer != ServerURLInput.text){
				FindObjectOfType<FileSystemController>().PurgeFileCache();
			}
		}

		// save the preference,set the active URL, refresh all the fields, and force a ping.

		PlayerPrefs.SetString ("webServicesDomainName", ServerURLInput.text);
		WebServiceTools.WebServiceDomainName = ServerURLInput.text;
		_pingWebServices.FireEvent(); 
	}

	public void OnSetRetries(){
		int retries = FileSystemController.maxRetries;
		if (int.TryParse (RetryCountInput.text, out retries)) {
			FileSystemController.maxRetries = retries;
			PlayerPrefs.SetString("maxRetries",retries.ToString());
		} else 
			RetryCountInput.text = FileSystemController.maxRetries.ToString ();
	}
}
