﻿using UnityEngine;
using MCoach;
using System.Collections.Generic;
using Assets.Scripts.Tools;


public class ContentListController : MonoBehaviour
{
    #region Properties 

    /// <summary>
    /// Stores the content cache when it arrives from the MyContentUpdated event
    /// </summary>
    private Dictionary<string, Content[]> _contentCache = new Dictionary<string, Content[]>();

    /// <summary>
    /// This event notifies the content list view that a new section is to be viewed
    /// </summary>
    private readonly UIEvent _myContentSectionNotify = new UIEvent(Constants.Events.MyContentSectionUpdateEventId.ToString());
	private readonly UIEvent _menu1Disable = new UIEvent(Constants.Events.Menu1DisableEventId.ToString());
	private readonly UIEvent _menu2Disable = new UIEvent(Constants.Events.Menu2DisableEventId.ToString());
	private readonly UIEvent _menu3Disable = new UIEvent(Constants.Events.Menu3DisableEventId.ToString());
	private readonly UIEvent _menu4Disable = new UIEvent(Constants.Events.Menu4DisableEventId.ToString());
	private readonly UIEvent _menu5Disable = new UIEvent(Constants.Events.Menu5DisableEventId.ToString());

	/// <summary>
    /// Stores the last requested content section
    /// </summary>
    private string _lastContentRequested = "";

    #endregion

    #region Methods

    /// <summary>
    /// Called once during the duration of the app.
    /// Initalizes events and listeners. Method should only
    /// be triggered one time!
    /// </summary>
    private void Awake()
    {
        //Debug.LogError("Content List Controller awoken");
        InitializeEvents();
        InitializeListeners();
    }

    /// <summary>
    /// Initializes the dashboard controller events. ONLY CALL ONCE!
    /// </summary>
    private void InitializeEvents()
    {
        GlobalMessageManager.AddEvent(_myContentSectionNotify);
		GlobalMessageManager.AddEvent(_menu1Disable);
		GlobalMessageManager.AddEvent(_menu2Disable);
		GlobalMessageManager.AddEvent(_menu3Disable);
		GlobalMessageManager.AddEvent(_menu4Disable);
		GlobalMessageManager.AddEvent(_menu5Disable);
    }

    /// <summary>
    /// Initializes listeners for dashboard controller. ONLY CALL ONCE!
    /// </summary>
    private void InitializeListeners()
    {
        GlobalMessageManager.AddListener(Constants.Events.MyContentUpdatedEventId.ToString(), OnMyContentUpdated);
        GlobalMessageManager.AddListener(Constants.Events.MyContentSectionRequestEventId.ToString(), OnRequestSection);
        GlobalMessageManager.AddListener(Constants.Events.DISCButtonClickEventId.ToString(), OnDiscButtonClicked);
        GlobalMessageManager.AddListener(Constants.Events.CompetencyButtonClickEventId.ToString(), OnCompentencyButtonClicked);
        GlobalMessageManager.AddListener(Constants.Events.CoachingGuideButtonClickEventId.ToString(), OnCoachingGuideButtonClicked);
        GlobalMessageManager.AddListener(Constants.Events.TrainingResourcesButtonClickEventId.ToString(), OnTrainingResourceButtonClicked);
        GlobalMessageManager.AddListener(Constants.Events.PulseButtonClickEventId.ToString(), OnPulseButtonClicked); 
    }

    /// <summary>
    /// Handles the user content load.
    /// </summary>
    /// <param name="eventName"></param>
    /// <param name="data"></param>
    private void OnMyContentUpdated(string eventName, ref object data)
    {
        if (data == null) return;
        
        _contentCache = (data as Dictionary<string, Content[]>) ?? new Dictionary<string, Content[]>();

		// disable any menu buttons if there is no content for that section
		//TODO this should not be hard coded to these particulat section names...
		if (!_contentCache.ContainsKey ("DISC") || _contentCache["DISC"].Length == 0)
			_menu1Disable.FireEvent ();
		if (!_contentCache.ContainsKey ("COMP_MODEL") || _contentCache["COMP_MODEL"].Length == 0)
			_menu2Disable.FireEvent ();
		if (!_contentCache.ContainsKey ("COACH_GUIDE") || _contentCache["COACH_GUIDE"].Length == 0)
			_menu3Disable.FireEvent ();
		if (!_contentCache.ContainsKey ("PULSE") || _contentCache["PULSE"].Length == 0)
			_menu4Disable.FireEvent ();
		if (!_contentCache.ContainsKey ("TRAIN_RES") || _contentCache["TRAIN_RES"].Length == 0)
			_menu5Disable.FireEvent ();
    }

    /// <summary>
    /// Handles the user content load.
    /// </summary>
    /// <param name="eventName"></param>
    /// <param name="data"></param>
    private void OnRequestSection(string eventName, ref object data)
    {
        if (_lastContentRequested == "") return;
        if (!_contentCache.ContainsKey(_lastContentRequested)) return;

        object section = _contentCache[_lastContentRequested];
        _myContentSectionNotify.FireEvent(ref section);
    }
    
    /// <summary>
    /// Handles the on DISC button click event
    /// </summary>
    private void OnDiscButtonClicked(string eventName, ref object data)
    {
        ContentButtonClickHandler("DISC");
    }

    /// <summary>
    /// Handles the on Compentency Model button click event
    /// </summary>
    private void OnCompentencyButtonClicked(string eventName, ref object data)
    {
        ContentButtonClickHandler("COMP_MODEL");
    }

    /// <summary>
    /// Handles the on Coaching Guide button click event
    /// </summary>
    private void OnCoachingGuideButtonClicked(string eventName, ref object data)
    {
        ContentButtonClickHandler("COACH_GUIDE");
    }

    /// <summary>
    /// Handles the on training resources button click event
    /// </summary>
    private void OnTrainingResourceButtonClicked(string eventName, ref object data)
    {
        ContentButtonClickHandler("TRAIN_RES");
    }

    /// <summary>
    /// Handles the on pulse button click event
    /// </summary>
    private void OnPulseButtonClicked(string eventName, ref object data)
    {
        ContentButtonClickHandler("PULSE");
    }

    /// <summary>
    /// handles a content button click
    /// </summary>
    /// <param name="sectionName"></param>
    private void ContentButtonClickHandler(string sectionName)
    {
        GameObject.Find("mCoachBackgroundImage").GetComponent<Animator>().Play("ContentList");

        _lastContentRequested = sectionName;        

        object section = (_contentCache.ContainsKey(sectionName)) ? _contentCache[_lastContentRequested] : new Content[0];

        _myContentSectionNotify.FireEvent(ref section);
    }

    #endregion
}

