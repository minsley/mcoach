﻿using UnityEngine;
using UnityEngine.UI;
using MCoach;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Tools;

/// <summary>
/// <para>Date: 07/06/2015</para>
/// <para>Author: NuMedia</para>
/// <para>Manages the mCoach Dashboard</para>
/// </summary>
public class DashboardController : MonoBehaviour
{
    /// <summary>
    /// Event is triggered when a copy of the current manifest is needed.
    /// Response event should be: ManifestUpdated
    /// </summary>
    private UIEvent _requestManifest = new UIEvent(Constants.Events.RequestManifestEventId.ToString());

	private UIEvent _competencyHeaderText = new UIEvent(Constants.Events.CompetencyHeaderTextEventId.ToString());
	private UIEvent _planNameText = new UIEvent(Constants.Events.PlanNameTextEventId.ToString());
	private UIEvent _previousPlanNameText = new UIEvent(Constants.Events.PreviousPlanNameTextEventId.ToString());
	private UIEvent _productHeaderText = new UIEvent(Constants.Events.ProductHeaderTextEventId.ToString());
    /// <summary>
    /// Event is triggered when the user's name is changed or updated.
    /// </summary>
    private UIEvent _userFullName = new UIEvent(Constants.Events.UserFullNameEventId.ToString());

    /// <summary>
    /// Event is triggered when the user's disctrict is either changed or updated.
    /// </summary>
    private UIEvent _userDistrict = new UIEvent(Constants.Events.UserDistrictEventId.ToString());

    /// <summary>
    /// Event is triggered when the user's last login date is
    /// updated.
    /// </summary>
    private UIEvent _userLastLoginDate = new UIEvent(Constants.Events.UserLastLoginDateEventId.ToString());
    private UIEvent _userFirstName = new UIEvent(Constants.Events.UserFirstNameEventId.ToString());
    private UIEvent _userLastName = new UIEvent(Constants.Events.UserLastNameEventId.ToString());
	private UIEvent _userLevelLabel = new UIEvent(Constants.Events.UserLevelLabelEventId.ToString());
    private UIEvent _userTerritory = new UIEvent(Constants.Events.UserTerritoryEventId.ToString());
    private UIEvent _userEmail = new UIEvent(Constants.Events.UserEmailEventId.ToString());
    private UIEvent _userName = new UIEvent(Constants.Events.UserNameEventId.ToString());

    /// <summary>
    /// Event is triggered when the user's team is updated.
    /// </summary>
    private UIEvent _myTeamUpdate = new UIEvent(Constants.Events.MyTeamUpdateEventId.ToString());
	private UIEvent _teamMemberSelected = new UIEvent(Constants.Events.TeamMemberSelectedEventId.ToString());
	
    /// <summary>
    /// Event is triggered when the user's content is updated.
    /// </summary>
    private UIEvent _myContentUpdate = new UIEvent(Constants.Events.MyContentUpdatedEventId.ToString());

    /// <summary>
    /// Event is triggered when the tip of the day title is updated.
    /// </summary>
    private UIEvent _tipOfTheDayTitle = new UIEvent(Constants.Events.TipOfTheDayTitleEventId.ToString());
    
    /// <summary>
    /// Event is triggered when a the tip of the day's description is updated.
    /// </summary>
    private UIEvent _tipOfTheDayDescription = new UIEvent(Constants.Events.TipOfTheDayDescriptionEventId.ToString());
    
	/// <summary>
	/// Event is triggered when new color scheme is received.
	/// </summary>
	private UIEvent _colorSchemeEvent = new UIEvent(Constants.Events.ColorSchemeEventId.ToString());

    /// <summary>
    /// Event is triggered when the current date is updated.
    /// </summary>
    private UIEvent _currentDate = new UIEvent(Constants.Events.CurrentDateEventId.ToString());
    
    /// <summary>
    /// Event is triggered when a payload token is received.
    /// </summary>
    private UIEvent _getPayload = new UIEvent(Constants.Events.GetPayloadEventId.ToString());
    
    /// <summary>
    /// Event is triggered when the profile image is needed.
    /// </summary>
    private UIEvent _requestProfileImage = new UIEvent(Constants.Events.RequestProfileImageEventId.ToString());
	private UIEvent _requestLogoImage = new UIEvent(Constants.Events.RequestLogoImageEventId.ToString());
	private UIEvent _requestMenu1Image = new UIEvent(Constants.Events.RequestMenu1ImageEventId.ToString());
	private UIEvent _requestMenu2Image = new UIEvent(Constants.Events.RequestMenu2ImageEventId.ToString());
	private UIEvent _requestMenu3Image = new UIEvent(Constants.Events.RequestMenu3ImageEventId.ToString());
	private UIEvent _requestMenu4Image = new UIEvent(Constants.Events.RequestMenu4ImageEventId.ToString());
	private UIEvent _requestMenu5Image = new UIEvent(Constants.Events.RequestMenu5ImageEventId.ToString());
	private UIEvent _menu1Text = new UIEvent(Constants.Events.Menu1TextEventId.ToString());
	private UIEvent _menu2Text = new UIEvent(Constants.Events.Menu2TextEventId.ToString());
	private UIEvent _menu3Text = new UIEvent(Constants.Events.Menu3TextEventId.ToString());
	private UIEvent _menu4Text = new UIEvent(Constants.Events.Menu4TextEventId.ToString());
	private UIEvent _menu5Text = new UIEvent(Constants.Events.Menu5TextEventId.ToString());
    /// <summary>
    /// Event resets all team members to their normal states.
    /// </summary>
    private UIEvent _teamMemberNormal = new UIEvent(Constants.Events.TeamMemberNormalEventId.ToString());
    /// <summary>
    /// Event fires when dashboard menu item should be highlighted.
    /// </summary>
    private UIEvent _selectDashboard = new UIEvent(Constants.Events.SelectDashboardEventId.ToString());
	private UIEvent _selectFCR = new UIEvent(Constants.Events.FCRButtonClickEventId.ToString());
	private UIEvent _FCRButtonDisable = new UIEvent(Constants.Events.FCRButtonDisableEventId.ToString());
	private UIEvent _coachingSkills = new UIEvent(Constants.Events.CoachingSkillsEventId.ToString());
	private UIEvent _globalSkillTitle = new UIEvent(Constants.Events.GlobalSkillTitleEventId.ToString());
	private UIEvent _ratings = new UIEvent(Constants.Events.RatingsEventId.ToString());
	private UIEvent _teamCoachingSkills = new UIEvent(Constants.Events.TeamCoachingSkillsEventId.ToString());
	private UIEvent _viewFCR = new UIEvent (Constants.Events.ViewFCREventID.ToString ());
	private UIEvent _viewFCRIndex = new UIEvent (Constants.Events.ViewFCRIndexEventID.ToString ());
	private Manifest _lastReceivedManifest = null;
	private TeamMember _currentTeamMember = null;
    /// <summary>
    /// An array storing the short form for all 12 months: 0-11.
    /// </summary>
    private string[] _monthList = { "Jan", "Feb", "Mar", "Apr", "May",
        "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec" };
        
    /// <summary>
    /// Flag representing of the DashboardLoaded event has been received.
    /// </summary>
    private bool _dashboardLoaded = false;

    /// <summary>
    /// Called once during the duration of the app.
    /// Initalizes events and listeners. Method should only
    /// be triggered one time!
    /// </summary>
    private void Awake()
    {
        InitializeEvents();
        InitializeListeners();
//       Debug.Log(Application.persistentDataPath);
    }

    /// <summary>
    /// Initializes the dashboard controller events. ONLY CALL ONCE!
    /// </summary>
    private void InitializeEvents()
    {
        GlobalMessageManager.AddEvent(_requestManifest);
		GlobalMessageManager.AddEvent(_competencyHeaderText);
		GlobalMessageManager.AddEvent(_planNameText);
		GlobalMessageManager.AddEvent(_previousPlanNameText);
		GlobalMessageManager.AddEvent(_productHeaderText);
        GlobalMessageManager.AddEvent(_userFullName);
        GlobalMessageManager.AddEvent(_userDistrict);
        GlobalMessageManager.AddEvent(_userLastLoginDate);
		GlobalMessageManager.AddEvent(_myTeamUpdate);
		GlobalMessageManager.AddEvent(_teamMemberSelected);
		GlobalMessageManager.AddEvent(_myContentUpdate);
        GlobalMessageManager.AddEvent(_tipOfTheDayTitle);
        GlobalMessageManager.AddEvent(_tipOfTheDayDescription);
		GlobalMessageManager.AddEvent(_colorSchemeEvent);
        GlobalMessageManager.AddEvent(_currentDate);
        GlobalMessageManager.AddEvent(_getPayload);
        GlobalMessageManager.AddEvent(_requestProfileImage);
		GlobalMessageManager.AddEvent(_requestLogoImage);
		GlobalMessageManager.AddEvent(_requestMenu1Image);
		GlobalMessageManager.AddEvent(_requestMenu2Image);
		GlobalMessageManager.AddEvent(_requestMenu3Image);
		GlobalMessageManager.AddEvent(_requestMenu4Image);
		GlobalMessageManager.AddEvent(_requestMenu5Image);
		GlobalMessageManager.AddEvent(_menu1Text);
		GlobalMessageManager.AddEvent(_menu2Text);
		GlobalMessageManager.AddEvent(_menu3Text);
		GlobalMessageManager.AddEvent(_menu4Text);
		GlobalMessageManager.AddEvent(_menu5Text);
		GlobalMessageManager.AddEvent(_selectDashboard);
		GlobalMessageManager.AddEvent (_selectFCR);
		GlobalMessageManager.AddEvent (_FCRButtonDisable);
        GlobalMessageManager.AddEvent(_teamMemberNormal);
        GlobalMessageManager.AddEvent(_userFirstName);
        GlobalMessageManager.AddEvent(_userLastName);
        GlobalMessageManager.AddEvent(_userLastLoginDate);
		GlobalMessageManager.AddEvent(_userLevelLabel);
        GlobalMessageManager.AddEvent(_userTerritory);
        GlobalMessageManager.AddEvent(_userEmail);
        GlobalMessageManager.AddEvent(_userName);
	    GlobalMessageManager.AddEvent(_coachingSkills);
	    GlobalMessageManager.AddEvent(_globalSkillTitle);
	    GlobalMessageManager.AddEvent(_ratings);
	    GlobalMessageManager.AddEvent(_teamCoachingSkills);
		GlobalMessageManager.AddEvent(_viewFCR);
		GlobalMessageManager.AddEvent(_viewFCRIndex);
    }

    /// <summary>
    /// Initializes listeners for dashboard controller. ONLY CALL ONCE!
    /// </summary>
    private void InitializeListeners()
    {
        GlobalMessageManager.AddListener(Constants.Events.DashboardLoadedEventId.ToString(), OnDashboardLoaded);
        GlobalMessageManager.AddListener(Constants.Events.ManifestUpdatedEventId.ToString(), OnManifestUpdated);
        GlobalMessageManager.AddListener(Constants.Events.ProfileButtonClickEventId.ToString(), OnProfile);
        GlobalMessageManager.AddListener(Constants.Events.DashboardButtonClickEventId.ToString(), OnDashboard);
		GlobalMessageManager.AddListener (Constants.Events.FCRButtonClickEventId.ToString (), OnFCRButtonClicked);
		GlobalMessageManager.AddListener (Constants.Events.UnsubmittedFCRUpdatedEventId.ToString (), OnUnsubmittedFCRUpdated);
        GlobalMessageManager.AddListener(Constants.Events.CoachingCategorySelectedEventId.ToString(), OnCoachingCategorySelected);
        GlobalMessageManager.AddListener(Constants.Events.TeamMemberSelectedEventId.ToString(), OnTeamMemberSelected);
        GlobalMessageManager.AddListener(Constants.Events.LogoutButtonClickEventId.ToString(), OnLogoutButtonClick);
        GlobalMessageManager.AddListener(Constants.Events.RepresentativeSummaryClickEventId.ToString(), OnRepresentativeSummaryClicked);
        GlobalMessageManager.AddListener(Constants.Events.TeamMemberCSIDsEventId.ToString(), OnTeamMemberCSIDs);
    }

    /// <summary>
    /// Checks the image extension and replaces it if necessary
    /// </summary>
    /// <param name="imagePath"></param>
    /// <returns></returns>
    private static string CheckThumbnailImage(string imagePath)
    {
        var ext = (System.IO.Path.GetExtension(imagePath) ?? "").ToLower();
        switch (ext)
        {
            case ".mp3":
                {
                    return "mp3.png";
                }
            case ".mp4":
                {
                    return "mp4.png";
                }
            case ".pdf":
                {
                    return "pdf.png";
                }
            default:
                {
                    return imagePath;
                }
        }
    }

	private void OnTeamMemberSelected(string EventName, ref object Data)
	{
		_currentTeamMember = (TeamMember)Data;
		
		List<CoachingSkillCategory> csCategories = new List<CoachingSkillCategory>();
		for (int csIndex = 0; csIndex < _currentTeamMember.CSID.Length; csIndex++)
		{
			for (int manifestCSIndex = 0; manifestCSIndex < _lastReceivedManifest.CoachingCategories.Length; manifestCSIndex++)
			{
				if (_currentTeamMember.CSID[csIndex].Equals(_lastReceivedManifest.CoachingCategories[manifestCSIndex].ID))
				{
					csCategories.Add(_lastReceivedManifest.CoachingCategories[manifestCSIndex]);
					break;
				}
			}
		}


//		object csCategoriesRef = csCategories.ToArray();
//		object parms = new object[]{_currentTeamMember, csCategoriesRef};
//		_teamCoachingSkills.FireEvent(ref parms);

		SetFCRButtonEnabledState ();
	}


	private void OnUnsubmittedFCRUpdated(string EventName,ref object Data){

		// immediately show the unsubmitted FCR for this team member
		if (_currentTeamMember != null &&
			_lastReceivedManifest != null &&
			_lastReceivedManifest.GetUnsubmittedFcr (_currentTeamMember.Id) != null) {
			// need to force the button state to change as if it had been pressed...
			GameObject.Find("mCoachBackgroundImage").GetComponent<Animator>().Play("ShowFCRScreen");
			_viewFCR.FireEvent (); //TODO send the FCR we want to view.
		}
	}
		
	private void OnFCRButtonClicked(string EventName,ref object Data){
		// if the current team member has some FCR's, put up a content list view of them
		// if there is only the current unsubmitted FCR, then just show those details.
		if (_currentTeamMember != null &&
			_lastReceivedManifest != null){
			int teamMemberIndex = -1;
			for (int i = 0; i< _lastReceivedManifest.UserInfo.MyTeam.Count; i++){
				if (_lastReceivedManifest.UserInfo.MyTeam[i] == _currentTeamMember){
					teamMemberIndex = i;
					break;
				}
			}
			if (teamMemberIndex <0) return;


			if (_lastReceivedManifest.UserInfo.MyTeam[teamMemberIndex].FCRList.Count == 1 &&
				_lastReceivedManifest.GetUnsubmittedFcr (_currentTeamMember.Id) != null) {
			GameObject.Find ("mCoachBackgroundImage").GetComponent<Animator> ().Play ("ShowFCRScreen"); // animation shows top level TeamMemberScreen
			_viewFCR.FireEvent (); //TODO send the FCR we want to view.
			}
			else
			{
				if (_lastReceivedManifest.UserInfo.MyTeam[teamMemberIndex].FCRList.Count >0){
					// package up the FCR list and show as an index
					FCR[] theFCRs = _lastReceivedManifest.UserInfo.MyTeam[teamMemberIndex].FCRList.ToArray();
					object data = (object)theFCRs;
					_viewFCRIndex.FireEvent(ref data);
				}
			}
		}

	}

    /// <summary>
    /// Handles the DashboardLoaded event. Requests the latest manifest.
    /// </summary>
    private void OnDashboardLoaded(string EventName, ref object Data)
    {
		// the level has just been loaded, determine if we are a rep logged in, a normal login, or here from a drill down...

		// if we are a rep, then we don't want to populate the scrolling team header, but just go to our selected page. as if that is our
		// profile page.  allow accessing some documentation buttons ?

		// if we are a drilldown, then we ... ?





        _dashboardLoaded = true;
        _requestManifest.FireEvent();
        _selectDashboard.FireEvent();
		SetFCRButtonEnabledState ();
		// set the version label copied from the login screen level
		GameObject versionObject = GameObject.Find ("VersionText");
		if (versionObject != null && versionObject.GetComponent<Text> () != null) {
			versionObject.GetComponent<Text> ().text  = "Version " + Application.version + "  ";
			//LoginController lc = GameObject.FindObjectOfType<LoginController> ();
			//if (lc != null)
				//versionObject.GetComponent<Text> ().text = lc.VersionText;

		}
    }

    /// <summary>
    /// Handles the ManifestUpdated event. Triggers events updating the Dashboard.
    /// </summary>
    private void OnManifestUpdated(string EventName, ref object Data)
    {
        if (!_dashboardLoaded) return;
//        Debug.Log("OnManifestUpdated");
        
        if (Data.GetType().ToString().Equals("MCoach.Manifest"))
        {
			Manifest receivedManifest = (Manifest)Data;
			_lastReceivedManifest = receivedManifest;


			object reselectTeamMemberRef = null;
			// See if we are just updating some info withing the manifest, and if so, don't reconstruct everything, keep the same team and selected member.
			if (_currentTeamMember != null && receivedManifest.UserInfo.LevelNumber != receivedManifest.MaxLevels){
				if ( receivedManifest.UserInfo.MyTeam.Contains(_currentTeamMember)){
					//TODO this is probably an oversimplification of what needs to happen here, but a starting point.
					return;
				}
				else
				{  // check to see if the currently selected team member username is in the team, and if so, select that.
					bool matched = false;
					foreach (TeamMember newInstanceTeamMember in receivedManifest.UserInfo.MyTeam){
						if (_currentTeamMember.Username == newInstanceTeamMember.Username){
							matched = true;
							_currentTeamMember = newInstanceTeamMember;
							reselectTeamMemberRef = (object)newInstanceTeamMember;
							break;
						}
					}
					if (!matched)
						_currentTeamMember = null;
				}
			}

            for (int teamIndex = 0; teamIndex < receivedManifest.UserInfo.MyTeam.Count; teamIndex++)
            {
                if (receivedManifest.UserInfo.MyTeam[teamIndex].CSID == null)
                {
                    receivedManifest.UserInfo.MyTeam[teamIndex].CSID = new[] { 0 };
                }
            }
            
	        
	        //SET MY TEAM ID --> //receivedManifest.UserInfo.MyTeam[0].CSID
			// misc dynamic text
			object competencyHeader = (object)"Action Item/Competency";//<COMPETENCY HEADER>";
			if (receivedManifest.GlobalLabels.ContainsKey("COMPETENCY HEADER") && receivedManifest.GlobalLabels["COMPETENCY HEADER"]!= ""){
				competencyHeader = (object)(receivedManifest.GlobalLabels["COMPETENCY HEADER"]);
			}
			object planName = (object)"Coaching Plan";//<PLAN NAME>";
			if (receivedManifest.GlobalLabels.ContainsKey("PLAN NAME") && receivedManifest.GlobalLabels["PLAN NAME"]!= ""){
				planName = (object)(receivedManifest.GlobalLabels["PLAN NAME"]);
			}
			object previousPlanName = (object)"Previous Plan";//<PLAN NAME>";
			if (receivedManifest.GlobalLabels.ContainsKey("PLAN NAME - PREVIOUS") && receivedManifest.GlobalLabels["PLAN NAME - PREVIOUS"]!= ""){
				previousPlanName = (object)(receivedManifest.GlobalLabels["PLAN NAME - PREVIOUS"]);
			}
			object productHeader = (object)"Sales Data";//<PRODUCT HEADER>";
			if (receivedManifest.GlobalLabels.ContainsKey("PRODUCT HEADER") && receivedManifest.GlobalLabels["PRODUCT HEADER"]!= ""){
				productHeader = (object)(receivedManifest.GlobalLabels["PRODUCT HEADER"]);
			}
//            Debug.Log("UserName: " + receivedManifest.UserInfo.Name);
            object fullName = receivedManifest.UserInfo.Name;
            
			string levelLabel = "District: ";
			if (receivedManifest.LevelLabels != null && 
			    receivedManifest.LevelLabels.ContainsKey(receivedManifest.UserInfo.LevelNumber) &&
			    receivedManifest.LevelLabels[receivedManifest.UserInfo.LevelNumber]!= ""){
				levelLabel = (receivedManifest.LevelLabels[receivedManifest.UserInfo.LevelNumber]+": ");
			}
			object district = (object)(levelLabel+receivedManifest.UserInfo.District);
			object lastLogin = MCoachModel.LastLoginDate;//receivedManifest.UserInfo.LastLoginDate;
            object firstname = "";
            object lastname = "";
			object territory = (object)(receivedManifest.UserInfo.Territory); //
			object levelLabelObject = (object)levelLabel;
            object email = receivedManifest.UserInfo.Email;
            object userName = receivedManifest.UserInfo.UserName;
            
            char[] tempDelimiter = { ' ' };
            string[] firstAndLast = receivedManifest.UserInfo.Name.Split(tempDelimiter);
            if (firstAndLast.Length > 1)
            {
                firstname = firstAndLast[0];
                lastname = firstAndLast[1];
            }
            
            List<TeamMember> myTeam = new List<TeamMember>();
            char[] slashDelimiter = { '/' };
            
            for (int myTeamIndex = 0; myTeamIndex < receivedManifest.UserInfo.MyTeam.Count; myTeamIndex++)
            {
                myTeam.Add(receivedManifest.UserInfo.MyTeam[myTeamIndex]);
                //if(Tools.StringArrayContains(receivedManifest.CachedFiles, myTeam[myTeamIndex].ThumbnailFile))
                if (myTeam[myTeamIndex].ThumbnailFile.Split(slashDelimiter).Length <= 1)
                {
                   myTeam[myTeamIndex].ThumbnailFile = Application.persistentDataPath + "/" + receivedManifest.UserInfo.Id + "/" + myTeam[myTeamIndex].ThumbnailFile; 
                }
            }

            foreach (var t in receivedManifest.CoachingCategories)
            {
                var infoFiles = t.InfoFiles;
                if (infoFiles == null) continue;

                for (var skillImageIndex = 0; skillImageIndex < infoFiles.Length; skillImageIndex++)
                {
                    if (infoFiles[skillImageIndex].Split(slashDelimiter).Length <= 1)
                    {
                        infoFiles[skillImageIndex] = Application.persistentDataPath + "/" + receivedManifest.UserInfo.Id +
                                                     "/" +
                                                     infoFiles[skillImageIndex];
                    }
                }
            }

            var content = new Dictionary<string, Content[]>();

            foreach (var section in receivedManifest.UserInfo.MyContent.Keys)
            {
                var recievedContent = receivedManifest.UserInfo.MyContent[section];
                var contentItems = new List<Content>();

                foreach (var t in recievedContent)
                {
                    if (t.ThumbnailUrl.Split(slashDelimiter).Length <= 1)
                    {
                        t.ThumbnailUrl = Application.persistentDataPath + "/" +
                                         receivedManifest.UserInfo.Id + "/" +
                                         CheckThumbnailImage(t.ThumbnailUrl);   
                    }

                    if (t.ContentUrl.Split(slashDelimiter).Length <= 1)
                    {
                        t.ContentUrl = Application.persistentDataPath + "/" +
                                       receivedManifest.UserInfo.Id + "/" +
                                       t.ContentUrl;
                    }

                    contentItems.Add(t);
                }

                content[section] = contentItems.ToArray();
            }

            object myTeamRef = myTeam;
            object myContent = content;

			object[] teamUpdateObjects = new object[2];

			teamUpdateObjects[0] = myTeam as object;
			teamUpdateObjects[1] = _lastReceivedManifest as object;
			object teamUpdateData = teamUpdateObjects as object;
			_myTeamUpdate.FireEvent(ref teamUpdateData); // myTeamRef

			if (DrilldownController.GetInstance().Aliases.Count == 0){
				// Only update these items on initial login, not while drilling up or down...

				_competencyHeaderText.FireEvent(ref competencyHeader);
				_planNameText.FireEvent(ref planName);
				_previousPlanNameText.FireEvent (ref previousPlanName);
				_productHeaderText.FireEvent(ref productHeader);

	            _myContentUpdate.FireEvent(ref myContent);
	            _userFullName.FireEvent(ref fullName);
	            _userFirstName.FireEvent(ref firstname);
	            _userLastName.FireEvent(ref lastname);
				_userLevelLabel.FireEvent( ref levelLabelObject );
	            _userTerritory.FireEvent(ref territory);
	            _userEmail.FireEvent(ref email);
	            _userName.FireEvent(ref userName);
	            _userDistrict.FireEvent(ref district);
	            _userLastLoginDate.FireEvent(ref lastLogin);

	            object tipOfTheDayTitle = receivedManifest.TipOfTheDayTitle;
	            object tipOfTheDayDescription = receivedManifest.TipOfTheDayDescription;
	            _tipOfTheDayTitle.FireEvent(ref tipOfTheDayTitle);
	            _tipOfTheDayDescription.FireEvent(ref tipOfTheDayDescription);

	            object curDateRef = GetCurrentDate();
	            _currentDate.FireEvent(ref curDateRef);
	            
	            object profileFile = Application.persistentDataPath + "/" +receivedManifest.UserInfo.Id + "/" + receivedManifest.UserInfo.ThumbnailFile;
	            _requestProfileImage.FireEvent(ref profileFile);

				if (receivedManifest.Logo != null && receivedManifest.Logo != ""){
					object logoFile = Application.persistentDataPath + "/" +receivedManifest.UserInfo.Id + "/" + receivedManifest.Logo;
					_requestLogoImage.FireEvent(ref logoFile);
				}
				if (receivedManifest.PrimaryColor != null && receivedManifest.PrimaryColor != ""){
					int numColors = 1;
					if (receivedManifest.SecondaryColor != null && receivedManifest.SecondaryColor != "")
						numColors++;
					string[] colorHexValues = new string[numColors];
					colorHexValues[0] = receivedManifest.PrimaryColor;
					if (numColors > 1)
						colorHexValues[1] = receivedManifest.SecondaryColor;
					object colorArrayObject = (object)colorHexValues;
					_colorSchemeEvent.FireEvent(ref colorArrayObject);
					CalendarController cc = GameObject.Find("mCoachBackgroundImage").transform.GetComponentsInChildren<CalendarController>(true)[0];
					//	FindObjectOfType<CalendarController>();
					if (cc != null){
						cc.selectedDayColor = MainMenuView.hexToColor(receivedManifest.PrimaryColor);
						cc.currentDayColor = MainMenuView.hexToColor(receivedManifest.SecondaryColor);
					}
				}
				if (receivedManifest.UserInfo.MyContent.ContainsKey("MENU_1")){
					Content[] menuContent = receivedManifest.UserInfo.MyContent["MENU_1"];
					if (menuContent.Length > 0 && menuContent[0].Description != null && menuContent[0].Description != ""){
						object menuText = menuContent[0].Description;
						_menu1Text.FireEvent(ref menuText);
					}
					if (menuContent.Length > 0 && menuContent[0].ThumbnailUrl != null && menuContent[0].ThumbnailUrl != ""){
						object iconFile = menuContent[0].ThumbnailUrl;
						_requestMenu1Image.FireEvent(ref iconFile);
					}                                         
				}
				if (receivedManifest.UserInfo.MyContent.ContainsKey("MENU_2")){
					Content[] menuContent = receivedManifest.UserInfo.MyContent["MENU_2"];
					if (menuContent.Length > 0 && menuContent[0].Description != null && menuContent[0].Description != ""){
						object menuText = menuContent[0].Description;
						_menu2Text.FireEvent(ref menuText);
					}
					if (menuContent.Length > 0 && menuContent[0].ThumbnailUrl != null && menuContent[0].ThumbnailUrl != ""){
						object iconFile = menuContent[0].ThumbnailUrl;
						_requestMenu2Image.FireEvent(ref iconFile);
					}                                         
				}

				if (receivedManifest.UserInfo.MyContent.ContainsKey("MENU_3")){
					Content[] menuContent = receivedManifest.UserInfo.MyContent["MENU_3"];
					if (menuContent.Length > 0 && menuContent[0].Description != null && menuContent[0].Description != ""){
						object menuText = menuContent[0].Description;
						_menu3Text.FireEvent(ref menuText);
					}
					if (menuContent.Length > 0 && menuContent[0].ThumbnailUrl != null && menuContent[0].ThumbnailUrl != ""){
						object iconFile = menuContent[0].ThumbnailUrl;
						_requestMenu3Image.FireEvent(ref iconFile);
					}                                         
				}

				if (receivedManifest.UserInfo.MyContent.ContainsKey("MENU_4")){
					Content[] menuContent = receivedManifest.UserInfo.MyContent["MENU_4"];
					if (menuContent.Length > 0 && menuContent[0].Description != null && menuContent[0].Description != ""){
						object menuText = menuContent[0].Description;
						_menu4Text.FireEvent(ref menuText);
					}
					if (menuContent.Length > 0 && menuContent[0].ThumbnailUrl != null && menuContent[0].ThumbnailUrl != ""){
						object iconFile = menuContent[0].ThumbnailUrl;
						_requestMenu4Image.FireEvent(ref iconFile);
					}                                         
				}

				if (receivedManifest.UserInfo.MyContent.ContainsKey("MENU_5")){
					Content[] menuContent = receivedManifest.UserInfo.MyContent["MENU_5"];
					if (menuContent.Length > 0 && menuContent[0].Description != null && menuContent[0].Description != ""){
						object menuText = menuContent[0].Description;
						_menu5Text.FireEvent(ref menuText);
					}
					if (menuContent.Length > 0 && menuContent[0].ThumbnailUrl != null && menuContent[0].ThumbnailUrl != ""){
						object iconFile = menuContent[0].ThumbnailUrl;
						_requestMenu5Image.FireEvent(ref iconFile);
					}                                         
				}


				
	            //  if(Tools.StringArrayContains(receivedManifest.CachedFiles,receivedManifest.UserInfo.ThumbnailFile))
	            //  {
	            //      object profileFile = Application.persistentDataPath + "/" +receivedManifest.UserInfo.Id + "/" + receivedManifest.UserInfo.ThumbnailFile;
	            //      _requestProfileImage.FireEvent(ref profileFile);
	            //  } 
	            
				if (receivedManifest.AddFiles.Length > 0 && (DrilldownController.GetInstance().Aliases.Count == 0)){
//					Debug.Log("Payload URL: " + receivedManifest.PayloadToken.ToString());
	                object payloadToken = receivedManifest.PayloadToken.ToString();
	                _getPayload.FireEvent(ref payloadToken);
	            }
	            else
	            {
//	                Debug.Log("Add files is ZERO!");
	            }
			}
            
			if (receivedManifest.UserInfo.LevelNumber == receivedManifest.MaxLevels)
			{
				// find the user in the list of team members
				//TODO, not in there yet so default to the first one.
				TeamMember userAsTeamMember = receivedManifest.UserInfo.MyTeam[0];
				foreach (TeamMember tm in receivedManifest.UserInfo.MyTeam){
					if (tm.Id == receivedManifest.UserInfo.Id){
						userAsTeamMember = tm;
						break;
					}
				}
				object teamMemberRef = userAsTeamMember;
				_teamMemberSelected.FireEvent (ref teamMemberRef);
			}
			else
			{
				OnCoachingSkills(receivedManifest);
			}
	        object coachingSkillsTitle = receivedManifest.GlobalSkillTitle;
	        _globalSkillTitle.FireEvent(ref coachingSkillsTitle);

			SetFCRButtonEnabledState();

			if (reselectTeamMemberRef != null)
				_teamMemberSelected.FireEvent (ref reselectTeamMemberRef);
        }
        else
        {
            Debug.LogError("OnManifestUpdated received wrong Data! Data Type: " + Data.GetType());
        }
    }

    private void OnCoachingSkills(Manifest refManifest)
    {
        CoachingSkillCategory[] cs = refManifest.CoachingCategories;
        
        if (refManifest.CoachingCategories != null && refManifest.CoachingCategories.Length > 0)
        {
            //  Debug.Log("COACHING SKILLS: " + cs.Length);
            object objManifest = refManifest as object;
			_coachingSkills.FireEvent(ref objManifest);
        }
        else
        {
            Debug.Log("Coaching skills is null");
        }
    }

    private void OnProfile(string EventName, ref object Data)
    {
        Debug.Log("ON PROFILE!");
        GameObject.Find("mCoachBackgroundImage").GetComponent<Animator>().Play("Profile");
		if (_lastReceivedManifest.UserInfo.LevelNumber < _lastReceivedManifest.MaxLevels)
        	_teamMemberNormal.FireEvent();
    }
    
    private void OnDashboard(string EventName, ref object Data)
    {
        //DashboardButtonClick
		if (_lastReceivedManifest.UserInfo.LevelNumber == _lastReceivedManifest.MaxLevels) {
			// find the user in the list of team members
			TeamMember userAsTeamMember = null;//_lastReceivedManifest.UserInfo.MyTeam [0];
			foreach (TeamMember tm in _lastReceivedManifest.UserInfo.MyTeam) {
				if (tm.Username.ToLower() == _lastReceivedManifest.UserInfo.UserName.ToLower ()) {
					userAsTeamMember = tm;
					break;
				}
			}
			if (userAsTeamMember != null){
				object teamMemberRef = userAsTeamMember;
				_teamMemberSelected.FireEvent (ref teamMemberRef);
			}
			GameObject.Find("mCoachBackgroundImage").GetComponent<Animator>().Play("Dashboard");
		} else 
		{
			Debug.Log("ON DASHBOARD, back to normal state");
			GameObject.Find("mCoachBackgroundImage").GetComponent<Animator>().Play("Dashboard");
			_teamMemberNormal.FireEvent ();
		}
		SetFCRButtonEnabledState ();
    }
	
	private void OnCoachingCategorySelected(string EventName, ref object Data)
	{
		CoachingSkillCategory csCategory = (CoachingSkillCategory)Data;
		GameObject.Find("mCoachBackgroundImage").GetComponent<Animator>().Play("RatingScreen");

	    if (_currentTeamMember == null) return;

        //check if there is an unsubmitted coaching skill for this FCR already.
		// We don't want to add this at this point any more.  moved to AddToFCR handler.
//	    var fcrSkill = _lastReceivedManifest.GetFcrCoachingSkill(_currentTeamMember.Id, csCategory.ID);
//	    if (fcrSkill == null)
//	    {
//          fcrSkill = new CoachingSkill(csCategory.ID);
//        _lastReceivedManifest.AddFcrCoachingSkill(_currentTeamMember.Id, fcrSkill);
//	    }

		object emptyString = csCategory.Ratings;
		_ratings.FireEvent(ref emptyString);
		
		print("Update rating description");
	}
    
    /// <summary>
    /// Logs out the user and returns to the login screen.
    /// </summary>
    /// <param name="EventName"></param>
    /// <param name="Data"></param>
    private void OnLogoutButtonClick(string EventName, ref object Data)
    {
		// there is probably a whole sequence we need to perform here, cleaning up any local temp files, logging out of the auth server,
		// not sure what else.
		// As a quick and dirty first pass, remove the MVC instantiation, clone a new one and head to the 2Login level.

		StartCoroutine ("LogOut");
        //Application.LoadLevel("2Login");
    }

	private IEnumerator LogOut(){
		GameObject oldMvc = GameObject.Find ("MVC(Clone)");
		if (oldMvc != null){
			// lock out further input, display some kind of 'logging out' status


			WebCallController wcc = FindObjectOfType<WebCallController>();
			if (wcc != null){
				wcc.RequestShutdown();
				while (wcc.WebCallActive){
					yield return new WaitForSeconds(0.25f);
				}
			}
			GlobalMessageManager.Reset();
			// should be like starting fresh...

			LogOutScript ls = FindObjectOfType<LogOutScript> ();
			if (ls != null) {
				ls.StartLogoutSequence(); // starts a co-routine that will load the login level
				DestroyImmediate(oldMvc); // this is US !
			}
		}
	}
    
    private void OnRepresentativeSummaryClicked(string EventName, ref object Data)
    {
        GameObject.Find("mCoachBackgroundImage").GetComponent<Animator>().Play("ShowTeamMemberScreen");
    }
    
    private void OnTeamMemberCSIDs(string EventName, ref object Data)
    {
        List<CoachingSkillCategory> coachingSkills = new List<CoachingSkillCategory>();
        int[] csIDS = (int[]) Data;
        
        for(int i = 0; i < csIDS.Length; i++)
        {
            for (int skillIndex = 0; skillIndex < _lastReceivedManifest.CoachingCategories.Length; skillIndex++)
            {
                if (csIDS[i] == _lastReceivedManifest.CoachingCategories[skillIndex].ID)
                {
                    coachingSkills.Add(_lastReceivedManifest.CoachingCategories[skillIndex]);
                    break;
                }
            }
        }
        object teamCoachingSkills = coachingSkills.ToArray();
		object parms = new object[]{_currentTeamMember, teamCoachingSkills};
		_teamCoachingSkills.FireEvent(ref parms); //TODO we are firing this twice, once from OnTeamMemberSelected, and again here
    }

	private void SetFCRButtonEnabledState(){
		bool disabledParam = false;
		if (_currentTeamMember == null || 
		    _currentTeamMember.FCRList.Count == 0) {
			disabledParam = true;
		}
		object parmObj = (object)disabledParam;
		_FCRButtonDisable.FireEvent (ref parmObj);
	}

    /// <summary>
    /// Returns a string of the current date in the form of: 
    /// </summary>
    private string GetCurrentDate()
    {
        string weekday = DateTime.Today.DayOfWeek.ToString().Substring(0, 3);
        string dayOfMonth = DateTime.Today.Day.ToString();
        string Month = _monthList[DateTime.Today.Month-1];

        return weekday + ", " + Month + " " + dayOfMonth;
    }

    /// <summary>
    /// Displays the coaching plan pop up
    /// </summary>
    /// <param name="eventName"></param>
    /// <param name="data"></param>
    private void OnShowCreateNewCoachingPlanPopUp(string eventName, ref object data)
    {
    }

    /// <summary>
    /// Hides the coaching plan pop up
    /// </summary>
    /// <param name="eventName"></param>
    /// <param name="data"></param>
    private void OnHidereateNewCoachingPlanPopUp(string eventName, ref object data)
    {
    }
}