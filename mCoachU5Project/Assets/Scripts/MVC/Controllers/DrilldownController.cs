﻿using UnityEngine;
using MCoach;
using Assets.Scripts.Tools;
using System.Collections;
using System.Collections.Generic;

public class DrilldownController : MonoBehaviour {

	// this class is persistent, but not grouped with the other controlers, as they are all re-created on new login.

	static DrilldownController _instance = null;

	private UIEvent _loginSuccess = new UIEvent(Constants.Events.LoginSuccessEventId.ToString());
	private UIEvent _startSyncIndication = new UIEvent(Constants.Events.DownloadProgressEventId.ToString ());
	public static DrilldownController GetInstance(){
		return _instance;
	}


	public List<Manifest> Aliases; // push one element for each drill down level
	public Manifest currentAlias;

	void Awake(){
		// persistent singleton
		if (_instance != null && _instance != this){
			DestroyImmediate(this.gameObject);
			return;
		}
		_instance = this;
		DontDestroyOnLoad(this.gameObject);
		DontDestroyOnLoad(this);
		Aliases = new List<Manifest> ();
		GlobalMessageManager.AddEvent(_loginSuccess); // fired when drilling back up with cached manifest
		GlobalMessageManager.AddEvent(_startSyncIndication);
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void OnLevelWasLoaded(int level){
		Aliases.Clear();
		Aliases = new List<Manifest>();
	}

	public static Manifest GetTopManifest(){
		if (_instance.Aliases.Count > 0)
			return _instance.Aliases [0];
		else
			return MCoachModel.GetManifest ();
	}

	public void DrillDownAs(TeamMember teamMember){

		// add our current manifest to the Alias stack

		Manifest cachedManifest = MCoachModel.GetManifest ();
		// just to be sure we don't try anything on log in or drill out...
		cachedManifest.AddFiles = new string[0];
		cachedManifest.DeleteFiles = new string[0];
		cachedManifest.PayloadToken = null;

		Aliases.Add(MCoachModel.GetManifest()); // Make a copy ?

		// start the sync button indicating activity

		float progress = 0;
		object progObj = (object)progress;
		_startSyncIndication.FireEvent (ref progObj);


		LoginController loginController = FindObjectOfType<LoginController> ();

		// fEED THE LOGIN controller the username and password and perform a login
		loginController.Username = teamMember.Username;
		loginController.Password = teamMember.Password;

		loginController.StartCoroutine("DoLoginSignin");
	}

	public void DrillOutAs(Manifest alias){

		if (Aliases.Count == 0)
			return;

		if (alias == null)
			alias = Aliases [0];

		// first, we have to rebuild the breadcrumb list up to this manifest
		List<Manifest> newAlias = new List<Manifest> ();
		foreach (Manifest m in Aliases) {
			if (m.UserInfo.UserName == alias.UserInfo.UserName)
				break;
			newAlias.Add(m);
		}
		Aliases.Clear ();
		Aliases = newAlias;
		// try just skipping the round trip, since we have a nice manifest cached...
		object manifestRef = (object)alias;
		_loginSuccess.FireEvent (ref manifestRef);
	}
}
