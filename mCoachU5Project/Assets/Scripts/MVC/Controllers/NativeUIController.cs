﻿using Assets.Scripts.Tools;
using UnityEngine;

/// <summary>
/// Manages native device functionality.
/// </summary>
public class NativeUIController : MonoBehaviour
{
    /// <summary>
    /// Stores true if class has been initialized.
    /// </summary>
    private bool _initialized = false;
    
    /// <summary>
    /// Fires before first rendered frame. Initializes event handlers.
    /// </summary>
    private void Awake()
    {
        if (_initialized) return;
        _initialized = true;
        GlobalMessageManager.AddListener(Constants.Events.NativePopupEventId.ToString(), OnNativePopUp);
        GlobalMessageManager.AddListener(Constants.Events.ShowLoadingEventId.ToString(), OnShowLoading);
        GlobalMessageManager.AddListener(Constants.Events.HideNativeUIEventId.ToString(), OnHideNativeUI);
    }

    /// <summary>
    /// Triggers the device's OS default popup.
    /// </summary>
    /// <param name="EventName"></param>
    /// <param name="Data"></param>
    private void OnNativePopUp(string EventName, ref object Data)
    {
        string[] dataArray = (string[])Data;
        string[] buttons = new string[] { dataArray[2] };
        Debug.Log("Native PopUp: " + dataArray[1]);
        
#if UNITY_IOS
        EtceteraBinding.showAlertWithTitleMessageAndButtons(dataArray[0], dataArray[1], buttons);
#endif
    }

    /// <summary>
    /// Displays the devices OS native loading notification.
    /// </summary>
    /// <param name="EventName"></param>
    /// <param name="Data"></param>
    private void OnShowLoading(string EventName, ref object Data)
    {
        Debug.Log("On Show Loading: " + (string)Data);

        string label = (string)Data;
#if UNITY_IOS
        EtceteraBinding.showBezelActivityViewWithLabel(label);
#endif
    }

    private void OnHideNativeUI(string EventName, ref object Data)
    {
//         Debug.Log("Hide Native UI");
#if UNITY_IOS
        EtceteraBinding.hideActivityView();
#endif
    }
}