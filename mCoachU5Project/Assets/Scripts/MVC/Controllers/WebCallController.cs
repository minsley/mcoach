﻿using UnityEngine;
using System.Collections;
using System.Threading;
using System.IO;
using System.Xml.Serialization;
using Assets.Scripts.Tools;
using MCoach;


/// <summary>
/// Handles connections made to the web server.
/// </summary>
public class WebCallController : MonoBehaviour
{
    /// <summary>
    /// Event is fired when the app's online status is validated.
    /// </summary>
    private UIEvent _onlineStatus = new UIEvent(Constants.Events.OnlineStatusDataEventId.ToString());
	/// <summary>
	/// Event is fired when the app's online status changes. Payload is bool isOnline
	/// </summary>
	private UIEvent _onlineStatusChanged = new UIEvent(Constants.Events.OnlineStatusChangedEventId.ToString());
	/// <summary>
	/// Event is fired when the app's online status changes. Payload is bool isOnline
	/// </summary>
	private UIEvent _onPingWebServicesResult = new UIEvent(Constants.Events.PingWebServicesResultEventId.ToString());
    /// <summary>
    /// Event is fired when the server returns json data relating to the most recent login.
    /// </summary>
    private UIEvent _loginComplete = new UIEvent(Constants.Events.LoginCompleteEventId.ToString());
    /// <summary>
    /// Event is fired when a payload is ready to download.
    /// </summary>
	private UIEvent _downloadPayload = new UIEvent(Constants.Events.DownloadPayloadEventId.ToString());
	/// <summary>
	/// Event is fired when a payload is ready to download.
	/// </summary>
	private UIEvent _downloadPayloadProgress = new UIEvent(Constants.Events.DownloadProgressEventId.ToString ());
	private UIEvent _downloadRetryCount = new UIEvent(Constants.Events.DownloadRetryEventId.ToString ());
	/// <summary>
    /// Event is fired while payload is being downloaded
    /// </summary>
    private UIEvent _getLocalFolderForPayload = new UIEvent(Constants.Events.FolderForPayloadEventId.ToString());
    /// <summary>
    /// Array of ascii int values for the word "Processing".
    /// </summary>
    private int[] _charArrayOfProcessing = { 13, 112, 114, 111, 99, 101, 115, 115, 105, 110, 103 };

	private UIEvent _nativeUICall = new UIEvent(Constants.Events.NativePopupEventId.ToString());

    /// <summary>
    /// Stores the last known payload.
    /// </summary>
    private string _lastPayloadToken = "";
    /// <summary>
    /// Flags if a webcall is currently running.
    /// </summary>
    private bool _webcallActive = false;
	private int _webCallsActive = 0;

	public bool WebCallActive{ // read only
		get {               
			return _webCallsActive > 0;
		}
	}

	public static bool isOnline = false;
	public static Manifest.ServerTypes ServerType = Manifest.ServerTypes.LIVE;

	private bool _shutdownRequested = false;

	// call this to tell any pending threads or web requests to abort, we are shutting down.
	public void RequestShutdown(){
		_shutdownRequested = true; //
		// remove the old handlers and events
		GlobalMessageManager.RemoveListener(Constants.Events.OnlineStatusEventId.ToString(), OnOnlineStatus);
		GlobalMessageManager.RemoveListener(Constants.Events.LoginSendPasswordEventId.ToString(), OnGetPassword);
		GlobalMessageManager.RemoveListener(Constants.Events.LoginWebCallEventId.ToString(), OnLogin);
		GlobalMessageManager.RemoveListener(Constants.Events.GetPayloadEventId.ToString(), OnGetPayload);
		// this could be risky where there are multiple listeners, but since we will restart and re register, should be okay
		GlobalMessageManager.RemoveEvent(_onlineStatus);
		GlobalMessageManager.RemoveEvent(_onlineStatusChanged);
		GlobalMessageManager.RemoveEvent(_loginComplete);
		GlobalMessageManager.RemoveEvent(_downloadPayload);
		GlobalMessageManager.RemoveEvent(_getLocalFolderForPayload);
		GlobalMessageManager.RemoveEvent (_onPingWebServicesResult);
		GlobalMessageManager.RemoveEvent (_downloadPayloadProgress);
		GlobalMessageManager.RemoveEvent (_downloadRetryCount);
		GlobalMessageManager.RemoveEvent(_nativeUICall);
	}

    /// <summary>
    /// Handles check for online connection.
    /// </summary>
    /// <param name="EventName"></param>
    /// <param name="Data"></param>
	public void OnOnlineStatus(string EventName, ref object Data)
    {
//        Debug.Log("ON ONLINE STATUS");
        StartCoroutine(TestConnection(Data));
        //  System.Net.WebClient client = null;
        //  System.IO.Stream stream = null;

        //  try
        //  {
        //      client = new System.Net.WebClient();
        //      stream = client.OpenRead("https://googledrive.com/host/0B_PoBFPZyLAycm1Wa2dGVUJISEU/");
        //      object status = new object[]{true, Data};
        //      Debug.Log("IS ONLINE");
        //      _onlineStatus.FireEvent(ref status);
        //  }
        //  catch (System.Exception ex)
        //  {
        //      object status = new object[]{false, Data};
        //      Debug.Log("IS OFFLINE");
        //      _onlineStatus.FireEvent(ref status);
        //  }
        //  finally
        //  {
        //      if (client != null) { client.Dispose(); }
        //      if (stream != null) { stream.Dispose(); }
        //  }
    }

    /// <summary>
    /// Fires first frame of the application.
    /// Initializes the class.
    /// </summary>
    private void Awake()
    {
        GlobalMessageManager.AddEvent(_onlineStatus);
		GlobalMessageManager.AddEvent(_onlineStatusChanged);
		GlobalMessageManager.AddEvent (_onPingWebServicesResult);
        GlobalMessageManager.AddEvent(_loginComplete);
        GlobalMessageManager.AddEvent(_downloadPayload);
        GlobalMessageManager.AddEvent(_getLocalFolderForPayload);
		GlobalMessageManager.AddEvent(_downloadPayloadProgress);
		GlobalMessageManager.AddEvent(_downloadRetryCount);
		GlobalMessageManager.AddEvent(_nativeUICall);
        GlobalMessageManager.AddListener(Constants.Events.OnlineStatusEventId.ToString(), OnOnlineStatus);
		GlobalMessageManager.AddListener(Constants.Events.PingWebServicesEventId.ToString(), OnPingWebServices);
        GlobalMessageManager.AddListener(Constants.Events.LoginSendPasswordEventId.ToString(), OnGetPassword);
        GlobalMessageManager.AddListener(Constants.Events.LoginWebCallEventId.ToString(), OnLogin);
        GlobalMessageManager.AddListener(Constants.Events.GetPayloadEventId.ToString(), OnGetPayload);
		if (PlayerPrefs.HasKey("webServicesDomainName"))
		    WebServiceTools.WebServiceDomainName = PlayerPrefs.GetString("webServicesDomainName");
		// We could keep track of if the app is online with this hearbeat method.
//		InvokeRepeating ("HeartbeatPing", 0.5f, 30); // ping once per 30 sec to verify online
    }

	/// <summary>
    /// Triggered when login data should be pushed to the server.
    /// </summary>
    /// <param name="EventName"></param>
    /// <param name="Data"></param>
    private void OnLogin(string EventName, ref object Data)
    {
		if (_webCallsActive > 0) return; //_webcallActive

        string jsonData = (string)Data;
        _webcallActive = true;
		_webCallsActive++;
        StartCoroutine(ThreadedLoginWebCall(jsonData));
    }
    
    /// <summary>
    /// Requests password in separate thread. Data must be email addresss
    /// </summary>
    /// <param name="EventName"></param>
    /// <param name="Data">(string)email</param>
    private void OnGetPassword(string EventName, ref object Data)
    {
        Debug.Log("GET PASSWORD: " + Data.GetType());
        
        string email = (string)Data;
        if (email == null || email.Equals(string.Empty)) return;
		_webCallsActive++;
        StartCoroutine(ThreadedResetPassword(email));
    }

    /// <summary>
    /// Triggered when a payload token is received.
    /// This method starts downloading payload in a separate thread.
    /// </summary>
    /// <param name="EventName"></param>
    /// <param name="Data">Payload Token as String</param>
    private void OnGetPayload(string EventName, ref object Data)
    {
        if (_webCallsActive > 0)
        {
            Debug.LogError("A Webcall is currently active!");
            return;
        }
        
        string token = (string)Data;
        if (!token.Equals(_lastPayloadToken))
        {
            _lastPayloadToken = token;
            _webcallActive = true;
			_webCallsActive++;
            StartCoroutine(ThreadedPayloadWebCall(token));
        }
        else
        {
            Debug.LogWarning("Received old payload token");
        }
    }

    /// <summary>
    /// Handles a login web call to the server.
    /// Starts webcall thread and waits until thread completes and triggers the WebcallComplete event.
    /// </summary>
    /// <param name="jsonData"></param>
    /// <returns></returns>
    IEnumerator ThreadedLoginWebCall(string jsonData)
    {
//        Debug.Log("Thread created");
        WebCall webCall = new WebCall(jsonData, true);
        Thread webThread = new Thread(webCall.StartCall);
        webThread.Start();
        
//        while (!webCall.ThreadStarted)
  //          yield return null;
            
        while (!webCall.ThreadCompleted && !_shutdownRequested)
            yield return null;
                        
        webThread.Abort();
        webThread.Join();
        object response = webCall.ResponseFromServer;
//        Debug.Log("Firing login webcall complete");
        _loginComplete.FireEvent(ref response);
        
        _webcallActive = false;
		_webCallsActive--;
    }

    /// <summary>
    /// Handles a payload webcall to the server.
    /// Starts webcall thread and waits until thread completes.
    /// </summary>
    /// <param name="jsonData"></param> 
    /// <returns></returns>
    IEnumerator ThreadedPayloadWebCall(string payloadToken)
    {
//        Debug.Log("Payload Thread created: " + payloadToken);
        WebCall webCall = new WebCall(payloadToken, false);
        Thread webThread = new Thread(new ThreadStart(webCall.StartCall));
//        Debug.Log("Thread start 1");
        webThread.Start();
//        Debug.Log("Thread start 2");
//        while (!webCall.ThreadRunning) // these were commented out but may be needed...
//          yield return null;			// timing on the device can cause this to hang here if the thread finishes too quickly,


//		yield return new WaitForSeconds(0.5f); // be sure the thread has had a chance to start

        Debug.Log("Payload Thread running");
        while (!webCall.ThreadCompleted && !_shutdownRequested)
            yield return null;
                        

        webThread.Abort();
        webThread.Join();
		// should clear webCallActive = false; //TODO here?
		_webCallsActive--;
        //  object response = webCall.ResponseFromServer;
        yield return null;

//        Debug.Log("Error: " + webCall.ErrorMessage);

        var response = WebServiceTools.GetReturnContent(webCall.ResponseFromServer);
        var doesEqual = (response.ToLower().IndexOf("processing") >= 0);
        //var doesEqual = true;

        //if (response.Length <= _charArrayOfProcessing.Length)
        //{
        //    for (var i = 0; i < response.Length; i++)
        //    {
        //        if (response[i] != _charArrayOfProcessing[i])
        //        {
        //            doesEqual = false;
        //        }
        //    }
        //}
        //else
        //{
        //    Debug.Log("DOES NOT EQUAL!");
        //    doesEqual = false;
        //}

        //  Debug.Log("Response: " + response);

        if (doesEqual)
        {
            yield return new WaitForSeconds(2f);
			_webCallsActive++;
            StartCoroutine(ThreadedPayloadWebCall(payloadToken));
        }
        else
        {
            if (!response.Equals(string.Empty))
            {
				_webCallsActive++;
                StartCoroutine(ThreadedDownloadPayload(response, Application.persistentDataPath + "/tempFile.7z"));
            }
        }
    }
    
    /// <summary>
    /// Handles the download of the payload in another thread.
    /// Coroutine continues to spin until thead has completed.
    /// </summary>
    /// <param name="url">payload url</param>
    /// <param name="localPayload">full path where payload will be downloaded</param> 
    /// <returns></returns>
    IEnumerator ThreadedDownloadPayload(string url, string localPayload)
    {
        url = url.Replace("\"", "");

        Debug.Log("Download Thread created: " + localPayload);
        
		float fProg = 0;
		object progress = (object)fProg;
		_downloadPayloadProgress.FireEvent (ref progress);

        DownloadFileInThread DownloadFile = new DownloadFileInThread(url, localPayload);
        Thread ThreadedFileDownload = new Thread(DownloadFile.DownloadURL);
        ThreadedFileDownload.Start();
        
//        while (!DownloadFile.ThreadRunning)
//          yield return null;
            
        while (!DownloadFile.ThreadCompleted && !_shutdownRequested) {
			yield return new WaitForSeconds (.5f);
			_downloadPayloadProgress.FireEvent (ref progress);
			if (DownloadFile.retryCount != 0){
				object retryCount = (object)DownloadFile.retryCount;
				_downloadRetryCount.FireEvent (ref retryCount);
			}
		}
                        
        ThreadedFileDownload.Abort();
        ThreadedFileDownload.Join();
		// should clear webCallActive = false; //TODO here?
		_webCallsActive--;
        
		Debug.Log("DownLoadRetries? " + DownloadFile.TooManyAttempts);
        Debug.Log("Was Successful? " + DownloadFile.LocalFileExists + DownloadFile.error);
        Debug.Log("File downloaded and saved at: " + localPayload);

		if (!DownloadFile.LocalFileExists) {
			object uiInfo = new string[] { "Server Busy", "Content failed to download.\nIncrease Max Retries setting\n" + DownloadFile.error, "OK" };
			_nativeUICall.FireEvent (ref uiInfo);
		}

        
		fProg = 1;
		progress = (object)fProg;
		_downloadPayloadProgress.FireEvent (ref progress);

        //Trigger async unzip!
        object localPath = localPayload;
        _getLocalFolderForPayload.FireEvent(ref localPath);
    }
    
    /// <summary>
    /// Handles the webcall in another thread.
    /// Coroutine continues to spin until thead has completed.
    /// </summary>
    /// <param name="url">payload url</param>
    /// <param name="localPayload">full path where payload will be downloaded</param> 
    /// <returns></returns>
    IEnumerator ThreadedResetPassword(string email)
    {
        
        PasswordWebCall webCall = new PasswordWebCall(email);
        Thread webCallThread = new Thread(webCall.StartCall);
        webCallThread.Start();
        
//        while (!webCall.ThreadRunning)
  //          yield return null;
            
        while (!webCall.ThreadCompleted && !_shutdownRequested)
            yield return new WaitForSeconds(.5f);
                        
        webCallThread.Abort();
        webCallThread.Join();
		// should clear webCallActive = false; //TODO here?
		_webCallsActive--;
        
        Debug.Log("Reset Pass repsonse: " + webCall.ResponseFromServer);
    }
    
    IEnumerator TestConnection(object Data)
    {
        float timeTaken = 0.0F;
        float maxTime = 10.0F;        
        var ipAddress = WebServiceTools.WebServiceIpAddress;
        bool timedOut = (ipAddress == "");
/*
        if (ipAddress != "")
        {
            Ping testPing = new Ping(ipAddress);
            timeTaken = 0.0F;
			float startTime = Time.realtimeSinceStartup;

            while (!testPing.isDone && !_shutdownRequested)
            {
				timeTaken = Time.realtimeSinceStartup - startTime;

                if (timeTaken > maxTime)
                {
                    timedOut = true;
                    // if time has exceeded the max
                    // time, break out and return false
                    //thereIsConnection = false;
                    break;
                }

                yield return null;
            }
        }
 */   
 //       if (true || timeTaken <= maxTime && !timedOut) 
		if (isOnline)
        {
            Debug.Log("Has internet connection!");
            object status = new object[]{true, Data};
            _onlineStatus.FireEvent(ref status);
        }
        else
        {
            Debug.Log("Has no internet connection!");
            object status = new object[]{false, Data};
            _onlineStatus.FireEvent(ref status);
        }//thereIsConnection = true;
        
		//Debug.Log("Is connected: " + thereIsConnection);
        yield return null;
    }
	private void OnPingWebServices(string EventName, ref object Data)
	{
		StartCoroutine (ThreadedPingWebServices ());
	}

	IEnumerator ThreadedPingWebServices(){

		PingWebCall webCall = new PingWebCall();
		Thread webCallThread = new Thread(webCall.StartCall);
		webCall.RequestServerType = false;

//		Debug.Log("WCC starting threaded ping to: "+webCall.Site);
		webCallThread.Start();

		while (!webCall.ThreadStarted)	// this is the second instance of a call hanging here due to device timing
		yield return null;

		
		while (!webCall.ThreadCompleted && !_shutdownRequested)
			yield return new WaitForSeconds(.5f);
		
		webCallThread.Abort();
		webCallThread.Join();
		// should clear webCallActive = false; //TODO here?
		_webCallsActive--;

		//  fire the event that notifies our status
//		Debug.Log("WebCallController ping result = "+webCall.ResponseFromServer);
		bool result = (webCall.ResponseFromServer.Contains ("ok"));
		object resultRef = (object)result;

		if (result != isOnline) {
			object status = (object)result;
			_onlineStatusChanged.FireEvent(ref status);
		}
		isOnline = result;

		if (result) {
			webCall = new PingWebCall();
			webCallThread = new Thread(webCall.StartCall);
			// do another ping to get the server type. it wil be an integer.
			webCall.RequestServerType = true;
			webCallThread.Start();
			
			while (!webCall.ThreadStarted)
				yield return null;
			
			while (!webCall.ThreadCompleted && !_shutdownRequested)
				yield return new WaitForSeconds(.5f);
			
			webCallThread.Abort();
			webCallThread.Join();

			string server_type = webCall.ResponseFromServer; 
			// now to strip and parse...
			// The System XML serializer doesnt like the xmlns attribute in our <string> tag, so we will just brute force parse this thing.
			/*
			var serializer = new XmlSerializer(typeof(string));
			string typeAsString;
			
			using (TextReader reader = new StringReader(server_type))
			{
				typeAsString = (string)serializer.Deserialize(reader);
			}
			int type = 0;
			if (int.TryParse(typeAsString,out type)){
				ServerType = (Manifest.ServerTypes)type;
			}
			*/
			if (server_type.Contains ("0</string>")) ServerType = (Manifest.ServerTypes)0;
			if (server_type.Contains ("1</string>")) ServerType = (Manifest.ServerTypes)1;
			if (server_type.Contains ("2</string>")) ServerType = (Manifest.ServerTypes)2;
			if (server_type.Contains ("3</string>")) ServerType = (Manifest.ServerTypes)3;
			if (server_type.Contains ("4</string>")) ServerType = (Manifest.ServerTypes)4;
			if (server_type.Contains ("ok")) ServerType = Manifest.ServerTypes.LIVE;

		}
		_onPingWebServicesResult.FireEvent (ref resultRef);
	
		yield return null;
	} 


	private void HeartbeatPing()// OBSOLETE, USE THE THREADED PING. OUR SERVER DOESNT NECESSARILY RESPOND TO PING
	{
		float timeTaken = 0.0F;
		float maxTime = 10.0F;        
		var ipAddress = WebServiceTools.WebServiceIpAddress;
		bool timedOut = (ipAddress == "");
		bool pingSucceeded = false;
		float hbTimeTaken = 0;
		
		if (ipAddress != "")
		{
			Ping testPing = new Ping(WebServiceTools.WebServiceDomainName);
			hbTimeTaken = 0.0F;
			float startTime = Time.realtimeSinceStartup;
			while (!testPing.isDone && !_shutdownRequested)
			{
				hbTimeTaken = Time.realtimeSinceStartup - startTime;
				
				if (hbTimeTaken > maxTime)
				{
					timedOut = true;
					// if time has exceeded the max
					// time, break out and return false
					//thereIsConnection = false;
					break;
				}
			}
			// we need to yield here
		}
		
		if (hbTimeTaken <= maxTime && !timedOut) {
			pingSucceeded = true;
		} else{
			pingSucceeded = false;
		}

		if (pingSucceeded != isOnline) {
			object status = (object)pingSucceeded;
			_onlineStatusChanged.FireEvent(ref status);
		}
		isOnline = pingSucceeded;
	}
}