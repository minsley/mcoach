﻿using Newtonsoft.Json;
using MCoach;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using Assets.Scripts.Tools;
using UnityEngine;

/// <summary>
/// Manages the local filesystem.
/// </summary>
public class FileSystemController : MonoBehaviour
{
    #region Members

    /// <summary>
    /// References the OnlineStatus Event. Event triggers when the
    /// connection status is requested.
    /// </summary>
    private readonly UIEvent _onlineStatus = new UIEvent(Constants.Events.OnlineStatusEventId.ToString());

    /// <summary>
    /// Triggered when the user has successfully logged in. Pushes
    /// the latest received manifest from the web.
    /// </summary>
    private readonly UIEvent _loginComplete = new UIEvent(Constants.Events.LoginCompleteEventId.ToString());

    /// <summary>
    /// Pushes manifest as response to user's login attempt.
    /// </summary>
    private UIEvent _pushLocalManifest = new UIEvent(Constants.Events.LocalLoginResponseEventId.ToString());

    /// <summary>
    /// Event is fired when a request for the device's native popup
    /// is made.
    /// </summary>
    private readonly UIEvent _nativeUICall = new UIEvent(Constants.Events.NativePopupEventId.ToString());

    /// <summary>
    /// Fires when user files are updated.
    /// </summary>
    private readonly UIEvent _localUserFiles = new UIEvent(Constants.Events.LocalUserFilesEventId.ToString());

	/// <summary>
	/// Fires when user files start decompressing.
	/// </summary>
	private readonly UIEvent _decompressBegin = new UIEvent(Constants.Events.PayloadDecompressBeginEventId.ToString());/// <summary>
	/// Fires when user files are decompressed.
	/// </summary>
	private readonly UIEvent _decompressEnd = new UIEvent(Constants.Events.PayloadDecompressEndEventId.ToString());

    /// <summary>
    /// Fires when the profile image is loaded
    /// </summary>
    private readonly UIEvent _profileImage = new UIEvent(Constants.Events.ProfileImageEventId.ToString());

    /// <summary>
    /// Fires when the team member profile image is loaded
    /// </summary>
    private readonly UIEvent _teamMemberProfileImage = new UIEvent(Constants.Events.TeamMemberProfileImageEventId.ToString());

	/// <summary>
	/// Fires when the team member profile image is loaded
	/// </summary>
	private readonly UIEvent _logoImage = new UIEvent(Constants.Events.LogoImageEventId.ToString());
	private readonly UIEvent _menu1Image = new UIEvent(Constants.Events.Menu1ImageEventId.ToString());
	private readonly UIEvent _menu2Image = new UIEvent(Constants.Events.Menu2ImageEventId.ToString());
	private readonly UIEvent _menu3Image = new UIEvent(Constants.Events.Menu3ImageEventId.ToString());
	private readonly UIEvent _menu4Image = new UIEvent(Constants.Events.Menu4ImageEventId.ToString());
	private readonly UIEvent _menu5Image = new UIEvent(Constants.Events.Menu5ImageEventId.ToString());

	

    /// <summary>
    /// Fires when the content list item image is loaded
    /// </summary>
    private readonly UIEvent _contnetListItemImage = new UIEvent(Constants.Events.ContentListItemThumbnailEventId.ToString());

    /// <summary>
    /// Loader objects
    /// </summary>
	private WWW _profileImageLoader, _teamLeaderImageLoader, _logoImageLoader, _contentListItemImageLoader;
	private WWW _menu1ImageLoader, _menu2ImageLoader, _menu3ImageLoader, _menu4ImageLoader, _menu5ImageLoader;

	public static int maxRetries = 30;

    #endregion

    #region Methods

    /// <summary>
    /// Coroutine spins off the file decompression call in a separate thread and keeps spinning
    /// until thread has completed. This should stop the logic from locking up the application.
    /// </summary>
    /// <param name="compressedPath"></param>
    /// <param name="decompressedPath"></param>
    private IEnumerator DecompressFile(string compressedPath, string decompressedPath)
    {
        DecompressPackageInThread decompressPackage = new DecompressPackageInThread(compressedPath, decompressedPath);
        Thread decompressThread = new Thread(decompressPackage.StartThread);

		_decompressBegin.FireEvent (); // let the sync icon know we've started decompressing

        decompressThread.Start();

//        while (!decompressPackage.ThreadRunning) //Waits for thread to start running     FAILS if the thread starts quickly.
//          yield return null;

//        while (decompressPackage.ThreadRunning) //Waits for thread to complete
//            yield return null;

		while (!decompressPackage.ThreadCompleted) //Waits for thread to scomplete
			yield return null;

        decompressThread.Abort();
        decompressThread.Join();

        if (!decompressPackage.Equals(string.Empty))
            Debug.Log("Decompress Error Report: " + decompressPackage.ErrorReport);
        //  Debug.Log("Decompression Report: " + decompressPackage.Report);

        if (decompressPackage.FileNames != null)
        {
            //Push files to manifest
            object filesRef = decompressPackage.FileNames;
            _localUserFiles.FireEvent(ref filesRef);
        }

		_decompressEnd.FireEvent ();// let the sync icon know we've finished decompressing
    }

    /// <summary>
    /// "Asynchronously" loads the profile image from the local cache
    /// </summary>
    /// <returns></returns>
    private IEnumerator LoadProfileImageCoRoutine()
    {
        if (!_profileImageLoader.isDone)
            yield return new WaitForSeconds(.5f);

        if (_profileImageLoader.bytes.Length > 0)
        {
            Texture2D texture = new Texture2D(128, 128);
            texture.LoadImage(_profileImageLoader.bytes);

            Debug.Log("Pushing new profile image!");
            object textureRef = (object) texture;
            _profileImage.FireEvent(ref textureRef);
        }
    }

    /// <summary>
    /// "Asynchronously" loads the team leader image from the local cache
    /// </summary>
    /// <returns></returns>
    private IEnumerator LoadTeamLeaderImageCoRoutine()
    {
        if (!_teamLeaderImageLoader.isDone)
            yield return new WaitForSeconds(.5f);

        if (_teamLeaderImageLoader.bytes.Length > 0)
        {
            Texture2D texture = new Texture2D(128, 128);
            texture.LoadImage(_teamLeaderImageLoader.bytes);

            Debug.Log("Pushing new team leader image!");
            object textureRef = (object) texture;
            _teamMemberProfileImage.FireEvent(ref textureRef);
        }
    }

	/// <summary>
	/// "Asynchronously" loads the profile image from the local cache
	/// </summary>
	/// <returns></returns>
	private IEnumerator LoadLogoImageCoRoutine()
	{
		if (!_logoImageLoader.isDone)
			yield return new WaitForSeconds(.5f);
		
		if (_logoImageLoader.bytes.Length > 0)
		{
			Texture2D texture = new Texture2D(128, 128);
			texture.LoadImage(_logoImageLoader.bytes);
			
			Debug.Log("Pushing new logo image!");
			object textureRef = (object) texture;
			_logoImage.FireEvent(ref textureRef);
		}
	}
	private IEnumerator LoadMenu1ImageCoRoutine()
	{
		if (!_menu1ImageLoader.isDone)
			yield return new WaitForSeconds(.5f);
		
		if (_menu1ImageLoader.bytes.Length > 0)
		{
			Texture2D texture = new Texture2D(128, 128);
			texture.LoadImage(_menu1ImageLoader.bytes);
			object textureRef = (object) texture;
			_menu1Image.FireEvent(ref textureRef);
		}
	}
	private IEnumerator LoadMenu2ImageCoRoutine()
	{
		if (!_menu2ImageLoader.isDone)
			yield return new WaitForSeconds(.5f);
		
		if (_menu2ImageLoader.bytes.Length > 0)
		{
			Texture2D texture = new Texture2D(128, 128);
			texture.LoadImage(_menu2ImageLoader.bytes);
			object textureRef = (object) texture;
			_menu2Image.FireEvent(ref textureRef);
		}
	}
	private IEnumerator LoadMenu3ImageCoRoutine()
	{
		if (!_menu3ImageLoader.isDone)
			yield return new WaitForSeconds(.5f);
		
		if (_menu3ImageLoader.bytes.Length > 0)
		{
			Texture2D texture = new Texture2D(128, 128);
			texture.LoadImage(_menu3ImageLoader.bytes);
			object textureRef = (object) texture;
			_menu3Image.FireEvent(ref textureRef);
		}
	}
	private IEnumerator LoadMenu4ImageCoRoutine()
	{
		if (!_menu4ImageLoader.isDone)
			yield return new WaitForSeconds(.5f);
		
		if (_menu4ImageLoader.bytes.Length > 0)
		{
			Texture2D texture = new Texture2D(128, 128);
			texture.LoadImage(_menu4ImageLoader.bytes);
			object textureRef = (object) texture;
			_menu4Image.FireEvent(ref textureRef);
		}
	}
	private IEnumerator LoadMenu5ImageCoRoutine()
	{
		if (!_menu5ImageLoader.isDone)
			yield return new WaitForSeconds(.5f);
		
		if (_menu5ImageLoader.bytes.Length > 0)
		{
			Texture2D texture = new Texture2D(128, 128);
			texture.LoadImage(_menu5ImageLoader.bytes);
			object textureRef = (object) texture;
			_menu5Image.FireEvent(ref textureRef);
		}
	}
	
    /// <summary>
    /// "Asynchronously" loads the content image from the local cache
    /// </summary>
    /// <returns></returns>
    private IEnumerator LoadContentImageCoRoutine()
    {
        if (!_contentListItemImageLoader.isDone)
            yield return new WaitForSeconds(.05f); // was 0.5f

        if (_contentListItemImageLoader.bytes.Length <= 0) yield break;
        var texture = new Texture2D(128, 128);
        texture.LoadImage(_contentListItemImageLoader.bytes);

        Debug.Log("Pushing content image!");
        var textureRef = (object)texture;
        _contnetListItemImage.FireEvent(ref textureRef);
    }

    #endregion

    #region Events

    /// <summary>
    /// Initializes the FileSystemManager. Fires before first rendered frame.
    /// </summary>
    private void Awake()
    {
        //  PlayerPrefs.DeleteAll();
        //Initialize Listeners
        GlobalMessageManager.AddListener(Constants.Events.LoginEventId.ToString(), OnLogin);
        GlobalMessageManager.AddListener(Constants.Events.LocalLoginEventId.ToString(), OnLocalLogin);
        GlobalMessageManager.AddListener(Constants.Events.ManifestUpdatedEventId.ToString(), OnManifestUpdated);
        GlobalMessageManager.AddListener(Constants.Events.DecompressPayloadEventId.ToString(), OnDecompressTempFile);
        GlobalMessageManager.AddListener(Constants.Events.RequestProfileImageEventId.ToString(), OnRequestProfileImage);
		GlobalMessageManager.AddListener(Constants.Events.RequestLogoImageEventId.ToString(), OnRequestLogoImage);
		GlobalMessageManager.AddListener(Constants.Events.RequestMenu1ImageEventId.ToString(), OnRequestMenu1Image);
		GlobalMessageManager.AddListener(Constants.Events.RequestMenu2ImageEventId.ToString(), OnRequestMenu2Image);
		GlobalMessageManager.AddListener(Constants.Events.RequestMenu3ImageEventId.ToString(), OnRequestMenu3Image);
		GlobalMessageManager.AddListener(Constants.Events.RequestMenu4ImageEventId.ToString(), OnRequestMenu4Image);
		GlobalMessageManager.AddListener(Constants.Events.RequestMenu5ImageEventId.ToString(), OnRequestMenu5Image);
		GlobalMessageManager.AddListener(Constants.Events.RequestTeamLeaderProfileImageEventId.ToString(), OnRequestTeamLeaderImage);
        GlobalMessageManager.AddListener(Constants.Events.RequestContentListItemImageEventId.ToString(), OnRequestContentImage);
        GlobalMessageManager.AddListener(Constants.Events.DeleteLocalFilesEventId.ToString(), OnDeleteLocalFiles);
		GlobalMessageManager.AddListener(Constants.Events.PurgeUserFilesEventId.ToString(), OnPurgeUserFiles);
		GlobalMessageManager.AddListener(Constants.Events.SyncManifestEventId.ToString(), OnSyncManifest);

        //this is a hack to prevent other listeners from refreshing 
        GlobalMessageManager.AddListener(Constants.Events.SaveLocalManifest.ToString(), OnManifestUpdated);

        //Initialize Events
        GlobalMessageManager.AddEvent(_onlineStatus);
        GlobalMessageManager.AddEvent(_loginComplete);
        GlobalMessageManager.AddEvent(_nativeUICall);
        GlobalMessageManager.AddEvent(_localUserFiles);
		GlobalMessageManager.AddEvent(_decompressBegin);
		GlobalMessageManager.AddEvent(_decompressEnd);
        GlobalMessageManager.AddEvent(_profileImage);
        GlobalMessageManager.AddEvent(_teamMemberProfileImage);
		GlobalMessageManager.AddEvent(_logoImage);
		GlobalMessageManager.AddEvent(_menu1Image);
		GlobalMessageManager.AddEvent(_menu2Image);
		GlobalMessageManager.AddEvent(_menu3Image);
		GlobalMessageManager.AddEvent(_menu4Image);
		GlobalMessageManager.AddEvent(_menu5Image);
		GlobalMessageManager.AddEvent(_contnetListItemImage);

		if (PlayerPrefs.HasKey("maxRetries")) // get max retries from settings if it exists
			int.TryParse( PlayerPrefs.GetString("maxRetries", "30"),out maxRetries);
    }

    /// <summary>
    /// Fires when the user requests to log in
    /// </summary>
    /// <param name="EventName"></param>
    /// <param name="Data"></param>
    private void OnLogin(string EventName, ref object Data)
    {
        Manifest userManifest = (Manifest) Data;

        if (PlayerPrefs.HasKey(userManifest.UserInfo.UserName))
        {
			string localManifestString = GetLocalManifestString(userManifest.UserInfo.UserName);
			if (localManifestString != null){
				Manifest localManifest = JsonConvert.DeserializeAnonymousType(localManifestString, userManifest);
				if (localManifest !=null){
					localManifest.AddFiles= null;
					localManifest.DeleteFiles= null;
					localManifest.PayloadToken = null;
					localManifest.Result="";
					LoginController loginController = FindObjectOfType<LoginController> ();
					localManifest.UserInfo.Password = loginController.Password;// update the password in case it was changed...
					userManifest = localManifest;
				}
			}

            string folderName = PlayerPrefs.GetString(userManifest.UserInfo.UserName);
            string manifestFolder = Application.persistentDataPath + "/" + folderName;
            string manifestPath = manifestFolder + "/json.text";

            if (File.Exists(manifestPath))
            {
                List<string> files = new List<string>(Directory.GetFiles(manifestFolder + "/"));
                char[] delimiters = {'/'};
                //Remove manifest file
                for (int fileIndex = 0; fileIndex < files.Count; fileIndex++)
                {
                    if (files[fileIndex].Equals(manifestPath))
                    {
                        files.RemoveAt(fileIndex);
                        break;
                    }
                }
                for (int fileIndex = 0; fileIndex < files.Count; fileIndex++)
                {
                    string[] splitFile = files[fileIndex].Split(delimiters);
                    files[fileIndex] = splitFile[splitFile.Length - 1];
                }

                //  foreach(var file in userManifest.CachedFiles)
                //  {
                //      Debug.Log("Cached File: " + file);
                //  }

                userManifest.CachedFiles = files.ToArray();
            }
        }

        object manifestRef = JsonConvert.SerializeObject(userManifest);
        _onlineStatus.FireEvent(ref manifestRef);
    }

    /// <summary>
    /// Handles a local login.
    /// </summary>
    private void OnLocalLogin(string EventName, ref object Data)
    {
        Manifest manifest = (Manifest) Data;
        //Debug.Log("On local login, there is no logic here! username: " + manifest.UserInfo.UserName);
        string localManifest = GetLocalManifestString(manifest.UserInfo.UserName);

        if (localManifest == null)
        {
            object uiInfo = new string[] {"No local data for this account", "Please login while connected online!", "OK"};
            _nativeUICall.FireEvent(ref uiInfo);
        }
        else
        {
            object refManifestString = localManifest;
            _loginComplete.FireEvent(ref refManifestString);
        }
    }

	/// <summary>
	/// Fires when the user requests to log in
	/// </summary>
	/// <param name="EventName"></param>
	/// <param name="Data"></param>
	private void OnSyncManifest(string EventName, ref object Data)
	{
		Manifest userManifest = (Manifest) Data;
		
		if (PlayerPrefs.HasKey(userManifest.UserInfo.UserName))
		{
			string folderName = PlayerPrefs.GetString(userManifest.UserInfo.UserName);
			string manifestFolder = Application.persistentDataPath + "/" + folderName;
			string manifestPath = manifestFolder + "/json.text";
			
			if (File.Exists(manifestPath))
			{
				List<string> files = new List<string>(Directory.GetFiles(manifestFolder + "/"));
				char[] delimiters = {'/'};
				//Remove manifest file
				for (int fileIndex = 0; fileIndex < files.Count; fileIndex++)
				{
					if (files[fileIndex].Equals(manifestPath))
					{
						files.RemoveAt(fileIndex);
						break;
					}
				}
				for (int fileIndex = 0; fileIndex < files.Count; fileIndex++)
				{
					string[] splitFile = files[fileIndex].Split(delimiters);
					files[fileIndex] = splitFile[splitFile.Length - 1];
				}
				
				//  foreach(var file in userManifest.CachedFiles)
				//  {
				//      Debug.Log("Cached File: " + file);
				//  }
				
				userManifest.CachedFiles = files.ToArray();
			}
		}

		// lets make sure our local copy of the manifest is up to date ?
		if (true) { // how do we know if we should ?
			UpdateLocalManifest(userManifest);
		}


		object manifestRef = JsonConvert.SerializeObject(userManifest);
		// this call will send the latest manifect to the host, if online
		_onlineStatus.FireEvent(ref manifestRef);
	}
	
	
	/// <summary>
	/// Handles the ManifestUpdated event. Saves the received manifest
	/// to the device's local drive.
	/// </summary>
	/// <param name="EventName"></param>
	/// <param name="Data"></param>
	private void OnManifestUpdated(string EventName, ref object Data)
	{
//		Debug.Log("File system: On Manifest Updated!");
		Manifest _receivedData = (Manifest) Data;
		
		if (_receivedData != null)
            UpdateLocalManifest(_receivedData);
        else
            Debug.Log("Received data is null!!");
    }

    /// <summary>
    /// Deletes the local file cache
    /// </summary>
    /// <param name="EventName"></param>
    /// <param name="Data"></param>
    private void OnDeleteLocalFiles(string EventName, ref object Data)
    {
        Manifest manifest = (Manifest) Data;
        for (int fileIndex = 0; fileIndex < manifest.DeleteFiles.Length; fileIndex++)
        {
            if (
                File.Exists(Application.persistentDataPath + "/" + manifest.UserInfo.Id + "/" +
                            manifest.DeleteFiles[fileIndex]))
            {
                File.Delete(Application.persistentDataPath + "/" + manifest.UserInfo.Id + "/" +
                            manifest.DeleteFiles[fileIndex]);
            }
        }
    }

	/// <summary>
	/// Deletes all the user's local file cache, not just the ones listed in the manifest.  For a full sync
	/// </summary>
	/// <param name="UserName"></param>
	private void OnPurgeUserFiles(string EventName, ref object Data)
	{
		Manifest userManifest = (Manifest)Data;

		string folderName = PlayerPrefs.GetString(userManifest.UserInfo.UserName);
		string manifestFolder = Application.persistentDataPath + "/" + folderName;
		string manifestPath = manifestFolder + "/json.text";
		
		if (File.Exists(manifestPath))
		{
			List<string> files = new List<string>(Directory.GetFiles(manifestFolder + "/"));
			char[] delimiters = {'/'};  //much simpler to just use the path, as below in the PurgeFileCache method.

			for (int fileIndex = 0; fileIndex < files.Count; fileIndex++)
			{
				string[] splitFile = files[fileIndex].Split(delimiters);
				files[fileIndex] = splitFile[splitFile.Length - 1];
			}
			
			for (int fileIndex = 0; fileIndex < files.Count; fileIndex++)
			{
				if (
					File.Exists(Application.persistentDataPath + "/" + folderName + "/" +
				            files[fileIndex]))
				{
					File.Delete(Application.persistentDataPath + "/" + folderName + "/" +
					            files[fileIndex]);
				}
			}
			userManifest.CachedFiles = null; 
			UpdateLocalManifest(userManifest); // the files are gone, we need to save in case anything happens.
		}
	}


	/// <summary>
	/// Deletes ALL the files in local file cache. Done when changing servers to avoid conflict between duplicate user id's
	/// </summary>
	public void PurgeFileCache()
	{
		List<string> directoryList = new List<string>(Directory.GetDirectories(Application.persistentDataPath));
		foreach (string folderName in directoryList) {
			// there was a .mono directory in here causing this to fail
			if (Directory.GetDirectories(folderName).Length == 0){
				List<string> files = new List<string> (Directory.GetFiles (folderName + "/")); // is this needed?
				foreach (string filename in files) {
					if (File.Exists (filename)) {
						File.Delete (filename);
					}
				}
				if (Directory.GetFiles(folderName).Length == 0)
					Directory.Delete(folderName);
			}
		}
	}

    /// <summary>
    /// Updates the locally stored manifest.
    /// </summary>
    private void UpdateLocalManifest(Manifest manifest)
    {
//        Debug.Log("File System: Saving manifest locally");

        string mainFolder = Application.persistentDataPath + "/" + manifest.UserInfo.Id;
        if (!File.Exists(mainFolder))
        {
            Directory.CreateDirectory(mainFolder);
        }

        string jsonData = JsonConvert.SerializeObject(manifest);
        File.WriteAllText(mainFolder + "/json.text", jsonData);
        PlayerPrefs.SetString(manifest.UserInfo.UserName, manifest.UserInfo.Id.ToString());
        PlayerPrefs.Save();

//        Debug.Log("Manifest saved locally");
    }

    /// <summary>
    /// If a manifest exists for the stored username returns the json string of data.
    /// </summary>
    private string GetLocalManifestString(string userName)
    {
        if (PlayerPrefs.HasKey(userName))
        {
            string folderName = PlayerPrefs.GetString(userName);
            string filePath = Application.persistentDataPath + "/" + folderName + "/json.text";

            Debug.Log("Manifest location: " + filePath);

            if (File.Exists(filePath))
            {
                Debug.Log("Player data should exist locally");
                string jsonText = File.ReadAllText(filePath);
                return jsonText == string.Empty ? null : jsonText;
            }
            else
            {
                Debug.Log("User data not found locally");
                return null;
            }
        }
        else
        {
            Debug.Log("Username not stored in playerprefs");
            return null;
        }
    }

    /// <summary>
    /// Decompresses a 7zip file.
    /// </summary>
    /// <param name="EventName"></param>
    /// <param name="Data">string array 0: compressedFilePath 1: decompressToFolder</param>
    private void OnDecompressTempFile(string EventName, ref object Data)
    {
        string[] files = (string[]) Data;
        StartCoroutine(DecompressFile(files[0], files[1]));
    }

    /// <summary>
    /// Creates a texture from a received file path.
    /// </summary>
    /// <param name="EventName"></param>
    /// <param name="Data">string array 0: compressedFilePath 1: decompressToFolder</param>
    private void OnRequestProfileImage(string EventName, ref object Data)
    {
        //  Debug.Log("File: " + (string) Data);
        //  Debug.Log("Does file exist? " + File.Exists((string)Data)); 
        var imageFile = (string) Data;

        if (File.Exists(imageFile))
        {
            var prefix = "file://";

#if UNITY_STANDALONE_WIN || UNITY_EDITOR_WIN

            if (imageFile.IndexOf(":/") >= 0)
            {
                prefix += "/";
            }
#endif
            _profileImageLoader = new WWW(prefix + imageFile);
            StartCoroutine(LoadProfileImageCoRoutine());
        }
    }

    /// <summary>
    /// Fires when the team leader thumbnail image is to be downloaded
    /// </summary>
    /// <param name="eventName"></param>
    /// <param name="data"></param>
    private void OnRequestTeamLeaderImage(string eventName, ref object data)
    {
        //  Debug.Log("Start coroutine for Team Leader Image");
        string path = (string) data;
        if (path.Equals(string.Empty) || !File.Exists(path))
        {
            object textureRef = Resources.Load("photoPlaceHolder");
            _teamMemberProfileImage.FireEvent(ref textureRef);
        }
        else
        {
            var prefix = "file://";

#if UNITY_STANDALONE_WIN || UNITY_EDITOR_WIN

            if (path.IndexOf(":/") >= 0)
            {
                prefix += "/";
            }

#endif

            _teamLeaderImageLoader = new WWW(prefix + path);
            StartCoroutine(LoadTeamLeaderImageCoRoutine());
        }
    }

	
	/// <summary>
	/// Creates a texture from a received file path.
	/// </summary>
	/// <param name="EventName"></param>
	/// <param name="Data">string array 0: compressedFilePath 1: decompressToFolder</param>
	private void OnRequestLogoImage(string EventName, ref object Data)
	{
		//  Debug.Log("File: " + (string) Data);
		//  Debug.Log("Does file exist? " + File.Exists((string)Data)); 
		var imageFile = (string) Data;
		
		if (File.Exists(imageFile))
		{
			var prefix = "file://";
			
			#if UNITY_STANDALONE_WIN || UNITY_EDITOR_WIN
			
			if (imageFile.IndexOf(":/") >= 0)
			{
				prefix += "/";
			}
			#endif
			_logoImageLoader = new WWW(prefix + imageFile);
			StartCoroutine(LoadLogoImageCoRoutine());
		}
	}

	private void OnRequestMenu1Image(string EventName, ref object Data)
	{
		var imageFile = (string) Data;
		if (File.Exists(imageFile))
		{
			var prefix = "file://";
			#if UNITY_STANDALONE_WIN || UNITY_EDITOR_WIN
			if (imageFile.IndexOf(":/") >= 0)
			{
				prefix += "/";
			}
			#endif
			_menu1ImageLoader = new WWW(prefix + imageFile);
			StartCoroutine(LoadMenu1ImageCoRoutine());
		}
	}
	private void OnRequestMenu2Image(string EventName, ref object Data)
	{
		var imageFile = (string) Data;
		if (File.Exists(imageFile))
		{
			var prefix = "file://";
			#if UNITY_STANDALONE_WIN || UNITY_EDITOR_WIN
			if (imageFile.IndexOf(":/") >= 0)
			{
				prefix += "/";
			}
			#endif
			_menu2ImageLoader = new WWW(prefix + imageFile);
			StartCoroutine(LoadMenu2ImageCoRoutine());
		}
	}
	private void OnRequestMenu3Image(string EventName, ref object Data)
	{
		var imageFile = (string) Data;
		if (File.Exists(imageFile))
		{
			var prefix = "file://";
			#if UNITY_STANDALONE_WIN || UNITY_EDITOR_WIN
			if (imageFile.IndexOf(":/") >= 0)
			{
				prefix += "/";
			}
			#endif
			_menu3ImageLoader = new WWW(prefix + imageFile);
			StartCoroutine(LoadMenu3ImageCoRoutine());
		}
	}
	private void OnRequestMenu4Image(string EventName, ref object Data)
	{
		var imageFile = (string) Data;
		if (File.Exists(imageFile))
		{
			var prefix = "file://";
			#if UNITY_STANDALONE_WIN || UNITY_EDITOR_WIN
			if (imageFile.IndexOf(":/") >= 0)
			{
				prefix += "/";
			}
			#endif
			_menu4ImageLoader = new WWW(prefix + imageFile);
			StartCoroutine(LoadMenu4ImageCoRoutine());
		}
	}
	private void OnRequestMenu5Image(string EventName, ref object Data)
	{
		var imageFile = (string) Data;
		if (File.Exists(imageFile))
		{
			var prefix = "file://";
			#if UNITY_STANDALONE_WIN || UNITY_EDITOR_WIN
			if (imageFile.IndexOf(":/") >= 0)
			{
				prefix += "/";
			}
			#endif
			_menu5ImageLoader = new WWW(prefix + imageFile);
			StartCoroutine(LoadMenu5ImageCoRoutine());
		}
	}
	/// <summary>
    /// Fires when the team leader thumbnail image is to be downloaded
    /// </summary>
    /// <param name="eventName"></param>
    /// <param name="data"></param>
    private void OnRequestContentImage(string eventName, ref object data)
    {
        //  Debug.Log("Start coroutine for Team Leader Image");
        var path = (string)data;

        if (path.Equals(string.Empty) || !File.Exists(path))
        {
            object textureRef = Resources.Load("photoPlaceHolder");
            _contnetListItemImage.FireEvent(ref textureRef);
        }
        else
        {
            var prefix = "file://";

#if UNITY_STANDALONE_WIN || UNITY_EDITOR_WIN

            if (path.IndexOf(":/") >= 0)
            {
                prefix += "/";
            }

#endif
            _contentListItemImageLoader = new WWW(prefix + path);
            StartCoroutine(LoadContentImageCoRoutine());
        }
    }

    #endregion
}