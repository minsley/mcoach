﻿using Assets.Scripts.Tools;
using UnityEngine;

namespace MCoach
{
    /// <summary>
    /// Handles logic in the Agreement scene.
    /// </summary>
    public class AgreementController : MonoBehaviour
    {
        /// <summary>
        /// Stores true if script has been initialized.
        /// </summary>
        private bool _initialized;

        /// <summary>
        /// Triggered before first frame of scene
        /// </summary>
        private void Awake()
        {
            if (_initialized) return;
            _initialized = true;
            GlobalMessageManager.AddListener(Constants.Events.AcceptAgreementEventId.ToString(), OnAcceptAgreement);
        }

        /// <summary>
        /// Triggered when the user accepts the agreement.
        /// </summary>
        /// <param name="EventName"></param>
        /// <param name="Data"></param>
        private void OnAcceptAgreement(string EventName, ref object Data)
        {
            Application.LoadLevel("2Login");
        }
    }
}