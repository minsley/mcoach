﻿using Assets.Scripts.Tools;
using MCoach;
using System;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using IndieYP;

public class ContentListItemView : MonoBehaviour
{
    private Text _txtPdfTitle;
    private Text _txtPdfDescription;
    private RawImage _imgPdfThumbnail;
    private Sprite _thumbnailSprite;

    private string _contentUrl;

    private readonly UIEvent _contentItemSelected = new UIEvent(Constants.Events.ContentListItemSelectedEventId.ToString());

    /// <summary>
    /// Fires when the item is awoken
    /// </summary>
    private void Awake()
    {
	    GlobalMessageManager.AddEvent(_contentItemSelected);
    }

    /// <summary>
    /// Fired when the item is destroyed
    /// </summary>
    private void OnDestroy()
    {
        GlobalMessageManager.RemoveEvent(_contentItemSelected);
    }

    /// <summary>
    /// Sets the item view's content 
    /// </summary>
    /// <param name="item"></param>
    public void SetContent(Content item)
    {
        _contentUrl = item.ContentUrl;

        var child = transform.Find("pdfTitle");
        if (child != null)
        {
            _txtPdfTitle = child.GetComponent<Text>();
            _txtPdfTitle.text = item.Title;
        }

        child = transform.Find("pdfDescription");
        if (child != null)
        {
            _txtPdfDescription = child.GetComponent<Text>();
            _txtPdfDescription.text = item.Description;
        }
        
        child = transform.Find("pdfThumbnail");
        if (child != null)
        {
            _imgPdfThumbnail = child.GetComponent<RawImage>();
        }
    }

    /// <summary>
    /// Sets the thumbnail texture
    /// </summary>
    /// <param name="texture"></param>
    public void SetThumbnail(Texture2D texture)
    {
        _imgPdfThumbnail.texture = texture;
    }
	
	public void OnClick()
	{
		try
		{
			print("Show PDF: " + _contentUrl);

			#if UNITY_IPHONE
			PDFReader.OpenHTMLLocal(_contentUrl, _txtPdfTitle.text);
			#endif
		}catch(Exception ex){
			var message = ex.Message;
			print ("ContentListItemView->OnClick(), Exception caught: " + ex.Message);
		}
	}
}