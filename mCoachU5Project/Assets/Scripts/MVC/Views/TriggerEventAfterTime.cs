﻿using UnityEngine;
using MCoach;

/// <summary>
/// This script triggers an event after a given amount of
/// active time has passed. The timer only starts once the
/// GameObject this script is attached to has been activated.
/// </summary>
public class TriggerEventAfterTime : MonoBehaviour
{
    /// <summary>
    /// The name of the event to be triggered.
    /// </summary>
    public string EventName;
    /// <summary>
    /// Amount of time that must elapse before event is triggered.
    /// </summary>
    public float ActiveTime;
    /// <summary>
    /// References the event object.
    /// </summary>
    private UIEvent _elapsedTimeEvent;
    /// <summary>
    /// Stores the application time when the script was activated.
    /// </summary>
    private float _startTime = 0;
    /// <summary>
    /// Flags if the event has been triggered.
    /// </summary>
    private bool _eventTriggered = false;
    /// <summary>
    /// Text element to be updated when event is received.
    /// </summary>
    private void Awake()
    {
//        Debug.Log("Initialized trigger event time");
        _elapsedTimeEvent = new UIEvent(EventName);
        GlobalMessageManager.AddEvent(_elapsedTimeEvent);
        _startTime = Time.time;
    }
    
    /// <summary>
    /// Checks if the elapsed time has passed. If so, the event is fired. Triggered every frame.
    /// </summary>
    private void Update()
    {
        if (!_eventTriggered && ActiveTime < (Time.time - _startTime))
        {
            _eventTriggered = true;
            object obj = "";
            _elapsedTimeEvent.FireEvent(ref obj);
        }
    }
}