﻿using UnityEngine;
using UnityEngine.UI;

public class ScrollingMarqueeManager : MonoBehaviour
{
    public Text MarqueeText;
    public bool FireTest;
    public Font font;
    public float SpeedPerChar = .1f;
    public bool Loop = true;
    private float _startPos, _endPos, _animTime, _startTime, _distance;
    private float _scriptStartTime = 0;
    private bool _animating;
    
    private void Start()
    {
        _scriptStartTime = Time.time;
    }
    
    private void SetMessage(string NewMessage)
    {
        MarqueeText.text = NewMessage;
    }
    
    void Update()
    {
        if (_animating)
            AnimateMarquee();
        if (!((Time.time - _scriptStartTime) > 2f && FireTest)) return;
        SetMessage("Co-Pay Legislation Update: 03/21/2014 - Massachusetts overturns co-pay card ban. - Medical Merketing and Media Latest News: 07/09/2014 - Top opiod prescribers not pain specialists");
        FireTest = false;
        StartAnimation();   
    }
    
    private void StartAnimation()
    {
        _startPos = MarqueeText.transform.localPosition.x;
        float width = 0f;
        int fontSize = MarqueeText.cachedTextGenerator.fontSizeUsedForBestFit;
        
        for (int charIndex = 0; charIndex < MarqueeText.text.Length; charIndex++)
        {
            CharacterInfo info;
            font.GetCharacterInfo(MarqueeText.text[charIndex], out info, fontSize, UnityEngine.FontStyle.Normal);
            width += info.width;
        }
        
        RectTransform rect = MarqueeText.GetComponent<RectTransform>();
        rect.sizeDelta = new Vector2(width, rect.sizeDelta.y);
        _startPos = Screen.width + width/2;
        _endPos = -width/2;
        MarqueeText.gameObject.transform.position = new Vector3(Screen.width + width/2, MarqueeText.gameObject.transform.position.y, 0);
        _distance = _startPos - _endPos;
        //Debug.Log("Marquee Width: " + _distance);
        _animTime = MarqueeText.text.Length * SpeedPerChar;
        _startTime = Time.time;
        _animating = true;
    }
    
    private void AnimateMarquee()
    {
        var percentageComplete = (Time.time - _startTime)/_animTime;
        if (percentageComplete < 1)
        {
            MarqueeText.gameObject.transform.position = new Vector3(_startPos - (percentageComplete * _distance), MarqueeText.gameObject.transform.position.y, 0);
        }
        else
        {
            _animating = false;
            MarqueeText.gameObject.transform.position = new Vector3(_endPos, MarqueeText.gameObject.transform.position.y, 0);
            StartAnimation();
        }
    }
}