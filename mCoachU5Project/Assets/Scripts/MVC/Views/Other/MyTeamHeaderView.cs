﻿using UnityEngine;
using System.Collections.Generic;
using Assets.Scripts.Tools;
using MCoach;

/// <summary>
/// <para>Date: 07/16/2015</para>
/// <para>Author: NuMedia</para>
/// <para>Manages the Dashboard MyTeam header. Adds and removes team member buttons.</para>
/// </summary>
public class MyTeamHeaderView : MonoBehaviour
{
	/// <summary>
    /// The Rect of the scroll panel
    /// </summary>
    public RectTransform ScrollRect;
    /// <summary>
    /// The Rect of the container/background to the main FCR items
    /// </summary>
    public RectTransform ContainerRect;
    /// <summary>
    /// The UI object to be spawned for the first team member.
    /// </summary>
    public GameObject TeamMemberFirstSpawnPrefab;
    /// <summary>
    /// The UI object to be spawned for every team member besides the first.
    /// </summary>
    public GameObject TeamMemberPrefab;
    /// <summary>
    /// The width to height ratio of a team member object. This is required for scaling purposes.
    /// </summary>
    public float TeamMemberRatio = 4.91489361702128f;
    public bool TestInstance = false;
    /// <summary>
    /// Stores all instances of spawned team member objects.
    /// </summary>
    private List<RectTransform> _teamMemberInstances = new List<RectTransform>();

	private Manifest _lastReceivedManifest;

    /// <summary>
    /// Responsible for instantiating all variables and events.
    /// </summary>
    private void Awake()
    {
        GlobalMessageManager.AddListener(Constants.Events.MyTeamUpdateEventId.ToString(), OnMyTeamUpdate);
    }
    
    void OnDestroy()
    {
        GlobalMessageManager.RemoveListener(Constants.Events.MyTeamUpdateEventId.ToString(), OnMyTeamUpdate);
    }

    private void Update()
    {
        if (TestInstance)
        {
            TestInstance = false;
            for (int teamIndex = 0; teamIndex < _teamMemberInstances.Count; teamIndex++)
            {
                Destroy(_teamMemberInstances[teamIndex].gameObject);
            }
            _teamMemberInstances.Clear();
            TeamMember testMember = new TeamMember();
            testMember.Name = "Test Member";
            for (var i = 0; i < 2; i++)
                NewTeamInstance(testMember);
        }
    }

    /// <summary>
    /// Handles the MyTeamUpdate event. Expected a List of TeamMember as received Data object.
    /// Spawns TeamMember UI Instances for each received TeamMember in list.
    /// </summary>
    /// <param name="EventName"></param>
    /// <param name="Data">List<string> as object</param>
    private void OnMyTeamUpdate(string EventName, ref object Data)
    {
		object[] DataObjects = Data as object[];
		if (DataObjects != null && DataObjects.Length > 1)
			_lastReceivedManifest = DataObjects [1] as Manifest;
        //Delete previous que.
        for (int teamIndex = 0; teamIndex < _teamMemberInstances.Count; teamIndex++)
        {
            Destroy(_teamMemberInstances[teamIndex].gameObject);
        }
        _teamMemberInstances.Clear();

        //Cast Data object to TeamMembers
		List<TeamMember> teamMembers = (List<TeamMember>)DataObjects [0];
        for (int teamMemberIndex = 0; teamMemberIndex < teamMembers.Count; teamMemberIndex++)
        {
            NewTeamInstance(teamMembers[teamMemberIndex]);
        }
    }

    /// <summary>
    /// Spawns a new TeamMember instance based off of received TeamMember.
    /// </summary>
    /// <param name="teamMember"></param>
    private void NewTeamInstance(TeamMember teamMember)
    {
        var newInstance = (Instantiate(_teamMemberInstances.Count == 0 ? TeamMemberFirstSpawnPrefab : TeamMemberPrefab)
            as GameObject).GetComponent<RectTransform>();
        var teamMemberInstance = newInstance.GetComponent<TeamMemberInstance>();
        teamMemberInstance.AddTeamMemberReference(teamMember, _lastReceivedManifest);
        newInstance.name = "TeamMemberInstance" + _teamMemberInstances.Count;
        newInstance.SetParent(ContainerRect.transform);
		newInstance.transform.localScale = new Vector3(1,1,1);
        newInstance.sizeDelta = new Vector2(((float)ScrollRect.rect.height * TeamMemberRatio), ScrollRect.rect.height);
        _teamMemberInstances.Add(newInstance);
        float instancesWidth = GetTotalWidth();
        float actualWidth = instancesWidth > ScrollRect.rect.width ? instancesWidth : ScrollRect.rect.width;
        RecalculateContainer(actualWidth);
        RepositionInstances(actualWidth);
    }

    /// <summary>
    /// Repositions all team member ui objects. Call this after resizing the team member container.
    /// </summary>
    /// <param name="containerWidth"></param>
    private void RepositionInstances(float containerWidth)
	{
		for (int i = 0; i < _teamMemberInstances.Count; i++)
		{
			if (i == 0)
				_teamMemberInstances[i].transform.localPosition = new Vector2(-(containerWidth/(float)2) , 0);
			else
				_teamMemberInstances[i].transform.localPosition = new Vector2(-(containerWidth/(float)2 
                    - (((float)_teamMemberInstances[i].rect.width)/1.747f) * i), 0);//1.45 
		}
    }

    /// <summary>
    /// Gets the total width of all team member instances. Use this to resize the team member's container.
    /// </summary>
    private float GetTotalWidth()
    {
        float width = 0;

        for (int i = 0; i < _teamMemberInstances.Count; i++)
        {
            if (i == 0)
                width = (float)_teamMemberInstances[i].rect.width / 2f;
            else
                width += (((float)_teamMemberInstances[i].rect.width) / 1.447f);//);//1.45f
        }

        return width;
    }
    
    /// <summary>
    /// Resizes the team member's container based off of received width.
    /// </summary>
    /// <param name="newWidth">new container width</param>
    private void RecalculateContainer(float newWidth)
	{
		float containerHeight = ContainerRect.rect.height;
		
		ContainerRect.anchorMax = new Vector2(.5f, .5f);
		ContainerRect.anchorMin = new Vector2(.5f, .5f);
		ContainerRect.sizeDelta = new Vector2(newWidth, containerHeight);
	}
}