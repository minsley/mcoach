﻿using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// <para>Date: 05/12/2015</para>
/// <para>Author: NuMedia</para>
/// <para>Manages the dynamic FCR panel.</para>
/// </summary>
/// 
/// So Sorry, I didnt see this class so I created a more Unity flavored TeamMemberFCRView class.
/// even that one would be simpler if I had used Unity's layout components for resizing, etc...
/// trying to follow that model for the coaching plan display on the Ratings screen.
/// 
public class FCRDynamicUIManager : MonoBehaviour
{
	/// <summary>
    /// Represents available instance types.
    /// </summary>
    public enum Instance { Sales, Development, Goal };
	/// <summary>
    /// The Rect of the scroll panel
    /// </summary>
    public RectTransform ScrollRect;
	/// <summary>
	/// The Rect of the container/background to the main FCR items
    /// </summary>
    public RectTransform ContainerRect;
	/// <summary>
    /// The rect of the Sales Header section.
    /// </summary>
    public RectTransform SalesDataHeaderRect;
	/// <summary>
    /// The rect of the development header section.
    /// </summary>
    public RectTransform DevelopmentHeaderRect;
	/// <summary>
    /// The rect of the Goal Header section
    /// </summary>
    public RectTransform GoalHeaderRect;
	/// <summary>
    /// The default instance for Sales data. This should reference the item
	/// Assets/Prefabs/SalesDataPrefab
    /// </summary>
	public GameObject SalesDataPrefab;
	/// <summary>
    /// The default instance for Development data. This should reference the item
	/// Assets/Prefabs/DevelopmentImagePrefab
    /// </summary>
	public GameObject DevelopmentPrefab;
	/// <summary>
    /// The default instance for Development data. This should reference the item
	/// Assets/Prefabs/GoalDataImagePrefab
    /// </summary>
	public GameObject GoalPrefab;
    /// <summary>
    /// The height to width ratio of the header. Times the container
	/// width by this number to get the correct SalesHeader height.
    /// </summary>
    public float SalesHeaderRatio = 0.156311f;
    /// <summary>
    /// The height to width ratio of the sales prefab. Times the container
    /// width by this number to get the correct prefab height.
    /// </summary>
    public float SalesDataHeightToWidthRatio = 0.07741436f;
	/// <summary>
    /// The height to width ratio of the header. Times the container
	/// width by this number to get the correct DevelopmentHeader height.
    /// </summary>
    public float DevelopmentHeaderRatio = 0.156311f;
    /// <summary>
    /// The height to width ratio of the development prefab. Times the container
    /// width by this number to get the correct prefab height.
    /// </summary>
    public float DevelopmentHeightToWidthRatio = 0.07741436f;
	/// <summary>
    /// The height to width ratio of the header. Times the container
	/// width by this number to get the correct GoalHeader height.
    /// </summary>
    public float GoalHeaderRatio = 0.156311f;
    /// <summary>
    /// The height to width ratio of the goal prefab. Times the container
    /// width by this number to get the correct prefab height.
    /// </summary>
    public float GoalHeightToWidthRatio = 0.07741436f;
	public bool ResizeAll = false;
    /// <summary>
    /// References the number of update events allowed to run. 
    /// </summary>
    private int _updatesRun = 0;
    /// <summary>
    /// References all instances of Sales Data
    /// </summary>
    private List<RectTransform> _salesInstances = new List<RectTransform>();
    /// <summary>
    /// References all instances of development data. 
    /// </summary>
    private List<RectTransform> _developmentInstances = new List<RectTransform>();
	/// <summary>
    /// References all instances of goal data. 
    /// </summary>
	private List<RectTransform> _goalInstances = new List<RectTransform>();
	
	/// <summary>
    /// Update is linked to the draw event. This is fired every frame. 
    /// </summary>
	private void Update()
	{
		if (ResizeAll)
		{
			ResizeAll = false;
			ResizeHeaders();
			var height = GetTotalKnownHeight();
			RecalculateContainer(height);
			RepositionInstances(height);
			RecalculateHeaderPositions(height);
		}
		if (_updatesRun > 10) return;
		
		if (_updatesRun == 10)
		{
			ResizeHeaders();
			var height = GetTotalKnownHeight();
			RecalculateContainer(height);
			RepositionInstances(height);
			RecalculateHeaderPositions(height);
		}
		_updatesRun++;
    }

	/// <summary>
    /// Resizes all headers of FCR. 
    /// </summary>
	private void ResizeHeaders()
	{
		SalesDataHeaderRect.anchorMax = new Vector2(.5f, .5f);
		SalesDataHeaderRect.anchorMin = new Vector2(.5f, .5f);
		DevelopmentHeaderRect.anchorMax = new Vector2(.5f, .5f);
		DevelopmentHeaderRect.anchorMin = new Vector2(.5f, .5f);
		GoalHeaderRect.anchorMax = new Vector2(.5f, .5f);
		GoalHeaderRect.anchorMin = new Vector2(.5f, .5f);
		
		SalesDataHeaderRect.sizeDelta = new Vector2(ScrollRect.rect.width, ((float)ScrollRect.rect.width * (float)SalesHeaderRatio));
		DevelopmentHeaderRect.sizeDelta = new Vector2(ScrollRect.rect.width, ((float)ScrollRect.rect.width * (float)DevelopmentHeaderRatio));
		GoalHeaderRect.sizeDelta = new Vector2(ScrollRect.rect.width, ((float)ScrollRect.rect.width * (float)GoalHeaderRatio));
    }
	
	/// <summary>
    /// Adds a new data instance. Must pass in
    /// the new Instance type.
    /// </summary>
    /// <param name="instance">Sales, Development, or Goal</param>
	private void AddInstance(Instance instance)
	{
		List<RectTransform> instanceList;
		GameObject instancePrefab;
		float ratio;
		
		//Populates variables based on selected instance.
		if (instance == Instance.Sales)
		{
			instanceList = _salesInstances;
			instancePrefab = SalesDataPrefab;
			ratio = SalesDataHeightToWidthRatio;
		}
		else if (instance == Instance.Development)
		{
			instanceList = _developmentInstances;
			instancePrefab = DevelopmentPrefab;
			ratio = DevelopmentHeightToWidthRatio;
		}
		else
		{
			instanceList = _goalInstances;
			instancePrefab = GoalPrefab;
			ratio = GoalHeightToWidthRatio;
		}
		
		ResizeHeaders();
		
		//Spawn, name, make child of container, and resize new UI instance.
 		var newInstance = (Instantiate(instancePrefab) as GameObject).GetComponent<RectTransform>();
		newInstance.name = instance.ToString() + "Instance" + instanceList.Count;
 		newInstance.SetParent(ContainerRect.transform);
		newInstance.transform.localScale = new Vector3(1,1,1);
 		newInstance.sizeDelta = new Vector2(ScrollRect.rect.width, ((float)ScrollRect.rect.width * ratio));
 		
		float containerHeight = newInstance.rect.height + GetTotalKnownHeight();
		RecalculateContainer(containerHeight);
		instanceList.Add(newInstance);
		RepositionInstances(containerHeight);
		RecalculateHeaderPositions(containerHeight);
    }
	
    /// <summary>
    /// Resizes the FCR container. Requires the new height
	/// the container should resize to.
    /// </summary>
    /// <param name="newHeight">New Container Height</param>
    private void RecalculateContainer(float newHeight)
	{
		float containerWidth = ContainerRect.rect.width;
		
		ContainerRect.anchorMax = new Vector2(.5f, .5f);
		ContainerRect.anchorMin = new Vector2(.5f, .5f);
		ContainerRect.sizeDelta = new Vector2(containerWidth, newHeight);
	}

    /// <summary>
    /// Repositions all spawned data instances. Requires the
	/// new container height. Call only after the container
	/// has been resized.
    /// </summary>
    /// <param name="newHeight">New Container Height</param>
    private void RepositionInstances(float containerHeight)
	{
		RepositionSalesInstances(containerHeight);
		RepositionDevelopmentInstances(containerHeight);
		RepositionGoalInstances(containerHeight);
    }
	
	/// <summary>
    /// Repositions all spawned sales instances. Requires the
	/// new container height. Call only after the container
	/// has been resized.
    /// </summary>
    /// <param name="newHeight">New Container Height</param>
	private void RepositionSalesInstances(float containerHeight)
	{
		for (int i = 0; i < _salesInstances.Count; i++)
		{
			if (i == 0)
				_salesInstances[i].transform.localPosition = new Vector2(0, (containerHeight/(float)2) - ((float)_salesInstances[0].rect.height/(float)2) - SalesDataHeaderRect.rect.height);
			else
				_salesInstances[i].transform.localPosition = new Vector2(0, (containerHeight/(float)2) - ((float)_salesInstances[0].rect.height * (i)) - ((float)_salesInstances[0].rect.height/(float)2) - SalesDataHeaderRect.rect.height);
		}
	}
	
	/// <summary>
    /// Repositions all spawned development instances. Requires the
	/// new container height. Call only after the container
	/// has been resized.
    /// </summary>
    /// <param name="newHeight">New Container Height</param>
	private void RepositionDevelopmentInstances(float containerHeight)
	{
		for (int i = 0; i < _developmentInstances.Count; i++)
		{
			if (i == 0)
				_developmentInstances[i].transform.localPosition = new Vector2(0, (containerHeight/(float)2) - ((float)_developmentInstances[0].rect.height/(float)2) - DevelopmentHeaderRect.rect.height - GetTotalSalesHeight());
			else
				_developmentInstances[i].transform.localPosition = new Vector2(0, (containerHeight/(float)2) - ((float)_developmentInstances[0].rect.height * (i)) - ((float)_developmentInstances[0].rect.height/(float)2) - DevelopmentHeaderRect.rect.height - GetTotalSalesHeight());
		}
	}

    /// <summary>
    /// Repositions all spawned goal instances. Requires the
    /// new container height. Call only after the container
	/// has been resized.
    /// </summary>
    /// <param name="newHeight">New Container Height</param>
    private void RepositionGoalInstances(float containerHeight)
	{
		for (int i = 0; i < _goalInstances.Count; i++)
		{
			if (i == 0)
				_goalInstances[i].transform.localPosition = new Vector2(0, (containerHeight/(float)2) - ((float)_goalInstances[0].rect.height/(float)2) - GoalHeaderRect.rect.height - GetTotalSalesHeight() - GetTotalDevelopmentHeight());
			else
				_goalInstances[i].transform.localPosition = new Vector2(0, (containerHeight/(float)2) - ((float)_goalInstances[0].rect.height * (i)) - ((float)_goalInstances[0].rect.height/(float)2) - GoalHeaderRect.rect.height - GetTotalSalesHeight() - GetTotalDevelopmentHeight());
		}
	}

    /// <summary>
    /// Repositions all headers for the new container height.
    /// This must be called after the container has been resized.
	/// Requires the new container height.
    /// </summary>
    /// <param name="newHeight">New Container Height</param>
    private void RecalculateHeaderPositions(float containerHeight)
	{
		SalesDataHeaderRect.transform.localPosition = new Vector2(0, (containerHeight/(float)2) - ((float)SalesDataHeaderRect.rect.height/(float)2));
		DevelopmentHeaderRect.transform.localPosition = new Vector2(0, (containerHeight/(float)2) - ((float)DevelopmentHeaderRect.rect.height/(float)2) - GetTotalSalesHeight());
		GoalHeaderRect.transform.localPosition = new Vector2(0, (containerHeight/(float)2) - ((float)GoalHeaderRect.rect.height/(float)2) - GetTotalSalesHeight() - GetTotalDevelopmentHeight());
	}
	
	/// <summary>
    /// Gets the total height of the all known UI inside of the container. 
    /// </summary>
	private float GetTotalKnownHeight()
	{	
		return GetTotalSalesHeight() + GetTotalDevelopmentHeight() + GetTotalGoalHeight();
	}
	
	/// <summary>
    /// Gets the height of the sales UI. 
    /// </summary>
	private float GetTotalSalesHeight()
	{
		float height = SalesDataHeaderRect.rect.height;
		
		for (int i = 0; i < _salesInstances.Count; i++)
		{
			height += _salesInstances[i].rect.height;
		}
		
		return height;
	}
	
	/// <summary>
    /// Gets the height of the development UI.
    /// </summary>
	private float GetTotalDevelopmentHeight()
	{
		float height = DevelopmentHeaderRect.rect.height;
		
		for (int i = 0; i < _developmentInstances.Count; i++)
		{
			height += _developmentInstances[i].rect.height;
		}
		
		return height;
	}
	
	/// <summary>
    /// Gets the height of the goal UI. 
    /// </summary>
	private float GetTotalGoalHeight()
	{
		float height = GoalHeaderRect.rect.height;
		
		for (int i = 0; i < _goalInstances.Count; i++)
		{
			height += _goalInstances[i].rect.height;
		}
		
		return height;
	}
}