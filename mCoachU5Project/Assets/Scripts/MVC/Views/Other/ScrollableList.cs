﻿using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// <para>Date: 05/07/2015</para>
/// <para>Author: NuMedia</para>
/// <para>About</para>
/// </summary>
public class ScrollableList : MonoBehaviour
{
    /// <summary>
    /// Prefab of the UI object to be duplicated
    /// </summary>
    public GameObject ItemPrefab;
    public bool FireTest;
    public bool Vertical;
    /// <summary>
    /// Number of items to be dynamically created for testing purposes
    /// </summary>
    private int _itemCount = 0;
    /// <summary>
    /// Number of columns
    /// </summary>
    private int _columnCount = 1;
    
    private RectTransform _scrollRectTransform;
    private RectTransform _containerRectTransform;
    private RectTransform _rowRectTransform;
    private List<GameObject> _spawnedItems = new List<GameObject>();
    private float _width;
    private float _ratio;
    private float _height = 0;
    private float _startingHeight;

    void Start()
    {
        _scrollRectTransform = transform.parent.gameObject.GetComponent<RectTransform>();
        _rowRectTransform = ItemPrefab.GetComponent<RectTransform>();
        _containerRectTransform = gameObject.GetComponent<RectTransform>();

        //calculate the width and height of each child item.
        _width = _containerRectTransform.rect.width / _columnCount;
        _ratio = _width / _rowRectTransform.rect.width;
        _height = _rowRectTransform.rect.height * _ratio;
    }
    
    void Update()
    {
        if (!FireTest) return;
        FireTest = false;
        AddInstance(1);
    }
    
    private void AddInstance(int NewInstances)
    {
        var startingCount = _itemCount;
        _itemCount += NewInstances;
        int rowCount;
        if (Vertical)
        {
            rowCount = 1;
            _columnCount = _itemCount;
        }
        else
        {
            rowCount = _itemCount / _columnCount;
        }
        if (_itemCount % rowCount > 0)
            rowCount++;

        //adjust the height of the container so that it will just barely fit all its children
		// Unity's Layout components could be doing this without any code.
        float scrollHeight = _height * rowCount;
        Debug.Log(_scrollRectTransform.rect.height + " : " + scrollHeight);
        if (_scrollRectTransform.rect.height < scrollHeight)
        {
            float offset = scrollHeight - _scrollRectTransform.rect.height;
            _containerRectTransform.offsetMin = new Vector2(_containerRectTransform.offsetMin.x, -offset);
             //_containerRectTransform.offsetMax = new Vector2(_containerRectTransform.offsetMin.x, offset);  
        }

        int j = 0;//_itemCount - NewInstances;
        for (int i = 0; i < _itemCount; i++)
        {   
            //this is used instead of a double for loop because itemCount may not fit perfectly into the rows/columns
            if (i % _columnCount == 0)
                j++;

             GameObject newItem = (i < startingCount) ? _spawnedItems[i] : Instantiate(ItemPrefab) as GameObject;
            //create a new item, name it, and set the parent
            if (i >= startingCount)
                _spawnedItems.Add(newItem);
            
            newItem.name = gameObject.name + " item at (" + i + "," + j + ")";
            newItem.transform.SetParent(gameObject.transform);
			newItem.transform.localScale = new Vector3(1,1,1);

            //move and size the new item
            RectTransform rectTransform = newItem.GetComponent<RectTransform>();

            float x = -_containerRectTransform.rect.width / 2 + _width * (i % _columnCount);
            float y = _containerRectTransform.rect.height / 2 - _height * j;
            rectTransform.offsetMin = new Vector2(x, y);

            x = rectTransform.offsetMin.x + _width;
            y = rectTransform.offsetMin.y + _height;
            rectTransform.offsetMax = new Vector2(x, y);
        }
    }
}