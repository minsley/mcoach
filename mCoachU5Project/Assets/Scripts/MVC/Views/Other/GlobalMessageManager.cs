﻿using UnityEngine;
using System.Collections.Generic;
using MCoach;

/// <summary>
/// <para>Author: NuMedia</para>
/// <para>Handles custom events.</para>
/// </summary>
public static class GlobalMessageManager
{
    /// <summary>
    /// Dictionary of created events.
    /// </summary>
    private static Dictionary<string, UIEvent> GlobalEvents = new Dictionary<string,UIEvent>();
    private static List<string> _createdEvents = new List<string>();

    /// <summary>
    /// Add a listener. Requires the event name being listened for and the handler.
    /// </summary>
    /// <param name="eventName"></param>
    /// <param name="handler"></param>
    public static void AddListener(string eventName, MessageEvent handler)
    {
        if (eventName == null) return;
        if (handler == null) return;

        var existingEvent = UIEvent.GetEventName(eventName);
        if (GlobalEvents.ContainsKey(existingEvent))
        {
			// this says, if a duplicate event name is registered, and the first one had no handler,
			// then return without adding the new handler...  why would we do that ?
            // if (GlobalEvents[existingEvent].CustomEvent == null) return;

            GlobalEvents[existingEvent].CustomEvent += handler;
        }
        else
        {
            //Debug.LogError("AddListener: " + eventName);
            var newEvent = new UIEvent(eventName);
            newEvent.CustomEvent += handler;
            GlobalEvents.Add(newEvent.EventName, newEvent);
        }
    }

    /// <summary>
    /// Removes a listener. Requires the event name and the function handling the event.
    /// </summary>
    /// <param name="eventName"></param>
    /// <param name="removeHandler"></param>
    public static void RemoveListener(string eventName, MessageEvent removeHandler)
    {
        if (removeHandler == null) return;

        eventName = UIEvent.GetEventName(eventName);

        if (!GlobalEvents.ContainsKey(eventName)) return;
        if (GlobalEvents[eventName].CustomEvent == null) return;
        GlobalEvents[eventName].CustomEvent -= removeHandler;
    }

    /// <summary>
    /// Adds a new event. Requires a UIEvent.
    /// </summary>
    /// <param name="newEvent"></param>
    public static void AddEvent(UIEvent newEvent)
    {
        if (newEvent == null)
        {
            Debug.LogError("Received Null Event!");
            return;
        }
        if (_createdEvents.Contains(newEvent.EventName))
        {
            
        }
            //  Debug.LogWarning("An event with the name: " + newEvent.EventName + " Already Exists");
        else
            _createdEvents.Add(newEvent.EventName);

        newEvent.CustomEvent += EventFired;
    }

    /// <summary>
    /// Removes a event. Requires a UIEvent.
    /// </summary>
    /// <param name="removeMe"></param>
    public static void RemoveEvent(UIEvent removeMe)
    {
        removeMe.CustomEvent -= EventFired;
    }

	public static void Reset(){
		// clear both lists in prep for a new level being loaded...
		GlobalEvents.Clear ();
		_createdEvents.Clear();
	}

    /// <summary>
    /// When an event is fired, checks the dictionary to see if a handler
    /// has been assigned to the event. If a handler exists the handler
    /// is called.
    /// </summary>
    /// <param name="EventName"></param>
    /// <param name="Data"></param>
    private static void EventFired(string EventName, ref object Data)
    {
        if (GlobalEvents.ContainsKey(EventName))
            GlobalEvents[EventName].FireEvent(ref Data);
    }
}