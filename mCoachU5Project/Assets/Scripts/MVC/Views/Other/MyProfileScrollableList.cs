﻿using UnityEngine;
using MCoach;
using System.Collections.Generic;
using Assets.Scripts.Tools;

/// <summary>
/// <para>Date: 05/07/2015</para>
/// <para>Author: NuMedia</para>
/// <para>About</para>
/// </summary>
public class MyProfileScrollableList : MonoBehaviour
{
    /// <summary>
    /// Prefab of the UI object to be duplicated
    /// </summary>
    public GameObject ItemPrefab;
    /// <summary>
    /// Number of items to be dynamically created for testing purposes
    /// </summary>
    private int _itemCount = 0;
    /// <summary>
    /// RectTransform of scroll panel.
    /// </summary>
    private RectTransform _scrollRectTransform;
    /// <summary>
    /// RectTransform of instance container.
    /// </summary>
    private RectTransform _containerRectTransform;
    /// <summary>
    /// RectTransform of an instance row.
    /// </summary>
    private RectTransform _rowRectTransform;
    /// <summary>
    /// Reference of spawned UI instances.
    /// </summary>
    private List<GameObject> _spawnedItems = new List<GameObject>();
    private int _updateCount = 0;
    /// <summary>
    /// Entire width of the container.
    /// </summary>
    private float _width;
    /// <summary>
    /// Ratio of spawned item instance.
    /// </summary>
    private float _ratio;
    /// <summary>
    /// Height of each row of spawned objets.
    /// </summary>
    private float _height = 0;
    /// <summary>
    /// Starting height of spawned instances.
    /// </summary>
    private float _startingHeight;
    /// <summary>
    /// Flag representing if listeners have been linked to their event.
    /// </summary>
    private bool _instantiated = false;

	private Manifest _lastReceivedManifest = null;

    /// <summary>
    /// Responsible for instantiating all events and event listeners.
    /// </summary>
    public void Awake()
    {
        if (_instantiated) return;
        _instantiated = true;
        GlobalMessageManager.AddListener(Constants.Events.MyTeamUpdateEventId.ToString(), OnTeamMembers);
        _scrollRectTransform = transform.parent.gameObject.GetComponent<RectTransform>();
        _rowRectTransform = ItemPrefab.GetComponent<RectTransform>();
        _containerRectTransform = gameObject.GetComponent<RectTransform>();

        //calculate the width and height of each child item.
        _width = _containerRectTransform.rect.width;
        _ratio = _width / _rowRectTransform.rect.width;
        _height = _rowRectTransform.rect.height * _ratio;
    }
    
    void OnDestroy()
    {
        GlobalMessageManager.RemoveListener(Constants.Events.MyTeamUpdateEventId.ToString(), OnTeamMembers);
    }

    /// <summary>
    /// Insantiates MyProfileScrollableList.
    /// </summary>
    void Start()
    {
        _scrollRectTransform = transform.parent.gameObject.GetComponent<RectTransform>();
        _rowRectTransform = ItemPrefab.GetComponent<RectTransform>();
        _containerRectTransform = gameObject.GetComponent<RectTransform>();

        //calculate the width and height of each child item.
        _width = _containerRectTransform.rect.width;
        _ratio = _width / _rowRectTransform.rect.width;
        _height = _rowRectTransform.rect.height * _ratio;
    }
    
    /// <summary>
    /// Fires every frame. Recalculates the profile instances after the first five active frames.
    /// </summary>
    void Update()
    {
        if (_updateCount > 5) return;
        if (_updateCount < 5)
            _updateCount++;
            
        if (_updateCount == 5)
        {
            RecalculateInstances();
            _updateCount++;
        }
    }
    
    /// <summary>
    /// Handles MyTeamUpdate event. Spawns UI instances representing each TeamMember name.
    /// </summary>
    private void OnTeamMembers(string EventName, ref object Data)
    {
        for (int spawnedItemIndex = 0; spawnedItemIndex < _spawnedItems.Count; spawnedItemIndex++)
        {
            Destroy(_spawnedItems[spawnedItemIndex].gameObject);
        }
        
        _spawnedItems.Clear();
        _itemCount = 0;

		object[] DataObjects = Data as object[];
		if (DataObjects != null && DataObjects.Length > 1)
			_lastReceivedManifest = DataObjects [1] as Manifest;

		//Cast Data object to TeamMembers
		List<TeamMember> teamMembers = (List<TeamMember>)DataObjects[0];
        for(int teamMemberIndex = 0; teamMemberIndex < teamMembers.Count; teamMemberIndex++)
        {
            AddInstance(teamMembers[teamMemberIndex].Name);
        }
    }
    
    /// <summary>
    /// Spawns new UI instance displaying the received TeamMember name.
    /// </summary>
    private void AddInstance(string teamMemberName)
    {
        var startingCount = _itemCount;
        _itemCount++;
        int rowCount = _itemCount;
        if (_itemCount % rowCount > 0)
            rowCount++;

        //adjust the height of the container so that it will just barely fit all its children
        float scrollHeight = _height * rowCount;
        if (_scrollRectTransform.rect.height < scrollHeight)
        {
            float offset = scrollHeight - _scrollRectTransform.rect.height;
            _containerRectTransform.offsetMin = new Vector2(_containerRectTransform.offsetMin.x, -offset);  
        }

        int checkedInstances = 0;
        for (int i = 0; i < _itemCount; i++)
        {   
            //this is used instead of a double for loop because itemCount may not fit perfectly into the rows/columns
                checkedInstances++;
             
             GameObject newItem;
             if (i < startingCount)
             {
                 newItem = _spawnedItems[i];
             }
             else
             {
                 newItem = Instantiate(ItemPrefab) as GameObject;
                 newItem.GetComponent<ProfileTeamMemberInstance>().FullName.text = teamMemberName;
                 newItem.name = teamMemberName;
             }
            //create a new item, name it, and set the parent
            if (i >= startingCount)
                _spawnedItems.Add(newItem);
            
            //newItem.name = teamMemberName;
            newItem.transform.SetParent(gameObject.transform);
			newItem.transform.localScale = new Vector3(1,1,1);

            //move and size the new item
            RectTransform rectTransform = newItem.GetComponent<RectTransform>();

            float x = -_containerRectTransform.rect.width / 2 + _width * 0;//i
            float y = _containerRectTransform.rect.height / 2 - _height * checkedInstances;
            rectTransform.offsetMin = new Vector2(x, y);

            x = rectTransform.offsetMin.x + _width;
            y = rectTransform.offsetMin.y + _height;
            rectTransform.offsetMax = new Vector2(x, y);
        }
    }
    
    private void RecalculateInstances()
    {
        var startingCount = _itemCount;
        int rowCount = _itemCount;

        //adjust the height of the container so that it will just barely fit all its children
        float scrollHeight = _height * rowCount;
        if (_scrollRectTransform.rect.height < scrollHeight)
        {
            float offset = scrollHeight - _scrollRectTransform.rect.height;
            _containerRectTransform.offsetMin = new Vector2(_containerRectTransform.offsetMin.x, -offset);  
        }

        int checkedInstances = 0;
        for (int i = 0; i < _itemCount; i++)
        {   
            //this is used instead of a double for loop because itemCount may not fit perfectly into the rows/columns
                checkedInstances++;
             
             GameObject newItem;
             if (i < startingCount)
             {
                 newItem = _spawnedItems[i];
             }
             else
             {
                 continue;
             }

            //create a new item, name it, and set the parent
            if (i >= startingCount)
                _spawnedItems.Add(newItem);
            
            //newItem.name = teamMemberName;
            newItem.transform.SetParent(gameObject.transform);
			newItem.transform.localScale = new Vector3(1,1,1);

            //move and size the new item
            RectTransform rectTransform = newItem.GetComponent<RectTransform>();

            float x = -_containerRectTransform.rect.width / 2 + _width * 0;//i
            float y = _containerRectTransform.rect.height / 2 - _height * checkedInstances;
            rectTransform.offsetMin = new Vector2(x, y);

            x = rectTransform.offsetMin.x + _width;
            y = rectTransform.offsetMin.y + _height;
            rectTransform.offsetMax = new Vector2(x, y);
        }
    }
}