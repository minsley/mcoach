﻿using UnityEngine;
using System.Collections.Generic;

public class HeaderMananger : MonoBehaviour
{
	/// <summary>
    /// The Rect of the scroll panel
    /// </summary>
    public RectTransform ScrollRect;
    /// <summary>
    /// The Rect of the container/background to the main FCR items
    /// </summary>
    public RectTransform ContainerRect;
    public GameObject TeamMemberFirstSpawnPrefab;
    public GameObject TeamMemberPrefab;
    public float TeamMemberRatio = 4.91489361702128f;
    public bool FireTest;
    private List<RectTransform> _teamMemberInstances = new List<RectTransform>();

    private void Update()
    {
        if (!FireTest) return;
        FireTest = false;
        NewTeamInstance();
    }

    private void NewTeamInstance()
    {   
        var newInstance = (Instantiate(_teamMemberInstances.Count == 0 ? TeamMemberFirstSpawnPrefab : TeamMemberPrefab) as GameObject).GetComponent<RectTransform>();
        Debug.Log("is null? " + (newInstance == null));
        newInstance.name = "TeamMemberInstance" + _teamMemberInstances.Count;
        newInstance.SetParent(ContainerRect.transform);
		newInstance.transform.localScale = new Vector3(1,1,1);
        newInstance.sizeDelta = new Vector2(((float)ScrollRect.rect.height * TeamMemberRatio), ScrollRect.rect.height);
        _teamMemberInstances.Add(newInstance);
        float instancesWidth = GetTotalWidth();
        float actualWidth = instancesWidth > ScrollRect.rect.width ? instancesWidth : ScrollRect.rect.width;
        RecalculateContainer(actualWidth);
        RepositionInstances(actualWidth);
    }
    
    private void RepositionInstances(float containerWidth)
	{
		for (int i = 0; i < _teamMemberInstances.Count; i++)
		{
			if (i == 0)
				_teamMemberInstances[i].transform.localPosition = new Vector2(-(containerWidth/(float)2) , 0);
			else
				_teamMemberInstances[i].transform.localPosition = new Vector2(-(containerWidth/(float)2 - (((float)_teamMemberInstances[i].rect.width)/1.45f) * i), 0);
		}
    }

    private float GetTotalWidth()
    {
        float width = 0;

        for (int i = 0; i < _teamMemberInstances.Count; i++)
        {
            if (i == 0)
                width = (float)_teamMemberInstances[i].rect.width / 2f;
            else
                width += (((float)_teamMemberInstances[i].rect.width) / 1.45f);
        }

        return width;
    }
    
    private void RecalculateContainer(float newWidth)
	{
		float containerHeight = ContainerRect.rect.height;
		
		ContainerRect.anchorMax = new Vector2(.5f, .5f);
		ContainerRect.anchorMin = new Vector2(.5f, .5f);
		ContainerRect.sizeDelta = new Vector2(newWidth, containerHeight);
	}
}