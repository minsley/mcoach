﻿using UnityEngine;
using System.Collections.Generic;
using Assets.Scripts.Tools;
using MCoach;

/// <summary>
/// <para>Date: 05/07/2015</para>
/// <para>Author: NuMedia</para>
/// <para>About</para>
/// </summary>
public class CoachingCategoryScrollableList : MonoBehaviour
{
    /// <summary>
    /// Prefab of the UI object to be duplicated
    /// </summary>
    public GameObject ItemPrefab;
    public bool FireTest;
    public bool Vertical;
    /// <summary>
    /// Number of items to be dynamically created for testing purposes
    /// </summary>
    private int _itemCount = 0;
    /// <summary>
    /// Number of columns
    /// </summary>
    private int _columnCount = 1;
    
    private RectTransform _scrollRectTransform;
    private RectTransform _containerRectTransform;
    private RectTransform _rowRectTransform;
	private List<GameObject> _spawnedItems = new List<GameObject>();
	private bool _instantiated = false;
    private float _width;
    private float _ratio;
    private float _height = 0;
    private float _startingHeight;
	private Manifest _lastManifestReceived = null;
    
    void Awake()
	{
		if (_instantiated) return;
		_instantiated = true;
        GlobalMessageManager.AddListener(Constants.Events.CoachingSkillsEventId.ToString(), OnCoachingSkills);
		GlobalMessageManager.AddListener(Constants.Events.ManifestUpdatedEventId.ToString(), OnManifestUpdated);
    }

    void Start()
    {
        _scrollRectTransform = transform.parent.gameObject.GetComponent<RectTransform>();
        _rowRectTransform = ItemPrefab.GetComponent<RectTransform>();
        _containerRectTransform = gameObject.GetComponent<RectTransform>();

        //calculate the width and height of each child item.
        _width = _containerRectTransform.rect.width / _columnCount;
        _ratio = _width / _rowRectTransform.rect.width;
        _height = _rowRectTransform.rect.height * _ratio+12;
    }

	private void OnManifestUpdated(string EventName, ref object Data){
		// this doesn't work very well, OnCoachingSkills usually comes in before this handler gets called (from Dashboard handler, since it registers first)
		_lastManifestReceived = (Manifest)Data;
	}

    
    private void OnCoachingSkills(string EventName, ref object Data)
    {
        for (int spawnedIndex = 0; spawnedIndex < _spawnedItems.Count; spawnedIndex++)
        {
            Destroy(_spawnedItems[spawnedIndex]);
        }

        _spawnedItems.Clear();
        _itemCount = 0;

		_lastManifestReceived = Data as Manifest;
        CoachingSkillCategory[] CS = _lastManifestReceived.CoachingCategories;
		Dictionary<int, SellingSkillInfo> teamSkillRatings =_lastManifestReceived.GetSkillRatings(-1);
			
		for (int coachingIndex = 0; coachingIndex < CS.Length; coachingIndex++)
        {
			if (CS[coachingIndex].IsActive){ // Added 10/27/16 PAA Defect 8092
				SellingSkillInfo info = null;
				if (teamSkillRatings.ContainsKey(CS[coachingIndex].ID))
					info = teamSkillRatings[CS[coachingIndex].ID];
	            AddInstance(CS[coachingIndex],info); // it seems like we wouldn't want to add this if info were null ?
			}
        }
    }
    
	private void AddInstance(CoachingSkillCategory coachingSkillCategory, SellingSkillInfo info)
	{
        //  Debug.Log("Coaching Skill Title: " + coachingSkillCategory.Title);
        
        var startingCount = _spawnedItems.Count;
        _itemCount += 1;
        int rowCount;
        if (Vertical)
        {
            rowCount = 1;
            _columnCount = _itemCount;
        }
        else
        {
            rowCount = _itemCount / _columnCount;
        }
        if (_itemCount % rowCount > 0)
            rowCount++;

        //adjust the height of the container so that it will just barely fit all its children
        float scrollHeight = _height * rowCount;
//        Debug.Log(_scrollRectTransform.rect.height + " : " + scrollHeight);
        if (_scrollRectTransform.rect.height < scrollHeight)
        {
            float offset = scrollHeight - _scrollRectTransform.rect.height;
            _containerRectTransform.offsetMin = new Vector2(_containerRectTransform.offsetMin.x, -offset);
             //_containerRectTransform.offsetMax = new Vector2(_containerRectTransform.offsetMin.x, offset);  
        }

        int j = 0;//_itemCount - NewInstances;
        for (int i = 0; i < _itemCount; i++)
        {   
            //this is used instead of a double for loop because itemCount may not fit perfectly into the rows/columns
            if (i % _columnCount == 0)
                j++;

             GameObject newItem; // = (i < startingCount) ? _spawnedItems[i] : Instantiate(ItemPrefab) as GameObject;
             if (i < startingCount)
             {
                newItem = _spawnedItems[i];
             }
             else
             {
                 newItem = Instantiate(ItemPrefab) as GameObject;
                 
                 var coachingInstance = newItem.GetComponent<CoachingSkillInstance>();
	             coachingInstance.SetCoachingSkill(coachingSkillCategory, false);
				if (_lastManifestReceived != null){
//					if (info == null) //TODO enable/delete when data arrives
//					else
//						info = TeamMemberInstance.RandomSellingSkillInfo();
					coachingInstance.DisplayRatings( info ,true ); 
				}
             }
	        
            //create a new item, name it, and set the parent
            if (i >= startingCount)
                _spawnedItems.Add(newItem);
            
            newItem.name = gameObject.name + " item at (" + i + "," + j + ")";
            newItem.transform.SetParent(gameObject.transform);
			newItem.transform.localScale = new Vector3(1,1,1); // compensate for the reparenting scale effect
/*
            //move and size the new item
            RectTransform rectTransform = newItem.GetComponent<RectTransform>();

            float x = -_containerRectTransform.rect.width / 2 + _width * (i % _columnCount);
            float y = _containerRectTransform.rect.height / 2 - _height * j;
            rectTransform.offsetMin = new Vector2(x, y);

            x = rectTransform.offsetMin.x + _width;
            y = rectTransform.offsetMin.y + _height;
            rectTransform.offsetMax = new Vector2(x, y);
*/
		}
    }
}