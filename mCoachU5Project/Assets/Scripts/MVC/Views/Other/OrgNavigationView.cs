﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using Assets.Scripts.Tools;
using MCoach;

/// <summary>
/// <para>Date: 07/16/2015</para>
/// <para>Author: NuMedia</para>
/// <para>Manages the Dashboard MyTeam header. Adds and removes team member buttons.</para>
/// </summary>
public class OrgNavigationView : MonoBehaviour
{
	public GameObject BreadcrumbContainer;
	public GameObject BreadcrumbTemplate;
	public GameObject TeamMemberContainer;
	public GameObject TeamMemberTemplate;

    /// <summary>
    /// Stores all instances of spawned team member objects.
    /// </summary>
    private List<GameObject> _teamMemberInstances = new List<GameObject>();
	private List<GameObject> _breadcrumbInstances = new List<GameObject> ();

	private Manifest _lastReceivedManifest;

    /// <summary>
    /// Responsible for instantiating all variables and events.
    /// </summary>
    private void Awake()
    {
        GlobalMessageManager.AddListener(Constants.Events.MyTeamUpdateEventId.ToString(), OnMyTeamUpdate);
    }
    
    void OnDestroy()
    {
        GlobalMessageManager.RemoveListener(Constants.Events.MyTeamUpdateEventId.ToString(), OnMyTeamUpdate);
    }

    private void Update()
    {
    }

	public void DrillToTop(){
		DrilldownController.GetInstance ().DrillOutAs (null); // defaults to topmost level
	}

    /// <summary>
    /// Handles the MyTeamUpdate event. Expected a List of TeamMember as received Data object.
    /// Spawns TeamMember UI Instances for each received TeamMember in list.
    /// </summary>
    /// <param name="EventName"></param>
    /// <param name="Data">List<string> as object</param>
    private void OnMyTeamUpdate(string EventName, ref object Data)
    {
		object[] DataObjects = Data as object[];
		if (DataObjects != null && DataObjects.Length > 1)
			_lastReceivedManifest = DataObjects [1] as Manifest;
        //Delete previous que.
        for (int teamIndex = 0; teamIndex < _teamMemberInstances.Count; teamIndex++)
        {
            Destroy(_teamMemberInstances[teamIndex]);
        }
        _teamMemberInstances.Clear();
		for (int breadcrumbIndex = 0; breadcrumbIndex < _breadcrumbInstances.Count; breadcrumbIndex++)
		{
			Destroy(_breadcrumbInstances[breadcrumbIndex]);
		}
		_breadcrumbInstances.Clear();

		Manifest[] aliases = DrilldownController.GetInstance ().Aliases.ToArray ();
		// add a breadcrumb for every alias except the first, and if there ARE aliases, also add a breadcrumb for the current manifest.
		for (int i = 1; i< aliases.Length; i++) {
			AddBreadcrumb(aliases[i],true);
		}
		if (aliases.Length > 0)
			AddBreadcrumb (MCoachModel.GetManifest (),false);

        //Cast Data object to TeamMembers
		List<TeamMember> teamMembers = (List<TeamMember>)DataObjects [0];
        for (int teamMemberIndex = 0; teamMemberIndex < teamMembers.Count; teamMemberIndex++)
        {
            NewTeamInstance(teamMembers[teamMemberIndex]);
        }
		// the SizeFitter component doesn't do well at this level, so we will set the container width ourselves.
//		TeamMemberContainer.GetComponent<RectTransform> ().SetSizeWithCurrentAnchors (RectTransform.Axis.Horizontal, (_teamMemberInstances.Count+0.5f) * TeamMemberTemplate.GetComponent<RectTransform> ().rect.width);

		TeamMemberContainer.GetComponent<RectTransform> ().anchorMax = new Vector2(.0f, 1);
		TeamMemberContainer.GetComponent<RectTransform> ().anchorMin = new Vector2(.0f, 0f);
		float width = (_teamMemberInstances.Count + 0.5f) * TeamMemberTemplate.GetComponent<RectTransform> ().rect.width;
		if (width < TeamMemberContainer.transform.parent.GetComponent<RectTransform> ().rect.width)//TODO shouldnt this be the size of the scroll rect ?
			width = TeamMemberContainer.transform.parent.GetComponent<RectTransform> ().rect.width;
		TeamMemberContainer.GetComponent<RectTransform> ().sizeDelta = new Vector2(width, 0);
		TeamMemberContainer.transform.parent.GetComponent<ScrollRect> ().horizontalNormalizedPosition = 0;
//		TeamMemberContainer.transform.parent.GetComponent<ScrollRect> ().SetDirty ();
//		TeamMemberContainer.transform.parent.GetComponent<ScrollRect> ().Rebuild (CanvasUpdate.PostLayout);
	}

	private void AddBreadcrumb(Manifest manifest, bool isInteractive)
	{
		GameObject newInstance = Instantiate(BreadcrumbTemplate) as GameObject;
		newInstance.SetActive (true);
		_breadcrumbInstances.Add (newInstance);
		// set the data....
		BreadcrumbInstance breadCrumbInstance = newInstance.GetComponent<BreadcrumbInstance> ();
		breadCrumbInstance.AddBreadcrumbReference (manifest, isInteractive);
		newInstance.name = "BreadcrumbInstance" + _breadcrumbInstances.Count;
		newInstance.transform.SetParent(BreadcrumbContainer.transform);
		newInstance.transform.localScale = new Vector3(1,1,1);
	}

    /// <summary>
    /// Spawns a new TeamMember instance based off of received TeamMember.
    /// </summary>
    /// <param name="teamMember"></param>
    private void NewTeamInstance(TeamMember teamMember)
    {
        GameObject newInstance = Instantiate(TeamMemberTemplate) as GameObject;
		newInstance.SetActive (true);
		_teamMemberInstances.Add (newInstance);
        var teamMemberInstance = newInstance.GetComponent<TeamMemberInstance>();
        teamMemberInstance.AddTeamMemberReference(teamMember, _lastReceivedManifest); // could we lose this reference ?
        newInstance.name = "TeamMemberInstance" + _teamMemberInstances.Count;
        newInstance.transform.SetParent(TeamMemberContainer.transform);
		newInstance.transform.localScale = new Vector3(1,1,1);
    }

}