using System;
using UnityEngine;
using UnityEngine.UI;

namespace MCoach
{
    /// <summary>
    /// <para>Author: NuMedia</para>
    /// <para>A UI element which can trigger a UIEvent</para>
    /// </summary>
    [Serializable]
    public class UIElement : MonoBehaviour
    {
        /// <summary>
        /// The types of data that can be pushed from the onclick event
        /// </summary>
        public enum DataType { Input, LabelText, Other };
        /// <summary>
        /// The name to be pushed to the view when the OnClick event is raised.
        /// </summary>
        public string EventName;
        /// <summary>
        /// Sets the datatype of Element
        /// </summary>
        public DataType UIWidgetDataType = DataType.Input;
        /// <summary>
        /// The UIEvent which is raised when OnClick is fired.
        /// </summary>
        public UIEvent ElementEvent;
        /// <summary>
        /// Flag representing if the application has been initialized.
        /// </summary>
        private bool _initialized = false;

        /// <summary>
        /// First method to fire whenever application
        /// is resumed or started.
        /// </summary>
        private void Awake()
        {
            Initialize();
        }

        /// <summary>
        /// Initializes the class. Class must be initialized before
        /// linking to events.
        /// </summary>
        public void Initialize()
        {
            if (_initialized) return;
            _initialized = true;
            ElementEvent = new UIEvent(EventName);
            GlobalMessageManager.AddEvent(ElementEvent);
        }

        /// <summary>
        /// Triggered when the UIElement is clicked.
        /// </summary>
        public void OnTrigger()
        {
            object data;
            if (UIWidgetDataType == DataType.Input)
                data = this.gameObject.GetComponent<InputField>().text;
            else if (UIWidgetDataType == DataType.LabelText)
                data = this.gameObject.GetComponent<Text>().text;
            else
                data = this.gameObject;

            if (ElementEvent != null)
                ElementEvent.FireEvent(ref data);
        }

        /// <summary>
        /// Decouples event when this script is destroyed.
        /// </summary>
        private void OnDestroy()
        {
            GlobalMessageManager.RemoveEvent(ElementEvent);
        }
    }
}