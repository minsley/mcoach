﻿using UnityEngine;
using UnityEngine.UI;
using System;
/// <summary>
/// Updates a text component when a listened for event
/// is received.
/// </summary>
public class UpdateText : MonoBehaviour
{
    /// <summary>
    /// Text element to be updated when event is received.
    /// </summary>
    public Text TextElement;
    /// <summary>
    /// Name of event being listened for.
    /// </summary>
    public string ListenEventName;
    /// <summary>
    /// Text that will always be displayed before and received text.
    /// Example: DefaultText + EventText
    /// </summary>
    public string DefaultText;
	public string TrailingText = "";

    private bool Initialized = false;

    /// <summary>
    /// Initializes script. First method to be triggered in script.
    /// </summary>
    public void Awake()
    {
        if (Initialized) return;
        Initialized = true;
        
//        Debug.Log("INITIALIZED: " + ListenEventName);
        GlobalMessageManager.AddListener(ListenEventName, OnTextChanged);
        
        if (TextElement == null)
            TextElement = GetComponent<Text>();
    }

    /// <summary>
    /// Handles the event referenced in ListenEventName when fired.
    /// </summary>
    private void OnTextChanged(string eventName, ref object Data)
	{
		try
		{
			TextElement.text = DefaultText + (string)Data + TrailingText;
		}
			catch (Exception e)
			{
				print("Text Change Exception: " + this.name);
				Debug.LogError(e);
			}
    }
}