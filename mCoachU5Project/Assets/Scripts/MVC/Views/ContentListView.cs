﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Tools;
using MCoach;
using UnityEngine;
using UnityEngine.UI;

public class ContentListView : MonoBehaviour
{
    /// <summary>
    /// Requests the selected section
    /// </summary>
    private readonly UIEvent _requestSection = new UIEvent(Constants.Events.MyContentSectionRequestEventId.ToString());

    /// <summary>
    /// Requests a thumbnail image from the file controller
    /// </summary>
    private readonly UIEvent _requestThumbnail =
        new UIEvent(Constants.Events.RequestContentListItemImageEventId.ToString());

    /// <summary>
    /// Queue for sequentially loading the content thumbnails
    /// </summary>
    private readonly Queue<string> _thumbnailQueue = new Queue<string>();

    /// <summary>
    /// Prefab of the UI object to be duplicated
    /// </summary>
    public GameObject ItemPrefab;

    public bool FireTest;
    public bool Vertical;

    /// <summary>
    /// Number of items to be dynamically created for testing purposes
    /// </summary>
    private int _itemCount = 0;

    /// <summary>
    /// Number of columns
    /// </summary>
    private int _columnCount = 3;

    private RectTransform _scrollRectTransform;
    private RectTransform _containerRectTransform;
    private RectTransform _rowRectTransform;
    private List<GameObject> _spawnedItems = new List<GameObject>();
    private bool _instantiated = false;
    private bool _firstFrame = false;
    private float _width;
    private float _ratio = 2;
    private float _height = 0;
    private float _startingHeight;

    private void Awake()
    {
        if (_instantiated) return;
        _instantiated = true;

        GlobalMessageManager.AddListener(Constants.Events.MyContentSectionUpdateEventId.ToString(),
            OnMyContentSectionUpdate);
        GlobalMessageManager.AddListener(Constants.Events.ContentListItemThumbnailEventId.ToString(),
            OnContentImageRecieved);
        GlobalMessageManager.AddEvent(_requestSection);
        GlobalMessageManager.AddEvent(_requestThumbnail);
    }

    private void Start()
    {
        _scrollRectTransform = transform.parent.gameObject.GetComponent<RectTransform>();
        _rowRectTransform = ItemPrefab.GetComponent<RectTransform>();
        _containerRectTransform = gameObject.GetComponent<RectTransform>();

        //calculate the width and height of each child item.
        _width = _containerRectTransform.rect.width/_columnCount;
        _ratio = _width/_rowRectTransform.rect.width;
		_ratio = 1; // let's not scale these up so big...just use the prefab size.
        _height = _rowRectTransform.rect.height*_ratio;
		_width = _rowRectTransform.rect.width;
        if (!_firstFrame)
        {
            _firstFrame = true;
        }
        _requestSection.FireEvent();
    }

    private void Update()
    {
        if (!FireTest) return;

        FireTest = false;
        AddInstance(new Content());
    }

    /// <summary>
    /// Fires when the content list is available.
    /// </summary>
    /// <param name="eventName"></param>
    /// <param name="data"></param>
    private void OnMyContentSectionUpdate(string eventName, ref object data)
    {
        var contentItems = (data as Content[]);
        if (contentItems == null) return;

        _thumbnailQueue.Clear();
        print("DESTROY ALL ITEMS");
        _itemCount = 0;
        while (_spawnedItems.Count > 0)
        {
            Destroy(_spawnedItems.First().gameObject);
            _spawnedItems.RemoveAt(0);
        }

        _spawnedItems.Clear();

        for (int i = 0; i < contentItems.Length; i++)
        {
            print("Title: " + contentItems[i].Title + " Location: " + contentItems[i].ContentUrl);
        }

        for (var index = 0; index < contentItems.Length; index++)
        {
            AddInstance(contentItems[index]);
            _thumbnailQueue.Enqueue(contentItems[index].ThumbnailUrl);
            //NewContentItem(index, contentItems[index]);
        }
		transform.parent.GetComponent<ScrollRect> ().verticalNormalizedPosition = 1;

        //kick off the image sequence download
        if (_thumbnailQueue.Count > 0)
        {
            RequestImage(_thumbnailQueue.Dequeue());
        }
    }

	// this revised version uses unity vertical layout group and content size fitter
    private void AddInstance(Content content)
    {
        print("NEW INSTANCE");
        var startingCount = _spawnedItems.Count;
        _itemCount += 1;

        print("Spawn New Instance 1");
        GameObject newItem = Instantiate(ItemPrefab) as GameObject;

        _spawnedItems.Add(newItem);
        newItem.GetComponent<ContentListItemView>().SetContent(content);

		newItem.name = gameObject.name + "_item_(" + _itemCount + ")";
		newItem.transform.SetParent(gameObject.transform);
		newItem.transform.localScale = new Vector3(1,1,1);
    }

	private void AddInstanceGridVersion(Content content)
	{
		print("NEW INSTANCE");
		var startingCount = _spawnedItems.Count;
		_itemCount += 1;
		int rowCount;
		if (Vertical)
		{
			rowCount = 1;
			_columnCount = _itemCount;
		}
		else
		{
			rowCount = _itemCount/_columnCount;
		}
		if (_itemCount > _columnCount && _itemCount%rowCount > 0)
			rowCount++;
		
		//adjust the height of the container so that it will just barely fit all its children
		float scrollHeight = _height*rowCount;
		if (_scrollRectTransform.rect.height < scrollHeight)
		{
			float offset = scrollHeight - _scrollRectTransform.rect.height;
			_containerRectTransform.offsetMin = new Vector2(_containerRectTransform.offsetMin.x, -offset);
		}
		
		int j = 0; //_itemCount - NewInstances;
		for (int i = 0; i < _itemCount; i++)
		{
			//this is used instead of a double for loop because itemCount may not fit perfectly into the rows/columns
			if (i%_columnCount == 0)
				j++;
			
			GameObject newItem; // = (i < startingCount) ? _spawnedItems[i] : Instantiate(ItemPrefab) as GameObject;
			if (i < startingCount)
			{
				newItem = _spawnedItems[i];
			}
			else
			{
				print("Spawn New Instance 1");
				newItem = Instantiate(ItemPrefab) as GameObject;
				if (newItem == null) continue;
				_spawnedItems.Add(newItem);
				newItem.GetComponent<ContentListItemView>().SetContent(content);
			}
			
			newItem.name = gameObject.name + " item at (" + i + "," + j + ")";
			newItem.transform.SetParent(gameObject.transform);
			newItem.transform.localScale = new Vector3(1,1,1);
			
			//move and size the new item
			RectTransform rectTransform = newItem.GetComponent<RectTransform>();
			
			var modulo = (i%_columnCount);
			float x = -_containerRectTransform.rect.width/2; // + _width * j;
			float y = _containerRectTransform.rect.height/2 - _height*(modulo + 1);
			rectTransform.offsetMin = new Vector2(x, y);
			
			x = rectTransform.offsetMin.x + _width;
			y = rectTransform.offsetMin.y + _height;
			rectTransform.offsetMax = new Vector2(x, y);            
		}
	}

    /// <summary>
    /// Fires when the object is deallocated by the runtime.
    /// </summary>
    private void OnDestroy()
    {
        GlobalMessageManager.RemoveListener(Constants.Events.MyContentSectionUpdateEventId.ToString(),
            OnMyContentSectionUpdate);
        GlobalMessageManager.RemoveEvent(_requestSection);
    }

    /// <summary>
    /// Requests a new image
    /// </summary>
    /// <param name="imagePath"></param>
    private void RequestImage(string imagePath)
    {
        //check to see if this is a special image

        var requestPath = (object) imagePath;

        _requestThumbnail.FireEvent(ref requestPath);
    }

    /// <summary>
    /// Fires when an image has been downloaded
    /// </summary>
    /// <param name="eventName"></param>
    /// <param name="data"></param>
    private void OnContentImageRecieved(string eventName, ref object data)
    {
        if (data == null) return;
        var index = ((_spawnedItems.Count - _thumbnailQueue.Count)-1);
        if (index < 0) return;
        if (index >= _spawnedItems.Count) return;
        var texture = (data as Texture2D);
        if (texture == null) return;

        _spawnedItems[index].GetComponent<ContentListItemView>().SetThumbnail(texture);

        if (_thumbnailQueue.Count <= 0) return;
        var nextImage = _thumbnailQueue.Dequeue();
        RequestImage(nextImage);
    }
}