﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Represents a UI instance of Team Member
/// </summary>
public class ProfileTeamMemberInstance : MonoBehaviour
{
    /// <summary>
    /// The UI text object responsible for displaying
    /// the team member's full name.
    /// </summary>
    public Text FullName;
}