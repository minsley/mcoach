﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using UnityEngine.UI;

public class WizardChangeFont : ScriptableWizard { 
	
	public Font daFont; 
	//	public Material daMaterial;

	[MenuItem ("GameObject/Change Font")]
	static void CreateWizard () {
		ScriptableWizard.DisplayWizard<WizardChangeFont>("Change Font","Apply");
	}
	void OnWizardCreate () {
		foreach (Object obj in Selection.objects) { 
			if (obj is GameObject) { 
                     // Check which component you want to work on (in this case: Transform) 
				UnityEngine.UI.Text objectio = ((GameObject)obj).GetComponent<UnityEngine.UI.Text>(); 
				if (objectio != null) { 
                         // do whatever you like to do ;-) 
					objectio.font = daFont;
				} 

			} 
		} 
	}  
	void OnWizardUpdate () {
		helpString = "Please set the font!";
	}   
	
 
}
