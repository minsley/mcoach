﻿using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEditor.iOS.Xcode;


public static class MyBuildPostprocess
{
    [PostProcessBuild(999)]
    public static void OnPostProcessBuild( BuildTarget buildTarget, string path)
    {
        if(buildTarget == BuildTarget.iOS)
        {
			Debug.Log("postprocessbuild running");
            string projectPath = path + "/Unity-iPhone.xcodeproj/project.pbxproj";

            PBXProject pbxProject = new PBXProject();
            pbxProject.ReadFromFile(projectPath);

            string target = pbxProject.TargetGuidByName("Unity-iPhone");     
			// turn off bitcode, as the IL2CPP intermediate build will fail to link under XCode 7 with this set to true
            pbxProject.SetBuildProperty(target, "ENABLE_BITCODE", "NO");

			// Add the settings bundle to the xcode project

			string settingsBundlePath = Application.dataPath+"/../../Imports/settings.bundle";
			Debug.Log("Postprocessbuild settings bundle path = "+settingsBundlePath);
			CopyAndReplaceDirectory(settingsBundlePath, path+"/Settings.bundle");
			string settingsGUID = pbxProject.AddFile( "Settings.bundle", "Settings.bundle", PBXSourceTree.Source);
			pbxProject.AddFileToBuild(target, settingsGUID);
			pbxProject.WriteToFile (projectPath);

        }
    }

	public static void CopyAndReplaceDirectory(string srcPath, string dstPath)
	{
		if (Directory.Exists(dstPath))
			Directory.Delete(dstPath);
		if (File.Exists(dstPath))
			File.Delete(dstPath);

		Directory.CreateDirectory(dstPath);

		foreach (var file in Directory.GetFiles(srcPath))
			File.Copy(file, Path.Combine(dstPath, Path.GetFileName(file)));

		foreach (var dir in Directory.GetDirectories(srcPath))
			CopyAndReplaceDirectory(dir, Path.Combine(dstPath, Path.GetFileName(dir)));
	}
}